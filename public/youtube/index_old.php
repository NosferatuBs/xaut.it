<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once __DIR__.'/vendor/autoload.php';

$channel = [];

function channelsListByUsername($service, $part, $params) {
    $params = array_filter($params);
    return $service->channels->listChannels(
        $part,
        $params
    );
}
function playlistItemsListByPlaylistId($service, $part, $params) {
    $params = array_filter($params);
    return $service->playlistItems->listPlaylistItems(
        $part,
        $params
    );
}

$headers = array('referer' => 'xauthenticity.com');
$guzzleClient = new \GuzzleHttp\Client([
	'curl' => [
		CURLOPT_SSL_VERIFYPEER => false
	],
	'headers' => $headers
]);        
$client = new Google_Client();
$client->setDeveloperKey('AIzaSyALj0N5U9OFV7xXgb8epn5CJ4zVBaXwtH4');
$client->setHttpClient($guzzleClient);

$service = new Google_Service_YouTube($client);
$channel = channelsListByUsername($service,
    'snippet,contentDetails', 
    array('forUsername' => 'consulpiu'));

if(!empty($channel))
{
	foreach($channel->items as $item)
	{
		$snippet = $item->getSnippet();
		$content = $item->getContentDetails();
		
		echo "<h1>Utente: " . $snippet->getTitle() . "</h1><br>";
		echo "<h3>Descrizione: " . $snippet->getDescription() . "</h3><br>";

		echo "<h2>Elenco video caricati: </h2><br>";

		$uploads = playlistItemsListByPlaylistId($service,
		    'snippet', 
		    array('maxResults' => 25, 'playlistId' => $content->getRelatedPlaylists()->getUploads()));
		foreach($uploads->getItems() as $video)
		{
			echo "Titolo: <a href=\"//youtu.be/" . $video->getSnippet()->getResourceId()->getVideoId() . "\" target=\"_blank\">" . $video->getSnippet()->getTitle() . "</a><br>";
		}
	}
}