<select class="form-control inputbox" name="survey_{{ $survey->id }}">
	<option value="">Seleziona una risposta</option>
	@foreach($survey->answers as $answer)
		@if(old('survey_' . $survey->id) == $answer->name)
			<option value="{{ $answer->name }}" selected="selected">{{ $answer->name }}</option>
		@else
			<option value="{{ $answer->name }}">{{ $answer->name }}</option>
		@endif
	@endforeach
</select>