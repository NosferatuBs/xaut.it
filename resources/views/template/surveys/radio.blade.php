@foreach($survey->answers as $answer)
<div class="radio">
    <label>
        <input type="radio" name="survey_{{ $survey->id }}" value="{{ $answer->name }}" {{ (old('survey_' . $survey->id) == $answer->name ) ? 'checked' : '' }}>
        {{ $answer->name }}
    </label>
</div>
@endforeach