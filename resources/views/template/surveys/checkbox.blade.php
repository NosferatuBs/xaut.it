@foreach($survey->answers as $answer)
<div class="checkbox">
    <label>
    	<input type="checkbox" name="survey_{{ $survey->id }}[]" value="{{ $answer->name }}"  @if(in_array($answer->name, old('survey_' . $survey->id, []))) checked @endif>
        {{ $answer->name }}
    </label>
</div>
@endforeach