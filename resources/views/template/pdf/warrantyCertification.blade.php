<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<style type="text/css">
		.page-break {
		    page-break-before: always;
		}
		table {
			text-align: center;
			border: 0;
			padding: 35px 0;
		}
		img {
			min-height: 100px;
			max-height: 180px;
			min-width: 100px;
			max-width: 200px;
		}
		</style>
	</head>
	<body>
		<div style="width:970px; height:605px; padding:20px; text-align:center; border: 10px solid #787878">
			<div style="width:920px; height:555px; padding:20px; text-align:center; border: 5px solid #787878">
				<span style="font-size:40px; font-weight:bold">@lang('pdf.warranty.title')</span>
				<br><br>
				<span style="font-size:25px">
					<i>
						@if($warranty->traceability->item_id)
							{{ $warranty->traceability->item->name }}
						@else
							@lang('pdf.warranty.original')
						@endif
						by {{ $warranty->traceability->brand->name }}
					</i>
				</span>
				<br><br>
				<span style="font-size:30px"><b>
					@if($warranty->traceability->number)
						{{ $warranty->traceability->number }}
					@endif
				</b></span>
				<br/><br/><br/>
				<img src="{{ asset('storage/' . $warranty->traceability->brand->photo->first()->stored) }}" />
				<br/><br/><br/>
				<table width="100%">
					<tr>
						<td>
							<span style="font-size:25px"><i>
								@lang('pdf.warranty.purchased')
							</i></span>
						</td>
						<td>
							<span style="font-size:25px"><i>
								@lang('pdf.warranty.expired')
							</i></span>
						</td>
					</tr>
					<tr>
						<td>
							<span style="font-size:30px"><b>
								{{ $warranty->bought_at }}
							</b></span>
						</td>
						<td>
							<span style="font-size:30px"><b>
								{{ $warranty->expired_at }}
							</b></span>
						</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="page-break"></div>

		<h4>Sette regole sulla garanzia obbligatoria da tenere ben presenti ad ogni acquisto:</h4>
		<p>
			1) La garanzia vale per qualsiasi prodotto acquistato da un privato cittadino in un negozio, o anche per corrispondenza o su internet (la garanzia non è obbligatoria per aziende e titolari di partita Iva, cioè se all'acquisto viene emessa la fattura e non solo lo scontrino fiscale).
		</p>
		<p>
			2) La sua durata prevista per legge è di 2 anni. Il produttore o il venditore non possono stabilire durate inferiori ma solo superiori (è il caso della garanzia commerciale che il produttore può aggiungere a quella obbligatoria).
		</p>
		<p>
			3) Copre tutti i difetti di conformità del prodotto, cioè tutti i casi in cui esso non è "idoneo all'uso" (es. guasto tecnico) o non ha le caratteristiche promesse dal venditore, indicate sull'etichetta o nelle pubblicità. Fanno eccezione solo gli articoli di cui, al momento dell'acquisto, si conoscono già i difetti (es. quelli venduti scontati proprio per questo motivo).
		</p>
		<p>
			4) Dà diritto alla riparazione o sostituzione gratuita del prodotto: il compratore (e non il venditore) ha facoltà di scegliere tra le due opzioni. Se non è possibile né riparare né sostituire il bene, l'acquirente ha diritto a una riduzione del prezzo o alla totale restituzione del denaro.
		</p>
		<p>
			5) Il prodotto va riportato al rivenditore entro 2 mesi dalla scoperta del difetto. Entro 6 mesi dall'acquisto si presume che ogni difetto sia preesistente all'acquisto stesso, quindi non dev'essere dimostrato dall'acquirente (cioè non c'è "l'onere della prova").
		</p>
		<p>
			6) È il venditore che risponde dei difetti di conformità del prodotto ed è lui che deve occuparsi di mandare il prodotto in riparazione o sostituirlo. Non può "scaricare" il cliente all'assistenza, anche se quasi sempre tenta di farlo.
		</p>
		<p>
			7) La garanzia è del tutto gratuita e non possono essere richieste spese accessorie per la riparazione o il trasporto del prodotto al centro di assistenza. Anche il montaggio (es. di mobili componibili) è coperto dalla garanzia.
		</p>
		<h4>Reclamo verbale e scritto</h4>
		<p>
			Per far valere la garanzia è necessario presentarsi nel negozio con lo scontrino (meglio farne anche una copia). Se il venditore si oppone o tergiversa, fate seguire alla richiesta verbale una raccomandata che dimostri la contestazione del difetto in data certa. Se nemmeno questa funziona, non vi resta che far valere i vostri diritti tramite le associazioni dei consumatori. (A.D.M.)
		</p>
	</body>
</html>