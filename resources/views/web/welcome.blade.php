@extends('layouts.landing')

@section('menu')
    @include('layouts.rootmenu')
@endsection
@section('content')
        <div class="kl-slideshow static-content__slideshow nobg maskcontainer--mask3 ">
            <div class="bgback">
            </div>
            <div class="kl-slideshow-inner static-content__wrapper static-content--height min-820">
                <div class="static-content__source ">
                    <div class="kl-bg-source">
                        <div class="kl-bg-source__bgimage" style="background-image:url(/customer/images/background-home.jpg);background-repeat:repeat;background-attachment:scroll;background-position-x:center;background-position-y:center;background-size:cover">
                        </div>
                        <div class="kl-bg-source__overlay" style="background-color:rgba(0,0,0,0.2)">
                        </div>
                    </div>
                    <div class="th-sparkles">
                    </div>
                </div>
                <!-- /.static-content__source -->
                <div class="static-content__inner container">
                    <div class="kl-slideshow-safepadding sc__container ">
                        <div class="static-content default-style static-content--with-login">
                            <div class="row">
                                <div class="col-sm-10 col-sm-offset-1 col-md-7 col-md-offset-0">
                                    <h2 class="static-content__title">@lang('landing.welcome.top.title')</h2>
                                    <h3 class="static-content__subtitle">@lang('landing.welcome.top.subtitle')</h3>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1 col-md-5 col-md-offset-0">

                                </div>
                                <!-- end section right -->
                            </div>
                            <!-- end row -->
                        </div>
                        <!-- end static content -->
                    </div>
                    <!-- /.container -->
                </div>
                <!-- /.static-content__inner -->
            </div>
            <!-- /.static-content__wrapper -->
            <div class="kl-bottommask kl-bottommask--mask3">
                <svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <defs>
                        <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                            <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                            <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                            <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                            <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                            <feMerge>
                                <feMergeNode in="SourceGraphic"></feMergeNode>
                                <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                            </feMerge>
                        </filter>
                    </defs>
                    <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
                </svg>
                <i class="glyphicon glyphicon-chevron-down"></i>
            </div>
        </div>

        <!-- Steps box section -->
        <section class="hg_section ptop-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">

                        <!-- Process steps element #1 style 2 -->
                        <div class="process_steps process_steps--style2 kl-bgc-light">
                            <!-- Introduction step -->
                            <div class="process_steps__step process_steps__intro process_steps__height">
                                <!-- Intro wrapper -->
                                <div class="process_steps__intro-wrp">
                                    <!-- Title -->
                                    <h3 class="process_steps__intro-title">@lang('landing.welcome.steps.title')</h3>
                                    <!--/ Title -->

                                    <!-- Description -->
                                    <p class="process_steps__intro-desc">
                                        @lang('landing.welcome.steps.text')
                                    </p>
                                    <!-- Description -->
                                </div>
                                <!--/ Intro wrapper -->
                            </div>
                            <!--/ Introduction step -->

                            <!-- Steps container -->
                            <div class="process_steps__container">
                                <!-- Steps wrapper -->
                                <div class="process_steps__inner process_steps__height">
                                    <!-- Step #1 -->
                                    <div class="process_steps__step">
                                        <!-- Image/Icon -->
                                        <div class="process_steps__step-icon process_steps__step-typeimg">
                                            <span class="statbox__fonticon glyphicon glyphicon-pencil" style="color:#CD2122"></span>
                                        </div>
                                        <!--/ Image/Icon -->

                                        <!-- Title -->
                                        <h3 class="process_steps__step-title">@lang('landing.welcome.steps.1')</h3>
                                        <!--/ Title -->

                                        <!-- Description -->
                                        <!--/ Description -->
                                    </div>
                                    <!--/ Step #1 -->

                                    <!-- Step #2 -->
                                    <div class="process_steps__step">
                                        <!-- Image/Icon -->
                                        <div class="process_steps__step-icon process_steps__step-typeimg">
                                            <span class="statbox__fonticon glyphicon glyphicon-barcode" style="color:#CD2122"></span>
                                        </div>
                                        <!--/ Image/Icon -->

                                        <!-- Title -->
                                        <h3 class="process_steps__step-title">@lang('landing.welcome.steps.2')</h3>
                                        <!--/ Title -->

                                    </div>
                                    <!--/ Step #2 -->

                                    <!-- Step #3 -->
                                    <div class="process_steps__step">
                                        <!-- Image/Icon -->
                                        <div class="process_steps__step-icon process_steps__step-typeimg">
                                            <span class="statbox__fonticon glyphicon glyphicon-ok" style="color:#CD2122"></span>
                                        </div>
                                        <!--/ Image/Icon -->

                                        <!-- Title -->
                                        <h3 class="process_steps__step-title">@lang('landing.welcome.steps.3')</h3>
                                        <!--/ Title -->

                                    </div>
                                    <!--/ Step #3 -->
                                </div>
                                <!--/ Steps wrapper -->
                            </div>
                            <!--/ Steps container -->

                            <div class="clearfix">
                            </div>
                        </div>
                        <!--/ Process steps element #1 style 2 -->
                    </div>
                    <!--/ col-md-12 col-sm-12 -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ Steps box section -->

@endsection
