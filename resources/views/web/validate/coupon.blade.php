@extends('layouts.tracking')

@section('content')

@include('layouts.header')

<section id="content" class="hg_section ptop-100 pbottom-100">
    <div class="container">
        <div class="row">
            <div class="col-md-9">

                <div class="action_box style3" data-arrowpos="center" style="margin-top:-25px;">
                    <div class="action_box_inner">
                        <div class="action_box_content row">
                            <div class="ac-content-text col-lg-12">
                                <h4 class="text">
                                    <span class="fw-thin">BLOCCA IL COUPON</span>
                                </h4>
                                <h5 class="ac-subtitle mb-md-30">
                                Inserisci i dati sottostanti ed il coupon verrà assegnato a te
                                </h5>
                            </div>

                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-md-12">
                        <form name="form" id="qrcode_form" method="post" class="form-horizontal" action="{{ route('validate.coupon.qrcode', $token) }}" autocomplete="off">
                            {{ csrf_field() }}

                        @if(session('status') == 'InvalidCode')
                        <div class="form-group">
                            <div class="action_box1 style2" data-arrowpos="center">
                                <div class="action_box_inner">
                                    <div class="action_box_content">
                                        <div class="ac-content-text">
                                            <h4 class="text">{!! __('customer.searchSku.fail.title') !!}</h4>
                                            <h5 class="ac-subtitle">{!! __('customer.searchSku.fail.subtitle') !!}</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif


                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="vat">Partita IVA *</label>
                                <div class="col-sm-8">
                                    <input type="text" name="vat" id="vat" class="form-control" value="{{ $vat }}" placeholder="" required>
                                    @if ($errors->has('vat'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('vat') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-4">
                                    <label>
                                        <input type="checkbox" name="privacy" id="privacy" required> Ho letto e accettato <a href="//www.google.it" target="_BLANK" style="text-decoration: underline;">l'informativa sulla privacy e il trattamento dei dati personali</a>.
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-4">
                                    <button type="submit" id="submit" class="btn btn-fullcolor">Prosegui</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script>
var form_submitting = false;
$('#qrcode_form').submit(function(event){
    if(form_submitting)
    {
        event.preventDefault();
        return;
    }

    form_submitting = true;
    $('#submit').attr('disabled','true').html('<i class="fa fa-spinner fa-spin"></i> Attendere...');
});
$(".select2").placecomplete({
    width: "100%",
});
$(".select2").on({
    "placecomplete:selected": function(evt, placeResult) {
        window.form.lat.value = placeResult.geometry.location.lat();
        window.form.lng.value = placeResult.geometry.location.lng();
    },
    "placecomplete:error": function(evt, errorMsg) {
        $('.select2').val(null).trigger('change');
        alert({!! json_encode(__('customer.placecomplete.error'), JSON_HEX_TAG) !!});
    }
});
</script>
@endsection
