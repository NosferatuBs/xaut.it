@extends('layouts.tracking')

@section('content')

@include('layouts.header')

<section id="content" class="hg_section ptop-100 pbottom-100">
    <div class="container">
        <div class="row">
            <div class="col-md-9">

                <div class="action_box style3" data-arrowpos="center" style="margin-top:-25px;">
                    <div class="action_box_inner">
                        <div class="action_box_content row">
                            <div class="ac-content-text col-lg-12">
                                <h4 class="text">
                                    <span class="fw-thin">OPERAZIONE AVVENUTA CON SUCCESSO</span>
                                </h4>
                                <h5 class="ac-subtitle mb-md-30">
                                Il coupon è stato bloccato
                                </h5>
                            </div>

                        </div>
                    </div>
                </div>

                <hr>

            </div>
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script>
var form_submitting = false;
$('#qrcode_form').submit(function(event){
    if(form_submitting)
    {
        event.preventDefault();
        return;
    }

    form_submitting = true;
    $('#submit').attr('disabled','true').html('<i class="fa fa-spinner fa-spin"></i> Attendere...');
});
$(".select2").placecomplete({
    width: "100%",
});
$(".select2").on({
    "placecomplete:selected": function(evt, placeResult) {
        window.form.lat.value = placeResult.geometry.location.lat();
        window.form.lng.value = placeResult.geometry.location.lng();
    },
    "placecomplete:error": function(evt, errorMsg) {
        $('.select2').val(null).trigger('change');
        alert({!! json_encode(__('customer.placecomplete.error'), JSON_HEX_TAG) !!});
    }
});
</script>
@endsection
