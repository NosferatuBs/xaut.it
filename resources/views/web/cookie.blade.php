@extends('layouts.landing')

@section('menu')
    @include('layouts.rootmenu')
@endsection
@section('content')

<div id="page_header" class="page-subheader site-subheader-cst uh_flat_dark_blue maskcontainer--mask3">
    <div class="bgback"></div>
    <div class="kl-bg-source">
        <div class="kl-bg-source__overlay" style="background:rgba(142,10,29,1); background: -moz-linear-gradient(left, rgba(142,10,29,1) 0%, rgba(0,0,0,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(142,10,29,1)), color-stop(100%,rgba(0,0,0,1))); background: -webkit-linear-gradient(left, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); background: -o-linear-gradient(left, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); background: -ms-linear-gradient(left, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); background: linear-gradient(to right, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); ">
        </div>
    </div>
    <div class="th-sparkles"></div>
    <div class="ph-content-wrap">
        <div class="ph-content-v-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="breadcrumbs fixclear">
                            <li><a href="/">Home</a></li>
                            <li>Cookie</li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-sm-6">
                        <div class="subheader-titles">
                            <h2 class="subheader-maintitle">@lang('landing.cookie.title')</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="2700px" height="57px" class="svgmask" viewBox="0 0 2700 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M-2,57 L-2,34.007 L1268,34.007 L1284,34.007 C1284,34.007 1291.89,33.258 1298,31.024 C1304.11,28.79 1329,11 1329,11 L1342,2 C1342,2 1345.121,-0.038 1350,-1.64313008e-14 C1355.267,-0.03 1358,2 1358,2 L1371,11 C1371,11 1395.89,28.79 1402,31.024 C1408.11,33.258 1416,34.007 1416,34.007 L1432,34.007 L2702,34.007 L2702,57 L1350,57 L-2,57 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
        </svg>
        <i class="glyphicon glyphicon-chevron-down"></i>
    </div>
</div>

        <!-- Content section -->
        <section class="hg_section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <!-- Title element with bottom line and sub-title with custom paddings -->
                        <div class="kl-title-block clearfix tbk--left tbk-symbol--line tbk-icon-pos--after-title" style="padding-bottom: 0;">
                            <!-- Title with custom montserrat font, size and black color -->
                            <h2 class="tbk__title montserrat fs-34 black"><strong>Cookie Policy</strong></h2>

                            <!-- Title bottom symbol -->
                            <div class="tbk__symbol ">
                                <span></span>
                            </div>
                            <!--/ Title bottom symbol -->

                        </div>
                        <!--/ Title element with bottom line and sub-title -->
                    </div>
                    <!--/ col-md-12 col-sm-12 -->


                    <div class="col-md-12 col-sm-12">
                        <!-- Text box element -->
                        <div class="text_box">
                            <p class="text-justify">
                                Un cookie è un breve testo inviato al tuo browser da un sito web visitato.Consente al sito di memorizzare informazioni sulla tua visita, come la tua lingua preferita e altre impostazioni. Ciò può facilitare la tua visita successiva e aumentare l'utilità del sito a tuo favore.<br />
                                I cookie svolgono un ruolo importante. Senza di essi, l'utilizzo del Web sarebbe un'esperienza molto più frustrante.<br /><br />

                                I cookie vengono utilizzati per vari scopi. Li utilizziamo, ad esempio, per rendere più pertinenti gli annunci che visualizzi, per contare il numero di visitatori che riceviamo su una pagina, per aiutarti a registrarti ai nostri servizi e per proteggere i tuoi dati.
                            </p>
                            <h5>Gestione dei cookie nel browser:</h5>
                            <p class="text-justify">
                                Alcune persone preferiscono non abilitare i cookie e per questo motivo quasi tutti i browser offrono la possibilità di gestirli in modo da rispettare le preferenze degli utenti. In alcuni browser è possibile impostare regole per gestire i cookie sito per sito, opzione che ti offre un controllo più preciso sulla tua privacy. Ciò significa che puoi disabilitare i cookie di tutti i siti, ad eccezione di quelli di cui ti fidi.<br />

                                Per ulteriori informazioni e supporto è possibile visitare la pagina di aiuto specifica del web browser che si sta utilizzando: Internet Explorer, Firefox, Safari, Chrome, Opera.<br />

                                A titolo di esempio, nel browser Google Chrome, il menu Strumenti contiene l'opzione Cancella dati di navigazione. Puoi utilizzare questa opzione per eliminare i cookie e altri dati di siti e plug-in, inclusi i dati memorizzati sul tuo dispositivo da Adobe Flash Player (comunemente noti come cookie Flash). Consulta le istruzioni sul sito si Google Chrome per la gestione dei cookie in Chrome.<br />

                                Un'altra funzione di Chrome è la sua modalità di navigazione in incognito. Puoi navigare in modalità in incognito quando non vuoi che le tue visite ai siti web o i tuoi download vengano registrati nelle cronologie di navigazione e dei download. Tutti i cookie creati in modalità di navigazione in incognito vengono eliminati dopo la chiusura di tutte le finestre di navigazione in incognito.<br />
                            </p>
                            <h5>Cookie tecnici utilizzati:</h5>
                            <p class="text-justify">
                                Utilizziamo diversi tipi di cookie tecnici per gestire le funzionalità e i contenuti del nostro sito. Nel tuo browser potrebbero essere memorizzati alcuni o tutti i cookie elencati qui di seguito. Puoi visualizzare e gestire i cookie nel tuo browser (anche se è possibile che i browser per dispositivi mobili non offrano questa visibilità).<br />

                                <strong>Cookie di sessione</strong>: Cookie tecnici per il corretto funzionamento del sito.
                            </p>
                            <h5>Cookie di terze parti:</h5>
                            <p class="text-justify">
                                Questo sito utilizza o embedda numerosi elementi provenienti da siti terzi che rilasciano cookie. Si rimanda alle rispettive pagine informative per titpologie e modalità di cookie rilasciati: 
                                <strong>Google Analytics</strong>.<br />
                                <strong>Cookie statistici</strong>: Cookie per ricevere informazioni statistiche dei visitatori e del sito sviluppati da Google Analytics.<br><br>
                                <strong>Google Maps</strong>.<br />
                                <strong>Cookie tecnici</strong>: Cookie per la corretta visualizzazione della mappa.
                            </p>

                        </div>
                        <!--/ Text box element -->  
                    </div>
                    <!--/ col-md-4 col-sm-4 -->

                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ Content section -->


@endsection