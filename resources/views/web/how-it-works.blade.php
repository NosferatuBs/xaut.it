@extends('layouts.landing')

@section('menu')
    @include('layouts.rootmenu')
@endsection
@section('content')

<div id="page_header" class="page-subheader site-subheader-cst uh_flat_dark_blue maskcontainer--mask3">
    <div class="bgback"></div>
    <div class="kl-bg-source">
        <div class="kl-bg-source__overlay" style="background:rgba(142,10,29,1); background: -moz-linear-gradient(left, rgba(142,10,29,1) 0%, rgba(0,0,0,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(142,10,29,1)), color-stop(100%,rgba(0,0,0,1))); background: -webkit-linear-gradient(left, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); background: -o-linear-gradient(left, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); background: -ms-linear-gradient(left, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); background: linear-gradient(to right, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); ">
        </div>
    </div>
    <div class="th-sparkles"></div>
    <div class="ph-content-wrap">
        <div class="ph-content-v-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="breadcrumbs fixclear">
                            <li><a href="/">Home</a></li>
                            <li>@lang('landing.comeFunziona.title')</li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-sm-6">
                        <div class="subheader-titles">
                            <h2 class="subheader-maintitle">@lang('landing.comeFunziona.title')</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="2700px" height="57px" class="svgmask" viewBox="0 0 2700 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M-2,57 L-2,34.007 L1268,34.007 L1284,34.007 C1284,34.007 1291.89,33.258 1298,31.024 C1304.11,28.79 1329,11 1329,11 L1342,2 C1342,2 1345.121,-0.038 1350,-1.64313008e-14 C1355.267,-0.03 1358,2 1358,2 L1371,11 C1371,11 1395.89,28.79 1402,31.024 C1408.11,33.258 1416,34.007 1416,34.007 L1432,34.007 L2702,34.007 L2702,57 L1350,57 L-2,57 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
        </svg>
        <i class="glyphicon glyphicon-chevron-down"></i>
    </div>
</div>

<section class="hg_section ptop-80 pbottom-80">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="kl-iconbox text-left">
                    <div class="kl-iconbox__inner">
                        <div class="kl-iconbox__icon-wrapper">
                            <img class="kl-iconbox__icon" src="/customer/images/ib-ico-31.svg" alt="Left aligned" title="Left aligned">
                        </div>

                        <div class="kl-iconbox__content-wrapper">
                            <div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
                                <h3 class="kl-iconbox__title">@lang('landing.comeFunziona.left.title')</h3>
                            </div>

                            <div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
                                <p class="kl-iconbox__desc">
                                    @lang('landing.comeFunziona.left.subtitle')
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="kl-iconbox text-center">
                    <div class="kl-iconbox__inner">
                        <div class="kl-iconbox__icon-wrapper ">
                            <img class="kl-iconbox__icon" src="/customer/images/ib-ico-12.svg" alt="Centerly">
                        </div>

                        <div class="kl-iconbox__content-wrapper">
                            <div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
                                <h3 class="kl-iconbox__title">@lang('landing.comeFunziona.center.title')</h3>
                            </div>

                            <div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
                                <p class="kl-iconbox__desc">
                                    @lang('landing.comeFunziona.center.subtitle')
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="kl-iconbox text-right">
                    <div class="kl-iconbox__inner">
                        <div class="kl-iconbox__icon-wrapper ">
                            <img class="kl-iconbox__icon" src="/customer/images/ib-ico-21.svg" alt="Right aligned">
                        </div>

                        <div class="kl-iconbox__content-wrapper">
                            <div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
                                <h3 class="kl-iconbox__title">@lang('landing.comeFunziona.right.title')</h3>
                            </div>

                            <div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
                                <p class="kl-iconbox__desc">
                                    @lang('landing.comeFunziona.right.subtitle')
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="action_box1 style1" data-arrowpos="center">
    <div class="action_box_inner">
        <div class="action_box_content">
            <div class="ac-content-text">
                <h4 class="text">@lang('landing.comeFunziona.action.title')</h4>
                <h5 class="ac-subtitle">@lang('landing.comeFunziona.action.subtitle')</h5>
            </div>

            <div class="ac-buttons">
                <a class="btn btn-lined ac-btn" href="{{ route('login') }}" >@lang('landing.comeFunziona.action.btn1')</a>
                <a class="btn btn-fullwhite ac-btn" href="/">@lang('landing.comeFunziona.action.btn2')</a>
            </div>
        </div>
    </div>
</div>
<section id="content" class="hg_section">
    <div class="container">
        <div class="row">
            <div class="col-sm-11 col-sm-offset-1 col-md-11 col-md-offset-0">
                <h3>
                    <span class="fw-thin">
                        @lang('landing.comeFunziona.text')
                    </span>
                </h3>

                <ul class="sc__features">
                    <li><i class="fa fa-check"></i> @lang('landing.comeFunziona.list.1')</li>
                    <li><i class="fa fa-check"></i> @lang('landing.comeFunziona.list.2')</li>
                    <li><i class="fa fa-check"></i> @lang('landing.comeFunziona.list.3')</li>
                    <li><i class="fa fa-check"></i> @lang('landing.comeFunziona.list.4')</li>
                </ul>
            </div>
        </div>
    </div>
</section>


@endsection