@extends('layouts.landing')

@section('menu')
    @include('layouts.rootmenu')
@endsection
@section('content')

<div id="page_header" class="page-subheader site-subheader-cst uh_flat_dark_blue maskcontainer--mask3">
    <div class="bgback"></div>
    <div class="kl-bg-source">
        <div class="kl-bg-source__overlay" style="background:rgba(142,10,29,1); background: -moz-linear-gradient(left, rgba(142,10,29,1) 0%, rgba(0,0,0,1) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(142,10,29,1)), color-stop(100%,rgba(0,0,0,1))); background: -webkit-linear-gradient(left, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); background: -o-linear-gradient(left, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); background: -ms-linear-gradient(left, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); background: linear-gradient(to right, rgba(142,10,29,1) 0%,rgba(0,0,0,1) 100%); ">
        </div>
    </div>
    <div class="th-sparkles"></div>
    <div class="ph-content-wrap">
        <div class="ph-content-v-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="breadcrumbs fixclear">
                            <li><a href="/">Home</a></li>
                            <li>Privacy</li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-sm-6">
                        <div class="subheader-titles">
                            <h2 class="subheader-maintitle">@lang('landing.privacy.title')</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="2700px" height="57px" class="svgmask" viewBox="0 0 2700 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M-2,57 L-2,34.007 L1268,34.007 L1284,34.007 C1284,34.007 1291.89,33.258 1298,31.024 C1304.11,28.79 1329,11 1329,11 L1342,2 C1342,2 1345.121,-0.038 1350,-1.64313008e-14 C1355.267,-0.03 1358,2 1358,2 L1371,11 C1371,11 1395.89,28.79 1402,31.024 C1408.11,33.258 1416,34.007 1416,34.007 L1432,34.007 L2702,34.007 L2702,57 L1350,57 L-2,57 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
        </svg>
        <i class="glyphicon glyphicon-chevron-down"></i>
    </div>
</div>

        <!-- Content section -->
        <section class="hg_section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <!-- Title element with bottom line and sub-title with custom paddings -->
                        <div class="kl-title-block clearfix tbk--left tbk-symbol--line tbk-icon-pos--after-title" style="padding-bottom: 0;">
                            <!-- Title with custom montserrat font, size and black color -->
                            <h2 class="tbk__title montserrat fs-34 black"><strong>Informativa sulla privacy</strong></h2>

                            <!-- Title bottom symbol -->
                            <div class="tbk__symbol ">
                                <span></span>
                            </div>
                            <!--/ Title bottom symbol -->

                        </div>
                        <!--/ Title element with bottom line and sub-title -->
                    </div>
                    <!--/ col-md-12 col-sm-12 -->


                    <div class="col-md-12 col-sm-12">
                        <!-- Text box element -->
                        <div class="text_box">
                            <p class="text-justify">
                                Il presente accordo è stato redatto in italiano (Italia). In caso di conflitto tra qualsiasi delle versioni tradotte del presente accordo e la versione in lingua italiana, sarà quest'ultima a prevalere.<br />
                            

                                Data dell'ultima revisione: 28.11.2016<br />
                                <br />
                            

                                Dichiarazione dei diritti e delle responsabilità<br />
                            

                                La presente Dichiarazione dei diritti e delle responsabilità ("Dichiarazione", "Condizioni" o "DDR") regola il nostro rapporto con gli utenti e con chiunque interagisca con XAuthenticity e con tutti i marchi, i prodotti e i servizi, di seguito definiti "Servizi di XAuthenticity" o "Servizi". L'utilizzo o l'accesso ai Servizi di XAuthenticity comporta l'accettazione della presente Dichiarazione e dei relativi aggiornamenti, che potrebbero essere apportati di tanto in tanto in conformità con la Sezione 8 di seguito.<br />
                                <br />
                                            
                                1. Privacy<br />
                                <br />
                                            
                                Il Titolare tratta i Dati Personali degli Utenti adottando le opportune misure di sicurezza volte ad impedire l’accesso, la divulgazione, la modifica o la distruzione non autorizzate dei Dati Personali.<br />
                                            
                                Il trattamento viene effettuato mediante strumenti informatici e/o telematici, con modalità organizzative e con logiche strettamente correlate alle finalità indicate. Oltre al Titolare, in alcuni casi, potrebbero avere accesso ai Dati categorie di incaricati coinvolti nell’organizzazione del sito (personale amministrativo, commerciale, marketing, legali, amministratori di sistema) ovvero soggetti esterni (come fornitori di servizi tecnici terzi, corrieri postali, hosting provider, società informatiche, agenzie di comunicazione) nominati anche, se necessario, Responsabili del Trattamento da parte del Titolare. L’elenco aggiornato dei Responsabili potrà sempre essere richiesto al Titolare del Trattamento.<br /><br />
                                            
                                2. Condivisione dei contenuti e delle informazioni<br />
                                <br />
                                            
                                L'utente è il proprietario di tutti i contenuti pubblicati su XAuthenticity e può modificare in qualsiasi momento i propri dati personali.<br />
                                            
                                Quando l'utente elimina e/o modifica genericamente ogni contenuto e impostazione a lui visibile e modificabile, vengono modificate e/o eliminate in modo permanente. Tuttavia, è possibile che i contenuti modificati e/o rimossi vengano conservati come copie di backup per un determinato periodo di tempo (pur non essendo visibili ad altri).<br />
                                            
                                I commenti o i suggerimenti degli utenti relativi a XAuthenticity sono sempre benvenuti. Tuttavia, l'utente deve essere al corrente del fatto che potremmo usarli senza alcun obbligo di compenso nei suoi confronti (allo stesso modo in cui l'utente non è obbligato a fornirli).<br /><br />
                                            
                                3. Sicurezza<br />
                                <br />

                                            
                                Ci impegniamo al massimo per fare in modo che XAuthenticity sia un sito sicuro, ma non possiamo garantirlo. Abbiamo bisogno che gli utenti contribuiscano a tutelare la sicurezza di XAuthenticity, ovvero che si impegnino a:<br />
                                            
                                -non pubblicare comunicazioni commerciali non autorizzate (ad esempio spam) su XAuthenticity;<br />
                                            
                                -non raccogliere contenuti o informazioni degli utenti, né accedere in altro modo a XAuthenticity usando strumenti automatizzati (come bot di raccolta, robot, spider o scraper) senza previa autorizzazione da parte nostra;<br />
                                            
                                -non intraprendere azioni di marketing multi-livello illegali, ad esempio schemi piramidali, su XAuthenticity;<br />
                                            
                                -non caricare virus o altri codici dannosi;<br />
                                            
                                -non cercare di ottenere informazioni di accesso o di accedere ad account di altri utenti;<br />
                                            
                                -non denigrare, intimidire o infastidire altri utenti;<br />
                                            
                                -non pubblicare contenuti minatori, pornografici, con incitazioni all'odio o alla violenza o con immagini di nudo o di violenza forte o gratuita;<br />
                                            
                                -non usare XAuthenticity per scopi illegali, ingannevoli, malevoli o discriminatori;<br />
                                            
                                -non intraprendere azioni che possano impedire, sovraccaricare o compromettere il corretto funzionamento o l'aspetto di XAuthenticity, ad esempio un attacco di negazione del servizio o altre azioni di disturbo che interferiscano con il rendering delle pagine o con altre funzioni di XAuthenticity;<br />
                                            
                                -non favorire o incoraggiare l'inottemperanza della presente Dichiarazione o delle nostre normative.<br />
                                            
                                 <br />
                                            
                                4. Registrazione e sicurezza dell'account<br />
                                            
                                <br />
                                            
                                Gli utenti di XAuthenticity forniscono il proprio nome e le proprie informazioni reali e invitiamo tutti a fare lo stesso. Per quanto riguarda la registrazione e al fine di garantire la sicurezza del proprio account, l'utente si impegna a:<br />
                                            
                                -non fornire informazioni personali false su XAuthenticity o creare un account per conto di un'altra persona senza autorizzazione;<br />
                                            
                                -non usare XAuthenticity se non ha raggiunto i 18 anni;<br />
                                            
                                -assicurarsi che le proprie informazioni di contatto siano sempre corrette e aggiornate;<br />
                                            
                                -non condividere la propria password (o, nel caso degli sviluppatori, la chiave segreta) né consentire ad altre persone di accedere al proprio account o di eseguire qualsiasi altra azione che potrebbe mettere a rischio la sicurezza dell'account.<br />
                                            
                                 <br />
                                            
                                5. Protezione dei diritti di terzi<br />
                                            
                                <br />
                                            
                                Rispettiamo i diritti di terzi e ci aspettiamo che l'utente faccia lo stesso.<br />
                                            
                                -È vietato pubblicare o eseguire azioni su XAuthenticity che non rispettano i diritti di terzi o le leggi vigenti.<br />
                                            
                                -L'utente non utilizzerà copyright o marchi commerciali di XAuthenticity o simboli simili che possono creare confusione, salvo qualora espressamente consentito dopo aver ottenuto il nostro consenso scritto.<br />
                                            
                                <br />
                                            
                                6. Cellulari e altri dispositivi<br />
                                            
                                <br />
                                            
                                I servizi di notifica che forniamo attualmente sono gratuiti, ma saranno comunque applicati gli addebiti e le tariffe standard degli operatori, ad esempio il costo del traffico dati per visionare le notifiche di posta elettronica.<br />
                                            
                                 <br />
                                            
                                7. Disposizioni speciali applicabili al software<br />
                                            
                                <br />
                                            
                                Quando scarica o usa il nostro software, l'utente accetta che il software potrebbe ricevere aggiornamenti, upgrade e funzioni aggiuntive da XAuthenticity allo scopo di migliorare, ottimizzare e sviluppare ulteriormente il software.<br />
                                            
                                L'utente accetta di non modificare, creare opere derivate, decompilare o tentare in altro modo di estrarre il codice sorgente, in assenza di espressa autorizzazione basata su licenza open source o in assenza di espressa autorizzazione scritta di XAuthenticity.<br />
                                            
                                <br />
                                            
                                8. Modifiche<br />
                                            
                                <br />
                                            
                                In caso di modifiche alle normative, alle linee guida o ad altre condizioni menzionate o inserite nella presente Dichiarazione, potremmo darne notifica sulla Pagina delle normative del sito e nella pagina principale di accesso ad XAuthenticity.<br />
                                            
                                L'uso ininterrotto dei Servizi di XAuthenticity in seguito alla comunicazione delle modifiche apportate alle nostre condizioni, normative o linee guida costituisce l'accettazione implicita delle condizioni, normative o linee guida modificate.<br />
                                            
                                 <br />
                                            
                                9. Risoluzione<br />
                                            
                                <br />
                                            
                                Se le azioni dell'utente non rispettano nella forma e nella sostanza la presente Dichiarazione o creano dei rischi legali per la società, ci riserviamo il diritto di interrompere la fornitura di parte o di tutti i servizi di XAuthenticity nei confronti dell'utente stesso. Invieremo all'utente un'e-mail di notifica oppure lo avviseremo al successivo tentativo di accesso all'account.<br />
                                            
                                 <br />
                                            
                                10. Dispute<br />
                                            
                                <br />
                                            
                                -Qualsiasi reclamo, diritto sostanziale o disputa ("reclamo") tra l'utente e XAuthenticity, derivante dalla presente Dichiarazione o dall'utilizzo di XAuthenticity, verrà risolto esclusivamente in un tribunale italiano. L'utente accetta di sottostare alla giurisdizione personale dei tribunali sopracitati allo scopo di portare avanti tali controversie. La presente Dichiarazione, nonché qualsiasi disputa che possa insorgere tra le due parti, sono regolate dalle leggi dello stato italiano, indipendentemente da conflitti nelle disposizioni di legge.<br />
                                            
                                -Nel caso in cui dovessero sorgere dei reclami nei nostri confronti relativi alle azioni, ai contenuti o alle informazioni dell'utente su XAuthenticity, l'utente sarà tenuto a risarcirci e ad assicurarci da e contro qualsiasi danno, perdita o spesa (incluse spese e costi legali ragionevoli) derivante da tale reclamo. Anche se forniamo delle regole per la condotta degli utenti, non controlliamo né guidiamo le azioni degli utenti su XAuthenticity e non siamo responsabili dei contenuti o delle informazioni che gli utenti trasmettono su XAuthenticity. Non siamo responsabili di alcuna informazione o contenuto offensivo, inappropriato, osceno, illegale o altrimenti deplorevole presente su XAuthenticity. Non siamo responsabili della condotta, sia online che offline, di alcun utente su XAuthenticity.<br />
                                            
                                -Ci impegniamo a mantenere XAuthenticity attivo, esente da errori e sicuro, ma l'utente accetta di utilizzarlo a suo rischio e pericolo. XAuthenticity viene fornito così com'è, senza alcuna garanzia espressa o implicita, tra cui, a titolo esemplificativo, le garanzie implicite di commerciabilità, idoneità a uno scopo specifico o non violazione. Non possiamo garantire che XAuthenticity sarà sempre sicuro o privo di errori o che funzionerà sempre senza interruzioni, ritardi o imperfezioni. XAuthenticity non è responsabile delle azioni, dei contenuti, delle informazioni o dei dati di terzi, pertanto noi, i nostri direttori, incaricati, dipendenti e agenti siamo sollevati da qualsiasi reclamo o danno, noto o sconosciuto, derivante dal suo utilizzo, e non siamo in alcun modo collegati con eventuali lamentele indirizzate contro i suddetti terzi.<br />
                                            
                                <br />
                                            
                                11. Definizioni<br />
                                            
                                <br />
                                            
                                -Con il termine "XAuthenticity" o "Servizi di XAuthenticity" si intende un insieme di funzioni e servizi messi a disposizione degli utenti, tramite (a) il sito Web www.xauthenticity.com e altri siti di XAuthenticity (inclusi i sottodomini, le versioni internazionali e quelle mobili e i widget); (b) la nostra Piattaforma. XAuthenticity si riserva il diritto di stabilire, a sua esclusiva discrezione, che alcuni dei nostri marchi, prodotti o servizi siano regolati da condizioni proprie e non dalla presente DDR.<br />
                                            
                                -Con il termine "Piattaforma" si intende un insieme di API e servizi (ad esempio i contenuti) che consentono ad altri, tra cui sviluppatori di applicazioni e gestori di siti Web, di recuperare dati da XAuthenticity o di fornirci dati.<br />
                                            
                                -Con il termine "informazioni" si intendono i dati personali e commerciali sull'utente, comprese informazioni relative ad articoli, ordini, marchi e/o brand.<br />
                                            
                                -Con il termine "contenuto" si intendono i contenuti che gli utenti pubblicano, forniscono o creano utilizzando i Servizi di XAuthenticity.<br />
                                            
                                -Con il termine "dati" o "dati degli utenti" si intendono tutti i tipi di dati, inclusi i contenuti e le informazioni che l'utente o terzi possono recuperare da XAuthenticity o fornire a XAuthenticity mediante la Piattaforma.<br />
                                            
                                -Con il termine "pubblicare" si intende l'atto di pubblicare qualcosa su XAuthenticity o di rendere disponibile un elemento, un'idea, un progetto tramite XAuthenticity.<br />
                                            
                                -Con il termine "uso" si intendono le seguenti operazioni: utilizzare, copiare, eseguire o esporre pubblicamente, distribuire, modificare, tradurre e creare opere derivative.<br />
                                            
                                -Con il termine "applicazione" si intende qualsiasi applicazione o sito Web che usa o accede alla Piattaforma, nonché qualsiasi altro elemento che riceve o ha ricevuto dati da XAuthenticity.  Se un utente non accede più alla piattaforma ma non ha eliminato i suoi dati da XAuthenticity, questa condizione sarà valida e verrà applicata fino all'eliminazione dei dati.<br />
                                            
                                 <br />
                                            
                                Altro<br />
                                            
                                <br />
                                            
                                -Niente di quanto indicato nella presente Dichiarazione può impedirci di rispettare la legge.<br />
                                            
                                -Ci riserviamo tutti i diritti non espressamente concessi all'utente.<br />
                                            
                                -L'utente si impegna a rispettare tutte le leggi applicabili ogni volta che usa o accede a XAuthenticity.<br />
                                            
                                <br />
                                            
                                Utilizzando o accedendo ai Servizi di XAuthenticity, l'utente accetta che possiamo raccogliere e utilizzare tali contenuti e informazioni in conformità a quanto sopra descritto. 
                            </p>

                        </div>
                        <!--/ Text box element -->  
                    </div>
                    <!--/ col-md-4 col-sm-4 -->

                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </section>
        <!--/ Content section -->


@endsection