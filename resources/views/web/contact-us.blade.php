@extends('layouts.landing')

@section('menu')
    @include('layouts.rootmenu')
@endsection
@section('content')
<!-- Google maps element -->
<div class="kl-slideshow static-content__slideshow scontent__maps">
    <div class="th-google_map" style="height: 700px;">
    </div>
    <!-- end map -->

    <!-- Google map content panel -->
    <div class="kl-contentmaps__panel">
        <a href="#" class="js-toggle-class kl-contentmaps__panel-tgg hidden-xs" data-target=".kl-contentmaps__panel" data-target-class="is-closed"></a>
        <!-- Image & Image pop-up -->
        <a href="/customer/images/sfondo-contatti.jpg" data-lightbox="image" class="kl-contentmaps__panel-img" style="background-image:url(/customer/images/sfondo-contatti.jpg)"></a>

        <!-- Content -->
        <div class="kl-contentmaps__panel-info">
            <h5 class="kl-contentmaps__panel-title">XAuthenticity by Consul+</h5>
            <div class="kl-contentmaps__panel-info-text">
                <p>
                    Via E. Fermi 2<br>
                    25013 Carpenedolo (BS)<br>
                     T. +39 030 7777184
                </p>
            </div>
        </div>
        <!--/ Content -->
    </div>
    <!-- Google map content panel -->
</div>
<!--/ Google maps element -->

<!-- Contact form & details section -->
<section class="hg_section ptop-160 pbottom-80">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9">
                <form action="{{ route('contatti') }}" method="post" class="contact_form row">
                    {{ csrf_field() }}

                    @if (session('status'))
                    <div class="col-sm-12">
                        <div class="alert alert-{{ session('status') }}">
                            @if (session('status') == 'success')
                                @lang('landing.contact.send.success')
                            @else
                                @lang('landing.contact.send.error')
                            @endif
                        </div>
                    </div>
                    @endif
                    <p class="col-sm-6 kl-fancy-form">
                        <input type="text" name="name" id="cf_name" class="form-control" placeholder="@lang('landing.contact.form.nome.placeholder')" value="" tabindex="1" maxlength="35" required>
                        <label class="control-label">@lang('landing.contact.form.nome')</label>
                    </p>
                    <p class="col-sm-12 kl-fancy-form">
                        <input type="text" name="email" id="cf_email" class="form-control h5-email" placeholder="@lang('landing.contact.form.email.placeholder')" value="" tabindex="1" maxlength="35" required>
                        <label class="control-label">@lang('landing.contact.form.email')</label>
                    </p>
                    <p class="col-sm-12 kl-fancy-form">
                        <input type="text" name="subject" id="cf_subject" class="form-control" placeholder="@lang('landing.contact.form.oggetto.placeholder')" value="" tabindex="1" maxlength="35" required>
                        <label class="control-label">@lang('landing.contact.form.oggetto')</label>
                    </p>
                    <p class="col-sm-12 kl-fancy-form">
                        <textarea name="message" id="cf_message" class="form-control" cols="30" rows="10" placeholder="@lang('landing.contact.form.messaggio.placeholder')" tabindex="4" required></textarea>
                        <label class="control-label">@lang('landing.contact.form.messaggio')</label>
                    </p>

                    @if (count($errors) > 0)
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

                    <p class="col-sm-12">
                        <button class="btn btn-fullcolor" type="submit">@lang('landing.contact.form.invia')</button>
                    </p>
                    
                </form>
            </div>
            <!--/ col-md-9 col-sm-9 -->

            <div class="col-md-3 col-sm-3">
                <!-- Contact details -->
                <div class="text_box">
                    <h3 class="text_box-title text_box-title--style2">@lang('landing.contact.contatti.titolo')</h3>
                    <h4>Via E. Fermi 2<br>25013 Carpenedolo (BS)</h4>
                    <p>
                        T. +39 030 7777184<br>
                        F. +39 030 7772459
                    </p>
                    <p>
                        <a href="mailto:#">info@ologrammi.com</a><br>
                        <a href="http://www.ologrammi.com/" target="_blank">www.ologrammi.com</a>
                    </p>
                </div>
                <!--/ Contact details -->
            </div>
            <!--/ col-md-3 col-sm-3 -->
        </div>
        <!--/ .row -->
    </div>
    <!--/ .container -->
</section>
<!--/ Contact form & details section -->

@endsection

@section('javascript')
<script type="text/javascript" src="/customer/js/plugins/jquery.gmap.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
    // Map Markers
    var mapMarkers = [{
        address: "Via Meucci 46, Carpenedolo Brescia",
        latitude: 45.360596,
        longitude: 10.448534,
        icon: {
            image: "/customer/images/map-marker.png",
            iconsize: [60, 70], // w, h
            iconanchor: [60, 70] // x, y
        }
    }];

    // Map Initial Location
    var initLatitude = 45.360596;
    var initLongitude = 10.448534;

    // Map Extended Settings
    var map = jQuery(".th-google_map").gMap({
        controls: {
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: true,
            streetViewControl: true,
            overviewMapControl: true
        },
        scrollwheel: false,
        markers: mapMarkers,
        latitude: initLatitude,
        longitude: initLongitude,
        zoom: 10,
        draggable: Modernizr.touch ? false : true
    });

});// end of document ready
</script>

@endsection