@extends('layouts.tracking')

@section('content')

    @include('layouts.header')

    <section id="content" class="hg_section ptop-100 pbottom-100">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="kl-title-block clearfix text-left tbk-symbol--line tbk-icon-pos--after-title">
                        <h2 class="tbk__title montserrat fs-34 fw-semibold black">@lang('customer.survey.custom.title')</h2>
                        <div class="tbk__symbol ">
                            <span></span>
                        </div>
                        <h4 class="tbk__subtitle fs-22 fw-thin">* @lang('customer.survey.custom.subtitle')</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form name="survey_form" id="survey_form" method="post" class="form-horizontal "
                                  action="{{ route('tracking.survey', [$brandId, $token]) }}">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label class="col-sm-4 control-label"
                                           for="stars">@lang('customer.survey.custom.evaluate') *</label>
                                    <div class="col-sm-8">
                                        @foreach($stars as $star)
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="stars"
                                                           value="{{ $star->id }}" {{ (old('stars') == $star->id ) ? 'checked' : '' }}>
                                                    @lang($star->name)
                                                </label>
                                            </div>
                                        @endforeach
                                        @if ($errors->has('stars'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('stars') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                @if($qrcode->brand->survey_option == 1)
                                    @foreach($surveys as $survey)
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label" for="survey_{{ $survey->id }}">
                                                @if( $survey->help != "")
                                                    <abbr title="{{ $survey->help }}"><i
                                                                class="fa fa-question-circle"></i></abbr>&nbsp;
                                                @endif
                                                {{ $survey->question }} {{ ($survey->required) ? '*' : '' }}
                                            </label>
                                            <div class="col-sm-8">
                                                @include($survey->surveyType->template_answer)

                                                @if ($errors->has('survey_' . $survey->id))
                                                    <span class="help-block">
                                            <strong>{{ $errors->first('survey_' . $survey->id) }}</strong>
                                        </span>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <a href="{{ $previous }}"
                                           class="btn btn-default">@lang('customer.survey.custom.previous')</a>
                                        <button type="submit" id="submit"
                                                class="btn btn-fullcolor">@lang('customer.survey.custom.next')</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @includeWhen($qrcode->brand->photo->count() > 0, 'web.tracking.carousel', ['photos' => $qrcode->brand->photo->sortByDesc('default')])
            </div>
        </div>
    </section>
@endsection

@section('javascript')
    <script>
        var form_submitting = false;
        $('#survey_form').submit(function (event) {
            if (form_submitting) {
                event.preventDefault();
                return;
            }

            form_submitting = true;
            $('#submit').attr('disabled', 'true').html('<i class="fa fa-spinner fa-spin"></i> Attendere...');
        });
    </script>
@endsection
