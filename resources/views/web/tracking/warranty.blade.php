@extends('layouts.tracking')

@section('content')

@include('layouts.header')

<section id="content" class="hg_section ptop-100 pbottom-100">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="kl-title-block clearfix text-left tbk-symbol--line tbk-icon-pos--after-title">
                    <h2 class="tbk__title montserrat fs-34 fw-semibold black">Dove hai acquistato il prodotto?</h2>
                    <div class="tbk__symbol ">
                        <span></span>
                    </div>
                    <h4 class="tbk__subtitle fs-22 fw-thin">* @lang('customer.survey.custom.subtitle')</h4>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <form name="form" id="warranty_form" method="post" class="form-horizontal" action="{{ route('tracking.warranty', $token) }}" enctype="multipart/form-data" autocomplete="off">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="shop">Nome del negozio *</label>
                                <div class="col-sm-8">
                                    <input type="text" name="shop" id="shop" class="form-control" value="{{ old('shop') }}" placeholder="Inserisci il nome del negozio" required>
                                    @if ($errors->has('shop'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('shop') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Data di acquisto *</label>
                                <div class="col-sm-2">
                                    <select class="form-control inputbox required" name="bought_day" id="bought_day" required>
                                        <option value="">Giorno</option>
                                        @for($i = 1; $i <= 31; $i ++)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control inputbox required" name="bought_month" id="bought_month" required>
                                        <option value="">Mese</option>
                                        <option value="1">Gennaio</option>
                                        <option value="2">Febbraio</option>
                                        <option value="3">Marzo</option>
                                        <option value="4">Aprile</option>
                                        <option value="5">Maggio</option>
                                        <option value="6">Giugno</option>
                                        <option value="7">Luglio</option>
                                        <option value="8">Agosto</option>
                                        <option value="9">Settembre</option>
                                        <option value="10">Ottobre</option>
                                        <option value="11">Novembre</option>
                                        <option value="12">Dicembre</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <select class="form-control inputbox required" name="bought_year" id="bought_year" required>
                                        <option value="">Anno</option>
                                        @for($i = date('Y'); $i >= date('Y') - 1; $i --)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                    @if ($errors->has('bought'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('bought') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label" for="invoice">Foto dello scontrino *</label>
                                <div class="col-sm-8">
                                    <input type="file" name="invoice" id="invoice" class="form-control" required>
                                    @if ($errors->has('invoice'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('invoice') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-4">
                                    <a href="{{ $previous }}" class="btn btn-default">@lang('customer.survey.custom.previous')</a>
                                    <button type="submit" id="submit" class="btn btn-fullcolor">Prosegui</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-4">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="background-color: #4CAF50; color: #FFF; padding: 5px; display: none; margin: 5px; border-radius: 5px;"></div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @includeWhen($qrcode->brand->photo->count() > 0, 'web.tracking.carousel', ['photos' => $qrcode->brand->photo->sortByDesc('default')])
        </div>
    </div>
</section>

@endsection

@section('javascript')
<script>
var form_submitting = false;
$('#warranty_form').submit(function(event){
    if(form_submitting)
    {
        event.preventDefault();
        return;
    }

    form_submitting = true;
    $('#submit').attr('disabled','true').html('<i class="fa fa-spinner fa-spin"></i> Attendere...');
});
</script>

<script>
$(document).ready(function() {

    $('#invoice').change(function(){
        $('.progress-bar').text('0%');
        $('.progress-bar').width('0%');
    });
    $('#submit').on('click', function() {
        $('.progress-bar').text('0%');
        $('.progress-bar').width('0%');
        $('.progress-bar').css('display', 'block');
        var uploadInput = $('#invoice');

        if (uploadInput[0].files[0] != undefined) {
            var formData = new FormData();
            formData.append('upload', uploadInput[0].files[0]);

            $.ajax({
                url: '/upload',
                type: 'POST',
                data: formData,
                //processType: false, WRONG syntax
                processData: false,
                contentType: false,
                success: function(data) {
                    $('#invoice').val('');
                },
                xhr: function() {
                    var xhr = new XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(e) {
                        if (e.lengthComputable) {
                            //var uploadPercent = e.loaded / e.total; typo uploadpercent (all lowercase)
                            var uploadpercent = e.loaded / e.total;
                            uploadpercent = Math.round(uploadpercent * 100); //optional Math.round(uploadpercent * 100)
                            $('.progress-bar').text(uploadpercent + '%');
                            $('.progress-bar').width(uploadpercent + '%');
                            if (uploadpercent == 100) {
                                $('.progress-bar').text('Completato. Attendere qualche istante!');
                            }
                        }
                    }, false);

                    return xhr;
                }
            })
        }
    });
});
</script>
@endsection
