@extends('layouts.tracking')

@section('content')

@include('layouts.header')

<section id="content" class="hg_section ptop-60">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div id="sidebar-widget" class="sidebar">
                    <div class="widget">
                        <div class="widget_search">

                            <h3 class="widgettitle title">
                                SCORRI LA PAGINA
                            </h3>

                            @if($traccia->brand->discount_option == 1)

                            <blockquote style="margin: 0;">Ti è arrivato lo sconto via email</blockquote>

                            <hr>

                            @endif

                            @if($traccia->brand->logo != null)
                                <img src="{{ asset('storage/' . $traccia->brand->logo) }}" style="max-height: 100px;" />
                            @endif

                            <h3 class="widgettitle title">{{ $traccia->brand->name }}</h3>

                            <div class="ptcarousel ptcarousel--frames-modern">
                                <div class="zn_simple_slider_container">
                                    <div class="th-controls controls">
                                        <a href="#" class="prev cfs--prev">
                                            <span class="glyphicon glyphicon-chevron-left icon-white"></span>
                                        </a>
                                        <a href="#" class="next cfs--next">
                                            <span class="glyphicon glyphicon-chevron-right icon-white"></span>
                                        </a>
                                    </div>

                                    <ul class="zn_general_carousel cfs--default" data-autoplay="1">
                                        @foreach( $traccia->brand->photo->sortByDesc('default') as $brandphoto )
                                        <li class="item kl-has-overlay cfs--item">
                                            <div class="img-intro">
                                                <a href="{{ asset('storage/' . $brandphoto->stored) }}" data-type="image" data-lightbox="image" title="{{ $brandphoto->title }}"></a>

                                                <img src="{{ asset('storage/' . $brandphoto->stored) }}" class="img-responsive" />

                                                <div class="overlay">
                                                    <div class="overlay-inner">
                                                        <span class="glyphicon glyphicon-picture"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                            <hr>

                            @if($traccia->brand->authenticity_option == 1)
                            <div class="action_box style2" data-arrowpos="center">
                                <div class="action_box_inner">
                                    <div class="action_box_content">
                                        <div class="ac-content-text">
                                            <h4 class="text">Il prodotto è autentico se è del marchio {{ $traccia->brand->name }}</h4>
                                            <img src="/images/etichetta-xaut.jpg" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            @endif

                            @if($traccia->brand->warranty_option == 1)

                            <h3 class="widgettitle title tcolor">@lang('customer.item.warranty.title')</h3>
                            @if(! $traccia->warranty)
                            <a class="btn btn-fullcolor btn-fullwidth" href="/active-warranties/activate/{{ $traccia->token }}">@lang('customer.item.warranty.action')</a>
                            @else
                            <div class="action_box style2" data-arrowpos="center">
                                <div class="action_box_inner">
                                    <div class="action_box_content">
                                        <div class="ac-content-text">
                                            <h4 class="text">@lang('customer.item.warranty.subtitle', ['date'=>$traccia->warranty->created_at->format('d/m/Y')])</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="btn btn-fullcolor btn-fullwidth" href="/tracking/warranty/{{ $traccia->token }}" target="_blank">@lang('customer.item.warranty.download')</a>
                            @endif
                            @endif


                        </div>
                    </div>

                </div>
            </div>

            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="top:80px">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel">@lang('customer.item.destroy.modal.title')</h4>
                        </div>
                        <div class="modal-body">
                            @lang('customer.item.destroy.modal.subtitle')
                        </div>
                        <div class="modal-footer">
                            <form method="post" action="/tracciaing">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <input type="hidden" name="token" value="{{ $traccia->token }}" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    @lang('customer.item.destroy.modal.dismiss')
                                </button>
                                <button type="submit" class="btn btn-danger">
                                    @lang('customer.item.destroy.modal.submit')
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div class="right_sidebar col-md-9">
                <div class="product">
                    <div class="row product-page">
                        <div class="main-data col-sm-7">
                            <div class="summary entry-summary">
                                <h2 class="product_title entry-title">
                                @if($traccia->item_id)
                                    {{ $traccia->item->name }}
                                @else
                                    @lang('customer.item.title')
                                @endif
                                by {{ $traccia->brand->name }}</h2>

                                <b>@lang('customer.item.subtitle', ['date'=>Carbon\Carbon::parse($traccia->created_at)->format('d/m/Y'),'time'=>Carbon\Carbon::parse($traccia->created_at)->format('H:i')])</b>

                                <hr>
                                @if($traccia->item_id)
                                <div>
                                    <p class="desc kw-details-desc">
                                        {!! nl2br($traccia->item->text) !!}
                                    </p>
                                </div>
                                @endif

                                <div class="hg_accordion_element style2">
                                    <div class="th-accordion">

                                        @if($traccia->brand->text)
                                        <div class="acc-group">
                                            <button data-toggle="collapse" data-target="#acc9" class="" aria-expanded="true">@lang('customer.item.brand.title')<span class="acc-icon"></span></button>

                                            <div id="acc9" class="collapse in" aria-expanded="true">
                                                <div class="content">
                                                    <p>
                                                        {!! nl2br($traccia->brand->text) !!}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="single_product_main_image col-sm-5">

                            @if($traccia->item_id && $traccia->item->url_video && preg_match('/.+(vimeo|youtube).+/', $traccia->item->url_video) > 0)
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="media-container h-300">
                                        <div class="kl-bg-source">
                                            <div class="kl-bg-source__overlay" style="background:rgba(53,53,53,0.85); background: -moz-linear-gradient(left, rgba(53,53,53,0.85) 0%, rgba(53,53,53,0.7) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(53,53,53,0.85)), color-stop(100%,rgba(53,53,53,0.7))); background: -webkit-linear-gradient(left, rgba(53,53,53,0.85) 0%,rgba(53,53,53,0.7) 100%); background: -o-linear-gradient(left, rgba(53,53,53,0.85) 0%,rgba(53,53,53,0.7) 100%); background: -ms-linear-gradient(left, rgba(53,53,53,0.85) 0%,rgba(53,53,53,0.7) 100%); background: linear-gradient(to right, rgba(53,53,53,0.85) 0%,rgba(53,53,53,0.7) 100%); background-image: url('{{$traccia->item->url_thumbnail}}'); background-size:cover; ">
                                            </div>
                                        </div>

                                        <a class="media-container__link media-container__link--btn media-container__link--style-circle " href="{{ $traccia->item->url_video }}" data-lightbox="iframe">
                                            <div class="circleanim-svg">
                                                <svg height="108" width="108" xmlns="http://www.w3.org/2000/svg">
                                                <circle stroke-opacity="0.1" fill="#FFFFFF" stroke-width="5" cx="54" cy="54" r="48" class="circleanim-svg__circle-back"></circle>
                                                <circle stroke-width="5" fill="#FFFFFF" cx="54" cy="54" r="48" class="circleanim-svg__circle-front" transform="rotate(50 54 54) "></circle>
                                                <path d="M62.1556183,56.1947505 L52,62.859375 C50.6192881,63.7654672 49.5,63.1544098 49.5,61.491212 L49.5,46.508788 C49.5,44.8470803 50.6250889,44.2383396 52,45.140625 L62.1556183,51.8052495 C64.0026693,53.0173767 63.9947588,54.9878145 62.1556183,56.1947505 Z" fill="#FFFFFF"></path>
                                                </svg>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <br />
                            @elseif( $traccia->brand->url_video && preg_match('/.+(vimeo|youtube).+/', $traccia->brand->url_video) > 0)
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="media-container h-300">
                                        <div class="kl-bg-source">
                                            <div class="kl-bg-source__overlay" style="background:rgba(53,53,53,0.85); background: -moz-linear-gradient(left, rgba(53,53,53,0.85) 0%, rgba(53,53,53,0.7) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(53,53,53,0.85)), color-stop(100%,rgba(53,53,53,0.7))); background: -webkit-linear-gradient(left, rgba(53,53,53,0.85) 0%,rgba(53,53,53,0.7) 100%); background: -o-linear-gradient(left, rgba(53,53,53,0.85) 0%,rgba(53,53,53,0.7) 100%); background: -ms-linear-gradient(left, rgba(53,53,53,0.85) 0%,rgba(53,53,53,0.7) 100%); background: linear-gradient(to right, rgba(53,53,53,0.85) 0%,rgba(53,53,53,0.7) 100%); background-image: url('{{$traccia->brand->url_thumbnail}}'); background-size:cover; ">
                                            </div>
                                        </div>

                                        <a class="media-container__link media-container__link--btn media-container__link--style-circle " href="{{ $traccia->brand->url_video }}" data-lightbox="iframe">
                                            <div class="circleanim-svg">
                                                <svg height="108" width="108" xmlns="http://www.w3.org/2000/svg">
                                                <circle stroke-opacity="0.1" fill="#FFFFFF" stroke-width="5" cx="54" cy="54" r="48" class="circleanim-svg__circle-back"></circle>
                                                <circle stroke-width="5" fill="#FFFFFF" cx="54" cy="54" r="48" class="circleanim-svg__circle-front" transform="rotate(50 54 54) "></circle>
                                                <path d="M62.1556183,56.1947505 L52,62.859375 C50.6192881,63.7654672 49.5,63.1544098 49.5,61.491212 L49.5,46.508788 C49.5,44.8470803 50.6250889,44.2383396 52,45.140625 L62.1556183,51.8052495 C64.0026693,53.0173767 63.9947588,54.9878145 62.1556183,56.1947505 Z" fill="#FFFFFF"></path>
                                                </svg>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>



                            <hr>
                            <h3 class="widgettitle title">}</h3>


                            <br/>

                            <br />
                            @endif

                            @if($item_cover)
                            <div class="images">
                                <a href="{{ asset('storage/' . $item_cover->stored) }}" class="kl-store-main-image zoom" title="{{ $item_cover->title }}">
                                    <img width="600" height="600" src="{{ asset('storage/' . $item_cover->stored) }}" class="attachment-shop_single wp-post-image" alt="{{ $item_cover->title }}" title="{{ $item_cover->title }}">
                                </a>
                                @if(count($traccia->item->photo) > 0)
                                <div class="thumbnails columns-4">
                                    @foreach($traccia->item->photo as $photo)
                                        <a href="{{ asset('storage/' . $photo->stored) }}" class="zoom" title="{{ $photo->title }}">
                                            <img width="180" height="180" src="{{ asset('storage/' . $photo->stored) }}" class="attachment-shop_thumbnail" alt="{{ $photo->title }}" title="{{ $photo->title }}">
                                        </a>
                                    @endforeach
                                </div>
                                @endif
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="/customer/js/jquery.carouFredSel-packed.js"></script>
<script>
$(document).ready(function() {

        var enable_general_carousel = function( content ){
            var elements = content.find('.zn_general_carousel'),
                fw = this;

            if(elements && elements.length && (typeof($.fn.carouFredSel) != 'undefined') )
            {
                jQuery.each(elements, function(i, e){

                    var $el = $(e);

                    var highlight = function(data) {
                        var item = $el.triggerHandler('currentVisible');
                        $el.children('.cfs--item').removeClass('cfs--active-item');
                        item.addClass('cfs--active-item');
                    };
                    var unhighlight = function(data) {
                        $el.children('.cfs--item').removeClass('cfs--active-item');
                    };

                    // Set the carousel defaults
                    var defaults = {
                        fancy: false,
                        transition : 'fade',
                        direction : 'left',
                        responsive: true,
                        auto: true,
                        items: {
                            width: 550,
                            visible: 1
                        },
                        scroll: {
                            fx: 'fade',
                            timeoutDuration : 3500,
                            easing: 'swing',
                            onBefore : unhighlight,
                            onAfter: highlight
                        },
                        swipe: {
                            onTouch: true,
                            onMouse: true
                        },
                        pagination: {
                            container: $el.parent().find('.cfs--pagination'),
                            anchorBuilder: function(nr, item) {
                                var thumb = '';
                                if( $el.is("[data-thumbs]") && $el.data('thumbs') == 'zn_has_thumbs' ){
                                    var items = $el.children('li');
                                    thumb = 'style="background-image:url('+ items.eq(nr-1).attr('data-thumb') + ');"';
                                }
                                return '<a href="#'+nr+'" '+ thumb +'></a>';
                            }
                        },
                        next : {
                            button: $el.parent().find('.cfs--next'),
                            key: 'right'
                        },
                        prev : {
                            button: $el.parent().find('.cfs--prev'),
                            key: 'left'
                        },
                        onCreate : highlight
                    }

                    if( $el.is("[data-fancy]") )
                        defaults.fancy = $el.data('fancy');

                    // Get the custom carousel settings from data attributes
                    var customSettings = {
                        scroll: {
                            fx : $el.is("[data-transition]") ? $el.data('transition') : defaults.transition,
                            timeoutDuration : $el.is("[data-timout]") ? parseFloat( $el.data('timout') ) : defaults.scroll.timeoutDuration,
                            easing: $el.is("[data-easing]") ? $el.data('easing') : defaults.scroll.easing,
                            onBefore : unhighlight,
                            onAfter: highlight
                        },
                        auto: {
                            play: $el.is('[data-autoplay]') && $el.attr('data-autoplay') == '1' ? defaults.auto : false
                        },
                        direction:  $el.is("[data-direction]") ? $el.data('direction') : defaults.direction
                    };

                    // Special case/callback for the fancy slider
                    if ( defaults.fancy ) {
                        // var callback = window['slideCompleteFancy']();
                        $.extend(customSettings.scroll, {
                            onBefore : function(e){ slideCompleteFancy(e, $el) },
                            onAfter : function(e){ slideCompleteFancy(e, $el) },
                        });
                    }

                    // Callback function for fancy slider
                    function slideCompleteFancy(args, slider) {
                        var _arg = $(slider),
                            slideshow =  $(slider).closest('.kl-slideshow'),
                            color = $(args.items.visible).attr('data-color');

                        // slideshow.animate({backgroundColor: color}, 400);
                        slideshow.css({backgroundColor: color});
                    }

                    // Start the carousel already :)
                    $el.imagesLoaded( function() {
                        $el.carouFredSel($.extend({}, defaults, customSettings));
                    });

                    // fix for up/down direction not riszing the slider
                    if( defaults.fancy ){
                        $( window ).on( 'debouncedresize' , function(){
                            if( $(window).width() < 1199 ){
                                $el.trigger("configuration", ["direction", "left"]);
                            } else {
                                $el.trigger("configuration", ["direction", "up"]);
                            }
                            $el.trigger('updateSizes');
                        });
                    }
                });
                return false;
            }
        };

        var simple_slider = $('.zn_simple_slider_container');
        if (simple_slider){
            enable_general_carousel( simple_slider );
        }
    });// end of document ready
</script>
@endsection
