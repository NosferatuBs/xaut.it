@extends('layouts.tracking')

@section('content')

    @include('layouts.header')

    <section id="content" class="hg_section ptop-100 pbottom-100">
        <div class="container">
            <div class="row">
                <div class="col-md-9">

                    <div class="action_box style3" data-arrowpos="center" style="margin-top:-25px;">
                        <div class="action_box_inner">
                            <div class="action_box_content row">
                                <div class="ac-content-text col-lg-12">
                                    <h4 class="text">
                                        <span class="fw-thin">CODICE NON VALIDO</span>
                                    </h4>
                                    <h5 class="ac-subtitle mb-md-30">
                                        Il QR-Code che hai scansionato non è valido oppure il prodotto potrebbe essere contraffatto.
                                    </h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection