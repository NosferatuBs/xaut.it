@extends('layouts.tracking', ['brand' => $brand])

@section('content')


    <style>
        .action_box.style3:before {
            background-color: #0B8B2A;
        }

        .style3 .action_box_inner:before {
            background-color: #0B8B2A;
        }
    </style>

    <section id="content" class="hg_section ptop-100 pbottom-100">
        <div class="container">
            <div class="row">
                <div class="col-md-9">

                    <div class="action_box style3" data-arrowpos="center" style="margin-top:-25px;">
                        <div class="action_box_inner" style="background-color: #0B8B2A;">
                            <div class="action_box_content row">
                                <div class="ac-content-text col-lg-12">
                                    <h4 class="text">
                                        <span class="fw-thin">ATTIVA IL TUO PRODOTTO AUTENTICO</span>
                                    </h4>
                                    <h4 class="ac-subtitle mb-md-30">
                                        Compila il seguente questionario per ricevere dei vantaggi.<br>
                                        {{$number->string_discount}}
                                    </h4>
                                </div>

                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <form name="form" id="qrcode_form" method="post" class="form-horizontal"
                                  action="{{ route('tracking.qrcode', [$brandId, $token]) }}" autocomplete="off">
                                {{ csrf_field() }}

                                @if(session('status') == 'InvalidCode')
                                    <div class="form-group">
                                        <div class="action_box1 style2" data-arrowpos="center">
                                            <div class="action_box_inner">
                                                <div class="action_box_content">
                                                    <div class="ac-content-text">
                                                        <h4 class="text">{!! __('customer.searchSku.fail.title') !!}</h4>
                                                        <h5 class="ac-subtitle">{!! __('customer.searchSku.fail.subtitle') !!}</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="code">La tua email *</label>
                                    <div class="col-sm-8">
                                        <input type="email" name="email" id="email" class="form-control"
                                               value="{{ old('email') }}" placeholder="Inserisci la tua email" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <div class="learfix text-left tbk-symbol--line tbk-icon-pos--after-title">
                                            <h2 class="tbk__title montserrat fs-34 fw-semibold black">Aiutaci brevemente
                                                a conoscerci</h2>
                                            <div class="tbk__symbol" style="margin-bottom: 0;">
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label"
                                           for="gender">@lang('customer.survey.profile.gender') *</label>
                                    <div class="col-sm-8">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="gender" value="male" required>
                                                @lang('customer.survey.profile.gender.male')
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="gender" value="female">
                                                @lang('customer.survey.profile.gender.female')
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="gender" value="other">
                                                @lang('customer.survey.profile.gender.other')
                                            </label>
                                        </div>
                                        @if ($errors->has('gender'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label"
                                           for="city">@lang('customer.survey.profile.city') *</label>
                                    <div class="col-sm-8">
                                        <input type="hidden" name="city" id="city" class="select2"
                                               placeholder="@lang('customer.survey.profile.city.holder')" value=""
                                               required>
                                        <input type="hidden" name="lat" value=""/>
                                        <input type="hidden" name="lng" value=""/>
                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Data di nascita *</label>
                                    <div class="col-sm-2">
                                        <select class="form-control inputbox required" name="birthday_day"
                                                id="birthday_day" required>
                                            <option value="">Giorno</option>
                                            @for($i = 1; $i <= 31; $i ++)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <select class="form-control inputbox required" name="birthday_month"
                                                id="birthday_month" required>
                                            <option value="">Mese</option>
                                            <option value="1">Gennaio</option>
                                            <option value="2">Febbraio</option>
                                            <option value="3">Marzo</option>
                                            <option value="4">Aprile</option>
                                            <option value="5">Maggio</option>
                                            <option value="6">Giugno</option>
                                            <option value="7">Luglio</option>
                                            <option value="8">Agosto</option>
                                            <option value="9">Settembre</option>
                                            <option value="10">Ottobre</option>
                                            <option value="11">Novembre</option>
                                            <option value="12">Dicembre</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <select class="form-control inputbox required" name="birthday_year"
                                                id="birthday_year" required>
                                            <option value="">Anno</option>
                                            @for($i = date('Y') - 13; $i > date('Y') - 100; $i --)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                        @if ($errors->has('birthday'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('birthday') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <label>
                                            <input type="checkbox" name="privacy" id="privacy" required> Ho letto e
                                            accettato <a href="//www.google.it" target="_BLANK"
                                                         style="text-decoration: underline;">l'informativa sulla privacy
                                                e il trattamento dei dati personali</a>.
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <label>
                                            <input type="checkbox" name="gdpr_newsletter_consent" id="gdpr_newsletter_consent">
                                            Acconsento ad essere ricontattato attraverso e-mail e/o SMS a fini marketing.
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <label>
                                            <input type="checkbox" name="gdpr_profilation_consent" id="gdpr_profilation_consent">
                                            Acconsento che i miei dati personali siano usati a scopo di profilazione a fini marketing.
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <label>
                                            <input type="checkbox" name="gdpr_16years_consent" id="gdpr_16years_consent">
                                            Confermo di avere almeno 16 anni di età.
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <button type="submit" id="submit" class="btn btn-fullcolor">Prosegui</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @includeWhen($number->brand->photo->count() > 0, 'web.tracking.carousel', ['photos' => $number->brand->photo->sortByDesc('default')])
            </div>
        </div>
    </section>

@endsection

@section('javascript')
    <script>
        var form_submitting = false;
        $('#qrcode_form').submit(function (event) {
            if (form_submitting) {
                event.preventDefault();
                return;
            }

            form_submitting = true;
            $('#submit').attr('disabled', 'true').html('<i class="fa fa-spinner fa-spin"></i> Attendere...');
        });
        $(".select2").placecomplete({
            width: "100%",
        });
        $(".select2").on({
            "placecomplete:selected": function (evt, placeResult) {
                window.form.lat.value = placeResult.geometry.location.lat();
                window.form.lng.value = placeResult.geometry.location.lng();
            },
            "placecomplete:error": function (evt, errorMsg) {
                $('.select2').val(null).trigger('change');
                alert({!! json_encode(__('customer.placecomplete.error'), JSON_HEX_TAG) !!});
            }
        });
    </script>
@endsection
