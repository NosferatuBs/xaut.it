@extends('admin.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Modifica negozi associati all'utente</h4>
            </div>

            <div class="panel-body">
                <form class="form-horizontal" action="{{ route('admin.users.shops.update', $user->id) }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nome</label>
                        <div class="col-sm-6">
                            <p class="form-control-static">{{ $user->name }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-6">
                            <p class="form-control-static">{{ $user->email }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="shops">Negozi</label>
                        <div class="col-sm-6">
                            <select name="shops[]" class="select2" multiple data-placeholder="Scegli un negozio">
                                <option value=""></option>
                                @foreach($shops as $shop)
                                    @if($selected->contains('id',$shop->id))
                                        <option value="{{ $shop->id }}" selected>{{ $shop->name }}</option>
                                    @else
                                        <option value="{{ $shop->id }}" >{{ $shop->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <button class="btn btn-primary">Salva</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script src="/js/select2.min.js"></script>
<script>
jQuery('.select2').select2({
    width: '100%'
});
</script>
@endsection
