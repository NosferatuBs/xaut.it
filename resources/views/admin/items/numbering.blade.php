@extends('brand.layout.app')

@section('content')
<div class="row">
@include('brand.items.preview')

    @if(Auth::user()->hasRole('admin'))
    <div class="col-md-12">
        <form name="form" id="form" method="post" action="/items/{{ $item->id }}/numbers" autocomplete="off">
        {{ csrf_field() }}
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Nuova numerazione</h4>
            </div>

            <div class="panel-body">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="first_char">Caratteri iniziali</label>
                        <input type="text" class="form-control" name="first_char" id="first_char" value="{{ old('first_char') }}" maxlength="2" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="start">Numero iniziale <span class="asterisk">*</span></label>
                        <input type="text" class="form-control" name="start" id="start" value="{{ old('start') }}" required />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="end">Numero finale</label>
                        <input type="text" class="form-control" name="end" id="end" value="{{ old('end') }}" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="control-label" for="second_char">Caratteri finali</label>
                        <input type="text" class="form-control" name="second_char" id="second_char" value="{{ old('second_char') }}" maxlength="2" />
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                @if (count($errors) > 0)
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @endif
                <button class="btn btn-primary add-item" type="submit">Aggiungi</button>
            </div>
        </div>
        </form>
    </div>
    @endif
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Elenco numerazioni</h4>
            </div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table" id="table1">
                        <thead>
                            <tr>
                                <th>Codice iniziale</th>
                                <th>Codice finale</th>
                                @if(Auth::user()->hasRole('admin'))
                                <th></th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                        @foreach( $item->numbers as $number)
                            <tr>
                                <td>{{ $number->first_char . $number->start . $number->second_char }}</td>
                                <td>{{ $number->first_char . $number->end . $number->second_char }}</td>
                                @if(Auth::user()->hasRole('admin'))
                                <td class="table-action">
                                    <button type="button" class="btn btn-xs btn-default btn-delete" data-number-id="{{ $number->id }}">
                                        Elimina
                                    </button>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@if(Auth::user()->hasRole('admin'))
<div class="modal fade modal-delete" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="deleteItem" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Confermi la cancellazione?</h4>
        </div>
        <div class="modal-body">
            <p>
                Codice iniziale: <span id="lbl-delete-start"></span><br />
                Codice finale: <span id="lbl-delete-end"></span><br />
            </p>
        </div>
        <div class="modal-footer">
            <form method="post" action="/items/{{ $item->id }}/numbers" name="form_number_delete">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <input type="hidden" name="number_id" value="" />

                <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">Annulla</button>
                <button class="btn btn-danger" type="submit">Elimina</button>
            </form>
        </div>
    </div>
  </div>
</div>
@endif
<script type="text/javascript">

var t = jQuery('#table1').DataTable(
{
    @if(Auth::user()->hasRole('admin'))
    "columnDefs":[{"targets":2, "sortable":false}]
    @endif
});
@if(Auth::user()->hasRole('admin'))
$("#table1").on('click','.btn-delete', function(){
    var array = t.row($(this).parents('tr')).data();

    document.getElementById('lbl-delete-start').innerHTML = array[0];
    document.getElementById('lbl-delete-end').innerHTML = array[1];

    form_number_delete.number_id.value = $(this).data("number-id");

    $('.modal-delete').modal('show');
});

$('.modal-delete').on('hidden.bs.modal', function (e) {
    document.getElementById('lbl-delete-start').innerHTML = "";
    document.getElementById('lbl-delete-end').innerHTML = "";

    form_number_delete.number_id.value = 0;
});
@endif

</script>
@endsection
