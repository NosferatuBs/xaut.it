<ul class="nav nav-pills nav-stacked nav-bracket">
    <h5 class="sidebartitle">Navigazione</h5>
    <li class="{{ ($menu->area == 'dashboard') ? 'active' : '' }}">
        <a href="{{ route('admin.dashboard') }}"><i class="fa fa-home"></i> <span>Riepilogo</span></a>
    </li>
     <li class="nav-parent {{ ($menu->area == 'marchi') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-shield"></i> <span>Marchi</span></a>
        <ul class="children" style="{{ ($menu->area == 'marchi') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'marchi' && $menu->option == 'elenco') ? 'active' : '' }}">
                <a href="/admin/brands"><i class="fa fa-caret-right"></i> Elenco</a>
            </li>
            <li class="{{ ($menu->area == 'marchi' && $menu->option == 'create') ? 'active' : '' }}">
                <a href="/admin/brands/create"><i class="fa fa-caret-right"></i> Aggiungi marchio</a>
            </li>
        </ul>
    </li>
    <li class="nav-parent {{ ($menu->area == 'utenti') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-users"></i> <span>Utenti</span></a>
        <ul class="children" style="{{ ($menu->area == 'utenti') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'utenti' && $menu->option == 'elenco') ? 'active' : '' }}">
                <a href="{{ route('admin.users.index') }}"><i class="fa fa-caret-right"></i> Elenco</a>
            </li>
            <li class="{{ ($menu->area == 'utenti' && $menu->option == 'create') ? 'active' : '' }}">
                <a href="{{ route('admin.users.create') }}"><i class="fa fa-caret-right"></i> Aggiungi utente</a>
            </li>
        </ul>
    </li>
     <li class="nav-parent {{ ($menu->area == 'import') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-upload"></i> <span>Importazioni</span></a>
        <ul class="children" style="{{ ($menu->area == 'import') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'import' && $menu->option == 'items') ? 'active' : '' }}">
                <a href="{{ route('admin.import.item.insert') }}"><i class="fa fa-caret-right"></i> Articoli</a>
            </li>
            @can('import_numbers')
            <li class="{{ ($menu->area == 'import' && $menu->option == 'numbers') ? 'active' : '' }}">
                <a href="/import/numbers"><i class="fa fa-caret-right"></i> Numerazioni</a>
            </li>
            @endcan
        </ul>
    </li>
</ul>
