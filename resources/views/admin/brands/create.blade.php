@extends('admin.layout.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Nuovo marchio</h4>
            </div>

            <div class="panel-body">
				<form class="form-horizontal" action="/admin/brands" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="col-sm-3 control-label" for="name">Nome interno <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" required />
							<span class="help-block">Visibile da amministratore e proprietario di brand.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="name_public">Nome pubblico <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="name_public" id="name_public" value="{{ old('name_public') }}" required />
							<span class="help-block">Visibile dal cliente finale.</span>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="name_public">Logo</label>
						<div class="col-sm-6">
							<input type="file" class="form-control" name="logo" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Marchi IlProject</label>
						<div class="col-sm-6">
							<select id="ilprojectBrands" name="ilprojectBrands[]" class="form-control" multiple>
								@foreach($ilprojectBrands as $ilprojectBrand)
									<option value="{{$ilprojectBrand->id_marchio}}">{{$ilprojectBrand->nome}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label">Utenti associati</label>
						<div class="col-sm-6">
							<select id="users" name="users[]" class="form-control" multiple>
								@foreach($users as $user)
									<option value="{{$user->id}}">{{$user->name}}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="url_video">URL Video</label>
						<div class="col-sm-6">
							<input type="url" class="form-control" name="url_video" id="url_video" value="{{ old('url_video') }}" />
							<span class="help-block">Copia e incolla il link del tuo video.</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="text">Descrizione</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="5" name="text" id="text">{{ old('text') }}</textarea>
							<span class="help-block">Inserisci una descrizione del tuo marchio</span>
						</div>
					</div>

					<hr>

					<div class="form-group">
						<label class="col-sm-3 control-label">Tipo negozio online</label>
						<div class="col-sm-6">
							<select name="shop_type" class="form-control">
								<option value="0">Nessuno</option>
								<option value="1">Shopify</option>
								<option value="2">Personalizzato</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="url_video">URL negozio online</label>
						<div class="col-sm-6">
							<input type="url" class="form-control" name="shop_url" value="{{ old('shop_url') }}" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="url_video">API key negozio online</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="shop_api_key" value="{{ old('shop_api_key') }}" />
						</div>
					</div>

					<hr>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="url_video">CSS header pagina</label>
						<div class="col-sm-6">
							<textarea class="form-control" name="header_css">{{ old('header_css') }}</textarea>
						</div>
					</div>

					<hr>

					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="checkbox block">
								<label>
									<input type="checkbox" name="survey_option" id="survey_option"> Attiva i sondaggi
								</label>
							</div>
						</div>
						<div class="col-sm-6 col-sm-offset-3">
							<div class="checkbox block">
								<label>
									<input type="checkbox" name="survey_branch_option" id="survey_branch_option" disabled="disabled"> Attiva i sondaggi per segmento
								</label>
							</div>
						</div>
						<div class="col-sm-6 col-sm-offset-3">
							<div class="checkbox block">
								<label>
									<input type="checkbox" name="survey_item_option" id="survey_item_option" disabled="disabled"> Attiva i sondaggi per articolo
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="checkbox block">
								<label>
									<input type="checkbox" name="stats_option" id="stats_option"> Attiva le statistiche base
								</label>
							</div>
						</div>
					</div>

					<hr>

					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="checkbox block">
								<label>
									<input type="checkbox" name="discount_option" id="discount_option"> Attiva lo sconto per gli utenti
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="discount_percentage">Sconto in %</label>
						<div class="col-sm-6">
							<input type="number" min="0" class="form-control" name="discount_percentage" id="discount_percentage" value="" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="discount_amount">Sconto in &euro;</label>
						<div class="col-sm-6">
							<input type="number" min="0" class="form-control" name="discount_amount" id="discount_amount" value="" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="discount_validity">Validità dello sconto in mesi </label>
						<div class="col-sm-6">
							<input type="number" min="0" class="form-control" name="discount_validity" id="discount_validity" value="24" />
						</div>
					</div>

					<hr>

					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="checkbox block">
								<label>
									<input type="checkbox" name="warranty_option" id="warranty_option"> Attiva la garanzia per gli utenti
								</label>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="warranty_extension">Estensione garanzia (mesi)</label>
						<div class="col-sm-6">
							<input type="number" min="0" class="form-control" name="warranty_extension" id="warranty_extension" value="" />
							<span class="help-block">Per legge la garanzia dura 24 mesi, l'estensione non è retroattiva.</span>
						</div>
					</div>

					@if (count($errors) > 0)
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					@endif
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<button class="btn btn-primary">Crea</button>
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

$('#survey_option').click(function(){

    if ($(this).is(':checked'))
    {
        $("#survey_branch_option").removeAttr("disabled");
        $("#survey_item_option").removeAttr("disabled");
    }
    else
    {
        $("#survey_branch_option").attr("disabled","disabled");
        $("#survey_item_option").attr("disabled","disabled");
    }
});

$(document).ready(function() {
	$("#ilprojectBrands").select2();
	$("#users").select2();
	$("#text").summernote();
});

</script>


@endsection

