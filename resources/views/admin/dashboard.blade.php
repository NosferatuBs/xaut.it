@extends('admin.layout.app')

@section('content')
<div class="row">
    <div class="col-sm-6 col-md-3">
        <div class="panel panel-success panel-stat">
        <div class="panel-heading">

            <div class="stat">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-shield" style="border:0px; opacity: 1;"></i>
                    </div>
                    <div class="col-xs-8">
                        <small class="stat-label">Marchi</small>
                        <h1>{{ $marchi->totale }}</h1>
                    </div>
                </div><!-- row -->

                <div class="mb15"></div>

                <small class="stat-label">Ultimo caricamento</small>
                <h4>{{ $marchi->ultimo }}</h4>
            </div><!-- stat -->

            </div><!-- panel-heading -->
        </div><!-- panel -->
    </div><!-- col-sm-6 -->

    <div class="col-sm-6 col-md-3">
        <div class="panel panel-danger panel-stat">
        <div class="panel-heading">

            <div class="stat">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-cubes" style="border:0px; opacity: 1;"></i>
                    </div>
                    <div class="col-xs-8">
                        <small class="stat-label">Articoli</small>
                        <h1>{{ $articoli->totale }}</h1>
                    </div>
                </div><!-- row -->

                <div class="mb15"></div>

                <small class="stat-label">Ultimo caricamento</small>
                <h4>{{ $articoli->ultimo }}</h4>
            </div><!-- stat -->

            </div><!-- panel-heading -->
        </div><!-- panel -->
    </div><!-- col-sm-6 -->

    <div class="col-sm-6 col-md-3">
        <div class="panel panel-primary panel-stat">
        <div class="panel-heading">

            <div class="stat">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-search-plus" style="border:0px; opacity: 1;"></i>
                    </div>
                    <div class="col-xs-8">
                        <small class="stat-label">Ricerche</small>
                        <h1>{{ $tracciabilita->totale }}</h1>
                    </div>
                </div><!-- row -->

                <div class="mb15"></div>

                <small class="stat-label">Ultima ricerca</small>
                <h4>{{ $tracciabilita->ultimo }}</h4>
            </div><!-- stat -->

            </div><!-- panel-heading -->
        </div><!-- panel -->
    </div><!-- col-sm-6 -->

    <div class="col-sm-6 col-md-3">
        <div class="panel panel-dark panel-stat">
        <div class="panel-heading">

            <div class="stat">
                <div class="row">
                    <div class="col-xs-4">
                        <i class="fa fa-picture-o" style="border:0px; opacity: 1;"></i>
                    </div>
                    <div class="col-xs-8">
                        <small class="stat-label">Foto articoli</small>
                        <h1>{{ $foto->totale }}</h1>
                    </div>
                </div><!-- row -->

                <div class="mb15"></div>

                <small class="stat-label">Ultimo caricamento</small>
                <h4>{{ $foto->ultimo }}</h4>
            </div><!-- stat -->

            </div><!-- panel-heading -->
        </div><!-- panel -->
    </div><!-- col-sm-6 -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Ultime 10 ricerche </div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>E-mail</th>
                                <th>Brand</th>
                                <th>Barcode</th>
                                <th>Articolo</th>
                                <th>Data</th>
                            </tr>
                        </thead>
                        <tbody>
                        @forelse ($ricerche as $t)
                            <tr>
                                <td>{{ $t->user->email }}</td>
                                <td>{{ $t->brand->name }}</td>
                                <td>
                                    @if($t->item_id)
                                    {{ $t->item->barcode }}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>
                                    @if($t->item_id)
                                    {{ $t->item->name }}
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>{{ $t->created_at->format('d/m/Y H:i') }}</td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">Nessun risultato</td>
                            </tr>
                        @endforelse
                      </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div>
        </div>
    </div>
</div>
@endsection
