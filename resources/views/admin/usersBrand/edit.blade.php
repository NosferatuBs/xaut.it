@extends('admin.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Modifica brand associati all'utente</h4>
            </div>

            <div class="panel-body">
                <form class="form-horizontal" action="{{ route('admin.users.brands.update', $user->id) }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nome</label>
                        <div class="col-sm-6">
                            <p class="form-control-static">{{ $user->name }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-6">
                            <p class="form-control-static">{{ $user->email }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="brands">Marchi</label>
                        <div class="col-sm-6">
                            <select name="brands[]" class="select2" multiple data-placeholder="Scegli un marchio">
                                <option value=""></option>
                                @foreach($brands as $brand)
                                    @if($selected->contains('id',$brand->id))
                                        <option value="{{ $brand->id }}" selected>{{ $brand->name }}</option>
                                    @else
                                        <option value="{{ $brand->id }}" >{{ $brand->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <button class="btn btn-primary">Salva</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script src="/js/select2.min.js"></script>
<script>
jQuery('.select2').select2({
    width: '100%'
});
</script>
@endsection
