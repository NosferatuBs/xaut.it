@extends('admin.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Modifica della password di accesso</h4>
            </div>

            <div class="panel-body">
                <form class="form-horizontal" action="{{ route('admin.account.password.update') }}" method="POST">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="current_password">Password attuale</label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" name="current_password" id="current_password" value="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="password">Nuova password</label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" name="password" id="password" value="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="password_confirmation">Conferma nuova password</label>
                        <div class="col-sm-6">
                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="" />
                        </div>
                    </div>

                    @if (count($errors) > 0)
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <button class="btn btn-primary">Aggiorna</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
