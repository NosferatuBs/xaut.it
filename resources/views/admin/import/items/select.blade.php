@extends('admin.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Importa articoli</h4>
            </div>

            <div class="panel-body">
				<form class="form-horizontal" action="{{ route('admin.import.item.load') }}" method="POST">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="col-sm-3 control-label" for="brand_id">Marchio <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<select class="form-control" name="brand_id" id="brand_id" required>
							<option value="">-- seleziona --</option>
							@foreach($brands as $brand)
								@if( old('brand_id') == $brand->id )
									<option value="{{ $brand->id }}" selected>{{ $brand->name }}</option>
								@else
									<option value="{{ $brand->id }}">{{ $brand->name }}</option>
								@endif
							@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Area di lavoro <span class="asterisk">*</span></label>
						<div class="col-md-9">
							<textarea class="form-control" rows="5" name="textarea" id="textarea"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Attenzione!</label>
						<div class="col-md-9">
							<p class="form-control-static">Il separatore è il carattere tabulatore. Copia e incolla i valori da un foglio Excel.</p>
							<p>Codice | Barcode | Listino | Sconto % | Sconto &euro;
							</p>
						</div>
					</div>


					@if (count($errors) > 0)
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					@endif
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<button class="btn btn-primary">Importa articoli</button>
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>

@endsection
