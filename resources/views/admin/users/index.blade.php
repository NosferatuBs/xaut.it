@extends('admin.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <a href="{{ route('admin.users.index') }}">Tutti</a> | <a href="{{ route('admin.users.index', 'workgroup_id=1') }}">Amministratori</a> | <a href="{{ route('admin.users.index', 'workgroup_id=2') }}">Proprietari del brand</a> | <a href="{{ route('admin.users.index', 'workgroup_id=') }}">Consumatori</a> | <a href="{{ route('admin.users.index', 'workgroup_id=3') }}">Negozi</a>
        <div class="panel panel-default">
            <div class="panel-body">
				<div class="table-responsive">
					<table class="table" id="table1">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Email</th>
								<th>Data creazione</th>
								<th>Ruolo</th>
								<th>Marchi</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($users as $user)
							<tr>
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>
								<td>{{ $user->created_at->format("d/m/Y H:i:s") }}</td>
								<td>
                                    @if($user->isSuperadmin())
                                    Amministratore
                                    @elseif($user->hasRoles())
    									@foreach ($user->workgroups as $workgroup)
    										{{ $workgroup->description }}
    									@endforeach
                                    @else
									Consumatore
									@endif
								</td>
								<td>
									@foreach($user->brands as $brand)
										<span class="badge badge-primary">{{ $brand->name }}</span>
									@endforeach
								</td>
								<td>
									<div class="btn-group" role="group">
										<a class="btn btn-xs btn-default" href="{{ route('admin.users.edit', $user->id) }}">Modifica</a>
										<button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu" role="menu">

											@if($user->workgroups->where('name','brandowner')->count() > 0)
											<li><a href="{{ route('admin.users.brands.edit', $user->id) }}">Abilita marchi</a></li>
											<li class="divider"></li>
											@endif

											@if($user->workgroups->where('name','shop')->count() > 0)
											<li><a href="{{ route('admin.users.shops.edit', $user->id) }}">Abilita negozio</a></li>
											<li class="divider"></li>
											@endif

												@if ($user->canBeImpersonated())
													<li><a class="dropdown-item" href="{{ route('impersonate.impersonate', $user->id) }}">Impersona</a></li>
													<li class="divider"></li>
												@endif

											<li><a href="#" class="btn-delete" data-action="{{ route('admin.users.destroy', $user->id) }}">Elimina</a></li>
										</ul>
									</div>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
                    {{ $users->links() }}
				</div><!-- table-responsive -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-delete" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="deleteItem" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Confermi la cancellazione?</h4>
        </div>
        <div class="modal-body">
        	<p>
        		Utente: <span id="lbl-delete-name"></span><br />
        		Email: <span id="lbl-delete-email"></span><br />
        	</p>
        </div>
        <div class="modal-footer">
        	<form method="post" action="" name="form_user_delete">
        		{{ csrf_field() }}
        		{{ method_field('DELETE') }}

	        	<button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">Annulla</button>
	        	<button class="btn btn-danger" type="submit">Elimina</button>
	        </form>
        </div>
    </div>
  </div>
</div>

@endsection

@section('javascript')
<script>

jQuery(document).ready(function() {
    "use strict";

    $("#table1").on('click','.btn-delete', function(){
		var $row = $(this).closest('tr');

		document.getElementById('lbl-delete-name').innerHTML = $row.children('td:eq(0)').text();
		document.getElementById('lbl-delete-email').innerHTML = $row.children('td:eq(1)').text();

		form_user_delete.action = $(this).data("action");

		$('.modal-delete').modal('show');
	});

	$('.modal-delete').on('hidden.bs.modal', function (e) {
		document.getElementById('lbl-delete-name').innerHTML = "";
		document.getElementById('lbl-delete-email').innerHTML = "";

		form_user_delete.action = "";
	});
});
</script>
@endsection
