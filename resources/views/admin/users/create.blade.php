@extends('admin.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Aggiungi utente</h4>
            </div>

            <div class="panel-body">
				<form class="form-horizontal" action="{{ route('admin.users.store') }}" method="POST">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="col-sm-3 control-label" for="name">Nome *</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" required />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="email">Email *</label>
						<div class="col-sm-6">
							<input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}" required />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="password">Password *</label>
						<div class="col-sm-6">
							<input type="password" class="form-control" name="password" id="password" value="" required />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="password_confirmation">Conferma password *</label>
						<div class="col-sm-6">
							<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" value="" required />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Tipo di utente</label>
						<div class="col-sm-6">
							<select class="form-control" name="is_superadmin">
								<option value="0">Utente</option>
								<option value="1">Amministratore</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="workgroup_id">Ruolo</label>
						<div class="col-sm-6">
							<select class="form-control" name="workgroup_id" id="workgroup_id">
								<option value="">Nessuno</option>
								@foreach ($workgroups as $workgroup)
									<option value="{{ $workgroup->id }}">{{ $workgroup->description }}</option>
								@endforeach
							</select>
						</div>
					</div>
					@if (count($errors) > 0)
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					@endif
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<button class="btn btn-primary">Crea</button>
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>

@endsection
