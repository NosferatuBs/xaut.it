@extends('layouts.app')

@section('content')
<form class="form-horizontal" action="/import/items" method="POST" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Caricamento veloce articoli</h4>
            </div>

            <div class="panel-body">
            	<div class="form-group">
					<label class="col-sm-3 control-label" for="brand_id">Marchio *</label>
					<div class="col-sm-6">
						<select name="brand_id" id="brand_id" class="form-control" required>
							<option value="">-- seleziona --</option>
							@foreach( $brands as $brand)
								<option value="{{ $brand->id }}" {{ (old('brand_id') == $brand->id) ? 'selected' : '' }}>{{ $brand->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="file">Il tuo documento *</label>
					<div class="col-sm-6">
						<input type="file" name="file" id="file" class="form-control" required/>
						<span class="help-block">
							Il documento deve essere in formato XLSX - Microsoft Excel 2007-2013 XML
						</span>
					</div>
				</div>
				
				@if (count($errors) > 0)
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				@endif

				@if(session('count'))
				<div class="form-group">
					<div class=" col-sm-6 col-sm-offset-3">
					@if(session('success'))
						<div class="alert alert-success">
							<p><strong>Perfetto!</strong> Sono state caricate {{ session('success') }} righe su {{ session('count') }}.</p>
						</div>
					@else
						<div class="alert alert-danger">
							<p><strong>Attenzione!</strong> Non è stata caricata nessuna riga delle {{ session('count') }} totali.</p>
						</div>
					@endif
					</div>
				</div>
				@endif
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<button class="btn btn-primary">Carica e importa</button>
						<a href="{{ asset('storage/excel-examples/articoli.xlsx') }}" class="btn btn-default">Scarica il documento di esempio</a>
					</div>
				</div>
				
            </div>
        </div>
    </div>
 </div>
 </form>
@endsection