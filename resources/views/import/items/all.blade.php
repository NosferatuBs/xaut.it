@extends('layouts.app')

@section('content')
<form class="form-horizontal" action="/import/items" method="POST" enctype="multipart/form-data">
{{ csrf_field() }}
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">Numerazione olografica degli articoli </div>

            <div class="panel-body">
            	<div class="form-group">
					<label class="col-sm-3 control-label" for="brand_id">Marchio *</label>
					<div class="col-sm-6">
						<select name="brand_id" id="brand_id" class="form-control" required>
							<option value="">Seleziona un marchio</option>
							@foreach( $brands as $brand)
								<option value="{{ $brand->id }}" {{ (old('brand_id') == $brand->id) ? 'selected' : '' }}>{{ $brand->name }}</option>
							@endforeach
						</select>
						<span class="help-block">
							In elenco sono visibili solo i marchi con la protezione olografica attiva
						</span>
					</div>
				</div>
            	<div class="form-group">
					<label class="col-sm-3 control-label">Documento di esempio precompilato</label>
					<div class="col-sm-6">
						<p class="form-control-static excel-template">È necessario selezionare prima il marchio</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="file">Il tuo documento *</label>
					<div class="col-sm-6">
						<input type="file" name="file" id="file" class="form-control" required/>
						<span class="help-block">
							Il documento deve essere in formato XLSX - Microsoft Excel 2007-2013 XML
						</span>
					</div>
				</div>
				
				@if (count($errors) > 0)
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				@endif

				@if(session('success'))
				<div class="form-group">
					<div class=" col-sm-6 col-sm-offset-3">
						<div class="alert alert-success">
							<p><strong>Perfetto!</strong> Sono state caricate {{ session('success') }} righe su {{ session('count') }}.</p>
						</div>
					</div>
				</div>
				@endif
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<button class="btn btn-primary">Carica e importa</button>
					</div>
				</div>
				
            </div>
        </div>
    </div>
 </div>
 </form>
@endsection

@section('javascript')
<script>
var brandsWithHologram = [
	@foreach( $brands as $brand )
	{ 'id' : {{ $brand->id }}, 'level' : {{ $brand->level }} },
	@endforeach
];

var pathtofolder = '{{ asset('storage/excel-examples') }}';

var path = {
	'2' : '/numerazione/progressiva.xlsx',
	'3' : '/numerazione/random.xlsx',
}
var label = {
	'2' : 'Scarica il modello per la numerazione progressiva',
	'3' : 'Scarica il modello per la numerazione random'
}

$('#brand_id').change(function(){
	var selected = this.value;
	if(selected > 0)
		loopBrand(selected);
	else
		setDefaultTemplateMessage();
});
function loopBrand(id)
{
	$.each(brandsWithHologram, function(index,value){
		if(value.id == id)
			changeExcelTemplateFromLevel(value.level);
	});
}
function changeExcelTemplateFromLevel(level)
{
	$('.excel-template').html('<a href=\'' + pathtofolder + path[level] + '\'>' + label[level] + '</a>');
}
function setDefaultTemplateMessage()
{
	$('.excel-template').html('È necessario selezionare prima il marchio');
}
$(document).ready(function(){
	var old = document.getElementById('brand_id').value;
	if(old > 0)
		loopBrand(old);
});

</script>
@endsection