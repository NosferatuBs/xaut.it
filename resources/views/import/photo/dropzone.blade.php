@extends('layouts.app')

@section('content')

<link href="/css/dropzone.css" rel="stylesheet">
<script src="/js/dropzone.min.js"></script>


<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Caricamento massivo delle fotografie</h4>
            </div>

            <div class="panel-body">
            	<form class="form-horizontal">
	            	<div class="form-group">
						<label class="col-sm-3 control-label" for="brand_id">Marchio *</label>
						<div class="col-sm-6">
							<select name="brand_id" id="brand_id" class="form-control" required>
								<option value="">-- seleziona --</option>
								@foreach( $brands as $brand)
									<option value="{{ $brand->id }}" {{ (old('brand_id') == $brand->id) ? 'selected' : '' }}>{{ $brand->name }}</option>
								@endforeach
							</select>
							<span class="help-block">
								Le foto devono avere come nome il codice prodotto e un numero progressivo, separati da un trattino (-).<br />
								AU25-0.jpg<br />
								AU25-1.jpg<br />
								Il carattere separatore non deve essere parte del codice prodotto, oppure non deve ricorrere solo una volta.<br />
								ERRORE: AU-25.jpg<br />
								ESATTO: AU-25-0.jpg
							</span>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<form class="form-horizontal dropzone" id="myDropzone" name="dropzone" action="/import/photos" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<input type="hidden" name="brand_id" value="" />
			<div class="fallback">
				<input name="file" type="file" multiple />
			</div>
		</form>
	</div>
</div>



<div class="modal fade brand-required" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="deleteItem" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Attenzione!</h4>
        </div>
        <div class="modal-body">
        	È necessario selezionare prima il marchio!
        </div>
        <div class="modal-footer">
        	<button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">Chiudi</button>
        </div>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script>
Dropzone.prototype.defaultOptions.dictDefaultMessage = "Trascina qui i file per caricarli";
Dropzone.prototype.defaultOptions.dictFallbackMessage = "Il tuo browser non supporta il caricamento con drag'n'drop.";
Dropzone.prototype.defaultOptions.dictFallbackText = "Perfavore usa la form sotto per caricare le foto.";
Dropzone.prototype.defaultOptions.dictFileTooBig = "Il file è troppo grande (@{{filesize}}MiB). Max filesize: @{{maxFilesize}}MiB.";
Dropzone.prototype.defaultOptions.dictInvalidFileType = "Non puoi caricare file di questo tipo.";
Dropzone.prototype.defaultOptions.dictResponseError = "Il server ha risposto con codice @{{statusCode}}.";
Dropzone.prototype.defaultOptions.dictCancelUpload = "Annulla caricamento";
Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "Sei sicuro di voler annullare questo caricamento?";
Dropzone.prototype.defaultOptions.dictRemoveFile = "Cancella file";
Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "Non puoi più aggiungere file.";

jQuery('document').ready(function(){
	$("#brand_id").change(function(){
		if(this.value == "")
		{
			myDropzone.removeAllFiles();
		}
		dropzone.brand_id.value = this.value;
	});
});

Dropzone.autoDiscover = false;

var myDropzone = new Dropzone("#myDropzone", {
	uploadMultiple: true,
	parallelUploads: 1,
});

myDropzone.on('addedfile', function(file) {
	if(dropzone.brand_id.value == "")
	{
		$('.brand-required').modal('show');
		myDropzone.removeFile(file);
	}
});
</script>
@endsection