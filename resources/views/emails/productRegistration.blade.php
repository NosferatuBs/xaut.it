@component('mail::message')

@include('emails.header')

<br>

# @lang('mail.registrationProduct.title')

{!! __('mail.registrationProduct.text') !!}

@lang('mail.greetings')<br>
{{ config('app.name') }}
@endcomponent
