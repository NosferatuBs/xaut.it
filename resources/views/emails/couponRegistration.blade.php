@component('mail::message')

@include('emails.header')

<br>

# @lang('mail.couponRegistration.title')

{!! __('mail.couponRegistration.text') !!}

@lang('mail.greetings')<br>
{{ config('app.name') }}
@endcomponent
