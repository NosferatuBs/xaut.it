<script type="text/javascript">
var myMapsApiKey = window.Laravel.googleAPI;
var chart = null;

var options = {
    region: null,
    displayMode: 'markers',
    colorAxis: {colors: ['#1CAF9A','#F0AD4E','#D9534F']},
};

function populateSelect(array,id)
{
    dropSelect(id);

    for(i = 0; i < array.length; i++)
    {
        var option = document.createElement('option');
        option.text = array[i].name;
        option.value = array[i].code;
        document.getElementById(id).appendChild(option);
    }
}
function dropSelect(id)
{
    $('#' + id + ' option').slice(1).remove();
}

google.charts.load('upcoming', { packages: [ 'geochart'], mapsApiKey: myMapsApiKey  });
google.charts.setOnLoadCallback(drawMarkersMap);
function drawMarkersMap() {
    chart = new google.visualization.GeoChart(document.getElementById('chart_div_geo'));
    chart.draw(google.visualization.arrayToDataTable(data), options);
};
var region = [];
$('#geochart_area').change(function(){
    dropSelect('geochart_countries');
    
    var selected = this.value;

    $.ajax({
        dataType: "json",
        url: '/geochart/geochartSubcontinent.json',
        success: function(result){
            var elements = [];
            for(i = 0; i < result.length; i++)
            {
                if(result[i].region_code == selected)
                    elements.push(result[i]);
            }
            populateSelect(elements, 'geochart_region');
            region = elements;
        }
    });
    options.region = this.value.length > 0 ? this.value : null;
    drawMarkersMap();
});
var countries = [];
$('#geochart_region').change(function(){
    var selected = this.value;
    var code = null;

    for(i = 0; i < region.length && !code; i++)
    {
        if(region[i].code == selected)
        {
            code = region[i].key;
        }
    }

    $.ajax({
        dataType: "json",
        url: '/geochart/geochartCountries.json',
        success: function(result){
            var elements = [];
            for(i = 0; i < result.length; i++)
            {
                if(result[i].subregion == code)
                {
                    var p = result[i];
                    var name = p.name.common;
                    if(p['translations']['ita'])
                    {
                        name = p['translations']['ita']['common'];
                    }
                    elements.push({
                        'name': name,
                        'code': p.cca2
                    });
                }
            }
            elements.sort(function(a,b) {return (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0);});
            populateSelect(elements, 'geochart_countries');
            countries = elements;
        }
    });
    options.region = this.value.length > 0 ? this.value : $('#geochart_area').val();
    drawMarkersMap();
});

$('#geochart_countries').change(function(){
    options.region = this.value.length > 0 ? this.value : $('#geochart_region').val();
    drawMarkersMap();
});

var continent = [];

$(document).ready(function(){
    $.ajax({
        dataType: "json",
        url: '/geochart/geochartContinent.json',
        success: function(result){
            populateSelect(result, 'geochart_area');
            continent = result;
        }
    });
});
</script>