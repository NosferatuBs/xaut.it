<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="/css/style.default.css" rel="stylesheet">
    <link href="/css/jquery.datatables.css" rel="stylesheet">
    <link href="/css/jquery.gritter.css" rel="stylesheet">
    <link href="/css/style.dodgerblue.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="signin">

<section>
  
    <div class="signinpanel"> 
        <div class="row">
            <div class="col-md-12">
                @yield('content')
            </div>
        </div>
        <div class="signup-footer">
            <div class="pull-left">
            &copy; 2016. All Rights Reserved. Consul+ srl
            </div>
            <div class="pull-right">
            Torna al sito: <a href="/">www.xauthenticity.com</a>
            </div>
        </div>
    </div>

</section>


<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/jquery-migrate-1.2.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/modernizr.min.js"></script>
<script src="/js/jquery.sparkline.min.js"></script>
<script src="/js/jquery.cookies.js"></script>

<script src="/js/toggles.min.js"></script>

<script src="/js/custom.js"></script>

</body>
</html>