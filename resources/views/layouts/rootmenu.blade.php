<li><a href="/">HOME</a></li>
<li class="{{ (isset($menu) && $menu->area == 'how-it-works') ? 'active' : '' }}">
	<a href="{{ LaravelLocalization::getURLFromRouteNameTranslated(LaravelLocalization::getCurrentLocale(), 'routes.come-funziona') }}">
		@lang('landing.index.header.comeFunziona')
	</a>
</li>
<li class="{{ (isset($menu) && $menu->area == 'contact-us') ? 'active' : '' }}">
	<a href="{{ LaravelLocalization::getURLFromRouteNameTranslated(LaravelLocalization::getCurrentLocale(), 'routes.contatti') }}">
		@lang('landing.index.header.contatti')
	</a>
</li>
@if(Auth::check())
<li>
	<a href="{{ Auth::user()->getRoleBasedHomePageUrl() }}">
		@lang('landing.index.header.area')
	</a>
</li>
@endif
