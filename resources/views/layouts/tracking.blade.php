<!doctype html>
<html lang="it-IT" class="no-js">
<head>

    <title>xAuthenticity</title>

    <!-- meta -->
    <meta charset="UTF-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1">

    <!-- Uncomment the meta tags you are going to use! Be relevant and don't spam! -->

    <!-- end descriptive meta tags -->

    <!-- Retina Images -->
    <!-- Simply uncomment to use this script !! More here http://retina-images.complexcompulsions.com/
    <script>(function(w){var dpr=((w.devicePixelRatio===undefined)?1:w.devicePixelRatio);if(!!w.navigator.standalone){var r=new XMLHttpRequest();r.open('GET','/retinaimages.php?devicePixelRatio='+dpr,false);r.send()}else{document.cookie='devicePixelRatio='+dpr+'; path=/'}})(window)</script>
    <noscript><style id="devicePixelRatio" media="only screen and (-moz-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)">html{background-image:url("php-helpers/_retinaimages.php?devicePixelRatio=2")}</style></noscript>-->
    <!-- End Retina Images -->

    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,600italic,700,800,800italic" rel="stylesheet" type="text/css">
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- ***** Boostrap Custom / Addons Stylesheets ***** -->
    <link rel="stylesheet" href="/customer/css/bootstrap.css" type="text/css" media="all">
    <link rel="stylesheet" href="/customer/css/bootstrap-datepicker3.css" type="text/css" media="all">
    <!--link rel="stylesheet" href="css/addons.css" type="text/css" media="all"-->
    <!-- ***** Main + Responsive & Base sizing CSS Stylesheet ***** -->
    <link rel="stylesheet" href="/customer/css/template-v2.css" type="text/css" media="all">
    <link rel="stylesheet" href="/customer/css/responsive.css" type="text/css" media="all">
    <link rel="stylesheet" href="/customer/css/base-sizing.css" type="text/css" media="all">
    <!-- Updates CSS Stylesheet (for future templates updates) -->
    <link rel="stylesheet" href="/customer/css/updates.css" type="text/css" />
    <!-- Custom CSS Stylesheet (where you should add your own css rules) -->
    <link rel="stylesheet" href="/customer/css/custom.css" type="text/css" />

    <link rel="stylesheet" href="/css/font-awesome.min.css">

    <!-- Page preloader animation -->
    <!--<script type="text/javascript" src="js/plugins/pace.js"></script>-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'googleAPI' => config('google.api')
        ]); ?>;
    </script>

    <!-- Modernizr Library -->
    <script type="text/javascript" src="/customer/js/vendor/modernizr.min.js"></script>

    <!-- jQuery Library -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.js"></script>

    <!-- PAGE CUSTOM SCRIPTS & STYLES -->


<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript">
    window.cookieconsent_options = {"message":"Questo sito fa uso di cookie per garantirti la migliore esperienza di navigazione","dismiss":"Ho capito!","learnMore":"Più informazioni","link":"https://www.xauthenticity.com/cookie","theme":"light-floating"};
</script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js"></script>
<!-- End Cookie Consent plugin -->


<!-- /.END PAGE CUSTOM SCRIPTS & STYLES -->
<script type="text/javascript" src="/js/select2.min.js"></script>
<script type="text/javascript" src="/js/jquery.placecomplete.js"></script>
<link rel="stylesheet" type="text/css" href="/css/select2.css" />
<style>
.jquery-placecomplete-google-attribution {
    padding-bottom: 15px;
    /* This inlined image is the "Powered by Google" logo required for proper
    attribution. See https://developers.google.com/places/policies for more
    details and for the original image. */
    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGgAAAAQCAYAAAD6bToNAAAJ3ElEQVQ4y+2VCVRU5xXH/2/evNlXZpidYWYAYRhkGzYZWYQhrKIgYKKIMQooEKmKItFEixvux2hjyGJiajUSaz2mUaM1GvE0SzmaGE00kUabNk2qGDwKMXF5fZA3OS8UsjRNzknSOefO+777fe/e+93fu/cDTdP4JUjx0m5MWNeDiZtuY9zaO8hbQQtTH6ILPE10TWpDd7570gGJbfRxSIJmg7TNQmxUPej51aBrJqNrQhleS83ANLkOYcFynDsxHfT5GaBPZ4F+KxFdRxNweKMNc0cDcd4ZmL6FRvqCm/A00MjeswZpJ93IfCsLmaeyMzwn81/QPhn8OHSEBioCkALLUqNxs/xubBzuQpxUCpKHL+PGLw1QxeY7KFxNF0ZOObvZnPb0w4aRz6yz5ry4I7y8c6997BvV0qAGf9LeAFfE/wBQI42xy24iet790GZJoR+jj7XsDf9AXOf3MniQgPk1eWJA10/FJ+PH/TCAWltbf2hxM6IeSvddAFVs6EXRGnqZIXXbq4TImU/5F/gLDBNlAuNEizS4YZE2/ew1kZGhoS7GvrpZDKDp3x/Q8h6k3f8xTO4TEAQOE/gfsb1MjBO1wQxU5TNwmhtAT69A908Y0CFGvEPpvm2cJcs/Qcm6O1WBBa92Aao4EBJIAqZAbK2C2FwGiSFPJtR685iPWh5h0uHS8jmgm2qYr3ua8lrFPZpXBgN0Nht0Z3Lg1eNJhoMbBgK6hZQHaBQt74Z34VWEltB80+o6ieYVQztRJNqliZRg35y7GUCMn7r7/HomlmnXDw//5QIas+S6ImkufUZsm7e9r7WAEIJSJTCAqiGzFEJmGt2vjg3Q44NV83HnN83Ca5UTpu/2JDb/aUTyY2dHZmyYotAFDAuWMYAYOBdmBP11b2zzEwvMi09sDd7+9o7AVfW5ULozZ2DG0wygB25khJefnZfScP2puNquVl3s/lq/xkx/RbvlCPKJXSNSLaA3LUFPTcWk7YnuFUcTEn93KMrdFC+VSQcFxBy2hZEq9vB9PzWr72sljay+kZ07+jZw1g8NSJ5vTxs7L2XXqlg/bZw21cKx7bNRyur61jqGANTCjZW118Y5T5+9Kt/cM/ez+NApt68K9Pc0EgTZRwgSYwYs3hfhmnwZQdk7IdSNwqzK55nE1fOvz6luLdfpHr1LqdRHi0T2447Io4c0zjdjQ9XaC+/UmC+/nHksykFWJ4ZRqvQIfnTnTvW5Aw/hDyFJU8iCdXS1OXn9bhCKSNmwxl8bRl+hKWn6JOk8EyluDz5GFYiee6upkrjdVDe/2mDcMdPuUH6YnZN1JSSRniDVNDGhDQqo/9DsuJUDoHWA3je+wiamlB17WSidbBI7B+gcbNLaOHdKKwd6GwvQwdpzsEm/MgSgVnbcwom13w9n7Padz1n5Wbptwp1eSWDdYpJSgEeKIQ8Yi8DcdkRMvQXPrFtInvc5PGWv4dzMipz3UkZe1gPxiVIZHJQAzVqL531V1M1KvbH+L0dHzT2yxtTJUJaEWfhQiQlsqhHW9G7DjcLCaWNDZ9GvCXTFCxXGRIj8XKQu9/0zcuumR0VbAwT8g9ZjghzhthuzK/X/GpP3Xp1GX9ubm+d9KW5EMwNnpR78JN7XAPKy4/4ksWN6QDVd4cAq5SS5hVMhXjaxjaz4YDUOqBSa3c+tUJ+tb9XiBsTks9UHtoPb4uwlF0MDyuhuZcxT+0mhH4TqaBiTH2cqaD9c9/Y4E2s/qRjV0D0eaa+MWOr0LPib09UdT0ndyZQMtSojHvS3qk6poy/UUPqth7eYtr6+Rt2pE0ExTAs8WS/CkZXipJ4ncKnUk9akLOl5XR71yC6bZzYyZ59ifHcdFqhrNgm3mCn+wcBjglzB1lve7JDPPWmXFxksK1OUyrEuUhgt5/HQB0fJI78ekA8M9zmI3gejk01SJweal9OafKIeAhB3j2OQPd94B3FicrN++0Fx4zam/Z7UZZ3Z6597+XOpbXKGf9xqWHOOI8D7AhRBlRky232LrEUf9RKO5TumqeWFnwbF3c6WqCryZSq8bY/HxchU4Rl/97tZIvmytqXyedd3Km7EBvJiKr080C/JGVHHXmpBV5hSmE8FP1imSDh60Z797Mz8pfS9WvemnUxLDRY+YSX5++3t/BzqmS0JTr/eiBFv/9YQ9EKYSIxJugD8w5mCNWoLtAyooQCVcpLfwdFXcfRt3GritJo2NjncNV8rc7DPwZJfynlHzbl/wAE/GKBGzn3VwTlHB7fV+UQd0QRF6OwYbeaHH2nST72nS37SHVjwDiwZe6CJ2wbrXQdEhvxrH8Mw59lxakp43Bje/pw65I37lP7md4MT8WnCXcUbZPZ3NBKeKydWYHx3jej8zlr+7qYiUkkfV5D0Yfn6xek4wLQ9BSgTrBm7okLKzj9sK+uixZapOQJDFASr7VLBvqAOVEgO4G4lsUAfMOuqyU3vCXCt2hwcaeoKT7O0aqw2BUEQQwHyyZf9m/0quWuOAcnwJbiKhQTOvIP73iCA3Jw9HRwQvsocqB94Bx0aeNew9r/S3vpEah4NmWUMZEFNKYrYw29q08/905J7eqMx7fky6zg6K7ise6bSfeQC5LkrbAICyRK5fpHYtPVhReCONoOzaYnculkF3iixGLDp+CiJIsO3TyKf3V5Ntu5fSDXXjuItIwFjSNJkZDScF8TV01GOotMz/dIv9uqzLp2WR09z8WstFFmu2EzkivYwDnL4JMFfKDXN/6M69MJ2bWj7OrX9sShKHM/Y+eYW921lQOJ+dBkknhZfxXNFbCmGzFoKmb0BQv1kBaW9Z7zQXLtCZJ6yUmSpmEPpx08C3xTJ5IZBAIQzbWcEJUU4KQzWgoyMgEAsBQ8yMYFQMx95ThJFTgKJViLQLIfFj3kn0lOOqY/SAZ76qzsVYY2bBbqC0XzFqBxZzPNvqOKPnOQrozSMCTAiZP6lzHWD8WINnIRQx/iIkICnNRJU/5afJSBf9fS1xiEBORohNE4DpSkGQfn3sQAh0EJgrGCeAX2zfl2kSIIUSgYnKYSe4CMGIihBfgmoMIJEiYtAgpVAoBSwhpcjfwmN1AfojcrIR05JTPmkfuRzMGcchC3nQIQq6cTf+aq0ZIJSAPwv3PAJAuViLVw8ERhAUBIkrDzBkIDcgx3sJwbIO9QZ/hPQOPCE1n4YPJHlewGyKymohy+GPud1+HvPrFcnn+jSJ2+JsRdfELmqaWnM/fRYWdjq3YwDUz+d/wbQz11+MEAWICggENKweshDpkJqn2yShj7ylDph30l96qHHjJl/XqsIXbiW4KuHw/f7P6AfGZDFClnYr6AIq4PUWgyBXyZjrzqSJ7Lnga9PAk8l+4IM8Z0B/Rv2bzAC7UdsvQAAAABJRU5ErkJggg==);
    background-repeat: no-repeat;
    background-position: right bottom;
}

#page-header {
    {{ $brand->header_css }};
    opacity: 1 !important;
    -webkit-box-shadow: 0px 4px 11px -6px #000000;
    box-shadow: 0px 4px 11px -6px #000000;
    position: relative !important;
}

.action_box {
    margin-top: 0px !important;
}

.action_box_inner {
    padding: 0 !important;
}
</style>

</head>

<body class="kl-store-page">

    <div id="page_wrapper">
        <header id="page-header" class="site-header cta_button">
            <div class="container  pt-20 pb-20">
                <!-- logo container-->
                <div class="logo-container logosize--yes clearfix">
                    <!-- Logo -->
                    <h1 class="site-logo logo " id="logo">
                        <img class="logo-img" src="{{ asset('storage/' . $brand->logo) }}" title="Logo">
                    </h1>

                </div>
            </div>
        </header><!-- /header -->

        @if(!Auth::guest())
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        @endif

        @yield('content')

        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5">
                        <div>
                            <h3 class="title m_title">MENU</h3>
                            <div class="sbs">
                                <ul class="menu">
                                    <li><a href="{{ route('login') }}">@lang('landing.index.header.accedi')</a></li>
                                    <li><a href="{{ LaravelLocalization::getURLFromRouteNameTranslated(LaravelLocalization::getCurrentLocale(), 'routes.come-funziona') }}">@lang('landing.index.header.comeFunziona')</a></li>
                                    <li><a href="{{ route('contatti') }}">@lang('landing.index.header.contatti')</a></li>
                                    <li><a href="{{ LaravelLocalization::getURLFromRouteNameTranslated(LaravelLocalization::getCurrentLocale(), 'routes.condizioni') }}">Privacy Policy</a></li>
                                    <li><a href="{{ LaravelLocalization::getURLFromRouteNameTranslated(LaravelLocalization::getCurrentLocale(), 'routes.cookie') }}">Cookie Policy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        &nbsp;
                    </div>
                    <div class="col-sm-3">
                        <div>
                            <h3 class="title m_title">@lang('landing.index.footer.contattaci')</h3>
                            <div class="contact-details"><p><strong>T. +39 030 7777184</strong><br>
                                Email: <a href="mailto:info@ologrammi.com">info@ologrammi.com</a></p>
                                <p>Consul+ Srl<br>P.IVA 01957910209<br>
                            </div>
                        </div>
                    </div>
                </div><!-- end row -->
                <div class="row">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">

                    </div>
                </div><!-- end row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="bottom clearfix">
                            <div class="copyright">
                                <a href="/">
                                    <img src="/customer/images/logo.png" alt="XAuthenticity">
                                </a>
                            </div><!-- end copyright -->
                        </div>
                        <!-- end bottom -->
                    </div>
                </div>
                <!-- end row -->
            </div>
        </footer>
        <!-- end footer -->
    </div><!-- end page-wrapper -->


    <a href="#" id="totop">TOP</a>
    <!-- JS FILES // These should be loaded in every page -->
    <script type="text/javascript" src="/customer/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/customer/js/kl-plugins.js"></script>
    <script type="text/javascript" src="/customer/addons/smoothscroll/smoothscroll.js"></script>
    <!-- Custom Kallyas JS codes -->
    <script type="text/javascript" src="/customer/js/kl-scripts.js"></script>

    <script type="text/javascript">
    //use the modernizr load to load up external scripts. This will load the scripts asynchronously, but the order listed matters. Although it will load all scripts in parallel, it will execute them in the order listed
    Modernizr.load([
        {
            // test for media query support, if not load respond.js
            test : Modernizr.mq('only all'),
            // If not, load the respond.js file
            nope : '//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js'
        }
    ]);

    </script>

    @yield('javascript')
    @stack('scripts')
</body>
</html>
