@extends('brand.layout.app')

@section('content')

<form class="form-horizontal" name="form" id="form" action="{{ route('brand.branches.update', $branch->id) }}" method="POST" enctype="multipart/form-data">
{{ csrf_field() }}
{{ method_field('PUT') }}
<input type="hidden" name="datatable" value="" />

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Modifica segmento</h4>
            </div>

            <div class="panel-body">
            	<div class="form-group">
					<label class="col-sm-3 control-label" for="brand_id">Marchio</label>
					<div class="col-sm-6">
						<p class="form-control-static">{{ $branch->brand->name }}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="name">Nome <span class="asterisk">*</span></label>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="name" id="name" value="{{ $branch->name }}" required />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="text">Descrizione</label>
					<div class="col-sm-6">
						<textarea class="form-control" rows="5" name="text" id="text">{{ $branch->text }}</textarea>
					</div>
				</div>
				@if (count($errors) > 0)
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				@endif
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<button class="btn btn-primary">Modifica</button>
					</div>
				</div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">

        	<div class="panel-heading">
        		<h4 class="panel-title">Elenco articoli</h4>
        		<p>Selezionare gli articoli da includere nel segmento</p>
        	</div>

            <div class="panel-body">
				<div class="table-responsive">
					<table class="table" id="table1" width="100%">
						<thead>
							<tr>
								<th>Barcode</th>
								<th>Codice</th>
								<th>Descrizione</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($items as $item)
							<tr>
								<td>{{ $item->barcode }}</td>
								<td>{{ $item->name }}</td>
								<td>{{ $item->text }}</td>
								<td class="table-action">
									@if( $branch->items->contains($item->id))
										<input type="checkbox" value="{{ $item->id }}" checked>
									@else
										<input type="checkbox" value="{{ $item->id }}">
									@endif
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div><!-- table-responsive -->
            </div>
        </div>
    </div>
</div>

</form>

@endsection

@section('javascript')
<script type="text/javascript">
$.fn.dataTable.ext.order['dom-checkbox'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).prop('checked') ? '1' : '0';
    } );
}


var t = $('#table1').DataTable({
	columnDefs: [
		{
			"orderable": false, "targets": 0
		},
		{
			"orderDataType": "dom-checkbox",
			"targets": 3
		},
		{
			"targets": 2,
			"data": '',
			"render": function ( data, type, full, meta ) {
				return type === 'display' && data.length > 40 ? '<span title="'+data+'">'+data.substr( 0, 38 )+'...</span>' : data;
			}
		},
	],
	order: [
		[3, "desc"]
	]
});

$('#form').submit(function(e){
	var matches = [];
	var checkedcollection = t.$("input:checked", { "page": "all" });
		checkedcollection.each(function (index, elem) {
		matches.push($(elem).val());
	});

	document.form.datatable.value = JSON.stringify(matches);
});

</script>
@endsection
