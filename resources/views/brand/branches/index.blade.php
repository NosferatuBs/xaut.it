@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
				<div class="table-responsive">
					<table class="table" id="table1">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Marchio</th>
								<th>Articoli</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($branches as $branch)
							<tr>
								<td>{{ $branch->name }}</td>
								<td>{{ $branch->brand->name }}</td>
								<td>{{ $branch->items_count }}</td>
								<td class="table-action">
									<div class="btn-group">
										<button class="btn btn-xs btn-default" type="button" onClick="return document.location.href = '{{ route('brand.branches.edit', $branch->id) }}';">Modifica</button>

										@if($branch->surveys_count == 0 && $branch->datasheets_count == 0)
										<button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu" role="menu">
											<li><a href="#" class="btn-delete" data-branch-id="{{ $branch->id }}">Elimina</a></li>
										</ul>
										@endif
									</div>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div><!-- table-responsive -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade modal-delete" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="deleteItem" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Confermi la cancellazione?</h4>
        </div>
        <div class="modal-body">
        	<p>
        		Nome: <span id="lbl-delete-name"></span><br />
        		Marchio: <span id="lbl-delete-brand"></span><br />
        	</p>
        </div>
        <div class="modal-footer">
        	<form method="post" action="" name="form_branch_delete">
        		{{ csrf_field() }}
        		{{ method_field('DELETE') }}

        		<input type="hidden" name="branch_id" value="" />

	        	<button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">Annulla</button>
	        	<button class="btn btn-danger" type="submit">Elimina</button>
	        </form>
        </div>
    </div>
  </div>
</div>

<script>
jQuery(document).ready(function() {
    "use strict";

    $("#table1").on('click','.btn-delete', function(){
		var array = t.row($(this).parents('tr')).data();

		document.getElementById('lbl-delete-name').innerText = array[0];
		document.getElementById('lbl-delete-brand').innerText = array[1];

		form_branch_delete.action = '/branches/' + $(this).data("branch-id");
		form_branch_delete.branch_id.value = $(this).data("branch-id");

		$('.modal-delete').modal('show');
	});

	$('.modal-delete').on('hidden.bs.modal', function (e) {
		document.getElementById('lbl-delete-name').innerText = "";
		document.getElementById('lbl-delete-brand').innerText = "";

		form_branch_delete.action = '';
		form_branch_delete.branch_id.value = 0;
	});
});
</script>

@endsection
