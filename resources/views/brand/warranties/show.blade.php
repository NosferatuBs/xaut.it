@extends('brand.layout.app')

@section('content')
<form class="form-horizontal">
	<div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	            	<h4 class="panel-title">Informazioni acquisto</h4>
	            </div>

	            <div class="panel-body">
	            	<div class="form-group">
						<label class="col-sm-3 control-label">Venditore</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $warranty->seller ?? '-' }} </p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Prezzo di acquisto</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $warranty->amount ?? '-' }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Data di acquisto</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $warranty->bought_at }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Data di attivazione</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $warranty->created_at->format('d/m/Y') }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Data di scadenza</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $warranty->expired_at }}</p>
						</div>
					</div>
	            </div>

	            @if($warranty->stored)
	            <div class="panel-footer">
	            	<a href="{{ url('storage/' . $warranty->stored) }}" class="btn btn-primary" target="_blank">Visualizza prova di acquisto</a>
	            </div>
	            @endif
	        </div>
	    </div>

	    <div class="col-md-12">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	            	<h4 class="panel-title">Informazioni utente</h4>
	            </div>

	            <div class="panel-body">
	            	<div class="form-group">
						<label class="col-sm-3 control-label">Email</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $warranty->traceability->email_user }} </p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Sesso</label>
						<div class="col-sm-6">
							<p class="form-control-static">
							@if($warranty->traceability->gender == 'male')
							Uomo
							@elseif($warranty->traceability->gender == 'female')
							Donna
							@else
							Altro
							@endif
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Data di nascita</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $warranty->traceability->birthday }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Paese di residenza</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $warranty->traceability->city }}</p>
						</div>
					</div>
	            </div>
	        </div>
	    </div>

	    <div class="col-md-12">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	            	<h4 class="panel-title">Informazioni marchio</h4>
	            </div>

	            <div class="panel-body">
	            	<div class="form-group">
						<label class="col-sm-3 control-label">Nome</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $warranty->traceability->brand->name }} </p>
						</div>
					</div>
					@if ($warranty->traceability->brand->url_video)
					<div class="form-group">
						<label class="col-sm-3 control-label">Sito web</label>
						<div class="col-sm-6">
							<p class="form-control-static"><a href="{{ $warranty->traceability->brand->url_video }}" target="_blank">{{ $warranty->traceability->brand->url_video }}</a></p>
						</div>
					</div>
					@endif
					<div class="form-group">
						<label class="col-sm-3 control-label">Descrizione</label>
						<div class="col-sm-6">
							<p class="form-control-static">{!! nl2br($warranty->traceability->brand->text) !!} </p>
						</div>
					</div>
	            </div>
	        </div>
	    </div>

@if($item)
	    <div class="col-md-12">
	        <div class="panel panel-default">
	            <div class="panel-heading">
	            	<h4 class="panel-title">Informazioni prodotto</h4>
	            </div>

	            <div class="panel-body">
	            	<div class="form-group">
						<label class="col-sm-3 control-label">Stock Keeping Unit</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $item->name }} </p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Barcode</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $item->barcode }} </p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Descrizione</label>
						<div class="col-sm-6">
							<p class="form-control-static">{!! nl2br($item->text) !!} </p>
						</div>
					</div>
					@if ($item->url_video)
					<div class="form-group">
						<label class="col-sm-3 control-label">Sito web</label>
						<div class="col-sm-6">
							<p class="form-control-static"><a href="{{ $item->url_video }}" target="_blank">{{ $item->url_video }}</a></p>
						</div>
					</div>
					@endif
	            </div>
	        </div>
	    </div>
	</div>
@endif


</form>
@endsection
