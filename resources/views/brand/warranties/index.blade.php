@extends('brand.layout.app')

@section('content')

@if($brands != $brands_option AND $brands_option != 0)
<div class="row">
    <div class="col-md-12">
		<div class="alert alert-danger alert-dismissible fade in">
			<h4>Le <strong>Garanzie</strong> non sono attive per alcuni tuoi marchi!</h4>
			<p>Fai l'upgrade al pacchetto GARANZIE per poter dare questo servizio online a tutti i tuoi clienti.<br>Contattaci tramite email o telefonicamente.</p>
		</div>
    </div>
</div>
@endif

@if($brands_option > 0)

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
				<div class="table-responsive">
					<table class="table" id="table1">
						<thead>
							<tr>
								<th>Email</th>
								<th>Articolo</th>
								<th>Marchio</th>
								<th>Venditore</th>
								<th>Data acquisto</th>
								<th>Data attivazione</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach($warranties as $warranty)
							<tr>
								<td>{{ $warranty->traceability->email_user }}</td>
								<td>
									@if($warranty->traceability->item_id)
										{{ $warranty->traceability->item->name }}
									@else
									-
									@endif
								</td>
								<td>{{ $warranty->traceability->brand->name }}</td>
								<td>{{ $warranty->seller }}</td>
								<td>{{ $warranty->bought_at }}</td>
								<td>{{ $warranty->created_at->format('d/m/Y H:i:s') }}</td>
								<td>
									<div class="btn-group">
										<a class="btn btn-xs btn-default" href="/warranties/{{ $warranty->id }}">Dettagli</a>
									</div>
								</td>
						@endforeach
						</tbody>
					</table>
				</div><!-- table-responsive -->
            </div>
        </div>
    </div>
</div>

<script>
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-euro-pre": function ( a ) {
        var x;

        if ( $.trim(a) !== '' ) {
            var frDatea = $.trim(a).split(' ');
            var frTimea = (undefined != frDatea[1]) ? frDatea[1].split(':') : [00,00,00];
            var frDatea2 = frDatea[0].split('/');
            x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;
        }
        else {
            x = Infinity;
        }

        return x;
    },

    "date-euro-asc": function ( a, b ) {
        return a - b;
    },

    "date-euro-desc": function ( a, b ) {
        return b - a;
    }
} );

jQuery(document).ready(function() {
    "use strict";

    var t = jQuery('#table1').DataTable({
    	columnDefs: [
			{
				"orderable": false, "targets": 6
			},
			{
				"type": 'date-euro', "targets": [4,5]
			}
		],
		order: [
			[5,"desc"]
		]
    });
});
</script>

@else
<div class="row">
    <div class="col-md-12">
		<div class="alert alert-danger alert-dismissible fade in">
			<h4>Le <strong>Garanzie</strong> non sono attive per i tuoi marchi!</h4>
			<p>Fai l'upgrade al pacchetto GARANZIE per poter dare questo servizio online a tutti i tuoi clienti.<br>Contattaci tramite email o telefonicamente.</p>
		</div>
    </div>
</div>
@endif

@endsection
