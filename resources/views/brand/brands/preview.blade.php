<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">Riepilogo marchio</h4>
        </div>
        <form class="form-horizontal">
            <div class="panel-body">
                <div class="col-md-4 text-center">
                    @if( $brand->logo != null )
                        <img src="{{ asset('storage/' . $brand->logo) }}" class="img-resposive" style="max-height: 200px;" />
                    @endif
                </div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nome</label>
                        <div class="col-sm-6">
                            <p class="form-control-static">{{ $brand->name }} </p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Descrizione</label>
                        <div class="col-sm-6">
                            <p class="form-control-static">{!! nl2br($brand->text) !!} </p>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
