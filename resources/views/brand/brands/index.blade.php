@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
				<div class="table-responsive">
					<table class="table" id="table1">
						<thead>
							<tr>
								<th>Logo</th>
								<th>Nome interno</th>
								<th>Nome pubblico</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($brands as $brand)
							<tr>
								<td>
									@if( $brand->logo != null )
										<img src="{{ asset('storage/' . $brand->logo) }}" style="max-height: 40px; max-width: 60px;" />
									@endif
								</td>
								<td>{{ $brand->name }}</td>
								<td>{{ $brand->name_public }}</td>
								<td>
									<div class="btn-group">
										<a class="btn btn-xs btn-default" href="{{ route('brand.brands.edit', $brand->id) }}" >Modifica</a>

										@if( (Auth::user()->can('delete_brand') && $brand->items->isEmpty()) || $brand->level == 1)
										<button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu" role="menu">
											@if($brand->level == 1)
											<li><a href="{{ route('brand.brands.numbering.edit', $brand->id) }}">Numerazione</a></li>
											@endif
                                            @if($brand->discount_option == 1)
                                            <li><a href="{{ route('brand.brands.coupon.edit', $brand->id) }}">Configura Coupon</a></li>
                                            @endif
											@if(Auth::user()->can('delete_brand') && $brand->items->isEmpty())
											<li><a href="#" class="btn-delete" data-brand-id="{{ $brand->id }}">Elimina</a></li>
											@endif
										</ul>
										@endif
									</div>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div><!-- table-responsive -->
            </div>
        </div>
    </div>
</div>

@can('delete_brand')
<div class="modal fade modal-delete" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="deleteItem" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Confermi la cancellazione?</h4>
        </div>
        <div class="modal-body">
        	<p>
        		Nome interno: <span id="lbl-delete-private"></span><br />
        		Nome pubblico: <span id="lbl-delete-public"></span><br />
			</p>
        </div>
        <div class="modal-footer">
        	<form method="post" action="/brands" name="form_brand_delete">
        		{{ csrf_field() }}
        		{{ method_field('DELETE') }}

        		<input type="hidden" name="brand_id" value="" />

	        	<button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">Annulla</button>
	        	<button class="btn btn-danger" type="submit">Elimina</button>
	        </form>
        </div>
    </div>
  </div>
</div>
@endcan
<script>
jQuery(document).ready(function() {
    "use strict";

    var t = jQuery('#table1').DataTable({
    	columnDefs: [
			{
				"orderable": false, "targets": [
					0,
					4
				]
			}
		],
		order: [
			[1,"asc"]
		]
    });

    @can('delete_brand')
    $("#table1").on('click','.btn-delete', function(){
		var array = t.row($(this).parents('tr')).data();

		document.getElementById('lbl-delete-private').innerHTML = array[1];
		document.getElementById('lbl-delete-public').innerHTML = array[2];

		form_brand_delete.brand_id.value = $(this).data("brand-id");

		$('.modal-delete').modal('show');
	});

	$('.modal-delete').on('hidden.bs.modal', function (e) {
		document.getElementById('lbl-delete-private').innerHTML = "";
		document.getElementById('lbl-delete-public').innerHTML = "";

		form_brand_delete.brand_id.value = 0;
	});
	@endcan
});
</script>

@endsection
