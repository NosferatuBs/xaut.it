@extends('brand.layout.app')

@section('content')

<form class="form-horizontal" action="{{ route('brand.brands.update', $brand->id) }}" method="POST" enctype="multipart/form-data">
{{ csrf_field() }}
{{ method_field('PUT') }}

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Modifica marchio</h4>
            </div>

            <div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="name">Nome interno <span class="asterisk">*</span></label>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="name" id="name" value="{{ $brand->name }}" required />
						<span class="help-block">Visibile da amministratore e proprietario di brand.</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="name_public">Nome pubblico <span class="asterisk">*</span></label>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="name_public" id="name_public" value="{{ $brand->name_public }}" required />
						<span class="help-block">Visibile dal cliente finale.</span>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="file">Logo</label>
					<div class="col-sm-6">
						<input type="file" class="form-control" name="logo" id="file" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="url_video">URL Video</label>
					<div class="col-sm-6">
						<input type="url" class="form-control" name="url_video" id="url_video" value="{{ $brand->url_video }}" />
						<span class="help-block">Copia e incolla il link del tuo video.</span>
					</div>

				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="text">Descrizione</label>
					<div class="col-sm-6">
						<textarea class="form-control" rows="5" name="text" id="text">{{ $brand->text }}</textarea>
						<span class="help-block">Inserisci una descrizione del tuo marchio</span>
					</div>
				</div>

				@if(Auth::user()->hasRole('admin'))

				<hr>

				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="checkbox block">
							<label>
								<input type="checkbox" name="survey_option" id="survey_option" {{($brand->survey_option == 1)?'checked':''}}> Attiva i sondaggi
							</label>
						</div>
					</div>
					<div class="col-sm-6 col-sm-offset-3">
						<div class="checkbox block">
							<label>
								<input type="checkbox" name="survey_item_option" id="survey_item_option" {{($brand->survey_item_option == 1)?'checked':''}} {{($brand->survey_option != 1)?'disabled="disabled"':''}}> Attiva i sondaggi per articolo
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="checkbox block">
							<label>
								<input type="checkbox" name="stats_option" id="stats_option" {{($brand->stats_option == 1)?'checked':''}}> Attiva le statistiche base
							</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="checkbox block">
							<label>
								<input type="checkbox" name="authenticity_option" id="authenticity_option" {{($brand->authenticity_option == 1)?'checked':''}}> Attiva l'autenticità
							</label>
						</div>
					</div>
				</div>

				<hr>

				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="checkbox block">
							<label>
								<input type="checkbox" name="discount_option" id="discount_option" {{($brand->discount_option == 1)?'checked':''}}> Attiva lo sconto per gli utenti
							</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="discount_percentage">Sconto in %</label>
					<div class="col-sm-6">
						<input type="number" min="0" class="form-control" name="discount_percentage" id="discount_percentage" value="{{ $brand->discount_percentage }}" />
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="discount_amount">Sconto in &euro;</label>
					<div class="col-sm-6">
						<input type="number" min="0" class="form-control" name="discount_amount" id="discount_amount" value="{{ $brand->discount_amount }}" />
					</div>
				</div>

				<hr>

				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="checkbox block">
							<label>
								<input type="checkbox" name="warranty_option" id="warranty_option" {{($brand->warranty_option == 1)?'checked':''}}> Attiva la garanzia per gli utenti
							</label>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="warranty_extension">Estensione garanzia (mesi)</label>
					<div class="col-sm-6">
						<input type="number" min="0" class="form-control" name="warranty_extension" id="warranty_extension" value="{{ $brand->warranty_extension }}" />
						<span class="help-block">Per legge la garanzia dura 24 mesi, l'estensione non è retroattiva.</span>
					</div>
				</div>

				@else

					@if($brand->survey_option == 1)
					<div class="alert alert-success">
						I sondaggi per i clienti di questo marchio sono attivi.
					</div>
					@else
					<div class="alert alert-danger">
						I sondaggi per i clienti di questo marchio NON sono attivi.<br>
						Contattaci per l'attivazione.
					</div>
					@endif

					@if($brand->stats_option == 1)
					<div class="alert alert-success">
						Le statistiche dei clienti di questo marchio sono attivi.
					</div>
					@else
					<div class="alert alert-danger">
						Le statistiche dei clienti di questo marchio NON sono attivi.<br>
						Contattaci per l'attivazione.
					</div>
					@endif

					@if($brand->authenticity_option == 1)
					<div class="alert alert-success">
						L'autenticità dei prodotti di questo marchio è attiva.
					</div>
					@else
					<div class="alert alert-danger">
						L'autenticità dei prodotti di questo marchio NON è attiva.<br>
						Contattaci per l'attivazione.
					</div>
					@endif
					@if($brand->discount_option == 1)
					<div class="alert alert-success">
						Gli sconti per i clienti di questo marchio sono attivi.<br>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="discount_percentage">Sconto in %</label>
						<div class="col-sm-6">
							<input type="number" min="0" class="form-control" name="discount_percentage" id="discount_percentage" value="{{ $brand->discount_percentage }}" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="discount_amount">Sconto in &euro;</label>
						<div class="col-sm-6">
							<input type="number" min="0" class="form-control" name="discount_amount" id="discount_amount" value="{{ $brand->discount_amount }}" />
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="discount_validity">Validità dello sconto in mesi </label>
						<div class="col-sm-6">
							<input type="number" min="1" class="form-control" name="discount_validity" id="discount_validity" value="{{ $brand->discount_validity }}" />
						</div>
					</div>

					@else
					<div class="alert alert-danger">
						Gli sconti per i clienti di questo marchio NON sono attivi.<br>
						Contattaci per l'attivazione.
					</div>
					@endif

					@if($brand->warranty_option == 1)
					<div class="alert alert-success">
						La garanzia per i clienti di questo marchio è attiva.<br>
					</div>

					<div class="form-group">
						<label class="col-sm-3 control-label" for="warranty_extension">Estensione garanzia (mesi)</label>
						<div class="col-sm-6">
							<input type="number" min="0" class="form-control" name="warranty_extension" id="warranty_extension" value="{{ $brand->warranty_extension }}" />
							<span class="help-block">Per legge la garanzia dura 24 mesi, l'estensione non è retroattiva.</span>
						</div>
					</div>

					@else
					<div class="alert alert-danger">
						La garanzia per i clienti di questo marchio NON è attiva.<br>
						Contattaci per l'attivazione.
					</div>
					@endif
				@endif

				<div class="form-group">
					<label class="col-sm-3 control-label" for="file">Galleria fotografica</label>
					<div class="col-sm-6">
						<input type="file" class="form-control" name="file[]" id="file" multiple />
						<span class="help-block">Puoi caricare più foto contemporaneamente tenendo premuto il tasto "Maiuscolo"</span>
					</div>
				</div>
				@if (count($errors) > 0)
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				@endif
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<button class="btn btn-primary">Modifica</button>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>

@if( count($brand->photo) > 0 )
<div class="row">
	<div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Galleria fotografica</h4>
            	<p>Trascina le foto per cambiarne l'ordine</p>
            </div>
            <div class="panel-body">
            	<table class="table">
					<thead>
						<tr>
							<th></th>
							<th>Anteprima</th>
							<th>Nome file</th>
							<th>Titolo</th>
							<th>Data caricamento</th>
							<th>Elimina</th>
						</tr>
					</thead>
					<tbody id="listWithHandle">
						@foreach( $brand->photo as $image )
						<tr>
							<td>
								<input type="hidden" name="gallery[]" value="{{ $image->id }}" />
								<button type="button" class="btn btn-xs draggable">
									<i class="fa fa-arrows-v"></i>
								</button>
							</td>
							<td>
								<a href="{{ asset('storage/' . $image->stored) }}" data-rel="prettyPhoto[gallery]">
					                <img src="{{ asset('storage/' . $image->stored) }}" class="img-responsive" alt="{{ $image->original }}" title="{{ $image->original }}" style="max-height: 40px; max-width: 60px;" />
					            </a>
							</td>
							<td>{{ $image->original }}</td>
							<td><input type="text" class="form-control" name="gallery_title[]" value="{{ $image->title }}" placeholder="Inserisci un titolo" /></td>
							<td>{{ $image->created_at->format('d/m/Y H:i') }}</td>
							<td>
								<input type="checkbox" class="gallery_delete" name="gallery_delete[]" value="{{ $image->id }}" />
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
            </div>
        </div>
    </div>
</div>
@endif

</form>

<script>
jQuery(document).ready(function(){

    "use strict";

    jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
    	social_tools: '',
    	show_title: true
    });

	$("#text").summernote();

    $(".gallery_delete").change(function(){
          $("#file").prop('disabled', $('.gallery_delete:checked').length>0);
	})

	Sortable.create(listWithHandle, {
	  handle: '.draggable',
	  animation: 150
	});

});

$('#survey_option').click(function(){

    if ($(this).is(':checked'))
    {
        $("#survey_item_option").removeAttr("disabled");
    }
    else
    {
        $("#survey_item_option").attr("disabled","disabled");
    }
});

</script>
@endsection

@section('css')
<style>
.draggable {
	cursor: move;
	cursor: -webkit-grabbing;
}
</style>
@endsection
