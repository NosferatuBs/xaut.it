@extends('brand.layout.app')

@section('content')

<form class="form-horizontal" action="{{ route('brand.brands.coupon.update', $brand->id) }}" method="POST" enctype="multipart/form-data">
{{ csrf_field() }}
{{ method_field('PUT') }}

<div class="row">
    @include('brand.brands.preview')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Modifica configurazione coupon</h4>
            </div>

            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="template">Template <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <input type="file" accept="application/pdf" class="form-control" name="template" id="template" />
                        @if($brand->coupon && $brand->coupon->template)
                        <span class="help-block">È già presente un file. <a href="{{ Storage::url($brand->coupon->template) }}" target="_BLANK">Visualizza</a></span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="coordinate_x">Coordinata orizzontale (X) <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="coordinate_x" id="coordinate_x" value="{{ $brand->coupon->x ?? null }}" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="coordinate_y">Coordinata verticale (Y) <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="coordinate_y" id="coordinate_y" value="{{ $brand->coupon->y ?? null }}" required />
                    </div>
                </div>

                @if (count($errors) > 0)
                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @endif
                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-3">
                        <button class="btn btn-primary">Modifica</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<form class="form-horizontal" action="{{ route('brand.brands.coupon.test', $brand->id) }}" method="GET" target="_BLANK">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Simula generazione coupon</h4>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="email">Email <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <input type="name" class="form-control" name="email" id="email" value="prova@prova.it" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="discount">Importo sconto <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="discount" id="discount" value="10" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="token">Token univoco sconto <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="token" id="token" value="{{ str_random() }}" required />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="expired_at">Data scadenza <span class="asterisk">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="expired_at" id="expired_at" value="31/12/2020" required />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-6 col-sm-offset-3">
                        <button class="btn btn-primary">Simula</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
