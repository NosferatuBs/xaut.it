<ul class="nav nav-pills nav-stacked nav-bracket">
    <h5 class="sidebartitle">Navigazione</h5>
    <li class="{{ ($menu->area == 'dashboard') ? 'active' : '' }}">
        <a href="{{ route('brand.dashboard') }}"><i class="fa fa-home"></i> <span>Riepilogo</span></a>
    </li>
    <li class="nav-parent {{ ($menu->area == 'articoli') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-cubes"></i> <span>Articoli</span></a>
        <ul class="children" style="{{ ($menu->area == 'articoli') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'articoli' && $menu->option == 'elenco') ? 'active' : '' }}">
                <a href="{{ route('brand.items.index') }}"><i class="fa fa-caret-right"></i> Elenco</a>
            </li>
            <li class="{{ ($menu->area == 'articoli' && $menu->option == 'create') ? 'active' : '' }}">
                <a href="{{ route('brand.items.create') }}"><i class="fa fa-caret-right"></i> Aggiungi articolo</a>
            </li>
            <!-- <li class="{{ ($menu->area == 'articoli' && $menu->option == 'track') ? 'active' : '' }}">
                <a href="/items/tracking"><i class="fa fa-caret-right"></i> Rintraccia articolo</a>
            </li> -->
        </ul>
    </li>
    <li class="nav-parent {{ ($menu->area == 'segmenti') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-code-fork"></i> <span>Segmenti</span></a>
        <ul class="children" style="{{ ($menu->area == 'segmenti') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'segmenti' && $menu->option == 'elenco') ? 'active' : '' }}">
                <a href="{{ route('brand.branches.index') }}"><i class="fa fa-caret-right"></i> Elenco</a>
            </li>
            <li class="{{ ($menu->area == 'segmenti' && $menu->option == 'create') ? 'active' : '' }}">
                <a href="{{ route('brand.branches.create') }}"><i class="fa fa-caret-right"></i> Aggiungi segmento</a>
            </li>
        </ul>
    </li>
    <li class="nav-parent {{ ($menu->area == 'marchi') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-shield"></i> <span>Marchi</span></a>
        <ul class="children" style="{{ ($menu->area == 'marchi') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'marchi' && $menu->option == 'elenco') ? 'active' : '' }}">
                <a href="{{ route('brand.brands.index') }}"><i class="fa fa-caret-right"></i> Elenco</a>
            </li>
        </ul>
    </li>
    <li class="nav-parent {{ ($menu->area == 'documenti') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-file-text"></i> <span>Documenti</span></a>
        <ul class="children" style="{{ ($menu->area == 'documenti') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'documenti' && $menu->option == 'elenco') ? 'active' : '' }}">
                <a href="{{ route('brand.datasheets.index') }}"><i class="fa fa-caret-right"></i> Elenco</a>
            </li>
            <li class="{{ ($menu->area == 'documenti' && $menu->option == 'create') ? 'active' : '' }}">
                <a href="{{ route('brand.datasheets.create') }}"><i class="fa fa-caret-right"></i> Aggiungi documento</a>
            </li>
        </ul>
    </li>
    <li class="nav-parent {{ ($menu->area == 'sondaggi') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-puzzle-piece"></i> <span>Sondaggi</span></a>
        <ul class="children" style="{{ ($menu->area == 'sondaggi') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'sondaggi' && $menu->option == 'elenco') ? 'active' : '' }}">
                <a href="{{ route('brand.surveys.index') }}"><i class="fa fa-caret-right"></i> Elenco</a>
            </li>
            <li class="{{ ($menu->area == 'sondaggi' && $menu->option == 'create') ? 'active' : '' }}">
                <a href="{{ route('brand.surveys.create') }}"><i class="fa fa-caret-right"></i> Aggiungi domanda</a>
            </li>
        </ul>
    </li>
    <li class="nav-parent {{ ($menu->area == 'garanzia') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-ticket"></i> <span>Garanzia</span></a>
        <ul class="children" style="{{ ($menu->area == 'garanzia') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'garanzia' && $menu->option == 'elenco') ? 'active' : '' }}">
                <a href="{{ route('brand.warranties.index') }}"><i class="fa fa-caret-right"></i> Elenco</a>
            </li>
        </ul>
    </li>
    <li class="nav-parent {{ ($menu->area == 'clienti') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-users"></i> <span>Clienti</span></a>
        <ul class="children" style="{{ ($menu->area == 'clienti') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'clienti' && $menu->option == 'elenco') ? 'active' : '' }}">
                <a href="{{ route('brand.customers.index') }}"><i class="fa fa-caret-right"></i> Elenco</a>
            </li>
        </ul>
    </li>
    <li class="nav-parent {{ ($menu->area == 'analytics') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-line-chart"></i> <span>Statistiche</span></a>
        <ul class="children" style="{{ ($menu->area == 'analytics') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'analytics' && $menu->option == 'customers') ? 'active' : '' }}">
                <a href="{{ route('brand.analytics.customers.overview') }}"><i class="fa fa-caret-right"></i> Analisi clienti</a>
            </li>
            <li class="{{ ($menu->area == 'analytics' && $menu->option == 'traceability') ? 'active' : '' }}">
                <a href="{{ route('brand.analytics.traceability.overview') }}"><i class="fa fa-caret-right"></i> Cronologia ricerche</a>
            </li>
            <li class="{{ ($menu->area == 'analytics' && $menu->option == 'items') ? 'active' : '' }}">
                <a href="{{ route('brand.analytics.items.overview') }}"><i class="fa fa-caret-right"></i> Valutazione articoli</a>
            </li>
            <li class="{{ ($menu->area == 'analytics' && $menu->option == 'surveys') ? 'active' : '' }}">
                <a href="{{ route('brand.analytics.surveys.overview') }}"><i class="fa fa-caret-right"></i> Risposte sondaggi</a>
            </li>
            <li class="{{ ($menu->area == 'analytics' && $menu->option == 'export') ? 'active' : '' }}">
                <a href="{{ route('brand.export.analytics.index') }}"><i class="fa fa-caret-right"></i> Esporta statistiche</a>
            </li>
        </ul>
    </li>
    <li class="nav-parent {{ ($menu->area == 'coupon') ? 'nav-active' : '' }}">
        <a href=""><i class="fa fa-tags"></i> <span>Coupon</span></a>
        <ul class="children" style="{{ ($menu->area == 'coupon') ? 'display:block;' : '' }}">
            <li class="{{ ($menu->area == 'coupon' && $menu->option == 'customers') ? 'active' : '' }}">
                <a href="{{ route('brand.coupon.list', 0) }}"><i class="fa fa-caret-right"></i> Elenco da saldare</a>
                <a href="{{ route('brand.coupon.list', 1) }}"><i class="fa fa-caret-right"></i> Elenco saldati</a>
            </li>
        </ul>
    </li>
</ul>
