<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="/css/style-v1.default.css" rel="stylesheet">
    <link href="/css/jquery.datatables.css" rel="stylesheet">
    <link href="/css/jquery.gritter.css" rel="stylesheet">
    <link href="/css/style.dodgerblue.css" rel="stylesheet">
    <link href="/css/prettyPhoto.css" rel="stylesheet">

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'googleAPI' => config('google.api')
        ]); ?>
    </script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->
    <script src="/js/jquery-1.11.1.min.js"></script>

    <script src="/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="/js/jquery-ui-1.10.3.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/modernizr.min.js"></script>
    <script src="/js/jquery.sparkline.min.js"></script>
    <script src="/js/toggles.min.js"></script>
    <script src="/js/jquery.cookies.js"></script>
    <script src="/js/select2.min.js"></script>
    <script src="/js/jquery.datatables.min.js"></script>
    <script src="/js/jquery.maskedinput.min.js"></script>

    <script src="/js/jquery.gritter.min.js"></script>
    <script src="/js/jquery.prettyPhoto.js"></script>
    <script src="/js/custom.js"></script>
    <script src="/js/Sortable.min.js"></script>

    <script>
    var dt_trans = {
        'it' : {
            "sEmptyTable":     "Nessun dato presente nella tabella",
            "sInfo":           "Vista da _START_ a _END_ di _TOTAL_ elementi",
            "sInfoEmpty":      "Vista da 0 a 0 di 0 elementi",
            "sInfoFiltered":   "(filtrati da _MAX_ elementi totali)",
            "sInfoPostFix":    "",
            "sInfoThousands":  ".",
            "sLengthMenu":     "Visualizza _MENU_ elementi",
            "sLoadingRecords": "Caricamento...",
            "sProcessing":     "Elaborazione...",
            "sSearch":         "Cerca:",
            "sZeroRecords":    "La ricerca non ha portato alcun risultato.",
            "oPaginate": {
                "sFirst":      "Inizio",
                "sPrevious":   "Precedente",
                "sNext":       "Successivo",
                "sLast":       "Fine"
            },
            "oAria": {
                "sSortAscending":  ": attiva per ordinare la colonna in ordine crescente",
                "sSortDescending": ": attiva per ordinare la colonna in ordine decrescente"
            }
        }
    };

    $.extend( true, $.fn.dataTable.defaults, {
        "language": dt_trans['it'],
    } );
    </script>

    @yield('css')

    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>

    <link href="/css/custom.css?v={{ time() }}" rel="stylesheet">
</head>
<body id="top" class="{{ (session('fullscreen'))? 'leftpanel-collapsed' : '' }}">

    <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

    <section>
        <div class="leftpanel">
            <div class="logopanel">
                <h1>{{ config('app.name', 'Laravel') }}</h1>
            </div><!-- logopanel -->

            <div class="leftpanelinner">
                <!-- This is only visible to small devices -->
                <div class="visible-xs hidden-sm hidden-md hidden-lg">
                    <div class="media userlogged">
                        <img alt="{{ Auth::user()->name }}" src="{{ Avatar::create(Auth::user()->name)->toBase64() }}" class="media-object">
                        <div class="media-body">
                            <h4>{{ Auth::user()->name }}</h4>
                        </div>
                    </div>

                    <h5 class="sidebartitle actitle">Account</h5>
                    <ul class="nav nav-pills nav-stacked nav-bracket mb30">
                        <li><a href="{{ route('brand.account.profile.edit') }}"><i class="fa fa-user"></i> <span>Profilo</span></a></li>
                        <li><a href="{{ route('brand.account.password.edit') }}"><i class="fa fa-key"></i> <span>Modifica password</span></a></li>
                        <li><a href="javascript:;" onclick="document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> <span>Disconnetti</span></a></li>
                    </ul>
                </div>
                @include('brand.layout.menu')
            </div><!-- leftpanelinner -->
        </div><!-- leftpanel -->

        <div class="mainpanel">
            <div class="headerbar">
                <a class="menutoggle"><i class="fa fa-bars"></i></a>

                <div class="header-right">
                    <ul class="headermenu">
                        <li>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{ Avatar::create(Auth::user()->name)->toBase64() }}" alt="{{ Auth::user()->name }}">
                                    {{ Auth::user()->name }}
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                                    <li><a href="{{ route('brand.account.profile.edit') }}"><i class="fa fa-user"></i> Profilo</a></li>
                                    <li><a href="{{ route('brand.account.password.edit') }}"><i class="fa fa-key"></i> Modifica password</a></li>
                                    @if(session('impersonated_by'))
                                        <li><a href="{{ route('impersonate.leaveImpersonation') }}"><i class="fa fa-arrow-left"></i> <span>Torna all'area admin</span></a></li>
                                    @endif
                                    <li><a href="javascript:;" onclick="document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Disconnetti</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div><!-- header-right -->
            </div><!-- headerbar -->

            <div class="pageheader">
                <h2><i class="{{ $title->icon }}"></i> {{ $title->title }}
                    @if ($title->description)
                        <span>{{ $title->description }}</span>
                    @endif
                </h2>
                <div class="breadcrumb-wrapper">
                    <span class="label">Sei in:</span>
                    <ol class="breadcrumb">
                        @foreach ($breadcumbs as $breadcumb)
                            @if ($breadcumb->url)
                                <li><a href="{{ $breadcumb->url }}">{{ $breadcumb->title }}</a></li>
                            @else
                                <li class="active">{{ $breadcumb->title }}</li>
                            @endif
                        @endforeach
                    </ol>
                </div>
            </div>

            <div class="contentpanel">
                @yield('content')
            </div><!-- contentpanel -->
        </div><!-- mainpanel -->
    </section>

    @yield('javascript')

    <script>
    $('document').ready(function(){
        $('input[type=url]').change(function(){
            var s = this.value;
            s = s.trim();
            if (s.length > 0 && !s.match(/^[a-zA-Z]+:\/\//))
            {
                s = 'http://' + s;
            }
            this.value = s;
        });

        $('.menutoggle').click(function(){
            if(viewport().width <= 1024)
                return;

            var goingToClose = 0;
            if($(this).hasClass('menu-collapsed'))
            {
                goingToClose = 1;
            }

            $.ajax({
                method: 'post',
                url: '/json/settings',
                data: {
                    '_token': window.Laravel.csrfToken,
                    'fullscreen': goingToClose
                },
                success: function(){
                    setGritter('Modalità di visualizzazione salvata correttamente','growl-success');
                },
                error: function(jqXHR, textStatus, errorThrown){
                    setGritter('Errore durante il salvataggio','growl-danger');
                }
            })
        });
        function setGritter(message, classe = '')
        {
            $.gritter.add({
                title: message,
                sticky: false,
                class_name: classe
            });
        }

        $('.alert-dismissible').on('closed.bs.alert', function () {
            var keyword = $('button', this).data('notificationkey');

            if(keyword.length > 0)
            {
                $.ajax({
                    method: 'post',
                    url: '/json/settings',
                    data: {
                        '_token': window.Laravel.csrfToken,
                        'alert': keyword
                    },
                    success: function(){
                        setGritter('La notifica è stata nascosta','growl-success');
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        setGritter('Errore durante il salvataggio','growl-danger');
                    }
                })
            }
        })

    });
    function viewport() {
        var e = window, a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
    }
    </script>
</body>
</html>
