@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
				<div class="table-responsive">
					<table class="table" id="table1" width="100%">
						<thead>
							<tr>
								<th>Anteprima</th>
								<th>Barcode</th>
								<th>Codice</th>
								<th>Descrizione</th>
								<th>Marchio</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
                        @foreach($items as $item)
                        <tr>

                            <td>
                                @if($item->picture)
                                <img src="{{ $item->picture }}" style="max-height: 40px; max-width: 60px;" />
                                @endif
                            </td>
                            <td>{{ $item->barcode }}</td>
                            <td>{{ $item->name }}</td>
                            <td>
                                @if(strlen($item->text) > 40)
                                <span title="{{ $item->text }}">{{ substr($item->text, 0, 38) . '...' }}</span>
                                @else
                                {{ $item->text }}
                                @endif
                            </td>
                            <td>{{ $item->brand->name }}</td>
                            <td>
                                @if(count($item->dropdown) > 0)
                                <div class="btn-group">
                                    <a
                                    @foreach($item->dropdown[0]['options'] as $key => $value)
                                    {{ $key }}="{{ $value}}"
                                    @endforeach
                                    >{{ $item->dropdown[0]['label'] }}</a>
                                    <button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown">
                                    <span class="caret"></span><span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                    @for($i = 1; $i < count($item->dropdown); $i++)
                                        @if(isset($item->dropdown[$i]['separator']) && $item->dropdown[$i]['separator'] == 1)
                                        <li class="divider"></li>
                                        @endif
                                        <li><a
                                        @foreach($item->dropdown[$i]['options'] as $key => $value)
                                        {{ $key }}="{{ $value}}"
                                        @endforeach
                                        >{{ $item->dropdown[$i]['label'] }}</a></li>
                                    @endfor
                                    </ul>
                                </div>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
					</table>
                    {{ $items->links() }}
				</div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-delete" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="deleteItem" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Confermi la cancellazione?</h4>
        </div>
        <div class="modal-body">
        	<p>
        		Barcode: <span id="lbl-delete-barcode"></span><br />
        		Codice: <span id="lbl-delete-codice"></span><br />
        		Descrizione: <span id="lbl-delete-descrizione"></span><br />
        		Marchio: <span id="lbl-delete-marchio"></span>
        	</p>
        </div>
        <div class="modal-footer">
        	<form method="post" action="/items" name="form_item_delete">
        		{{ csrf_field() }}
        		{{ method_field('DELETE') }}

        		<input type="hidden" name="item_id" value="" />

	        	<button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">Annulla</button>
	        	<button class="btn btn-danger" type="submit">Elimina</button>
	        </form>
        </div>
    </div>
  </div>
</div>

<script>
$('document').ready(function(){

	$("#table1").on('click','.btn-delete', function(){

        var $row = $(this).closest('tr');

		document.getElementById('lbl-delete-barcode').innerHTML = $row.children('td:eq(1)').text();
		document.getElementById('lbl-delete-codice').innerHTML = $row.children('td:eq(2)').text();
		document.getElementById('lbl-delete-descrizione').innerHTML = $row.children('td:eq(3)').text();
		document.getElementById('lbl-delete-marchio').innerHTML = $row.children('td:eq(4)').text();

		form_item_delete.action = $(this).data("action");

		$('.modal-delete').modal('show');
	});

	$('.modal-delete').on('hidden.bs.modal', function (e) {
		document.getElementById('lbl-delete-barcode').innerHTML = "";
		document.getElementById('lbl-delete-codice').innerHTML = "";
		document.getElementById('lbl-delete-descrizione').innerHTML = "";
		document.getElementById('lbl-delete-marchio').innerHTML = "";

		form_item_delete.action = "";
	});
});
</script>

@endsection
