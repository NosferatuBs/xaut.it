@extends('brand.layout.app')

@section('content')
<form class="form-horizontal" action="{{ route('brand.items.update', $item->id) }}" method="POST" enctype="multipart/form-data">
{{ csrf_field() }}
{{ method_field('PUT') }}

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Modifica articolo</h4>
            </div>

            <div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="brand_id">Marchio <span class="asterisk">*</span></label>
					<div class="col-sm-6">
						<select class="form-control" name="brand_id" id="brand_id" required>
						<option value="">-- seleziona --</option>
						@foreach($brands as $brand)
							<option value="{{ $brand->id }}" {{ ($item->brand_id == $brand->id) ? 'selected' : '' }}>{{ $brand->name }}</option>
						@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="name">Codice <span class="asterisk">*</span></label>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="name" id="name" value="{{ $item->name }}" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="barcode">Barcode</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="barcode" id="barcode" value="{{ $item->barcode }}" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="text">Descrizione</label>
					<div class="col-sm-6">
						<textarea class="form-control" rows="5" name="text" id="text">{{ $item->text }}</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="url_video">URL Video</label>
					<div class="col-sm-6">
						<input type="url" class="form-control" name="url_video" id="url_video" value="{{ $item->url_video }}" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="file">Galleria fotografica</label>
					<div class="col-sm-6">
						<input type="file" class="form-control" name="file[]" id="file" multiple />
						<span class="help-block">Puoi caricare più foto contemporaneamente tenendo premuto il tasto "Maiuscolo"</span>
					</div>
				</div>

				@if (count($errors) > 0)
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				@endif
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<button class="btn btn-primary">Modifica</button>
					</div>
				</div>
            </div>
        </div>
    </div>
 </div>
@if( count($item->photo) > 0 )
<div class="row">
	<div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Galleria fotografica</h4>
            	<p>Trascina le foto per cambiarne l'ordine</p>
            </div>
            <div class="panel-body">
            	<table class="table">
					<thead>
						<tr>
							<th></th>
							<th>Anteprima</th>
							<th>Nome file</th>
							<th>Titolo</th>
							<th>Data caricamento</th>
							<th>Elimina</th>
						</tr>
					</thead>
					<tbody id="listWithHandle">
						@foreach( $item->photo as $image )
						<tr>
							<td>
								<input type="hidden" name="gallery[]" value="{{ $image->id }}" />
								<button type="button" class="btn btn-xs draggable">
									<i class="fa fa-arrows-v"></i>
								</button>
							</td>
							<td>
								<a href="{{ asset('storage/' . $image->stored) }}" data-rel="prettyPhoto[gallery]">
					                <img src="{{ asset('storage/' . $image->stored) }}" class="img-responsive" alt="{{ $image->original }}" title="{{ $image->original }}" style="max-height: 40px; max-width: 60px;" />
					            </a>
							</td>
							<td>{{ $image->original }}</td>
							<td><input type="text" class="form-control" name="gallery_title[]" value="{{ $image->title }}" placeholder="Inserisci un titolo" /></td>
							<td>{{ $image->created_at->format('d/m/Y H:i') }}</td>
							<td>
								<input type="checkbox" class="gallery_delete" name="gallery_delete[]" value="{{ $image->id }}" />
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
            </div>
        </div>
    </div>
</div>
@endif
</form>

<script>
jQuery(document).ready(function(){

    "use strict";

    jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
    	social_tools: '',
    	show_title: true
    });

    $(".gallery_delete").change(function(){
          $("#file").prop('disabled', $('.gallery_delete:checked').length>0);
	})

	Sortable.create(listWithHandle, {
	  draggable: 'tr',
	  handle: '.draggable',
	  animation: 150
	});

});
</script>
@endsection

@section('css')
<style>
.draggable {
	cursor: move;
	cursor: -webkit-grabbing;
}
</style>
@endsection
