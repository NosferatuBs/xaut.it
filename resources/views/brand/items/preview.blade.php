<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
        	<h4 class="panel-title">Riepilogo articolo</h4>
        </div>
        <form class="form-horizontal">
            <div class="panel-body">
            	<div class="col-md-4 text-center">
					@if( !$item->photo->isEmpty() )
						<img src="{{ asset('storage/' . $item->photo->first()->stored) }}" class="img-resposive" style="max-height: 200px;" />
					@endif
            	</div>
            	<div class="col-md-8">
	            	<div class="form-group">
						<label class="col-sm-3 control-label">Codice</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $item->name }} </p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Barcode</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $item->barcode }} </p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Descrizione</label>
						<div class="col-sm-6">
							<p class="form-control-static">{!! nl2br($item->text) !!} </p>
						</div>
					</div>
				</div>
            </div>
        </form>
    </div>
</div>