@extends('brand.layout.app')

@section('content')
<div class="row">

    @include('brand.items.preview')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Nuovo barcode</h4>
            </div>

            <div class="panel-body">
                <form class="form-horizontal" action="/barcode/{{ $item->id }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Barcode principale</label>
                        <div class="col-sm-6">
                            <p class="form-control-static">{{ $item->barcode }}</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="barcode">Barcode <span class="asterisk">*</span></label>
                        <div class="col-sm-6">
                            <textarea class="form-control" rows="5" id="barcode" name="barcode" required>{{ old('barcode') }}</textarea>
                            <span class="help-block">Digitare un barcode per riga</span>
                        </div>
                    </div>
                    @if (count($errors) > 0)
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <button class="btn btn-primary">Aggiungi</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Elenco</h4>
            </div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Barcode</th>
                                <th>Data creazione</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($barcodes as $barcode)
                            <tr>
                                <td>{{ $barcode->barcode }}</td>
                                <td>{{ $barcode->created_at->format('d/m/Y H:i') }}</td>
                                <td class="table-action">
                                    <div class="btn-group">
                                        <button class="btn btn-xs btn-default btn-delete" type="button" data-barcode-id="{{ $barcode->id }}">Elimina</button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $barcodes->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-delete" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="deleteItem" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Confermi la cancellazione?</h4>
        </div>
        <div class="modal-footer">
            <form method="post" action="/barcode/{{ $item->id }}" name="form_barcode_delete">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <input type="hidden" name="barcode_id" value="" />

                <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">Annulla</button>
                <button class="btn btn-danger" type="submit">Elimina</button>
            </form>
        </div>
    </div>
  </div>
</div>

<script>
$(".btn-delete").on('click', function(){
    form_barcode_delete.barcode_id.value = $(this).data("barcode-id");

    $('.modal-delete').modal('show');
});

$('.modal-delete').on('hidden.bs.modal', function (e) {
    form_barcode_delete.barcode_id.value = 0;
});
</script>
@endsection
