@extends('brand.layout.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Nuovo articolo</h4>
            </div>

            <div class="panel-body">
				<form class="form-horizontal" action="{{ route('brand.items.store') }}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="col-sm-3 control-label" for="brand_id">Marchio <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<select class="form-control" name="brand_id" id="brand_id" required>
							<option value="">-- seleziona --</option>
							@foreach($brands as $brand)
								@if( old('brand_id') == $brand->id )
									<option value="{{ $brand->id }}" selected>{{ $brand->name }}</option>
								@else
									<option value="{{ $brand->id }}">{{ $brand->name }}</option>
								@endif
							@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="name">Codice <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="barcode">Barcode</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="barcode" id="barcode" value="{{ old('barcode') }}" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="text">Descrizione</label>
						<div class="col-sm-6">
							<textarea class="form-control" rows="5" name="text" id="text">{{ old('text') }}</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="url_video">URL Video</label>
						<div class="col-sm-6">
							<input type="url" class="form-control" name="url_video" id="url_video" value="{{ old('url_video') }}" />
						</div>
					</div>

					@if (count($errors) > 0)
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					@endif
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<button class="btn btn-primary">Crea</button>
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>
@endsection
