@extends('brand.layout.app')

@section('content')
<div class="row">

    @include('brand.items.preview')

	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Elenco numerazioni</h4>
			</div>

			<div class="panel-body">
				<div class="table-responsive">
					<table class="table" id="table1">
						<thead>
						<tr>
							<th>Articolo</th>
							<th>Prima lettera</th>
							<th>Seconda lettera</th>
							<th>Primo numero</th>
							<th>Ultimo numero</th>
							<th>Data creazione</th>
						</tr>
						</thead>
						<tbody>
						@foreach($numbering as $number)
							<tr>
								<td>{{ $number->name }}</td>
								<td>{{ $number->first_letter }}</td>
								<td>{{ $number->second_letter }}</td>
								<td>{{ $number->first_number }}</td>
								<td>{{ $number->last_number }}</td>
								<td>{{ $number->created_at->format('d/m/Y') }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
					{{ $numbering->links() }}
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
