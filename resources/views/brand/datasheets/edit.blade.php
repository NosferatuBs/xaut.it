@extends('brand.layout.app')

@section('content')

<div class="row">
<form id="form" class="form-horizontal" action="{{ route('brand.datasheets.update', $datasheet->id) }}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
	{{ method_field('PUT') }}

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Modifica documento</h4>
            </div>

            <div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label">Marchio</label>
					<div class="col-sm-6">
						<p class="form-control-static">{{ $datasheet->brand->name }}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="datasheet_type_id">Tipologia documento <span class="asterisk">*</span></label>
					<div class="col-sm-6">
						<select class="form-control" name="datasheet_type_id" id="datasheet_type_id" required>
							@foreach($datasheet_types as $type)
								@if($type->id == $datasheet->datasheet_type_id)
									<option value="{{ $type->id }}" selected="selected">{{ $type->name }}</option>
								@else
									<option value="{{ $type->id }}">{{ $type->name }}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="name">Descrizione <span class="asterisk">*</span></label>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="name" id="name" value="{{ $datasheet->name }}" required />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="datasheet">File</label>
					<div class="col-sm-6">
						@if(! $datasheet->stored)
							<input type="file" class="form-control mb15" name="datasheet" id="datasheet" />
							<div class="alert alert-info alert-dismissible fade in">
								<h4>Che tipologie di <strong>Documenti</strong> posso caricare?</h4>
								<p>
									<i class="fa fa-file-pdf-o"></i> Documento PDF<br/>
									<i class="fa fa-file-archive-o"></i> Archivio ZIP<br/>
								</p>
								<p>Il formato PDF è compatibile con qualsiasi dispositivo, mentre non tutti i clienti dispongono di una suite Office.</p>
								<p>Per caricare documenti di formato diverso utilizzare gli archivi.</p>
							</div>
						@else
							<a href="{{ asset('storage/' . $datasheet->stored) }}" class="btn btn-default attachments" target="_blank">Visualizza allegato</a>
							<div class="checkbox block">
								<label>
									<input type="checkbox" name="delete" id="delete" value="1" />
									Elimina
								</label>
							</div>
						@endif
					</div>
				</div>

				@if (count($errors) > 0)
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				@endif
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Associa segmenti</h4>
            	<p>Se nessun segmento è selezionato, il documento verrà visualizzato per ogni articolo del marchio</p>
            </div>

            <div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="branches">Segmenti</label>
					<div class="col-sm-6">
						<select class="select2" name="branches[]" id="branches" multiple data-placeholder="Scegli uno o più segmenti">
						@foreach($branches as $branch)
							@if($datasheet->branches->contains($branch->id))
							<option value="{{ $branch->id }}" selected>{{ $branch->name }}</option>
							@else
							<option value="{{ $branch->id }}">{{ $branch->name }}</option>
							@endif
						@endforeach
						</select>
					</div>
				</div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Opzioni documento</h4>
            </div>

            <div class="panel-body">
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="checkbox block">
							<label>
								<input type="checkbox" name="active" id="active" {{ ($datasheet->active) ? 'checked' : '' }}> Mostra documento
							</label>
						</div>
						<div class="alert alert-info alert-dismissible fade in">
							<h4><i class="fa fa-check-square-o"></i> <strong>Mostra documento</strong>!</h4>
							<p>Il documento non è ancora visibile dai tuoi clienti.</p>
							<p>Quando sei pronto attivalo qui sopra.</p>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<button class="btn btn-primary">Modifica</button>
					</div>
				</div>
			</div>
		</div>
	</div>

</form>
</div>

<div class="modal fade modal-datasheet" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="datasheet" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Attenzione!</h4>
            <p>Questo documento per essere visibile deve avere almeno un allegato</p>
        </div>
        <div class="modal-footer">
	        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">Ho capito</button>
        </div>
    </div>
  </div>
</div>

@endsection

@section('javascript')
<script>
$('document').ready(function(){
	$(".select2").select2({
		width: '100%'
	});

	$('#active').change(function(){
		if(this.checked)
		{
			if(toggleActiveCheckbox())
			{
				$('.modal-datasheet').modal('show');
			}
		}
	});

	$('#delete').change(function(){
		if(this.checked)
		{
			$('#active').prop('checked', false);
			$('.attachments').attr('disabled', true);
		}
		else
		{
			$('.attachments').attr('disabled', false);
		}
	});

	var toggleActiveCheckbox = function(){
		var count = $('.attachments').length;
		var input = $('#datasheet');
		var value = input ? input.val() : null;

		if(!count && !value)
		{
			$('#active').prop('checked', false);
		}
		return !count && !value;
	}
});
</script>
@endsection
