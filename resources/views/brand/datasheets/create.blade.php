@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading">Nuovo documento</div>

            <div class="panel-body">
				<form class="form-horizontal" action="{{ route('brand.datasheets.store') }}" method="POST">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="col-sm-3 control-label" for="brand_id">Marchio <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<select class="form-control" name="brand_id" id="brand_id" required>
							<option value="">-- seleziona --</option>
							@foreach($brands as $brand)
								@if( old('brand_id') == $brand->id )
									<option value="{{ $brand->id }}" selected>{{ $brand->name }}</option>
								@else
									<option value="{{ $brand->id }}">{{ $brand->name }}</option>
								@endif
							@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="datasheet_type_id">Tipologia documento <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<select class="form-control" name="datasheet_type_id" id="datasheet_type_id" required>
							@foreach($datasheet_types as $type)
								@if( old('datasheet_type_id') == $type->id )
									<option value="{{ $type->id }}" selected>{{ $type->name }}</option>
								@else
									<option value="{{ $type->id }}">{{ $type->name }}</option>
								@endif
							@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="name">Descrizione <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}" required />
							<span class="help-block">Visibile da amministratore e proprietario di brand.</span>
						</div>
					</div>

					@if (count($errors) > 0)
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					@endif
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<button class="btn btn-primary">Crea</button>
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>

@endsection
