@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
				<div class="table-responsive">
					<table class="table" id="table1">
						<thead>
							<tr>
								<th>Descrizione</th>
								<th>Marchio</th>
								<th>Tipologia documento</th>
								<th>Formato documento</th>
								<th>Pubblico</th>
								<th>Visibile</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($datasheets as $sheet)
							<tr>
								<td>{{ $sheet->name }}</td>
								<td>{{ $sheet->brand->name }}</td>
								<td>{{ $sheet->datasheetType->name }}</td>
								<td>{{ $sheet->mime }}</td>
								<td>{{ $sheet->branches_count }}</td>
								<td>{{ $sheet->active }}</td>
								<td>
									<div class="btn-group">
										<a class="btn btn-xs btn-default" href="{{ route('brand.datasheets.edit', $sheet->id) }}">Modifica</a>
										<button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu" role="menu">
											@if($sheet->stored)
											<li><a href="{{ asset('storage/' . $sheet->stored) }}" target="_blank">Visualizza allegato</a></li>
											<li class="divider"></li>
											@endif
											<li><a href="#" class="btn-delete" data-datasheet-id="{{ $sheet->id }}">Elimina</a></li>
										</ul>
									</div>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-delete" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="deleteItem" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Confermi la cancellazione?</h4>
        </div>
        <div class="modal-body">
        	<p>
        		Descrizione: <span id="lbl-delete-name"></span><br />
        		Marchio: <span id="lbl-delete-brand"></span><br />
        	</p>
        </div>
        <div class="modal-footer">
        	<form method="post" action="" name="form_datasheet_delete">
        		{{ csrf_field() }}
        		{{ method_field('DELETE') }}

        		<input type="hidden" name="datasheet_id" value="" />

	        	<button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">Annulla</button>
	        	<button class="btn btn-danger" type="submit">Elimina</button>
	        </form>
        </div>
    </div>
  </div>
</div>
@endsection

@section('javascript')
<script>
jQuery(document).ready(function() {
    "use strict";

    $("#table1").on('click','.btn-delete', function(){
		var array = t.row($(this).parents('tr')).data();

		document.getElementById('lbl-delete-name').innerHTML = array[0];
		document.getElementById('lbl-delete-brand').innerHTML = array[1];

		form_datasheet_delete.action = '/datasheets/' + $(this).data("datasheet-id");
		form_datasheet_delete.datasheet_id.value = $(this).data("datasheet-id");

		$('.modal-delete').modal('show');
	});

	$('.modal-delete').on('hidden.bs.modal', function (e) {
		document.getElementById('lbl-delete-name').innerHTML = "";
		document.getElementById('lbl-delete-brand').innerHTML = "";

		form_datasheet_delete.action = '';
		form_datasheet_delete.datasheet_id.value = 0;
	});
});
</script>
@endsection
