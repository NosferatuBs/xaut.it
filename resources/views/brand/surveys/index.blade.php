@extends('brand.layout.app')

@section('content')



<div class="row">
    <div class="col-md-12">
    	<div class="alert alert-info alert-dismissible fade in">
			<h4>Scopri i <strong>Sondaggi</strong>!</h4>
			<p>Quando un cliente verifica il suo acquisto, conserviamo per te l'età, la provenienza e il grado di soddisfazione del prodotto.</p>
			<p>Da oggi puoi aggiungere tutte le domande che vuoi, per tutti i prodotti o solo per alcuni!</p>
		</div>
		@if($brands_option == 0)

		<div class="alert alert-danger alert-dismissible fade in">
			<h4>I <strong>Sondaggi</strong> non sono attivi sui tuoi marchi!</h4>
			<p>Fai l'upgrade al pacchetto SONDAGGI per poter avere un feedback dai tuoi clienti.<br>Contattaci tramite email o telefonicamente.</p>
		</div>

		@elseif($brands != $brands_option)

		<div class="alert alert-danger alert-dismissible fade in">
			<h4>I <strong>Sondaggi</strong> non sono attivi su alcuni dei tuoi marchi!</h4>
			<p>Fai l'upgrade al pacchetto SONDAGGI per poter avere un feedback dai tuoi clienti.<br>Contattaci tramite email o telefonicamente.</p>
		</div>

		@endif
        <div class="panel panel-default">
            <div class="panel-body">
				<div class="table-responsive">
					<table class="table" id="table1">
						<thead>
							<tr>
								<th>Domanda</th>
								<th>Risposta</th>
								<th>Marchio</th>
								<th>Pubblico</th>
								<th>Attiva</th>
								<th>Obbligatoria</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($surveys as $survey)
							<tr>
								<td> <img src="/customer/images/{{ $survey->locale }}.png" style="padding: 0 10px;" /> {{ $survey->question }}</td>
								<td><i class="{{ $survey->surveyType->icon }}" style="padding: 0 10px;"></i>{{ $survey->surveyType->name }}</td>
								<td>{{ $survey->brand->name }}</td>
								<td>{{ $survey->branches_count }}</td>
								<td>{{ $survey->active }}</td>
								<td>{{ $survey->required }}</td>
								<td>
									<div class="btn-group">
										<a class="btn btn-xs btn-default" href="{{ route('brand.surveys.edit', $survey->id) }}">Modifica</a>
										<button class="btn btn-xs btn-default dropdown-toggle" type="button" data-toggle="dropdown">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu" role="menu">
											@if($survey->customer_answers_count > 0)
											<li><a href="/export/surveys/{{ $survey->id }}">Scarica risposte</a></li>
											<li class="divider"></li>
											@endif
											<li><a href="#" class="btn-delete" data-survey-id="{{ $survey->id }}">Elimina</a></li>
										</ul>
									</div>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div><!-- table-responsive -->
            </div>
        </div>
    </div>
</div>


<div class="modal fade modal-delete" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="deleteItem" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Confermi la cancellazione?</h4>
            <p>Perderai anche le risposte e le statistiche per questo sondaggio</p>
        </div>
        <div class="modal-body">
        	<p>
        		Domanda: <span id="lbl-delete-name"></span><br />
        		Marchio: <span id="lbl-delete-brand"></span><br />
        	</p>
        </div>
        <div class="modal-footer">
        	<form method="post" action="" name="form_survey_delete">
        		{{ csrf_field() }}
        		{{ method_field('DELETE') }}

        		<input type="hidden" name="survey_id" value="" />

	        	<button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">Annulla</button>
	        	<button class="btn btn-danger" type="submit">Elimina</button>
	        </form>
        </div>
    </div>
  </div>
</div>

<script>
jQuery(document).ready(function() {
    "use strict";

    $("#table1").on('click','.btn-delete', function(){
		var array = t.row($(this).parents('tr')).data();

		document.getElementById('lbl-delete-name').innerText = array[0];
		document.getElementById('lbl-delete-brand').innerText = array[2];

		form_survey_delete.action = '/surveys/' + $(this).data("survey-id");
		form_survey_delete.survey_id.value = $(this).data("survey-id");

		$('.modal-delete').modal('show');
	});

	$('.modal-delete').on('hidden.bs.modal', function (e) {
		document.getElementById('lbl-delete-name').innerText = "";
		document.getElementById('lbl-delete-brand').innerText = "";

		form_survey_delete.action = '';
		form_survey_delete.survey_id.value = 0;
	});
});
</script>
@endsection
