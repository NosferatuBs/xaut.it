@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
    	<div class="alert alert-info alert-dismissible fade in">
			<h4>Che tipologie di <strong>Sondaggi</strong> posso creare?</h4>
			<p>
				<i class="fa fa-align-left"></i> Risposta breve - Breve risposta aperta scritta dall'utente<br/>
				<i class="fa fa-align-justify"></i> Paragrafo - Lunga risposta aperta scritta dall'utente<br/>
				<i class="fa fa-dot-circle-o"></i> Scelta multipla - Una sola risposta da un elenco di opzioni<br/>
				<i class="fa fa-check-square-o"></i> Caselle di controllo - Più risposte da un elenco di opzioni <br/>
				<i class="fa fa-chevron-circle-down"></i> Elenco a discesa - Una sola risposta da un elenco a discesa (ideale per tante opzioni)
			</p>
			<p>Una volta scelta la tipologia, non sarà più possibile modificarla.</p>
		</div>

        <div class="panel panel-default">
            <div class="panel-heading">Nuovo sondaggio </div>

            <div class="panel-body">
				<form class="form-horizontal" action="{{ route('brand.surveys.store') }}" method="POST">
					{{ csrf_field() }}
					<div class="form-group">
						<label class="col-sm-3 control-label" for="brand_id">Marchio <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<select class="form-control" name="brand_id" id="brand_id" required>
							<option value="">-- seleziona --</option>
							@foreach($brands as $brand)
								@if( old('brand_id') == $brand->id )
									<option value="{{ $brand->id }}" selected>{{ $brand->name }}</option>
								@else
									<option value="{{ $brand->id }}">{{ $brand->name }}</option>
								@endif
							@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="question">Domanda <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<input type="text" class="form-control" name="question" id="question" value="{{ old('question') }}" required />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="survey_type">Tipo di domanda <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<select class="select2" name="survey_type" id="survey_type" data-placeholder="Scegli la tipologia" required="">
								<option></option>
							@foreach( $survey_types as $survey_type)
								@if( old('survey_type') == $survey_type->id)
								<option data-icon="{{ $survey_type->icon }}" value="{{ $survey_type->id }}" selected>{{ $survey_type->name }}</option>
								@else
								<option data-icon="{{ $survey_type->icon }}" value="{{ $survey_type->id }}">{{ $survey_type->name }}</option>
								@endif
							@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="locale">Lingua <span class="asterisk">*</span></label>
						<div class="col-sm-6">
							<select class="select2" name="locale" id="locale" data-placeholder="Scegli la lingua" required>
								<option></option>
							@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                @if( old('locale') == $localeCode)
                                <option data-flag="{{ $localeCode }}" value="{{ $localeCode }}" selected>{{ $properties['native'] }}</option>
                                @else
                                <option data-flag="{{ $localeCode }}" value="{{ $localeCode }}">{{ $properties['native'] }}</option>
                                @endif
                            @endforeach
							</select>
							<span class="help-block">La domanda verrà mostrata solo agli utenti di questa lingua</span>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="checkbox block">
								<label>
									<input type="checkbox" name="required" id="required" checked> Domanda obbligatoria
								</label>
							</div>
						</div>
					</div>

					@if (count($errors) > 0)
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="alert alert-danger">
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						</div>
					</div>
					@endif
					<div class="form-group">
						<div class="col-sm-6 col-sm-offset-3">
							<button class="btn btn-primary">Crea</button>
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script>
function format (answer) {
	var originalOption = answer.element;

	if($(originalOption).data('icon'))
		return '<span><i class="' + $(originalOption).data('icon') + '" style="padding: 0 10px;"></i> ' + answer.text + '</span>';
	else if($(originalOption).data('flag'))
		return '<span><img src="/customer/images/' + $(originalOption).data('flag') + '.png" style="padding: 0 10px;" /> ' + answer.text + '</span>';
	else
		return;
};

$('document').ready(function(){
	$(".select2").select2({
		width: '100%',
		formatResult: format,
	    formatSelection: format,
	    escapeMarkup: function(m) { return m; }
	});
});
</script>
@endsection
