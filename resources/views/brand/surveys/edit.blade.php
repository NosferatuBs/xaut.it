@extends('brand.layout.app')

@section('content')

<div class="row">
<form id="form" class="form-horizontal" action="{{ route('brand.surveys.update', ['survey' => $survey->id]) }}" method="POST">
	{{ csrf_field() }}
	{{ method_field('PUT') }}
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Modifica sondaggio</h4>
            </div>

            <div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label">Marchio</label>
					<div class="col-sm-6">
						<p class="form-control-static">{{ $survey->brand->name }}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="question">Domanda <span class="asterisk">*</span></label>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="question" id="question" value="{{ $survey->question }}" required />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="help">Testo della guida</label>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="help" id="help" value="{{ $survey->help }}" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Tipo di domanda</label>
					<div class="col-sm-6">
						<p class="form-control-static"><i class="{{ $survey->surveyType->icon }}" style="padding: 0 10px;"></i> {{ $survey->surveyType->name }}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Lingua</label>
					<div class="col-sm-6">
						<p class="form-control-static">
						@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
							@if($survey->locale == $localeCode)
								<img src="/customer/images/{{ $survey->locale }}.png" style="padding: 0 10px;" />
								{{ $properties['native'] }}
								@break
							@endif
						@endforeach
						</p>
					</div>
				</div>

				@if (count($errors) > 0)
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				@endif
            </div>
        </div>
    </div>

    @if( $survey->surveyType->has_answer )

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Elenco risposte</h4>
            	<p>Trascina le risposte per cambiarne l'ordine</p>
            </div>

            <div class="panel-body">
            	<div id="listWithHandle" class="list-group">
            	@forelse($survey->answers->sortBy('order') as $answer)
					<div class="form-group item-list">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="input-group">
								<span class="input-group-addon draggable"><i class="fa fa-arrows-v"></i></span>
								<input type="hidden" name="answer_id[]" value="{{ $answer->id }}">
								<input type="text" class="form-control answer-input" name="answer_name[]" value="{{ $answer->name }}" autocomplete="off" />
								<span class="input-group-addon hand-cursor"><i class="fa fa-trash"></i></span>
							</div>
						</div>
					</div>
				@empty
					<div class="form-group item-list">
						<div class="col-sm-6 col-sm-offset-3">
							<div class="input-group">
								<span class="input-group-addon draggable"><i class="fa fa-arrows-v"></i></span>
								<input type="hidden" name="answer_id[]" value="0">
								<input type="text" class="form-control answer-input" name="answer_name[]" value="" autocomplete="off" />
								<span class="input-group-addon hand-cursor"><i class="fa fa-trash"></i></span>
							</div>
						</div>
					</div>
				@endforelse
				</div>
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<button type="button" class="btn btn-primary" id="btnNewAnswer">Aggiungi risposta</button>
					</div>
				</div>
            </div>
        </div>
    </div>

    @endif

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Associa segmenti</h4>
            	<p>Se nessun segmento è selezionato, il sondaggio verrà visualizzato per ogni articolo del marchio</p>
                @if($survey->brand->survey_branch_option != 1)
                    <div class="alert alert-danger alert-dismissible fade in">
                        Questa opzione non è attiva. Il sondaggio sarà per tutti i segmenti.
                    </div>
                @endif
            </div>

            <div class="panel-body">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="branches">Segmenti</label>
					<div class="col-sm-6">
						<select class="select2" name="branches[]" id="branches" multiple data-placeholder="Scegli uno o più segmenti">
						@foreach($branches as $branch)
							@if($survey->branches->contains($branch->id))
							<option value="{{ $branch->id }}" selected>{{ $branch->name }}</option>
							@else
							<option value="{{ $branch->id }}">{{ $branch->name }}</option>
							@endif
						@endforeach
						</select>
					</div>
				</div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Associa articoli</h4>
                <p>Se nessun articolo è selezionato, il sondaggio verrà visualizzato per ogni articolo del marchio</p>
                @if($survey->brand->survey_item_option != 1)
                    <div class="alert alert-danger alert-dismissible fade in">
                        Questa opzione non è attiva. Il sondaggio sarà per tutti gli articoli.
                    </div>
                @endif
            </div>

            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="items">Articoli</label>
                    <div class="col-sm-6">
                        <select class="select2" name="items[]" id="items" multiple data-placeholder="Scegli uno o più segmenti">
                        @foreach($items as $item)
                            @if($survey->items->contains($item->id))
                            <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                            @else
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endif
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Opzioni sondaggio</h4>
            </div>

            <div class="panel-body">
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="checkbox block">
							<label>
								<input type="checkbox" name="required" id="required" {{ ($survey->required) ? 'checked' : '' }}> Domanda obbligatoria
							</label>
						</div>
					</div>
					<div class="col-sm-6 col-sm-offset-3">
						<div class="checkbox block">
							<label>
								<input type="checkbox" name="active" id="active" {{ ($survey->active) ? 'checked' : '' }}> Attiva la domanda
							</label>
						</div>
						<div class="alert alert-info alert-dismissible fade in">
							<h4><i class="fa fa-check-square-o"></i> <strong>Attiva domanda</strong>!</h4>
							<p>La domanda non è ancora visibile dai tuoi clienti.</p>
							<p>Quando sei pronto attivalo qui sopra.</p>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-6 col-sm-offset-3">
						<button class="btn btn-primary">Modifica</button>
					</div>
				</div>
			</div>
		</div>
	</div>

</form>
</div>

@if( $survey->surveyType->has_answer )
<div class="modal fade modal-answers" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="deleteItem" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Attenzione!</h4>
            <p>Questo sondaggio per essere attivo deve avere almeno una risposta</p>
        </div>
        <div class="modal-footer">
	        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default" type="button">Ho capito</button>
        </div>
    </div>
  </div>
</div>
@endif

@endsection

@section('javascript')
<script>
$('document').ready(function(){
	$(".select2").select2({
		width: '100%'
	});

	@if( $survey->surveyType->has_answer )

	Sortable.create(listWithHandle, {
	  handle: '.draggable',
	  animation: 150
	});

	$('#listWithHandle').on('click', '.hand-cursor', function(){
		$(this).parents('.item-list').remove();

		toggleActiveCheckbox();
	});

	$('#btnNewAnswer').click(function(){
		var answer_element = $('<div>', {'class': 'form-group item-list'})
			.append($('<div>', {'class': 'col-sm-6 col-sm-offset-3'})
				.append($('<div>', {'class': 'input-group'})
					.append($('<span>', {'class': 'input-group-addon draggable'})
						.append($('<i>', {'class': 'fa fa-arrows-v'}))
					)
					.append($('<input>', {'type': 'hidden', 'name': 'answer_id[]', 'value': '0'}))
					.append($('<input>', {'type': 'text', 'class': 'form-control answer-input', 'name': 'answer_name[]'}).attr('autocomplete','off'))
					.append($('<span>', {'class': 'input-group-addon hand-cursor'})
						.append($('<i>', {'class': 'fa fa-trash'}))
					)
				)
			);

		$('#listWithHandle').append(answer_element);

		$('#listWithHandle').find('.item-list:last-child .answer-input').focus();
	})

	$('#active').change(function(){
		if(this.checked)
		{
			if(toggleActiveCheckbox())
			{
				$('.modal-answers').modal('show');
			}
		}
	});

	var toggleActiveCheckbox = function(){
		var count = $('.answer-input').filter(function(){ return this.value !== '';}).length;
		if(!count)
		{
			$('#active').prop('checked', false);
		}
		return !count;
	}
	@endif
});
</script>
@endsection

@section('css')
<style>
.draggable {
  cursor: move;
  cursor: -webkit-grabbing;
}
.hand-cursor {
  cursor: pointer;
  cursor: -webkit-pointer;
}
</style>
@endsection
