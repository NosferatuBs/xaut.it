@extends('brand.layout.app')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4 class="panel-title">Parametri di ricerca</h4>
			</div>
			<div class="panel-body">
				<form class="form-inline" action="{{ route('brand.customers.index') }}" method="GET">
					<div class="form-group">
						<label for="search">Cerca</label>
						<input class="form-control form-control-sm" name="search" />
					</div>
					<div class="form-group">
						<label for="gdpr_newsletter_consent">Consenso newsletter</label>
						<select class="form-control form-control-sm" name="gdpr_newsletter_consent">
							<option value="0">---</option>
							<option value="2" {{ $form->gdpr_newsletter_consent == 2 ? 'selected' : '' }}>Sì</option>
							<option value="1" {{ $form->gdpr_newsletter_consent == 1 ? 'selected' : '' }}>No</option>
						</select>
					</div>
					<div class="form-group">
						<label for="gdpr_profilation_consent">Consenso profilazione</label>
						<select class="form-control form-control-sm" name="gdpr_profilation_consent">
							<option value="0">---</option>
							<option value="2" {{ $form->gdpr_profilation_consent == 2 ? 'selected' : '' }}>Sì</option>
							<option value="1" {{ $form->gdpr_profilation_consent == 1 ? 'selected' : '' }}>No</option>
						</select>
					</div>
					<button type="submit" class="btn btn-primary">Aggiorna</button>
				</form>
			</div>
		</div>
	</div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
				<div class="table-responsive">
					<table class="table" id="table1">
						<thead>
							<tr>
								<th>Nome</th>
								<th>Email</th>
								<th>Compleanno</th>
								<th>Genere</th>
								<th>Consenso newsletter</th>
								<th>Consenso profilazione</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						@foreach ($customers as $customer)
							<tr>
								<td>{{ $customer->name }}</td>
								<td>{{ $customer->email }}</td>
								<td>{{ $customer->birthday }}</td>
								<td>
									@if($customer->gender == "male")
										<span>Uomo</span>
									@else
										<span>Donna</span>
									@endif
								</td>
								<td>
									@if($customer->gdpr_newsletter_consent)
										<i class="fa-solid fa-check"></i>
									@endif
								</td>
								<td>
									@if($customer->gdpr_profilation_consent)
										<i class="fa-solid fa-check"></i>
									@endif
								</td>
								<td>
									<div class="btn-group">
										<a class="btn btn-xs btn-default" href="{{ route('brand.customers.show', $customer->id) }}">Dettagli</a>
									</div>
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
            </div>
            <div class="panel-footer">
             	<a href="/export/customers" class="btn btn-primary">Scarica l'elenco clienti in Excel</a>
            </div>
        </div>
    </div>
</div>
@endsection
