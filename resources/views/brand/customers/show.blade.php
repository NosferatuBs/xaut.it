@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<h4 class="panel-title">Dettagli utente</h4>
            </div>

            <div class="panel-body">
				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-3 control-label">Nome</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $customer->name }}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Email</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $customer->email }}</p>
						</div>
					</div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Paese</label>
                        <div class="col-sm-6">
                            <p class="form-control-static">{{ $customer->city }}</p>
                        </div>
                    </div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Sesso</label>
						<div class="col-sm-6">
							<p class="form-control-static">
								@if($customer->gender == 'male')
								Uomo
								@elseif($customer->gender == 'female')
								Donna
								@else
								Altro
								@endif
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Data di nascita</label>
						<div class="col-sm-6">
							<p class="form-control-static">{{ $customer->birthday }}</p>
						</div>
					</div>
				</form>
            </div>
        </div>
    </div>
</div>

@endsection
