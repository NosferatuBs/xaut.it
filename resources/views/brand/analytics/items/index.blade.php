@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Parametri di ricerca</h4>
            </div>
            <div class="panel-body">
                <form class="form-inline" action="{{ route('brand.analytics.items.overview') }}" method="GET">
                    <div class="form-group">
                        <label for="year">Marchio</label>
                        <select class="form-control" name="brandId" id="brandId">
                            @foreach($brands as $brand)
                                <option value="{{ $brand->id }}" {{ $form->brandId == $brand->id ? 'selected' : '' }}>{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="year">Anno di riferimento</label>
                        <select class="form-control" name="year" id="year">
                            @foreach($years as $year)
                                <option value="{{ $year }}" {{ $form->year != null ? 'selected' : '' }}>{{ $year }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Aggiorna</button>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Grafico riepilogativo</h4>
            </div>

            <div class="panel-body">
                <div id="piechart_stelle" style="width: 100%; height: 300px"></div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Elenco articoli</h4>
            </div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table" id="table1">
                        <thead>
                            <tr>
                                <th>Barcode</th>
                                <th>Codice</th>
                                <th>Marchio</th>
                                <th>Votaz.ne Media</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($list as $t)
                            <tr>
                                <td>{{ $t->barcode }}</td>
                                <td>{{ $t->name }}</td>
                                <td>{{ $t->brand->name }}</td>
                                <td>
                                    @if( $t->avg )
                                        @php
                                        $avg = round($t->avg * 2) / 2;

                                        for($star = 1; $star <= 5; $star++)
                                            {
                                            if($star <= $avg)
                                                print "<i class=\"fa fa-star\"></i>";
                                            elseif(($star - 0.5) == $avg)
                                                print "<i class=\"fa fa-star-half-o\"></i>";
                                            else
                                                print "<i class=\"fa fa-star-o\"></i>";

                                            }
                                        @endphp
                                    @else
                                        -
                                    @endif
                                </td>
                                <td class="table-action">
                                    @if( $t->avg )
                                    <div class="btn-group">
                                        <a class="btn btn-xs btn-default" href="{{ route('brand.analytics.items.show', $t->id) }}">Dettagli</a>
                                    </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="/js/flot/jquery.flot.min.js"></script>
<script src="/js/flot/jquery.flot.resize.min.js"></script>
<script src="/js/flot/jquery.flot.symbol.min.js"></script>
<script src="/js/flot/jquery.flot.crosshair.min.js"></script>
<script src="/js/flot/jquery.flot.categories.min.js"></script>
<script src="/js/flot/jquery.flot.pie.min.js"></script>
<script src="/js/morris.min.js"></script>
<script src="/js/raphael-2.1.0.min.js"></script>
<script src='//www.gstatic.com/charts/loader.js'></script>

<script>
var piedata3 = [
    { label: labelStars(5), data: [{{ $valutazione->five }}], color: '#1CAF9A'},
    { label: labelStars(4), data: [{{ $valutazione->four }}], color: '#428BCA'},
    { label: labelStars(3), data: [{{ $valutazione->three }}], color: '#5BC0DE'},
    { label: labelStars(2), data: [{{ $valutazione->two }}], color: '#F0AD4E'},
    { label: labelStars(1), data: [{{ $valutazione->one }}], color: '#D9534F'},
];

jQuery.plot('#piechart_stelle', piedata3, {
    series: {
        pie: {
            show: true,
            radius: 1,
            label: {
                show: true,
                radius: 2/3,
                formatter: labelFormatter,
                threshold: 0.1
            }
        }
    },
    grid: {
        hoverable: true,
        clickable: true
    }
});

function labelStars(stars) {
    t = '';
    for(var i = 0; i < stars; i++)
        t += '<i class=\'fa fa-star\'></i>';
    return t;
}
function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}
</script>
@endsection
