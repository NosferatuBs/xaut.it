@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Parametri di ricerca</h4>
            </div>
            <div class="panel-body">
                <form class="form-inline" action="{{ route('brand.analytics.customers.overview') }}" method="GET">
                    <div class="form-group">
                        <label for="year">Marchio</label>
                        <select class="form-control" name="brandId" id="brandId">
                        @foreach($brands as $brand)
                            <option value="{{ $brand->id }}" {{ $form->brandId == $brand->id ? 'selected' : '' }}>{{ $brand->name }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="year">Anno di riferimento</label>
                        <select class="form-control" name="year" id="year">
                            @foreach($years as $year)
                                <option value="{{ $year }}" {{ $form->year != null ? 'selected' : '' }}>{{ $year }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Aggiorna</button>
                </form>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Sesso del cliente</h4>
            </div>

            <div class="panel-body">
                <div id="piechart_sesso" style="width: 100%; height: 300px"></div>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Età media del cliente</h4>
            </div>

            <div class="panel-body">
                <div id="barchart" style="width: 100%; height: 300px"></div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Località di acquisto</h4>
            </div>
            <div class="panel-body">
                <form class="form-inline">
                    <label>
                        Filtra per area geografica:
                    </label>
                    <select class="form-control" id="geochart_area" name="geochart_area">
                        <option value="">-- continente --</option>
                    </select>
                    <select class="form-control" id="geochart_region" name="geochart_region">
                        <option value="">-- sub continente --</option>
                    </select>
                    <select class="form-control" id="geochart_countries" name="geochart_countries">
                        <option value="">-- nazione --</option>
                    </select>
                </form>
                <br>
                <div id="chart_div_geo" style="width: 100%; height: 600px; border:1px #CDCDCD solid;"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script src="/js/flot/jquery.flot.min.js"></script>
<script src="/js/flot/jquery.flot.resize.min.js"></script>
<script src="/js/flot/jquery.flot.symbol.min.js"></script>
<script src="/js/flot/jquery.flot.crosshair.min.js"></script>
<script src="/js/flot/jquery.flot.categories.min.js"></script>
<script src="/js/flot/jquery.flot.pie.min.js"></script>
<script src="/js/morris.min.js"></script>
<script src="/js/raphael-2.1.0.min.js"></script>
<script src='//www.gstatic.com/charts/loader.js'></script>

<script>
var piedata2 = [
    { label: "Maschio", data: [{{ $sesso->male }}], color: '#428BCA'},
    { label: "Femmina", data: [{{ $sesso->female }}], color: '#FF69B4'},
    { label: "Altro", data: [{{ $sesso->other }}], color: '#32B848'},
];

jQuery.plot('#piechart_sesso', piedata2, {
    series: {
        pie: {
            show: true,
            radius: 1,
            label: {
                show: true,
                radius: 2/3,
                formatter: labelFormatter,
                threshold: 0.1
            }
        }
    },
    grid: {
        hoverable: true,
        clickable: true
    }
});

function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}

var bardata = [
    ["0-17", {{ round( ($eta->get('0-17') / $eta->count() ) * 100, 0) }}],
    ["18-27", {{ round( ($eta->get('18-27') / $eta->count() ) * 100, 0) }}],
    ["28-37", {{ round( ($eta->get('28-37') / $eta->count() ) * 100, 0) }}],
    ["38-47", {{ round( ($eta->get('38-47') / $eta->count() ) * 100, 0) }}],
    ["48-57", {{ round( ($eta->get('48-57') / $eta->count() ) * 100, 0) }}],
    ["58-67", {{ round( ($eta->get('58-67') / $eta->count() ) * 100, 0) }}],
    ["68+", {{ round( ($eta->get('68+') / $eta->count() ) * 100, 0) }}]
];

jQuery.plot("#barchart", [ bardata ], {
    series: {
        lines: {
            lineWidth: 1
        },
        bars: {
            show: true,
            barWidth: 0.5,
            align: "center",
            lineWidth: 0,
            fillColor: "#428BCA"
        }
    },
    grid: {
        borderColor: '#ddd',
        borderWidth: 1,
        labelMargin: 10
    },
    xaxis: {
        mode: "categories",
        tickLength: 0
    },
    yaxis: {
        tickDecimals: 0,
        max: 100,
        min: 0,
        tickFormatter: function(val) {
            return val + '%';
        }
    }
});

var data = {!! json_encode($geochart) !!};
</script>

@include('scripts.geochart')

@endsection
