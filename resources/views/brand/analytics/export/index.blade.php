@extends('brand.layout.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Parametri di ricerca</h4>
                </div>
                <div class="panel-body">
                    <form class="form-inline" action="{{ route('brand.export.analytics.pdf') }}" method="GET">
                        <div class="form-group">
                            <label for="year">Marchio</label>
                            <select class="form-control" name="brandId" id="brandId">
                                @foreach($brands as $brand)
                                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="from">Data inizio</label>
                            <input type="date" class="form-control" name="from" id="from" value="{{ $from }}" />
                        </div>
                        <div class="form-group">
                            <label for="from">Data fine</label>
                            <input type="date" class="form-control" name="to" id="to" value="{{ $to }}" />
                        </div>
                        <button type="submit" class="btn btn-primary">Genera report PDF</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection