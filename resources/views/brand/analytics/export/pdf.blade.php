<html>
    <head>
        <style>
            body { font-family: 'Helvetica'; }
            #logo { text-align: center;  max-width: 75px; margin: auto; display: block; }
            header { text-align: center; }
            img.chart { margin: auto; display: block; }
            h4 { text-align: center; }
            td { vertical-align: top; text-align: center; }
            #items { width: 700px; border-collapse: collapse; }
            #items td, #items th { border-collapse: collapse; border: thin #aaa solid; }
            #items thead { backgrond: #eee; }
            #surveys { width: 700px; border-collapse: collapse; }
            #surveys td, #surveys th { border-collapse: collapse; border: thin #aaa solid; vertical-align: middle; }
            #surveys thead { backgrond: #eee; }
            .pagebreak { page-break-before: always; }
        </style>
    </head>
<body>
<header>
    <img id="logo" src="data:image/png;base64, {{ $brandLogo }}" />
    <h2>{{ $brandName }}</h2>

    <hr>

    <table style="width: 700px">
        <tr>
            <td style="vertical-align: top; text-align: center;">
                <h4>Sesso</h4>
                <img class="chart" width="400" src="data:image/png;base64, {{ $gendersChart }}" />
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; text-align: center;">
                <h4>Età</h4>
                <img class="chart" width="400" src="data:image/png;base64, {{ $agesChart }}" />
            </td>
        </tr>
    </table>


    <div class="pagebreak"> </div>

    <table style="width: 700px">
        <tr>
            <td>
                <h4>Geolocalizzazione</h4>
                <img width="700" height="700" src="data:image/png;base64, {{ $geolocationChart }}" />
            </td>
        </tr>
    </table>

    <table id="items">
        <thead>
        <tr>
            <th>Città</th>
            <th>Latitudine</th>
            <th>Longitudine</th>
            <th>Valore</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($geolocationData as $el)
            <tr>
                <td>{{ $el->city }}</td>
                <td>{{ $el->latitude }}</td>
                <td>{{ $el->longitude }}</td>
                <td>{{ $el->frequency }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="pagebreak"> </div>

    <table style="width: 700px">
        <tr>
            <td>
                <h4>Valutazione prodotti</h4>
                <img class="chart" style="width: 600px;" src="data:image/png;base64, {{ $ratingsChart }}" />
            </td>
        </tr>
    </table>

    <table id="items">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Valutazione media</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($items as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->avg }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="pagebreak"> </div>

    <h4>Sondaggi</h4>

    <table id="surveys" style="width: 700px">
        @foreach($surveyCharts as $survey)
            <tr>
                <td>{{ $survey->question }}</td>
                <td>
                    <img class="chart" style="width: 200px;" src="data:image/png;base64, {{ $survey->chart }}" />
                </td>
            </tr>
        @endforeach
    </table>
</header>
</body>
</html>



