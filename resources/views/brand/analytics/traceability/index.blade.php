@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Parametri di ricerca</h4>
            </div>
            <div class="panel-body">
                <form class="form-inline" action="{{ route('brand.analytics.traceability.overview') }}" method="GET">
                    <div class="form-group">
                        <label for="year">Marchio</label>
                        <select class="form-control" name="brandId" id="brandId">
                            @foreach($brands as $brand)
                                <option value="{{ $brand->id }}" {{ $form->brandId == $brand->id ? 'selected' : '' }}>{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="year">Anno di riferimento</label>
                        <select class="form-control" name="year" id="year">
                            @foreach($years as $year)
                                <option value="{{ $year }}" {{ $form->year != null ? 'selected' : '' }}>{{ $year }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Aggiorna</button>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Grafico riepilogativo</h4>
            </div>

            <div class="panel-body">
                <div id="barchart" style="width: 100%; height: 300px"></div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Storico ricerche</h4>
            </div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table" id="table1">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Brand</th>
                                <th>Barcode</th>
                                <th>Articolo</th>
                                <th>Valutazione</th>
                                <th>Data</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($list as $t)
                            <tr>
                                <td>{{ $t->user->email }}</td>
                                <td>{{ $t->brand->name }}</td>
                                <td>{{ $t->code }}</td>
                                <td>{{ $t->name }}</td>
                                <td>{{ $t->rate }}/5</td>
                                <td>{{ $t->created_at->format('d/m/Y H:i:s') }}</td>
                            </tr>
                        @endforeach
                      </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script src="/js/flot/jquery.flot.min.js"></script>
<script src="/js/flot/jquery.flot.resize.min.js"></script>
<script src="/js/flot/jquery.flot.symbol.min.js"></script>
<script src="/js/flot/jquery.flot.crosshair.min.js"></script>
<script src="/js/flot/jquery.flot.categories.min.js"></script>
<script src="/js/flot/jquery.flot.pie.min.js"></script>
<script src="/js/morris.min.js"></script>
<script src="/js/raphael-2.1.0.min.js"></script>
<script>
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-euro-pre": function ( a ) {
        var x;

        if ( $.trim(a) !== '' ) {
            var frDatea = $.trim(a).split(' ');
            var frTimea = (undefined != frDatea[1]) ? frDatea[1].split(':') : [00,00,00];
            var frDatea2 = frDatea[0].split('/');
            x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;
        }
        else {
            x = Infinity;
        }

        return x;
    },

    "date-euro-asc": function ( a, b ) {
        return a - b;
    },

    "date-euro-desc": function ( a, b ) {
        return b - a;
    }
} );

jQuery(document).ready(function() {
    "use strict";

    jQuery('#table1').dataTable({
        "order": [[ 6, "desc" ]],
        columnDefs: [
            { type: 'date-euro', targets: 6 }
        ]
    });
});

var months = ["Gen","Feb","Mar","Apr","Mag","Giu","Lug","Ago","Set","Ott","Nov","Dic"];
var bardata = [
    @foreach($graph as $key => $value)
    [ months[{{ $key }}], {{ $value }} ],
    @endforeach
];

jQuery.plot("#barchart", [ bardata ], {
    series: {
        lines: {
            lineWidth: 1
        },
        bars: {
            show: true,
            barWidth: 0.5,
            align: "center",
            lineWidth: 0,
            fillColor: "#428BCA"
        }
    },
    grid: {
        borderColor: '#ddd',
        borderWidth: 1,
        labelMargin: 10
    },
    xaxis: {
        mode: "categories",
        tickLength: 0
    },
    yaxis: {
        tickDecimals: 0
    }
});
</script>
@endsection
