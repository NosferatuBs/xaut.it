@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Parametri di ricerca</h4>
            </div>
            <div class="panel-body">
                <form class="form-inline" action="{{ route('brand.analytics.surveys.overview') }}" method="GET">
                    <div class="form-group">
                        <label for="year">Marchio</label>
                        <select class="form-control" name="brandId" id="brandId">
                            @foreach($brands as $brand)
                                <option value="{{ $brand->id }}" {{ $form->brandId == $brand->id ? 'selected' : '' }}>{{ $brand->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="year">Anno di riferimento</label>
                        <select class="form-control" name="year" id="year">
                            @foreach($years as $year)
                                <option value="{{ $year }}" {{ $form->year != null ? 'selected' : '' }}>{{ $year }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Aggiorna</button>
                </form>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table" id="table1">
                        <thead>
                            <tr>
                                <th>Domanda</th>
                                <th>Risposta</th>
                                <th>Marchio</th>
                                <th>Pubblico</th>
                                <th>Risposte</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($surveys as $survey)
                            <tr>
                                <td>{{ $survey->question }}</td>
                                <td><i class="{{ $survey->surveyType->icon }}" style="padding: 0 10px;"></i>{{ $survey->surveyType->name }}</td>
                                <td>{{ $survey->brand->name }}</td>
                                <td>{{ $survey->branches_count }}</td>
                                <td>{{ $survey->customer_answers_count }}</td>
                                <td class="table-action">
                                    <div class="btn-group">
                                        <a class="btn btn-xs btn-default" href="{{ route('brand.analytics.surveys.show', $survey->id) }}">Dettagli</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div>
        </div>
    </div>
</div>

<script>
jQuery(document).ready(function() {
    "use strict";

    var t = jQuery('#table1').DataTable({
        columnDefs: [
            {
                "targets": 3,
                "render": function ( data, type, full, meta ) {
                    if(data == 0)
                        return "Marchio";
                    else if(data == 1)
                        return data + " Segmento";
                    else
                        return data + " Segmenti";
                }
            },
            {
                "orderable": false, "targets": 5
            }
        ],
        order: [
            [0,"asc"]
        ]
    });
});
</script>

@endsection
