@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">Parametri di ricerca</h4>
            </div>
            <div class="panel-body">
                <form class="form-inline" action="{{ route('brand.analytics.surveys.show', $survey->id) }}" method="GET">
                    <div class="form-group">
                        <label for="year">Anno di riferimento</label>
                        <select class="form-control" name="year" id="year">
                        @foreach($years as $key => $val)
                            <option value="{{ $val['year'] }}" {{ ( $val['selected'] ) ? 'selected' : '' }}>{{ $val['year'] }}</option>
                        @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Aggiorna</button>
                </form>
            </div>
        </div>
    </div>

    @if($survey->surveyType->has_answer && array_sum($frequenza) > 0)

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">{{ $survey->question }}</h4>
                <p>Grafico del sondaggio</p>
            </div>
            <div class="panel-body">
                <div id="piechart_stelle" style="width: 100%; height: 300px"></div>
            </div>
        </div>
    </div>
    @endif

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">{{ $survey->question }}</h4>
                <p>Elenco risposte</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table" id="table1">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Sesso</th>
                                <th>Compleanno</th>
                                <th>Articolo</th>
                                <th>Risposta</th>
                                <th>Data</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($survey->customerAnswers as $answer)
                            <tr>
                                <td>{{ $answer->traceability->user->name }}</td>
                                <td>{{ $answer->traceability->user->email }}</td>
                                <td>{{ $answer->traceability->user->gender }}</td>
                                <td>{{ $answer->traceability->user->birthday }}</td>
                                <td>{{ ($answer->traceability->item_id) ? $answer->traceability->item->name : '-' }}</td>
                                <td>{{ $answer->answer }}</td>
                                <td>{{ $answer->created_at->format('d/m/Y H:i:s') }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- table-responsive -->
            </div>
            <div class="panel-footer">
                <a href="/export/surveys/{{ $survey->id }}" class="btn btn-primary">Scarica le risposte in Excel</a>
            </div>
        </div>
    </div>
</div>

<script>
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "date-euro-pre": function ( a ) {
        var x;

        if ( $.trim(a) !== '' ) {
            var frDatea = $.trim(a).split(' ');
            var frTimea = (undefined != frDatea[1]) ? frDatea[1].split(':') : [00,00,00];
            var frDatea2 = frDatea[0].split('/');
            x = (frDatea2[2] + frDatea2[1] + frDatea2[0] + frTimea[0] + frTimea[1] + frTimea[2]) * 1;
        }
        else {
            x = Infinity;
        }

        return x;
    },

    "date-euro-asc": function ( a, b ) {
        return a - b;
    },

    "date-euro-desc": function ( a, b ) {
        return b - a;
    }
} );

jQuery(document).ready(function() {
    "use strict";

    var t = jQuery('#table1').DataTable({
        columnDefs: [
            {
                "targets": 2,
                "render": function ( data, type, full, meta ) {
                    if(data == 'male')
                        return 'Uomo';
                    else if(data == 'female')
                        return 'Donna';
                    else
                        return 'Altro';
                }
            },
            {
                "targets": 5,
                "render": function ( data, type, full, meta ) {
                    return type === 'display' && data.length > 40 ? '<span title="' + data.replace(/"/g, '&quot;') + '">'+data.substr( 0, 38 )+'...</span>' : data;
                }
            },
            {
                type: 'date-euro', targets: 6
            }
        ],
        order: [
            [0,"asc"]
        ]
    });
});
</script>

@endsection

@if($survey->surveyType->has_answer && array_sum($frequenza) > 0)
@section('javascript')
<script src="/js/flot/jquery.flot.min.js"></script>
<script src="/js/flot/jquery.flot.resize.min.js"></script>
<script src="/js/flot/jquery.flot.symbol.min.js"></script>
<script src="/js/flot/jquery.flot.crosshair.min.js"></script>
<script src="/js/flot/jquery.flot.categories.min.js"></script>
<script src="/js/flot/jquery.flot.pie.min.js"></script>
<script src="/js/morris.min.js"></script>
<script src="/js/raphael-2.1.0.min.js"></script>
<script src='//www.gstatic.com/charts/loader.js'></script>
<script>

    var piedata3 = [
        @foreach($frequenza as $key => $value)
            { label: {!! json_encode($key) !!}, data: [{{ $value }}] },
        @endforeach
    ];

    jQuery.plot('#piechart_stelle', piedata3, {
        series: {
            pie: {
                show: true,
                radius: 1,
                label: {
                    show: true,
                    radius: 2/3,
                    formatter: labelFormatter,
                    threshold: 0.1
                }
            }
        },
        grid: {
            hoverable: true,
            clickable: true
        }
    });
    function labelFormatter(label, series) {
        return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
    }
</script>
@endsection
@endif
