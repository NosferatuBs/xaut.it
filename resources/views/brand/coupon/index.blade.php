@extends('brand.layout.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
				<div class="table-responsive">
                    <form action="/brand/coupon/{{ $flag }}" method="post" id="modulo">
                        <input type="hidden" name="activation_id" id="activation_id" value="0">
                        {{ csrf_field() }}
    					<table class="table" id="table1" width="100%">
    						<thead>
    							<tr>
                                    <th>Marchio</th>
    								<th>Coupon</th>
    								<th>Sconto</th>
    								<th>P. IVA</th>
    								<th>Data registrazione</th>
                                    <th>Data saldato</th>
                                    <th>&nbsp;</th>
    							</tr>
    						</thead>
    						<tbody>
                            @foreach($activations as $activation)
                            <tr>
                                <td>{{ $activation->name }}<br>({{ $activation->barcode }})</td>
                                <td>{{ $activation->token }}</td>
                                <td>{{ $activation->amount }}.00 &euro;</td>
                                <td>{{ $activation->vat }}</td>
                                <td>{{ $activation->created_at->format('d/m/Y H:i') }}</td>
                                <td>{{ $activation->checked_at }}</td>
                                <td><button class="btn btn-xs btn-default" type="button" onclick="document.getElementById('activation_id').value='{{ $activation->id }}';document.getElementById('modulo').submit();">
                            @if($flag == 1)
                                Togli saldato
                            @else
                                Salda ora
                            @endif
                            </button></td>
                            </tr>
                            @endforeach
                            </tbody>
    					</table>
                    {{ $activations->links() }}
				</div>
            </div>
        </div>
    </div>
</div>

@endsection
