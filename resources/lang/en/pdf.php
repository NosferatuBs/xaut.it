<?php

return [
	'warranty.title' => 'Warranty Certificate',
	'warranty.original' => 'Original product',
	'warranty.purchased' => 'Purchased on',
	'warranty.expired' => 'Expiring on',
];