<?php

return [
	"come-funziona" => "how-it-works",
	"contatti" => "contact-us",
	"condizioni" => "conditions",
	"accedi" => "login",
	"registrati" => "register",
	"cookie" => "cookie"
];