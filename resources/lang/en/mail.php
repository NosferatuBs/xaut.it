<?php

return [
	'template.title.error' => 'Your attention, please!',
	'template.title.success' => 'Hello!',
	'template.thanks' => 'Thank you and best regards,',
	'template.helpaction' => 'If you have trouble clicking the button ":actionLabel", copy and paste the address here below in your browser url bar:',

	'resetLink.line.1' => 'You are receiving this email because we received a password reset request for your account.',
	'resetLink.line.2' => 'If you did not request a password reset, no further action is required.',
	'resetLink.action' => 'Reset Password',
];