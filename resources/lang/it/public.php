<?php

return [
	'modalWarranty.title' => 'Attiva la garanzia al tuo prodotto.',
	'modalWarranty.description' => 'Per poter attivare la garanzia devi accedere al software',
	'Warranty.download' => 'Scarica la garanzia in PDF.',

];