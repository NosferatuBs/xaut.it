<?php

return [
	'template.title.error' => 'Attenzione!',
	'template.title.success' => 'Ciao!',
	'template.thanks' => 'Grazie e cordiali saluti,',
	'template.helpaction' => 'Se hai problemi cliccando il tasto ":actionLabel",copia e incolla l\'indirizzo qui sotto nel tuo browser:',

	'resetLink.line.1' => 'Ricevi questa email perché abbiamo ricevuto una richiesta di ripristino password per il tuo account.',
	'resetLink.line.2' => 'Se non hai richiesto tu il ripristino della password, allora ignora questa email.',
	'resetLink.action' => 'Ripristina Password',

	'registrationProduct.subject' => 'Registrazione prodotto',
	'registrationProduct.title' => 'Il tuo prodotto è stato registrato in modo corretto!!!',
	'registrationProduct.text' => 'Testo da inserire',

	'couponRegistration.subject' => 'Ecco il tuo coupon di sconto',
	'couponRegistration.title' => 'Il tuo coupon di sconto è pronto!!!',
	'couponRegistration.text' => 'Stampa il coupon allegato a questa email e portalo con te in negozio per ottenere lo sconto sul tuo nuovo acquisto del marchio indicato.',

	'greetings' => 'Grazie,',
];
