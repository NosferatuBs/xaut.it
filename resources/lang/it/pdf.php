<?php

return [
	'warranty.title' => 'Certificato di Garanzia',
	'warranty.original' => 'Prodotto originale',
	'warranty.purchased' => 'Acquistato il giorno',
	'warranty.expired' => 'Garanzia valida fino al',
];