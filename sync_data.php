<?php

$pdoIlProject = new PDO(
    "mysql:host=localhost;dbname=ilproject;charset=utf8mb4",
    "albireo_cloud",
    "DianuskA.90",
    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
);

$pdoXAuth = new PDO(
    "mysql:host=localhost;dbname=xauth;charset=utf8mb4",
    "albireo_cloud",
    "DianuskA.90",
    array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
);

$pdoXAuth->beginTransaction();

// sync headquarters
$query = "SELECT * FROM case_madre ";
$headquartersStmt = $pdoIlProject->query($query);
while ($row = $headquartersStmt->fetch())
{
    $query = "SELECT * FROM headquarters WHERE ilproject_id = ? ";
    $stmt = $pdoXAuth->prepare($query);
    $stmt->execute([$row["id_casa_madre"]]);
    if ($stmt->rowCount() == 0)
    {
        $query = "INSERT INTO headquarters(ilproject_id, name, piva, codice_fiscale) VALUES(?, ?, ?, ?)";
        $stmt = $pdoXAuth->prepare($query);
        $stmt->execute([
            $row["id_casa_madre"],
            $row["ragione_sociale"],
            $row["p_iva"],
            $row["codice_fiscale"]
        ]);
    }
}

// Sync users
$query = "SELECT * FROM case_madre_utenti ";
$headquarterUsersStmt = $pdoIlProject->query($query);
while ($row = $headquarterUsersStmt->fetch())
{
    $query = "SELECT * FROM users WHERE ilproject_id = ? ";
    $stmt = $pdoXAuth->prepare($query);
    $stmt->execute([$row["id_utente"]]);
    if ($stmt->rowCount() == 0)
    {
        $query = "INSERT INTO users(ilproject_id, name, email, password) VALUES(?, ?, ?, ?)";
        $stmt = $pdoXAuth->prepare($query);
        $stmt->execute([
            $row["id_utente"],
            $row["nome"],
            $row["email"],
            $row["password"]
        ]);
    }
}

$pdoXAuth->commit();