<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create([
            "name" => "Riccardo Galeazzi",
            "email" => "glr93@hotmail.it",
            "password" => bcrypt("1234")
        ]);

        factory(\App\User::class)->create([
            "name" => "Alessandro Cadei",
            "email" => "casertabene@gmail.com",
            "password" => bcrypt("1234")
        ]);
    }
}
