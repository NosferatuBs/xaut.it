<?php

use Illuminate\Database\Seeder;

class UsersWorkgroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (App\User::all() as $user) {
        	factory(\App\UserWorkgroup::class)->create([
        		"user_id" => $user->id,
			    "workgroup_id" => App\Workgroup::where('name','admin')->first()->id
			]);
        }
    }
}
