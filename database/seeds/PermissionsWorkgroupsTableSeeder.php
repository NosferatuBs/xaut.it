<?php

use Illuminate\Database\Seeder;

class PermissionsWorkgroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (App\Workgroup::all() as $workgroup) {
        	factory(\App\PermissionWorkgroup::class)->create([
        		"permission_id" => App\Permission::first()->id,
			    "workgroup_id" => $workgroup->id
			]);
        }
    }
}
