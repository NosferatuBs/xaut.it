<?php

use Illuminate\Database\Seeder;

class WorkgroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Workgroup::class)->create([
            "name" => "admin",
        ]);
        
        factory(\App\Workgroup::class)->create([
            "name" => "brandowner",
        ]);
        
       factory(\App\Workgroup::class)->create([
            "name" => "customer",
        ]);
    }
}
