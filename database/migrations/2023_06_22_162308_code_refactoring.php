<?php

use App\Entities\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CodeRefactoring extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brand_numbers', function (Blueprint $table) {
            $table->dropColumn('code');
            $table->string("first_letter", 1);
            $table->string("second_letter", 1);
            $table->unsignedBigInteger("first_number");
            $table->unsignedBigInteger("last_number");
            $table->unsignedBigInteger("ilproject_id");
        });

        Schema::table('traceability', function (Blueprint $table) {
            $table->dropColumn('token');
        });

        if (!Schema::hasColumn('items', 'ilproject_id'))
        {
            Schema::table('items', function (Blueprint $table) {
                $table->unsignedBigInteger("ilproject_id");
            });
        }

        \DB::statement("DELETE FROM brand_numbers");
        \DB::statement("DELETE FROM traceability");
        \DB::statement("DELETE FROM discounts");
        \DB::statement("DELETE FROM barcode_item");
        \DB::statement("DELETE FROM branch_item");
        \DB::statement("DELETE FROM brand_user");
        \DB::statement("DELETE FROM branches");
        \DB::statement("DELETE FROM item_photo");
        \DB::statement("DELETE FROM shop_brands");
        \DB::statement("DELETE FROM shop_user");
        \DB::statement("DELETE FROM shops");
        \DB::statement("DELETE FROM traceability");
        \DB::statement("DELETE FROM brand_photo");
        \DB::statement("DELETE FROM customer_answers");
        \DB::statement("DELETE FROM survey_answers");
        \DB::statement("DELETE FROM surveys");
        \DB::statement("DELETE FROM items");
        \DB::statement("DELETE FROM brands");
        \DB::statement("DELETE FROM headquarters");
        \DB::statement("DELETE FROM permission_workgroup");
        \DB::statement("DELETE FROM user_workgroup");
        \DB::statement("DELETE FROM workgroups");
        \DB::statement("DELETE FROM users");

        $password = Hash::make('Akaaka@13');
        $users = [
                "email" => "info@brandoasi.com",
                "password" => $password,
                "is_superadmin" => 1,
                "name" => "Antonio",
                "gender" => "male"
            ];

        User::insert($users);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
