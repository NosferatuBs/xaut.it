<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatasheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datasheets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('datasheet_type_id');
            $table->boolean('active')->default(false);
            $table->string('name');
            $table->string('original')->nullable();
            $table->string('stored')->nullable();
            $table->string('mime')->nullable();
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('datasheet_type_id')->references('id')->on('datasheet_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('datasheets', function (Blueprint $table) {
            $table->dropForeign(['brand_id']);
            $table->dropForeign(['datasheet_type_id']);
        });
        Schema::dropIfExists('datasheets');
    }
}
