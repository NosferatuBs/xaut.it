<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchitemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_item', function (Blueprint $table) {
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('item_id');
            $table->timestamps();

            $table->primary(['branch_id','item_id']);
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branch_item', function (Blueprint $table) {
            $table->dropForeign(['branch_id']);
            $table->dropForeign(['item_id']);
        });
        Schema::dropIfExists('branch_item');
    }
}
