<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_survey', function (Blueprint $table) {
            $table->unsignedInteger('branch_id');
            $table->unsignedInteger('survey_id');
            $table->timestamps();

            $table->primary(['branch_id','survey_id']);
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('survey_id')->references('id')->on('surveys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('branch_survey', function (Blueprint $table) {
            $table->dropForeign(['branch_id']);
            $table->dropForeign(['survey_id']);
        });
        Schema::dropIfExists('branch_survey');
    }
}
