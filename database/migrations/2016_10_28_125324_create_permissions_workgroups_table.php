<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsWorkgroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_workgroup', function (Blueprint $table) {
            $table->unsignedInteger('workgroup_id');
            $table->unsignedInteger('permission_id');
            $table->timestamps();

            $table->primary(['workgroup_id','permission_id']);
            $table->foreign('workgroup_id')->references('id')->on('workgroups');
            $table->foreign('permission_id')->references('id')->on('permissions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permission_workgroup', function (Blueprint $table) {
            $table->dropForeign(['workgroup_id']);
            $table->dropForeign(['permission_id']);
        });
        Schema::dropIfExists('permission_workgroup');
    }
}
