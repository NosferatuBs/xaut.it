<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('item_id')->nullable();
            $table->char('first_char',2)->nullable();
            $table->char('second_char',2)->nullable();
            $table->unsignedInteger('start');
            $table->unsignedInteger('end')->nullable();
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('item_id')->references('id')->on('items');

            $table->index(['first_char', 'second_char']);
            $table->index(['start', 'end']);
            $table->unique(['first_char', 'second_char', 'start']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brand_numbers', function (Blueprint $table) {
            $table->dropForeign(['brand_id']);
            $table->dropForeign(['item_id']);
        });
        Schema::dropIfExists('brand_numbers');
    }
}
