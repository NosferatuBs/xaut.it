<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('traceability', function (Blueprint $table) {
            $table->unsignedInteger('city_id');
            $table->softDeletes();

            $table->foreign('city_id')->references('id')->on('cities');
        });

        Schema::table('item_photo', function (Blueprint $table) {
            $table->string('title')->nullable();
        });

        Schema::table('items', function (Blueprint $table) {
            $table->softDeletes();

            $table->unique('barcode');
            $table->unique(['brand_id','name']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('city_id')->nullable();
            $table->dropColumn('town');

            $table->foreign('city_id')->references('id')->on('cities');
        });

        Schema::table('brands', function (Blueprint $table) {
            $table->string('name_public');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
