<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraceabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traceability', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('item_id');
            $table->char('chars',2)->nullable();
            $table->unsignedInteger('digits')->nullable();
            $table->boolean('gift');
            $table->tinyInteger('rate');
            $table->string('token');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('traceability', function (Blueprint $table) {
            $table->dropForeign('user_id');
            $table->dropForeign('item_id');
        });
        Schema::dropIfExists('traceability');
    }
}
