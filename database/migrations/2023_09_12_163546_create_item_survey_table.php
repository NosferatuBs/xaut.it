<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemSurveyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_survey', function (Blueprint $table) {
            $table->unsignedInteger('item_id');
            $table->unsignedInteger('survey_id');
            $table->timestamps();

            $table->primary(['item_id','survey_id']);
            $table->foreign('item_id')->references('id')->on('itemes');
            $table->foreign('survey_id')->references('id')->on('surveys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_survey', function (Blueprint $table) {
            $table->dropForeign(['item_id']);
            $table->dropForeign(['survey_id']);
        });
        Schema::dropIfExists('item_survey');
    }
}
