<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('survey_id');
            $table->unsignedInteger('traceability_id');
            $table->string('answer');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('survey_id')->references('id')->on('surveys');
            $table->foreign('traceability_id')->references('id')->on('traceability');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_answers', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['survey_id']);
            $table->dropForeign(['traceability_id']);
        });
        Schema::dropIfExists('customer_answers');
    }
}
