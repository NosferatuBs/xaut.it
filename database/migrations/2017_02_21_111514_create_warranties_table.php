<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarrantiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warranties', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('traceability_id');
            $table->string('seller')->nullable();
            $table->decimal('amount')->nullable();
            $table->date('bought_at')->nullable();
            $table->string('original');
            $table->string('stored');
            $table->string('mime');
            $table->timestamps();

            $table->foreign('traceability_id')->references('id')->on('traceability');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('warranties', function (Blueprint $table) {
            $table->dropForeign(['traceability_id']);
        });
        Schema::dropIfExists('warranties');
    }
}
