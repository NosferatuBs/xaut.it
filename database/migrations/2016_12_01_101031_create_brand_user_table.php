<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_user', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('brand_id');
            $table->timestamps();

            $table->primary(['brand_id','user_id']);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brand_user', function (Blueprint $table) {
            $table->dropForeign(['brand_id']);
            $table->dropForeign(['user_id']);
        });
        Schema::dropIfExists('brand_user');
    }
}
