<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemSequentialNumbers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_sequential_numbers', function (Blueprint $table) {
            $table->unsignedInteger('item_id');
            $table->char('code', 2);
            $table->unsignedInteger('start');
            $table->unsignedInteger('end');
            $table->timestamps();

            $table->primary(['code', 'item_id', 'start']);
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_sequential_numbers', function (Blueprint $table) {
            $table->dropForeign(['item_id']);
        });
        Schema::dropIfExists('item_sequential_numbers');
    }
}
