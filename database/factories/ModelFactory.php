<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Permission::class, function(Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(App\Workgroup::class, function(Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(App\PermissionWorkgroup::class, function (Faker\Generator $faker) {
    return [
        'permission_id' => function () {
            return factory(App\Permission::class)->create()->id;
        },
        'workgroup_id' => function () {
            return factory(App\Workgroup::class)->create()->id;
        }
    ];
});

$factory->define(App\UserWorkgroup::class, function (Faker\Generator $faker) {
    return [
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
        'workgroup_id' => function () {
            return factory(App\Workgroup::class)->create()->id;
        }
    ];
});