<?php

Route::get('dashboard', 'HomeController@dashboard')
    ->name('dashboard');

Route::resource('brands', 'BrandController', ['except' => 'show']);

Route::get('brands/{brand}/importDataFromIlProject', 'BrandController@importDataFromIlProject');
Route::get('import/brands', 'ImportController@importBrands');
Route::get('import/numbers/{brandId}', 'ImportController@importNumbers');

Route::resource('users', 'UserController', ['except' => 'show']);

Route::get('users/{user}/brands/edit', 'UserBrandController@edit')
    ->name('users.brands.edit');
Route::post('users/{user}/brands', 'UserBrandController@update')
    ->name('users.brands.update');

Route::get('users/{user}/shops/edit', 'UserShopController@edit')
    ->name('users.shops.edit');
Route::post('users/{user}/shops', 'UserShopController@update')
    ->name('users.shops.update');

Route::group([
    'prefix' => 'account',
    'as' => 'account.',
    'namespace' => 'Account'
    ], function() {

    Route::get('profile/edit', 'ProfileController@edit')
        ->name('profile.edit');
    Route::post('profile', 'ProfileController@update')
        ->name('profile.update');

    Route::get('password/edit', 'PasswordController@edit')
        ->name('password.edit');
    Route::post('password', 'PasswordController@update')
        ->name('password.update');
});

// Route::get('headquarters', 'HeadquarterController@all');
// Route::post('headquarters', 'HeadquarterController@insert');
// Route::delete('headquarters', 'HeadquarterController@destroy');
// Route::get('headquarters/create', 'HeadquarterController@create');
// Route::get('headquarters/{headquarter}', 'HeadquarterController@edit')->middleware('can:update,headquarter');
// Route::post('headquarters/{headquarter}', 'HeadquarterController@update')->middleware('can:update,headquarter');

Route::get('brands/{brand}/numbers', 'NumbersController@brandEdit');
Route::post('brands/{brand}/numbers', 'NumbersController@brandUpdate');
Route::delete('brands/{brand}/numbers', 'NumbersController@brandDelete');

Route::post('items/{item}/numbers', 'NumbersController@itemUpdate');
Route::delete('items/{item}/numbers', 'NumbersController@itemDelete');

Route::get('import/items', 'ItemImportController@itemSelect')
    ->name('import.item.insert');
Route::post('import/items', 'ItemImportController@itemLoad')
    ->name('import.item.load');

Route::post('json/settings', function(Request $request){
    if($request->has('fullscreen'))
    {
        $request->session()->put('fullscreen', !! $request->fullscreen);

        return response('Ok',200);
    }

    if($request->has('alert'))
    {
        $notification = Notification::where(['user_id' => $request->user()->id, 'message' => $request->alert])->first();

        if(is_null($notification))
        {
            $notification = new Notification;
        }

        $notification->user_id = $request->user()->id;
        $notification->message = $request->alert;
        $notification->read = true;

        $notification->save();

        return response('Ok',200);
    }

    return response('Bad request',400);
});

Route::post('sync/putUsers', 'IlProjectSyncController@putUsers')
    ->name('sync.putUsers');