<?php

Route::get('mail', function(){

    $t = \App\Entities\Traceability::first();
    return new \App\Mail\ProductRegistration\AttendingConfirmation($t);
});
