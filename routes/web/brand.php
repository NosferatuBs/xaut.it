<?php

Route::get('dashboard', 'HomeController@dashboard')
    ->name('dashboard');

Route::resource('items', 'ItemController', ['except' => 'show']);
Route::resource('brands', 'BrandController', ['except' => 'show']);
Route::resource('branches', 'BranchController', ['except' => ['show']]);
Route::resource('surveys', 'SurveyController', ['except' => ['show']]);
Route::resource('datasheets', 'DatasheetController', ['except' => ['show']]);
Route::resource('customers', 'CustomerController', ['only' => ['index', 'show']]);

Route::get('warranties', 'WarrantyController@index')
    ->name('warranties.index');
Route::get('warranties/{warranty}', 'WarrantyController@show')
    ->name('warranties.show');

Route::get('export/customers', 'BrandCustomerController@excel');
Route::get('export/surveys/{survey}', 'ExportSurveyController@excel');
Route::get('export/analytics', 'BrandController@export')->name('export.analytics.index');
Route::get('export/analytics/pdf', 'BrandController@pdf')->name('export.analytics.pdf');

Route::group([
    'prefix' => 'account',
    'as' => 'account.',
    'namespace' => 'Account'
    ], function() {

    Route::get('profile/edit', 'ProfileController@edit')
        ->name('profile.edit');
    Route::post('profile', 'ProfileController@update')
        ->name('profile.update');

    Route::get('password/edit', 'PasswordController@edit')
        ->name('password.edit');
    Route::post('password', 'PasswordController@update')
        ->name('password.update');
});

Route::group([
    'prefix' => 'brands',
    'as' => 'brands.'
    ], function() {

    Route::get('{brand}/numbering/edit', 'BrandNumberingController@edit')
        ->name('numbering.edit');
    Route::put('{brand}/numbering', 'BrandNumberingController@update')
        ->name('numbering.update');

    Route::get('{brand}/coupon/edit', 'BrandCouponController@edit')
        ->name('coupon.edit');
    Route::put('{brand}/coupon', 'BrandCouponController@update')
        ->name('coupon.update');

    Route::get('{brand}/coupon/test', 'BrandCouponController@test')
        ->name('coupon.test');
});

Route::group([
    'prefix' => 'items',
    'as' => 'items.'
    ], function() {

    Route::get('{item}/numbering/edit', 'ItemNumberingController@edit')
        ->name('numbering.edit');
    Route::put('{item}/numbering', 'ItemNumberingController@update')
        ->name('numbering.update');

    Route::get('{item}/barcodes/edit', 'ItemBarcodeController@edit')
        ->name('barcodes.edit');
    Route::put('{item}/barcodes', 'ItemBarcodeController@update')
        ->name('barcodes.update');
});

Route::group([
    'prefix' => 'analytics',
    'as' => 'analytics.',
    'namespace' => 'Analytics'
    ], function() {

    Route::get('customers/overview', 'CustomerController@overview')
        ->name('customers.overview');

    Route::get('traceability/overview', 'TraceabilityController@overview')
        ->name('traceability.overview');

    Route::get('items/overview', 'ItemController@overview')
        ->name('items.overview');
    Route::get('items/{item}', 'ItemController@show')
        // ->middleware('can:show,item')
        ->name('items.show');

    Route::get('surveys/overview', 'SurveyController@overview')
        ->name('surveys.overview');
    Route::get('surveys/{survey}', 'SurveyController@show')
        ->name('surveys.show');
});

Route::group([
    'prefix' => 'coupon',
    'as' => 'coupon.'
    ], function() {

    Route::get('/{flag}', 'CouponController@index')
        ->name('list');

    Route::post('/{flag}', 'CouponController@process');
});

Route::post('/json/settings', function(Request $request){
    if($request->has('fullscreen'))
    {
        $request->session()->put('fullscreen', !! $request->fullscreen);

        return response('Ok',200);
    }

    return response('Bad request',400);
});
