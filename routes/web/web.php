<?php

use App\Notification;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::group(['prefix' => LaravelLocalization::setLocale(),
	'middleware' => ['localize', 'localeSessionRedirect', 'localizationRedirect' ]
], function() {
	Route::get('/', 'Web\IndexController@index')
		->name('home');
	Route::get(LaravelLocalization::transRoute('routes.come-funziona'), 'Web\IndexController@comeFunziona')
		->name('comeFunziona');
	Route::get(LaravelLocalization::transRoute('routes.contatti'), 'Web\IndexController@contatti')
		->name('contatti');
	Route::post(LaravelLocalization::transRoute('routes.contatti'), 'Web\IndexController@contattiMail');
	Route::get(LaravelLocalization::transRoute('routes.condizioni'), 'Web\IndexController@privacy');
	Route::get('cookie', 'IndexController@cookie');
});

Route::get('login', 'Auth\LoginController@showLoginForm')
	->name('login');
Route::post('login', 'Auth\LoginController@login');

Route::post('logout', 'Auth\LoginController@logout')
	->name('logout');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')
	->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')
	->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')
	->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')
	->name('password.reset.post');

Route::get('qr/{token}', 'Web\Tracking\Create\QrCodeController@redirect')
->where('token', '[A-Za-z\d]+')
->name('qrcode');


Route::group([
	'prefix' => 'tracking',
	'as' => 'tracking.'
	], function() {

	Route::get('create/qr-code/{brandId}/{token}', 'Web\Tracking\Create\QrCodeController@show')
		->where('token', '[A-Za-z\d]+')
		->name('qrcode');

	Route::post('create/qr-code/{brandId}/{token}', 'Web\Tracking\Create\QrCodeController@process')
		->where('token', '[A-Za-z\d]+');

	Route::get('create/survey/{brandId}/{token}', 'Web\Tracking\Create\SurveyController@show')
		->where('token', '[A-Za-z\d]+')
		->name('survey');

	Route::post('create/survey/{brandId}/{token}', 'Web\Tracking\Create\SurveyController@process')
		->where('token', '[A-Za-z\d]+');

	Route::get('create/warranty/{token}', 'Web\Tracking\Create\WarrantyController@show')
		->where('token', '[A-Za-z\d]+')
		->name('warranty');

	Route::post('create/warranty/{token}', 'Web\Tracking\Create\WarrantyController@process')
		->where('token', '[A-Za-z\d]+');

	Route::get('show/{token}', 'Web\Tracking\ResultController@show')
		->where('token', '[A-Za-z\d]+')
		->name('show');

    Route::get('warranty/{token}', 'Web\Tracking\Create\WarrantyController@pdf')
        ->where('token', '[A-Za-z\d]+')
        ->name('showWarranty');
});

Route::group([
	'prefix' => 'validate',
	'as' => 'validate.'
	], function() {

	Route::get('coupon/qr-code/{token}', 'Web\Validate\QrCodeController@show')
		->where('token', '[A-Za-z\d]+')
		->name('coupon.qrcode');

	Route::post('coupon/qr-code/{token}', 'Web\Validate\QrCodeController@process')
		->where('token', '[A-Za-z\d]+')
		->name('coupon.qrcodeProcess');
});

// Route::get('/active-warranties', 'PublicWarrantyController@index');
// Route::delete('/active-warranties', 'PublicWarrantyController@destroy');
// Route::get('/active-warranties/activate/{token}', 'PublicWarrantyController@create');
// Route::post('/active-warranties/activate/{token}', 'PublicWarrantyController@insert');
// Route::get('/active-warranties/{warranty}', 'PublicWarrantyController@show');

// Route::post('/warranty/register', 'PublicWarrantyController@register');

// Route::get('/export/warranties/certificate/{token}', 'ExportWarrantyController@pdf');

Route::get('/export/discount/{token}', 'Web\ExportDiscountController@pdf');
	
	Route::get('/test', function (Request $request) {
		\App\Http\Repositories\CouponRepository::registerCouponOnShopify(85);
	
	});