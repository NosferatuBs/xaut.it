<?php
	namespace App\Http\Controllers;
	
	use App\Entities\User;
	use Illuminate\Http\Request;
	
	class ImpersonateController extends Controller
	{
		public function impersonate(Request $request, User $user)
		{
			if ($request->user()->canImpersonate())
			{
				if ($user->canBeImpersonated())
				{
					auth()->user()->impersonate($user);
					return redirect(route('brand.dashboard'));
				}
			}
			return back();
		}
		
		public function leaveImpersonation()
		{
			auth()->user()->leaveImpersonation();
			return redirect(route('admin.dashboard'));
		}
	}