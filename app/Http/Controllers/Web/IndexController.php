<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Mail\ContactUsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class IndexController extends Controller
{
    public function index()
    {
    	$menu = new \stdClass();
		$menu->area = "home";

		return view('web.welcome')
            ->with('menu', $menu);
    }

    public function comeFunziona()
    {
    	$menu = new \stdClass();
	    $menu->area = "how-it-works";

		return view('web.how-it-works')
            ->with('menu', $menu);
    }

    public function contatti()
    {
    	$menu = new \stdClass();
		$menu->area = "contact-us";

		return view('web.contact-us')
            ->with('menu', $menu);
    }

    public function contattiMail(Request $request)
    {
    	$fields = [
    		'name' => 'required',
	        'email' => 'required|email',
	        'subject' => 'required',
	        'message' => 'required'
    	];

    	$this->validate($request, $fields);

		Mail::send(new ContactUsRequest($request));

		return redirect()->route('contatti')->with('status', 'success');
    }

    public function cookie()
    {
		$menu = new \stdClass();
		$menu->area = "cookie";

		return view('web.cookie')
            ->with('menu', $menu);
    }

    public function privacy()
    {
		$menu = new \stdClass();
		$menu->area = "privacy";

		return view('web.privacy')
            ->with('menu', $menu);
    }
}
