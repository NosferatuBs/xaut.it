<?php

namespace App\Http\Controllers\Web\Validate;

use App\Entities\Discount;
use App\Entities\DiscountActivation;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Cookie\CookieJar;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class QrCodeController extends Controller
{
    public function show(Request $request, $token, CookieJar $cookieJar)
    {

        try {
        $discount = Discount::where('token', $token)
            ->where('blocked_at', null)
            ->firstOrFail();
        } catch (ModelNotFoundException $ex) {
            return view('web.validate.error');
        }

        $vat = $request->cookie('vat');

        return view('web.validate.coupon')
            ->with([
                'token' => $token,
                'vat' => $vat
            ]);
    }

    public function process(Request $request, $token, CookieJar $cookieJar)
    {

        $discount = Discount::where('token', $token)
            ->where('blocked_at', null)
            ->firstOrFail();

        $fields = [
            'vat' => 'required',
            'privacy'=> 'accepted'
        ];

        $this->validate($request, $fields);

        $cookieJar->queue(cookie('vat', $request->vat, 45000));

        $discount->blocked_at = Carbon::now()->toDateTimeString();
        $discount->save();

        $activation = new DiscountActivation();
        $activation->vat = $request->vat;
        $activation->discount_id = $discount->id;
        $activation->save();

        return view('web.validate.ok');
    }

}
