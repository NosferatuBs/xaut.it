<?php

namespace App\Http\Controllers\Web;

use App\Entities\Brand;
use App\Entities\Discount;
use App\Entities\Traceability;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ExportDiscountController extends Controller
{

    public function pdf(Request $request, $token)
    {
        $discount = Discount::select('users.email', 'traceability.brand_id', 'brands.name', 'discounts.token', 'discounts.amount', 'discounts.created_at', 'brand_coupons.template')
            ->leftJoin('traceability','traceability_id','traceability.id')
            ->leftJoin('brands','traceability.brand_id','brands.id')
            ->leftJoin('brand_coupons','brands.id','brand_coupons.brand_id')
            ->leftJoin('users','user_id','users.id')
            ->where('traceability.token', $token)
            ->first();

		$email = $discount->email;
        $amount = $discount->amount;
		$isPercentage = $discount->is_percentage;
        $url = route('validate.coupon.qrcode', $discount->token);
        $discount_token = $discount->token;
        $expired_at = $discount->created_at->addYear(2);
        $template = Storage::path($discount->template);

        $pdf = \App\Libraries\CouponGenerator::make($email, $amount, $isPercentage, $discount->brand_id, $url, $discount_token, $expired_at, $template);

        $pdf->Output('I', 'coupon-xaut.pdf');
    }
}
