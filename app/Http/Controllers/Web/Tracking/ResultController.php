<?php

namespace App\Http\Controllers\Web\Tracking;

use App\Entities\Traceability;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResultController extends Controller
{
    public function show(Request $request, $token)
    {
        $traccia = Traceability::with(['brand.photo' => function($query) {
                $query->orderBy('position');
            },'item.photo' => function($query) { $query->orderBy('position'); },'warranty'])
            ->where(['token' => $token])
            ->firstOrFail();

        if($traccia->item_id && $traccia->item->url_video && preg_match('/.+(youtube).+/', $traccia->item->url_video) > 0)
        {
            preg_match('/(?<=v=)[a-z\d\-\_]+/i', $traccia->item->url_video, $url_thumbnail);
            $traccia->item->url_thumbnail = "http://i.ytimg.com/vi/$url_thumbnail[0]/hqdefault.jpg";
        }
        elseif($traccia->item_id && $traccia->item->url_video && preg_match('/.+(vimeo).+/', $traccia->item->url_video) > 0)
        {
            $url_json = file_get_contents("https://vimeo.com/api/oembed.json?url=".$traccia->item->url_video);
            $json = json_decode($url_json);
            preg_match('/(?<=video\/)[\d]+/', $json->thumbnail_url, $url_thumbnail);
            $traccia->item->url_thumbnail = "https://i.vimeocdn.com/video/$url_thumbnail[0]_1920.jpg";
        }
        elseif($traccia->brand->url_video && preg_match('/.+(youtube).+/', $traccia->brand->url_video) > 0)
        {
            preg_match('/(?<=v=)[a-z\d\-\_]+/i', $traccia->brand->url_video, $url_thumbnail);
            $traccia->brand->url_thumbnail = "http://i.ytimg.com/vi/$url_thumbnail[0]/hqdefault.jpg";
        }
        elseif($traccia->brand->url_video && preg_match('/.+(vimeo).+/', $traccia->brand->url_video) > 0)
        {
            $url_json = file_get_contents("https://vimeo.com/api/oembed.json?url=".$traccia->brand->url_video);
            $json = json_decode($url_json);
            preg_match('/(?<=video\/)[\d]+/', $json->thumbnail_url, $url_thumbnail);
            $traccia->brand->url_thumbnail = "https://i.vimeocdn.com/video/$url_thumbnail[0]_1920.jpg";
        }

        $item_cover = null;
        if($traccia->item_id && !$traccia->item->photo->isEmpty() )
        {
            $item_cover = $traccia->item->photo->shift();
        }

        // // Recupero le schede tecniche del marchio e dei segmenti
        // $datasheets = Datasheet::whereDoesntHave('branches')
        //     ->where(['brand_id' => $traccia->brand_id, 'active' => '1'])->get();

        // if($traccia->item_id)
        // {
        //     $branch_datasheet = Datasheet::select('datasheets.*')
        //         ->join('branch_datasheet','datasheets.id','branch_datasheet.datasheet_id')
        //         ->join('branches','branches.id','branch_datasheet.branch_id')
        //         ->join('branch_item','branches.id','branch_item.branch_id')
        //         ->join('items','items.id','branch_item.item_id')
        //         ->where('items.id',$traccia->item_id)
        //         ->where('datasheets.active','1')
        //         ->get();

        //     $datasheets = $datasheets->merge($branch_datasheet);

        //     $datasheets->load('datasheetType');
        // }

        $discount = $request->session()->get('discount');

        if($discount > 0)
            $discount = "DI $discount €";
        else
            $discount = "";

        return view("web.tracking.item")
            ->with([
                'traccia' => $traccia,
                'item_cover' => $item_cover,
                'discount' => $discount
            ]);
    }

}
