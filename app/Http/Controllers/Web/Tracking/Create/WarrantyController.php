<?php

namespace App\Http\Controllers\Web\Tracking\Create;

use App\Entities\BrandNumber;
use App\Entities\Item;
use App\Entities\Warranty;
use App\Http\Controllers\Controller;
use App\Libraries\SurveyGenerator;
use App\Traits\SaveUserTraceability;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WarrantyController extends Controller
{
    use SaveUserTraceability;

    public function show(Request $request, $token)
    {
        if(! $request->session()->has('tracking') && ! $request->session()->has('tracking.rate'))
        {
            abort(403);
        }

        $qrcode = BrandNumber::where('code', $token)
            ->whereNotNull('brand_id')
            ->with('brand.photo')
            ->firstOrFail();

        $session = $request->session()->get('tracking');

        return view('web.tracking.warranty')
            ->with([
                'previous' => route('tracking.survey', $token),
                'token' => $token,
                'qrcode' => $qrcode
            ]);
    }

    public function process(Request $request, $token)
    {
        if(! $request->session()->has('tracking') && ! $request->session()->has('tracking.rate'))
        {
            abort(403);
        }

        $qrcode = BrandNumber::where('code', $token)
            ->whereNotNull('brand_id')
            ->with('brand')
            ->firstOrFail();

        $fields = [
            'shop' => 'required',
            'bought_day' => 'required',
            'bought_month' => 'required',
            'bought_year' => 'required',
            'invoice' => 'file',
        ];

        $this->validate($request, $fields);

        $session = $request->session()->get('tracking');

        $item_id = $session['item_id'];
        $item = Item::find($item_id);
        $locale = \App::getLocale();

        $surveys = new SurveyGenerator($session['brand_id'], is_null($item) ? null : $item->id, $locale);

        $traccia = $this->saveTraceability($surveys, $session);

        $bought_full = Carbon::create($request->bought_year, $request->bought_month, $request->bought_day, 0, 0, 0);
        $expired_full = Carbon::create($request->bought_year, $request->bought_month, $request->bought_day, 0, 0, 0);

        $warranty = new Warranty;
        $warranty->seller = $request->shop;
        $warranty->bought_at = $bought_full;
        $expired_at = $expired_full;
        $warranty->expired_at = $expired_at->addMonth($qrcode->brand->warranty_extension + 24);

        $now = now();
        $today_directory = sprintf('year_%d.month_%d.day_%d',
            $now->format('Y'),
            $now->format('m'),
            $now->format('d')
        );
        $today_directory = str_replace(".", DIRECTORY_SEPARATOR, $today_directory);

        $path = $request->file('invoice')->store($today_directory);
        $warranty->invoice = $path;

        $traccia->warranty()->save($warranty);

        $request->session()->forget('tracking');

        event(new \App\Events\NewProductRegistration($traccia));

        if($qrcode->brand->discount_option == 1)
        {

            if(!is_null($item->discount_amount))
                $amount = $item->discount_amount;
            else
            {
                if(!is_null($qrcode->brand->discount_amount))
                     $amount = $qrcode->brand->discount_amount;
                 else
                    $amount = round($item->price / 100 * $qrcode->brand->discount_percentage);
            }

            $discount = $this->saveDiscount($traccia, $amount);

            event(new \App\Events\NewDiscountRegistration($traccia, $discount));
        }

        return redirect()->route('tracking.show', $traccia->token);
    }

    public function pdf(Request $request, $token)
    {

        $warranty = Warranty::whereHas('traceability', function($query) use($token){
            $query->where('token', $token);
        })->firstOrFail();

        $warranty->load(['traceability.brand.photo' => function($query) {
                $query->orderBy('position');
            },'traceability.item']);

        $pdf = \PDF::loadView('template.pdf.warrantyCertification', compact('warranty'))->setPaper('a4', 'landscape');
        return $pdf->stream('certificato.pdf');
    }
}
