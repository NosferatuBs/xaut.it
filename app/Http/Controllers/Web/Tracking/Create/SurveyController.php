<?php

namespace App\Http\Controllers\Web\Tracking\Create;

use App\Entities\Brand;
use App\Entities\BrandNumber;
use App\Entities\Discount;
use App\Entities\Item;
use App\Entities\Traceability;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Repositories\CouponRepository;
use App\Libraries\SurveyGenerator;
use App\Traits\SaveUserTraceability;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SurveyController extends Controller
{
    use SaveUserTraceability;

    public function show(Request $request, $brandId, $token)
    {
        if(! $request->session()->has('tracking'))
        {
            abort(403);
        }

        $firstLetter = substr($token, 0, 1);
        $secondLetter = substr($token, 1, 1);
        $number = substr($token, 2, strlen($token) - 2);

        $qrcode = BrandNumber::with("brand.photo")
            ->with("brand")
            ->whereNotNull("brand_id")
            ->where("brand_id", $brandId)
            ->where("first_letter", $firstLetter)
            ->where("second_letter", $secondLetter)
            ->where("first_number", "<=", $number)
            ->where("last_number", ">=", $number)
            ->firstOrFail();

        $brand = Brand::where("id", $brandId)->first();

        $session = $request->session()->get('tracking');

        $item_id = $session['item_id'];
        $item = Item::find($item_id);
        $locale = \App::getLocale();

        $surveys = new SurveyGenerator($session['brand_id'], is_null($item) ? null : $item->id, $locale);

        return view('web.tracking.survey')
            ->with([
                'stars' => $this->getStars(),
                'surveys' => $surveys->all(),
                'brand' => $brand,
                'previous' => route('tracking.qrcode', [$brandId, $token]),
                'qrcode' => $qrcode,
                'token' => $token,
                'brandId' => $brandId
            ]);
    }

    public function process(Request $request, $brandId, $token)
    {
        if(! $request->session()->has('tracking'))
        {
            abort(403);
        }

        $firstLetter = substr($token, 0, 1);
        $secondLetter = substr($token, 1, 1);
        $number = substr($token, 2, strlen($token) - 2);

        $qrcode = BrandNumber::with("brand.photo")
            ->with("brand")
            ->whereNotNull("brand_id")
            ->where("brand_id", $brandId)
            ->where("first_letter", $firstLetter)
            ->where("second_letter", $secondLetter)
            ->where("first_number", "<=", $number)
            ->where("last_number", ">=", $number)
            ->firstOrFail();

        $session = $request->session()->get('tracking');

        $item_id = $session['item_id'];
        $item = Item::find($item_id);
        $locale = \App::getLocale();

        $surveys = new SurveyGenerator($session['brand_id'], is_null($item) ? null : $item->id, $locale);

        $fields = [
            'stars' => 'required|min:1|max:5'
        ];

        foreach($surveys->all() as $survey)
        {
            $properties = [];
            if($survey->required)
            {
                $properties[] = 'required';
            }
            if(! $survey->surveyType->has_answer)
            {
                $properties[] = 'max:255';
            }

            if(! empty($properties))
            {
                $fields = $fields + ['survey_' . $survey->id => $properties];
            }
        }
		
        $this->validate($request, $fields, [
            'required' => trans('customer.survey.custom.required'),
            'max' => trans('customer.survey.custom.length')
        ]);

        $answers = [];
        foreach($surveys->all() as $survey)
        {
            $answers[$survey->id] = $request->input('survey_'.$survey->id);
        }

        if($qrcode->brand->warranty_option == 1)
        {
            $request->session()->put('tracking.rate', $request->stars);
            $request->session()->put('tracking.answers', $answers);

            return redirect()->route('tracking.warranty', $token);
        }
        else
        {
            $array = array_merge($session, ['rate' => $request->stars, 'answers' => $answers]);

            $traceability = $this->saveTraceability($surveys, $array);
            $traceability->load('brand', 'user');

            event(new \App\Events\NewProductRegistration($traceability));

            if($qrcode->brand->discount_option == 1)
            {
	            $isPercentage = false;
				
                if($item != null && !is_null($item->discount_amount))
                {
	                $amount = $item->discount_amount;
	                $isPercentage = false;
                }
                else
                {
                    if(!is_null($qrcode->brand->discount_amount))
                    {
	                    $amount = $qrcode->brand->discount_amount;
	                    $isPercentage = false;
                    }
                     else
                     {
	                     $amount = $qrcode->brand->discount_percentage;
	                     $isPercentage = true;
                     }
                }

                $discount = $this->saveDiscount($traceability, $amount, $isPercentage);
				
				// Save eventually the coupon to online store
	            $brand = Brand::where('id', $brandId)->first();
	            CouponRepository::registerCoupon($brand, $traceability);

                event(new \App\Events\NewDiscountRegistration($traceability, $discount));
            }

            $request->session()->forget('tracking');

            return redirect()->route('tracking.show', $traceability->token);
        }
    }

    protected function getStars()
    {
        return [
            (object)["id" => 1, "name" => "customer.survey.stars.1"],
            (object)["id" => 2, "name" => "customer.survey.stars.2"],
            (object)["id" => 3, "name" => "customer.survey.stars.3"],
            (object)["id" => 4, "name" => "customer.survey.stars.4"],
            (object)["id" => 5, "name" => "customer.survey.stars.5"]
        ];
    }
}
