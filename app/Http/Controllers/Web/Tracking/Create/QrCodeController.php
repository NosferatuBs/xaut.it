<?php

namespace App\Http\Controllers\Web\Tracking\Create;

use App\Entities\Brand;
use App\Entities\BrandIlProjectBrand;
use App\Entities\Item;
use App\Entities\BrandNumber;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class QrCodeController extends Controller
{
    public function redirect(Request $request, $token, $brandId)
    {
        return redirect("tracking/create/qr-code/" . $brandId . "/" . $token);
    }

    public function show(Request $request, $brandId, $token)
    {
        // http://localhost:8000/tracking/create/qr-code/67/AA5826647

        $firstLetter = substr($token, 0, 1);
        $secondLetter = substr($token, 1, 1);
        $number = substr($token, 2, strlen($token) - 2);

        $brandIlProjectBrand = BrandIlProjectBrand::where("ilproject_brand_id", $brandId)->first();
        if ($brandIlProjectBrand == null)
        {
	        return view("web.tracking.invalid");
        }
        $brandId = $brandIlProjectBrand->brand_id;
		$brand = Brand::where("id", $brandId)->first();
		
        $number = BrandNumber::with("brand.photo")
                            ->whereNotNull("brand_id")
                            ->where("brand_id", $brandId)
                            ->where("first_letter", $firstLetter)
                            ->where("second_letter", $secondLetter)
                            ->where("first_number", "<=", $number)
                            ->where("last_number", ">=", $number)
                            ->first();
		if ($number == null)
		{
			return view("web.tracking.invalid");
		}
		
        // If coupons are active, adds a string for the customer
        if($number["brand"]["discount_option"] == 1)
        {
	        $number["string_discount"] = "Subito per te un buono sconto via e-mail al termine dell'attivazione!";
        }
		
        return view("web.tracking.qrcode")
            ->with([
                "token" => $token,
				"brand" => $brand,
                "brandId" => $brandId,
                "number" => $number
            ]);
    }

    public function process(Request $request, $brandId, $token)
    {
        $firstLetter = substr($token, 0, 1);
        $secondLetter = substr($token, 1, 1);
        $number = substr($token, 2, strlen($token) - 2);

        $qrcode = BrandNumber::with("brand.photo")
            ->with("brand")
            ->whereNotNull("brand_id")
            ->where("brand_id", $brandId)
            ->where("first_letter", $firstLetter)
            ->where("second_letter", $secondLetter)
            ->where("first_number", "<=", $number)
            ->where("last_number", ">=", $number)
            ->first();

		if ($qrcode == null)
		{
			return back()->withInput()->with("status","InvalidCode");
		}
		
        $fields = [
            "email" => "required|email",
            "gender" => "required|in:male,female,other",
            "city" => "required",
            "lat" => "required",
            "lng" => "required",
            "birthday_day" => "required",
            "birthday_month" => "required",
            "birthday_year" => "required",
            "privacy"=> "accepted"
        ];

        $this->validate($request, $fields);

        $brand_id = $qrcode->brand->id;

        if($qrcode->brand->survey_item_option == 1)
        {
			// Surveys for item are enabled
            $item = Item::where("id", $qrcode->item_id)->first();

            if(is_null($item))
            {
                $request->session()->forget("tracking");
                return back()->withInput()->with("status","InvalidCode");
            }

            $item_id = $item->id;
        }
        else
        {
			// Surveys for item are disabled, we'll have only generic questions about the brand
            $item_id = null;

            $request->session()->forget("item");
        }

        $birthday_full = Carbon::create($request->birthday_year, $request->birthday_month, $request->birthday_day, 0, 0, 0);

        $request->session()->put("tracking.brand_number_id", $qrcode->id);
        $request->session()->put("tracking.brand_id", $qrcode->brand->id);
        $request->session()->put("tracking.item_id", $item_id);
        $request->session()->put("tracking.token", $token);

        $request->session()->put("tracking.email", $request->email);
        $request->session()->put("tracking.gender", $request->gender);
        $request->session()->put("tracking.city", $request->city);
        $request->session()->put("tracking.lat", $request->lat);
        $request->session()->put("tracking.lng", $request->lng);
        $request->session()->put("tracking.birthday", $birthday_full);
        $request->session()->put("tracking.gdpr_newsletter_consent", $request->gdpr_newsletter_consent);
        $request->session()->put("tracking.gdpr_profilation_consent", $request->gdpr_profilation_consent);
        $request->session()->put("tracking.gdpr_16years_consent", $request->gdpr_16years_consent);

        return redirect()->route("tracking.survey", [$brandId, $token]);
    }
}
