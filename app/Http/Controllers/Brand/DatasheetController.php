<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Branch;
use App\Entities\Brand;
use App\Entities\Datasheet;
use App\Entities\DatasheetType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DatasheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-file-text";
        $title->title = "Documenti";
        $title->description = "Elenco";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "documenti";
        $menu->option = "elenco";

        $datasheets = Datasheet::withCount('branches')->whereBrandIn($user->brands->pluck('id')->toArray())->get();

        return view('brand.datasheets.index')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'datasheets' => $datasheets
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-file-text";
        $title->title = "Documenti";
        $title->description = "Nuovo";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Nuovo', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "documenti";
        $menu->option = "create";

        $brands = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->orderBy('name')->get();
        $datasheet_types = DatasheetType::all();

        return view('brand.datasheets.create')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'brands' => $brands,
                'datasheet_types' => $datasheet_types
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = [
            'brand_id' => 'required',
            'datasheet_type_id' => 'required',
            'name' => 'required',
        ];
        $this->validate($request, $fields);

        $sheet = new Datasheet;
        $sheet->brand_id = $request->brand_id;
        $sheet->datasheet_type_id = $request->datasheet_type_id;
        $sheet->name = $request->name;
        $sheet->original = NULL;
        $sheet->stored = NULL;
        $sheet->mime = NULL;
        $sheet->save();

        return redirect()->route('brand.datasheets.edit', $sheet->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Datasheet  $datasheet
     * @return \Illuminate\Http\Response
     */
    public function edit(Datasheet $datasheet)
    {
        $this->authorize('update', $datasheet);

        $title = new \stdClass();
        $title->icon = "fa fa-file-text";
        $title->title = "Documenti";
        $title->description = "Modifica";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => route('brand.datasheets.index')),
            (object)array('title' => 'Modifica', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "documenti";
        $menu->option = "elenco";

        $datasheet_types = DatasheetType::all();
        $branches = Branch::where('brand_id', $datasheet->brand_id)->get();

        return view('brand.datasheets.edit')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'datasheet' => $datasheet,
                'datasheet_types' => $datasheet_types,
                'branches' => $branches
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fields = [
            'datasheet_type_id' => 'required',
            'name' => 'required',
            'datasheet' => 'file|max:1024|mimes:pdf,zip',
        ];
        $this->validate($request, $fields);

        $sheet = Datasheet::findOrFail($id);

        $this->authorize('update',$sheet);

        $path = NULL;
        $original = NULL;
        $mime = NULL;
        if($request->hasFile('datasheet') && $request->file('datasheet')->isValid())
        {
            $photo = $request->file('datasheet');
            $hash = $photo->hashName();
            $folder = substr($hash, 0, 2);
            $dir = 'datasheets' . DIRECTORY_SEPARATOR . $folder;

            $path = $photo->storeAs($dir, $sheet->id . $hash,'public');
            $original = $photo->getClientOriginalName();
            $mime = $photo->getMimeType();
        }
        if (!is_null($path) || $request->delete == 1)
        {
            if(!is_null($sheet->stored))
            {
                Storage::disk('public')->delete($sheet->stored);
            }
            if($request->delete == 1)
            {
                $path = NULL;
                $original = NULL;
                $mime = NULL;
            }
            $sheet->stored = $path;
            $sheet->original = $original;
            $sheet->mime = $mime;
        }

        $sheet->datasheet_type_id = $request->datasheet_type_id;
        $sheet->name = $request->name;
        $sheet->active = $request->has('active') ? true : false;

        $sheet->branches()->sync($request->branches ?? []);

        if(is_null($sheet->stored))
        {
            $sheet->active = 0;
        }
        $sheet->save();

        return redirect()->route('brand.datasheets.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Datasheet  $datasheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Datasheet $datasheet)
    {
        $this->authorize('delete',$datasheet);

        $datasheet->delete();

        return back();
    }
}
