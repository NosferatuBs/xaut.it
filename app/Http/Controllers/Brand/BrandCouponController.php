<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Brand;
use App\Entities\BrandCoupon;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class BrandCouponController extends Controller
{
    public function edit(Request $request, Brand $brand)
    {
        $brand->load('coupon');

        $title = new \stdClass();
        $title->icon = "fa fa-shield";
        $title->title = "Marchi";
        $title->description = "Modifica configurazione coupon";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => route('brand.brands.index')),
            (object)array('title' => 'Modifica coupon', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "marchi";
        $menu->option = "elenco";

        return view('brand.brands.coupon')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'brand' => $brand
            ]);
    }

    public function update(Request $request, Brand $brand)
    {
        $brand->load('coupon');
        $coupon = $brand->coupon;

        $fields = [
            'coordinate_x' => 'required',
            'coordinate_y' => 'required'
        ];

        if(is_null($coupon))
        {
            $fields['template'] = 'required|mimetypes:application/pdf';

            $coupon = new BrandCoupon;
            $coupon->brand_id = $brand->id;
        }

        if($request->hasFile('template') && $request->file('template')->isValid())
        {
            Storage::delete($coupon->template);

            $now = now();
            $today_directory = sprintf('year_%d.month_%d.day_%d',
                $now->format('Y'),
                $now->format('m'),
                $now->format('d')
            );
            $today_directory = str_replace(".", DIRECTORY_SEPARATOR, $today_directory);

            $path = $request->file('template')->store($today_directory);
            $coupon->template = $path;
        }

        $coupon->x = $request->coordinate_x;
        $coupon->y = $request->coordinate_y;
        $coupon->save();

        return back();
    }

    public function test(Request $request, Brand $brand)
    {
        $brand->load('coupon');
        $coupon = $brand->coupon;

        $email = $request->email;
        $discount = $request->discount;
	    $isPercentage = true;
        $url = route('validate.coupon.qrcode', $request->token);
        $token = $request->token;
        $expired_at = Carbon::createFromFormat('d/m/Y', $request->expired_at);
        $template = Storage::path($coupon->template);

        $pdf = \App\Libraries\CouponGenerator::make($email, $discount, $isPercentage, $brand->id, $url, $token, $expired_at, $template);

        $pdf->Output('I', 'coupon.pdf');
    }
}
