<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Brand;
use App\Entities\BrandPhoto;
use App\Http\Controllers\Controller;
use App\Http\Repositories\AnalyticsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class BrandController extends Controller
{
    public function __construct()
    {

    }

    public function create()
    {
        $this->authorize('create_brand');

    	$title = new \stdClass();
        $title->icon = "fa fa-shield";
        $title->title = "Marchi";
        $title->description = "Nuova";

    	$breadcumbs = [
    		(object)array('title' => 'Home', 'url' => route('brand.dashboard')),
    		(object)array('title' => 'Nuovo', 'url' => null)
    	];

    	$menu = new \stdClass();
        $menu->area = "marchi";
        $menu->option = "create";

    	return view('brands.create', ['title' => $title, 'breadcumbs' => $breadcumbs, 'menu' => $menu]);
    }

    public function store(Request $request)
    {
        $this->authorize('create_brand');

    	$fields = [
            'name' => [
                'required',
                Rule::unique('brands')
            ],
    		'name_public' => 'required',
			'url_video' => 'nullable|url'
    	];

    	$this->validate($request, $fields);

		$marchio = new Brand();
        $marchio->name = $request->name;
		$marchio->name_public = $request->name_public;
		$marchio->url_video = $request->url_video;
		$marchio->text = $request->text;
        $marchio->warranty_extension = $request->warranty_extension;
        $marchio->discount_percentage = $request->discount_percentage;
        $marchio->discount_amount = $request->discount_amount;
        $marchio->discount_option = $request->has('discount_option') ? 1 : 0;
        $marchio->survey_option = $request->has('survey_option') ? 1 : 0;
        $marchio->stats_option = $request->has('stats_option') ? 1 : 0;
        $marchio->authenticity_option = $request->has('authenticity_option') ? 1 : 0;
		
		$marchio->save();

		return redirect()->route('brand.brands.edit', $marchio->id);
    }

    public function index(Request $request)
    {
        $user = $request->user();

    	$title = new \stdClass();
        $title->icon = "fa fa-shield";
        $title->title = "Marchi";
        $title->description = "Elenco";

    	$breadcumbs = [
    		(object)array('title' => 'Home', 'url' => route('brand.dashboard')),
    		(object)array('title' => 'Elenco', 'url' => null)
    	];

    	$menu = new \stdClass();
        $menu->area = "marchi";
        $menu->option = "elenco";

        $brands = Brand::with('photo')
            ->whereBrandIn($user->brands->pluck('id')->toArray())
            ->get();

        $brands->load(['photo' => function ($query) {
            $query->orderBy('position', 'asc');
        }]);

    	return view('brand.brands.index', ['brands' => $brands ,'title' => $title, 'breadcumbs' => $breadcumbs, 'menu' => $menu]);
    }

    public function edit(Request $request, Brand $brand)
    {
        $this->authorize('update', $brand);

        $brand->load(['photo' => function ($query) {
            $query->orderBy('position', 'asc');
        }]);

        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-shield";
        $title->title = "Marchi";
        $title->description = "Modifica";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => route('brand.brands.index')),
            (object)array('title' => 'Modifica', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "marchi";
        $menu->option = "elenco";

        return view('brand.brands.edit', ['brand' => $brand ,'title' => $title, 'breadcumbs' => $breadcumbs, 'menu' => $menu]);
    }

    public function update(Request $request, Brand $brand)
    {
        $this->authorize('update', $brand);

        $brand->load('photo');
        $user = $request->user();

        $lastPosition = count($brand->photo);


        $fields = [
            'name' => [
                'required',
                Rule::unique('brands')->ignore($brand->id)
            ],
            'name_public' => 'required',
            'url_video' => 'nullable|url',
            'warranty_extension' => 'numeric',
            'barcode' => 'nullable|digits:6',
            'discount_percentage' => 'required_if:discount_option,|required_without:discount_amount',
            'discount_amount' => 'required_if:discount_option,|required_without:discount_percentage',
            'discount_validity' => 'required_if:discount_option,|numeric'
        ];

        if(is_array($request->file))
        {
            $nbr = count($request->file) - 1;
            foreach(range(0, $nbr) as $index) {
                $fields['file.' . $index] = 'file|mimes:jpeg,bmp,png|max:256';
            }
        }

        $this->validate($request, $fields);

        $brand->name = $request->name;
        $brand->name_public = $request->name_public;
        $brand->url_video = $request->url_video;
        $brand->text = $request->text;
        $brand->barcode = $request->barcode;
        $brand->warranty_extension = empty($request->warranty_extension)? 0 : $request->warranty_extension;
        $brand->discount_percentage = empty($request->discount_percentage)? 0 : $request->discount_percentage;
        $brand->discount_amount = empty($request->discount_amount)? 0 : $request->discount_amount;
        $brand->discount_validity = empty($request->discount_validity)? 0 : $request->discount_validity;
        $brand->search_option = $request->search_option;
	
        $brand->save();

        if(is_array($request->file))
        {
            foreach($request->file as $key => $photo)
            {
                $hash = $photo->hashName();
                $folder = substr($hash, 0, 2);
                $dir = 'brands' . DIRECTORY_SEPARATOR . $folder;
                $path = $photo->storeAs($dir, $hash,'public');

                BrandPhoto::updateOrCreate([
                    'stored' => $path,
                    'brand_id' => $brand->id
                ], [
                    'original' => $photo->getClientOriginalName(),
                    'mime' => $photo->getMimeType(),
                    'position' => $key + $lastPosition
                ]);
            }
        }

        if(is_array($request->gallery))
        {
            foreach ($request->gallery as $key => $gallery_id)
            {
                $immagine = $brand->photo->find($gallery_id);
                $immagine->title = $request->gallery_title[$key];
                $immagine->position = $key;
                $immagine->save();
            }
        }

        if(is_array($request->gallery_delete))
        {
            foreach($request->gallery_delete as $photo_id)
            {
                $immagine = $brand->photo->find($photo_id);
                Storage::disk('public')->delete($immagine->stored);
                $immagine->delete();
            }
        }

        return redirect()->route('brand.brands.index');
    }
    public function destroy(Request $request)
    {
        $this->authorize('delete_brand');
        $user = $request->user();

        if($user->hasRole('brandowner'))
        {
            $brand = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->where('id',$request->brand_id)->first();
        }
        else
        {
            $brand = Brand::find($request->brand_id);
        }

        if(! is_null($brand) && $brand->items->isEmpty())
        {
            foreach($brand->photo()->get() as $image)
            {
                Storage::disk('public')->delete($image->stored);
            }
            $brand->photo()->delete();
            $brand->numbers()->delete();
            $brand->users()->detach();
            $brand->delete();
        }

        return redirect()->route('brand.brands.index');
    }
	
	public function export()
	{
		$brands = Brand::with('photo')->get();
		
		$title = new \stdClass();
		$title->icon = "fa fa-cubes";
		$title->title = "Esportazione statistiche";
		$title->description = "Generazione report PDF";
		
		$breadcumbs = [
			(object)array('title' => 'Home', 'url' => '/home'),
			(object)array('title' => 'Esporta statistiche', 'url' => null)
		];
		
		$menu = new \stdClass();
		$menu->area = "analytics";
		$menu->option = "export";
		
		return view('brand.analytics.export.index', [
			'brands' => $brands,
			'breadcumbs' => $breadcumbs,
			'menu' => $menu,
			'title' => $title,
			'from' => date('Y') . '-01-01',
			'to' => date('Y') . '-12-31',
		]);
	}
	
	public function pdf(Request $request)
	{
		return AnalyticsRepository::createPdfReport(
			$request->get('brandId'),
			$request->get('from'),
			$request->get('to'));
	}
}
