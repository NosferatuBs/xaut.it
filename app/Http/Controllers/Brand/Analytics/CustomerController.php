<?php
	
	namespace App\Http\Controllers\Brand\Analytics;
	
	use App\Entities\Brand;
	use App\Entities\Traceability;
	use App\Http\Controllers\Controller;
	use App\Libraries\Ages;
	use Illuminate\Http\Request;
	use Illuminate\Support\Carbon;
	use Illuminate\Support\Facades\DB;
	
	class CustomerController extends Controller
	{
		public function overview(Request $request)
		{
			$user = $request->user();
			
			if ($request->has('year') && $request->year)
				$date = Carbon::createFromDate($request->year, 1, 1);
			else
				$date = Carbon::now();

			$title = new \stdClass();
			$title->icon = "fa fa-line-chart";
			$title->title = "Statistiche";
			$title->description = "Analisi dei clienti nell'anno " . $date->year;
			
			$breadcumbs = [
				(object)array('title' => 'Home', 'url' => route('brand.dashboard')),
				(object)array('title' => 'Statistiche clienti', 'url' => null)
			];
			
			$menu = new \stdClass();
			$menu->area = "analytics";
			$menu->option = "customers";
			
			$years = [];
			foreach ($this->rangeYears() as $year)
			{
				$years[] = $year;
			}
			
			$brands = Brand::with('photo')
				->whereBrandIn($user->brands
					->where('stats_option', '>', 0)
					->pluck('id')
					->toArray())
				->get();
			
			if (count($brands) == 0)
			{
				abort(403);
			}
			
			$form = new \stdClass();
			
			$form->year = date('Y');
			if ($request->has('year'))
			{
				$form->year = $request->get('year');
			}
			
			$form->brandId = $brands[0]->id;
			if ($request->has('brandId'))
			{
				$form->brandId = $request->get('brandId');
			}
			
			$list = Traceability::select('city', 'lat', 'lng', DB::raw('COUNT(city) as frequenza'))
				->whereYear('traceability.created_at', $date->year)
				->where('brand_id', $form->brandId)
				->groupBy('city', 'lat', 'lng')
				->get();
			
			$geochart = [
				['Latitude', 'Longitude', 'Città', 'Frequenza']
			];
			foreach ($list as $el)
			{
				$geochart[] = [
					$el->lat,
					$el->lng,
					$el->city,
					(int)$el->frequenza
				];
			}
			
			$sesso = Traceability::select(DB::raw('SUM( CASE WHEN gender=\'male\' THEN 1 ELSE 0 END) AS male'), DB::raw('SUM( CASE WHEN gender=\'female\' THEN 1 ELSE 0 END) AS female'), DB::raw('SUM( CASE WHEN gender=\'other\' THEN 1 ELSE 0 END) AS other'))
				->join('users', 'users.id', '=', 'traceability.user_id')
				->join('brands', 'traceability.brand_id', '=', 'brands.id')
				->whereYear('traceability.created_at', $date->year)
				->where('brand_id', $form->brandId)
				->where('brands.stats_option', '1')
				->first();
			
			$eta = Traceability::select('users.id', 'users.birthday')
				->join('users', 'users.id', '=', 'traceability.user_id')
				->join('brands', 'traceability.brand_id', '=', 'brands.id')
				->whereYear('traceability.created_at', $date->year)
				->where('brand_id', $form->brandId)
				->where('brands.stats_option', '1')
				->get();
			
			$ageGroup = new Ages(['0-17', '18-27', '28-37', '38-47', '48-57', '58-67', '68+']);
			
			foreach ($eta as $utente)
			{
				$age = $this->getAge($utente->birthday);
				
				if ($age >= 0 && $age <= 17)
					$ageGroup->increments('0-17');
				elseif ($age >= 18 && $age <= 27)
					$ageGroup->increments('18-27');
				elseif ($age >= 28 && $age <= 37)
					$ageGroup->increments('28-37');
				elseif ($age >= 38 && $age <= 47)
					$ageGroup->increments('38-47');
				elseif ($age >= 48 && $age <= 57)
					$ageGroup->increments('48-57');
				elseif ($age >= 58 && $age <= 67)
					$ageGroup->increments('58-67');
				elseif ($age >= 68)
					$ageGroup->increments('68+');
			}
			
			return view('brand.analytics.customers.index')
				->with([
					'title' => $title,
					'breadcumbs' => $breadcumbs,
					'menu' => $menu,
					'years' => $years,
					'sesso' => $sesso,
					'form' => $form,
					'brands' => $brands,
					'eta' => $ageGroup,
					'geochart' => $geochart
				]);
		}
		
		protected function rangeYears($from = null)
		{
			return range($from ?? 2017, Carbon::now()->year);
		}
		
		protected function getAge($birthday)
		{
			if (is_null($birthday))
				return null;
			
			return Carbon::make($birthday)->diffInYears();
		}
	}
