<?php
namespace App\Http\Controllers\Brand\Analytics;

use App\Entities\Brand;
use App\Entities\CustomerAnswer;
use App\Entities\Item;
use App\Entities\Survey;
use App\Entities\Traceability;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Libraries\Ages;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SurveyController extends Controller
{

    public function overview(Request $request)
    {
        $user = $request->user();

        if($request->has('year') && $request->year)
            $date = Carbon::createFromDate($request->year, 1, 1);
        else
            $date = Carbon::now();

        $title = new \stdClass();
        $title->icon = "fa fa-line-chart";
        $title->title = "Statistiche";
        $title->description = "Analisi delle risposte ai sondaggi nell'anno " . $date->year;

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Statistiche', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "analytics";
        $menu->option = "surveys";

        $years = [];
	    $years = [];
	    foreach ($this->rangeYears() as $year)
	    {
		    $years[] = $year;
	    }
	    
	    $brands = Brand::with('photo')
		    ->whereBrandIn($user->brands
			    ->where('stats_option', '>', 0)
			    ->pluck('id')
			    ->toArray())
		    ->get();
	    
	    if (count($brands) == 0)
	    {
		    abort(403);
	    }
	    
	    $form = new \stdClass();
	    
	    $form->year = date('Y');
	    if ($request->has('year'))
	    {
		    $form->year = $request->get('year');
	    }
	    
	    $form->brandId = $brands[0]->id;
	    if ($request->has('brandId'))
	    {
		    $form->brandId = $request->get('brandId');
	    }

        $surveys = Survey::withCount(['customerAnswers' => function ($query) use($date){
            $query->whereYear('created_at', $date->format('Y'));
        } ,'branches'])->whereHas('customerAnswers', function ($query) use($date){
            $query->whereYear('created_at', $date->format('Y'));
        })->whereBrandIn($user->brands->pluck('id')->toArray())->get();

        return view('brand.analytics.surveys.index')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'years' => $years,
	            'form' => $form,
	            'brands' => $brands,
                'surveys' => $surveys
            ]);
    }

    public function show(Request $request, Survey $survey)
    {
        $this->authorize('view', $survey);

        $user = $request->user();

        if($request->has('year') && $request->year)
            $date = Carbon::createFromDate($request->year, 1, 1);
        else
            $date = Carbon::now();

        $title = new \stdClass();
        $title->icon = "fa fa-line-chart";
        $title->title = "Statistiche";
        $title->description = "Dettagli sondaggio nell'anno " . $date->year;

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Risposte sondaggi', 'url' =>  route('brand.analytics.surveys.overview')),
            (object)array('title' => 'Dettagli sondaggio', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "analytics";
        $menu->option = "surveys";

        $survey->load(['customerAnswers' => function ($query) use($date){
                $query->whereYear('created_at', $date->format('Y'));
        },'customerAnswers.traceability.item','customerAnswers.traceability.user','surveyType','answers']);

        $years = [];
        foreach ($this->rangeYears($survey->created_at->format('Y')) as $year) {
            $years[] = array("year" => $year, "selected" => ($date->year == $year) ? true : false);
        }

        $query = CustomerAnswer::select(DB::raw(' ifnull(survey_answers.name,"Altro") as name,customer_answers.survey_id, count(*) as tot'))
            ->leftJoin('survey_answers',function ($join){
                $join->on('customer_answers.answer', '=', 'survey_answers.name')
                    ->on('customer_answers.survey_id', '=', 'survey_answers.survey_id');
            })
            ->where('customer_answers.survey_id',$survey->id)
            ->whereYear('customer_answers.created_at', $date->format('Y'))
            ->groupBy('customer_answers.survey_id','survey_answers.name')
            ->get();

        $frequenza = [];

        if($survey->surveyType->has_answer)
        {
            foreach($survey->answers as $answer)
            {
                if(! array_key_exists($answer->name, $frequenza))
                    $frequenza[ $answer->name ] = 0;
            }
            foreach ($query as $answer) {
                $frequenza[ $answer->name ] = $answer->tot;
            }
        }

        return view('brand.analytics.surveys.show')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'years' => $years,
                'survey' => $survey,
                'frequenza' => $frequenza
            ]);
    }

    protected function rangeYears($from = null)
    {
        return range($from ?? 2017, Carbon::now()->year);
    }
}
