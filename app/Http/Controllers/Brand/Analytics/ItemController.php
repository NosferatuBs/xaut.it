<?php
namespace App\Http\Controllers\Brand\Analytics;

use App\Entities\Brand;
use App\Entities\Item;
use App\Entities\Traceability;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Libraries\Ages;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    public function overview(Request $request)
    {
        $user = $request->user();

        if($request->has('year') && $request->year)
            $date = Carbon::createFromDate($request->year, 1, 1);
        else
            $date = Carbon::now();

        $title = new \stdClass();
        $title->icon = "fa fa-line-chart";
        $title->title = "Statistiche";
        $title->description = "Valutazione media articoli nell'anno " . $date->year;

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Statistiche', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "analytics";
        $menu->option = "items";
	    
	    $years = [];
	    foreach ($this->rangeYears() as $year)
	    {
		    $years[] = $year;
	    }
	    
	    $brands = Brand::with('photo')
		    ->whereBrandIn($user->brands
			    ->where('stats_option', '>', 0)
			    ->pluck('id')
			    ->toArray())
		    ->get();
	    
	    if (count($brands) == 0)
	    {
		    abort(403);
	    }
	    
	    $form = new \stdClass();
	    
	    $form->year = date('Y');
	    if ($request->has('year'))
	    {
		    $form->year = $request->get('year');
	    }
	    
	    $form->brandId = $brands[0]->id;
	    if ($request->has('brandId'))
	    {
		    $form->brandId = $request->get('brandId');
	    }

        $valutazione = Traceability::select(DB::raw('SUM( CASE WHEN rate=1 THEN 1 ELSE 0 END) AS one'),
            DB::raw('SUM( CASE WHEN rate=2 THEN 1 ELSE 0 END) AS two'),
            DB::raw('SUM( CASE WHEN rate=3 THEN 1 ELSE 0 END) AS three'),
            DB::raw('SUM( CASE WHEN rate=4 THEN 1 ELSE 0 END) AS four'),
            DB::raw('SUM( CASE WHEN rate=5 THEN 1 ELSE 0 END) AS five'))
            ->whereYear('traceability.created_at', $date->year)
            ->where('brand_id', $form->brandId)
            ->first();
        $list = Item::select('items.*', DB::raw('AVG(traceability.rate) as avg'))
            ->leftJoin('traceability', function($join) use($date){
                $join->on('items.id', '=', 'traceability.item_id')
                    ->whereYear('traceability.created_at', $date->year);
            })
	        ->where('items.brand_id', $form->brandId)
            ->groupBy('items.id', 'items.brand_id', 'items.name', 'items.barcode', 'items.url_video', 'items.text', 'items.created_at', 'items.updated_at')
            ->havingRaw('AVG(traceability.rate)')
            ->with('brand')
            ->get();

        return view('brand.analytics.items.index')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'list' => $list,
                'years' => $years,
				'form' => $form,
				'brands' => $brands,
                'valutazione' => $valutazione
            ]);
    }
    public function show(Request $request, Item $item)
    {
        $user = $request->user();

        $item->load(['photo' => function($query) {
            $query->orderBy('position');
        }]);

        if($request->has('year') && $request->year)
            $date = Carbon::createFromDate($request->year, 1, 1);
        else
            $date = Carbon::now();

        $title = new \stdClass();
        $title->icon = "fa fa-line-chart";
        $title->title = "Statistiche";
        $title->description = "Dettagli articolo nell'anno " . $date->year;

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Valutazione articoli', 'url' => route('brand.analytics.items.overview')),
            (object)array('title' => 'Dettagli articolo', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "analytics";
        $menu->option = "items";

        $years = [];
        foreach ($this->rangeYears($item->created_at->format('Y')) as $year) {
            $years[] = array("year" => $year, "selected" => ($date->year == $year) ? true : false);
        }

        $list = Traceability::select('city', 'lat', 'lng', DB::raw('COUNT(city) as frequenza'))
            ->whereYear('traceability.created_at', $date->year)
            ->where('item_id',$item->id)
            ->groupBy('city', 'lat', 'lng')
            ->get();

        $geochart = [
            ['Latitude','Longitude','Città', 'Frequenza']
        ];
        foreach($list as $el)
        {
            $geochart[] = [
                $el->lat,
                $el->lng,
                $el->city,
                (int)$el->frequenza
            ];
        }

        $sesso = Traceability::select(DB::raw('SUM( CASE WHEN gender=\'male\' THEN 1 ELSE 0 END) AS male'), DB::raw('SUM( CASE WHEN gender=\'female\' THEN 1 ELSE 0 END) AS female'), DB::raw('SUM( CASE WHEN gender=\'other\' THEN 1 ELSE 0 END) AS other'))
            ->join('users', 'users.id', '=', 'traceability.user_id')
            ->whereYear('traceability.created_at', $date->year)
            ->where('item_id',$item->id)
            ->first();

        $valutazione = Traceability::select(DB::raw('SUM( CASE WHEN rate=1 THEN 1 ELSE 0 END) AS one'),
            DB::raw('SUM( CASE WHEN rate=2 THEN 1 ELSE 0 END) AS two'),
            DB::raw('SUM( CASE WHEN rate=3 THEN 1 ELSE 0 END) AS three'),
            DB::raw('SUM( CASE WHEN rate=4 THEN 1 ELSE 0 END) AS four'),
            DB::raw('SUM( CASE WHEN rate=5 THEN 1 ELSE 0 END) AS five'))
            ->whereYear('traceability.created_at', $date->year)
            ->where('item_id',$item->id)
            ->first();

        $eta = Traceability::select('users.id','users.birthday')
            ->join('users', 'users.id', '=', 'traceability.user_id')
            ->whereYear('traceability.created_at', $date->year)
            ->where('item_id',$item->id)
            ->get();

        $list_count = $list->sum('frequenza');
        $list->map(function($el) use ($list_count){
            $el->frequenza_100 = ( $el->frequenza / $list_count ) * 100;
            return $el;
        });

        $ageGroup = new Ages(['0-17','18-27','28-37','38-47','48-57','58-67','68+']);

        foreach ($eta as $utente)
        {
            $age = $this->getAge($utente->birthday);

            if($age >= 0 && $age <= 17)
                $ageGroup->increments('0-17');
            elseif($age >= 18 && $age <= 27)
                $ageGroup->increments('18-27');
            elseif($age >= 28 && $age <= 37)
                $ageGroup->increments('28-37');
            elseif($age >= 38 && $age <= 47)
                $ageGroup->increments('38-47');
            elseif($age >= 48 && $age <= 57)
                $ageGroup->increments('48-57');
            elseif($age >= 58 && $age <= 67)
                $ageGroup->increments('58-67');
            elseif($age >= 68)
                $ageGroup->increments('68+');
        }

        return view('brand.analytics.items.show')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'years' => $years,
                'item' => $item,
                'geochart' => $geochart,
                'sesso' => $sesso,
                'valutazione' => $valutazione,
                'eta' => $ageGroup
            ]);
    }
    protected function rangeYears($from = null)
    {
        return range($from ?? 2017, Carbon::now()->year);
    }

    protected function getAge($birthday)
    {
        if(is_null($birthday))
            return null;

        return Carbon::make($birthday)->diffInYears();
    }
}
