<?php
namespace App\Http\Controllers\Brand\Analytics;

use App\Entities\Brand;
use App\Entities\Traceability;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TraceabilityController extends Controller
{
    public function overview(Request $request)
    {
        $user = $request->user();

        if($request->has('year') && $request->year)
            $date = Carbon::createFromDate($request->year, 1, 1);
        else
            $date = Carbon::now();

        $title = new \stdClass();
        $title->icon = "fa fa-line-chart";
        $title->title = "Statistiche";
        $title->description = "Ricerche effettuate nell'anno " . $date->year;

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Statistiche richerche', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "analytics";
        $menu->option = "traceability";
	    
	    $years = [];
	    foreach ($this->rangeYears() as $year)
	    {
		    $years[] = $year;
	    }
	    
	    $brands = Brand::with('photo')
		    ->whereBrandIn($user->brands
			    ->where('stats_option', '>', 0)
			    ->pluck('id')
			    ->toArray())
		    ->get();
	    
	    if (count($brands) == 0)
	    {
		    abort(403);
	    }
	    
	    $form = new \stdClass();
	    
	    $form->year = date('Y');
	    if ($request->has('year'))
	    {
		    $form->year = $request->get('year');
	    }
	    
	    $form->brandId = $brands[0]->id;
	    if ($request->has('brandId'))
	    {
		    $form->brandId = $request->get('brandId');
	    }

        $list = Traceability::with(['user','item.brand'])
            ->selectRaw('traceability.*, items.name')
	        ->join('brand_numbers', 'traceability.brand_number_id', '=', 'brand_numbers.id')
	        ->join('items', 'brand_numbers.item_id', '=', 'items.id')
            ->whereBrandIn($user->brands->pluck('id')->toArray())
            ->whereYear('traceability.created_at', $date->year)->get();

        $group = Traceability::select(DB::raw('month(traceability.created_at) as month, count(*) as sum'))
            ->whereBrandIn($user->brands->pluck('id')->toArray())
            ->whereYear('traceability.created_at', $date->year)
            ->groupBy('month')
            ->get();

        $filter = $group->keyBy('month')->toArray();

        $graph = [];
        foreach(range(1,12) as $month)
        {
            $value = 0;
            if(isset($filter[$month]))
            {
                $value = $filter[$month]['sum'];
            }
            $graph[$month - 1] = $value;
        }

        return view('brand.analytics.traceability.index')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'graph' => $graph,
                'list' => $list,
	            'form' => $form,
	            'brands' => $brands,
                'years' => $years
            ]);
    }

    protected function rangeYears($from = null)
    {
        return range($from ?? 2017, Carbon::now()->year);
    }
}
