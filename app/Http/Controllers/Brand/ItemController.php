<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Brand;
use App\Http\Controllers\Controller;
use App\Entities\Item;
use App\Entities\ItemPhoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class ItemController extends Controller
{
    protected $title;
    protected $menu;
    protected $breadcumbs;

    public function __construct()
    {
        $this->title = new \stdClass();
        $this->menu = new \stdClass();
        $this->breadcumbs = [];
    }

    public function index(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-cubes";
        $title->title = "Articoli";
        $title->description = "Elenco";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "articoli";
        $menu->option = "elenco";

        $items = Item::with(['brand','photo' => function($query) {
                $query->orderBy('position');
            }])
            ->whereBrandIn($user->brands->pluck('id')->toArray())
            ->withCount('traceability')
            ->paginate();

        foreach ($items as $item)
        {
            if( !$item->photo->isEmpty() )
                $item->picture = asset('storage/' . $item->photo->first()->stored);
            else
                $item->picture = '';

            $array = [];
            array_push($array, [
                'separator' => 0,
                'label' => 'Modifica',
                'options' => [
                    'href' => route('brand.items.edit', $item->id),
                    'class' => 'btn btn-xs btn-default'
                ]
            ]);

            if($item->url_video)
                array_push($array, [
                    'separator' => 0,
                    'label' => 'Apri sito web',
                    'options' => [
                        'href' => $item->url_video,
                        'target' => '_blank'
                    ]
                ]);
            if($item->brand->level == 1)
                array_push($array, [
                    'separator' => 0,
                    'label' => 'Numerazione',
                    'options' => [
                        'href' => route('brand.items.numbering.edit', $item->id)
                    ]
                ]);

            array_push($array, [
                'separator' => 0,
                'label' => 'Barcode alternativi',
                'options' => [
                    'href' => route('brand.items.barcodes.edit', $item->id)
                ]
            ]);

            if($item->traceability_count == 0)
                array_push($array, [
                    'separator' => 1,
                    'label' => 'Elimina',
                    'options' => [
                        'href' => 'javascript:void(0);',
                        'class' => 'btn-delete',
                        'data-action' => route('brand.items.destroy', $item->id)
                    ]
                ]);

            $item->dropdown = $array;
        }

        return view('brand.items.index')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'items' => $items
            ]);
    }

    public function create(Request $request)
    {
        $user = $request->user();

        $this->title->icon = "fa fa-cubes";
        $this->title->title = "Articoli";
        $this->title->description = "Nuovo";

        $this->breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Nuovo', 'url' => null)
        ];

        $this->menu->area = "articoli";
        $this->menu->option = "create";

        $brands = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->orderBy('name')->get();

        return view('brand.items.create')
            ->with([
                'title' => $this->title,
                'breadcumbs' => $this->breadcumbs,
                'menu' => $this->menu,
                'brands' => $brands
            ]);
    }

    public function store(Request $request)
    {
        $fields = [
            'name' => 'required',
            'brand_id' => 'required',
            'barcode' => [
                'nullable',
                'numeric',
                Rule::unique('items'),
                Rule::unique('barcode_item','barcode')
            ],
            'url_video' => 'nullable|url'
        ];

        $this->validate($request, $fields);

        $item = new Item();
        $item->name = $request->name;
        $item->barcode = $request->barcode;
        $item->url_video = $request->url_video;
        $item->text = $request->text;
        $item->brand_id = $request->brand_id;

        $item->save();

        return redirect()->route('brand.items.edit', $item->id);
    }

    public function edit(Request $request, Item $item)
    {
        $user = $request->user();

        $this->title->icon = "fa fa-cubes";
        $this->title->title = "Articoli";
        $this->title->description = "Modifica";

        $this->breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => route('brand.items.index')),
            (object)array('title' => 'Modifica', 'url' => null)
        ];

        $this->menu->area = "articoli";
        $this->menu->option = "elenco";

        $item->load(['photo' => function($query) {
            $query->orderBy('position');
        }]);

        $brands = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->orderBy('name')->get();

        $title = $this->title;
        $breadcumbs = $this->breadcumbs;
        $menu = $this->menu;

        return view('brand.items.edit')
            ->with([
                'brands' => $brands,
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'item' => $item
            ]);
    }

    public function update(Request $request, Item $item)
    {
        $fields = [
            'name' => 'required',
            'brand_id' => 'required',
            'barcode' => [
                'nullable',
                'numeric',
                Rule::unique('items')->ignore($item->id),
                Rule::unique('barcode_item','barcode')
            ],
            'url_video' => 'nullable|url',
        ];
        if(is_array($request->file))
        {
            $nbr = count($request->file) - 1;
            foreach(range(0, $nbr) as $index) {
                $fields['file.' . $index] = 'file|mimes:jpeg,bmp,png|max:256';
            }
        }

        $this->validate($request, $fields);

        $item->load('photo');
        $lastPosition = count($item->photo);

        $item->name = $request->name;
        $item->barcode = $request->barcode;
        $item->url_video = empty($request->url_video) ? null : $request->url_video;
        $item->text = $request->text;
        $item->brand_id = $request->brand_id;
        $item->save();

        if(is_array($request->file))
        {
            foreach($request->file as $key => $photo)
            {
                $hash = $photo->hashName();
                $folder = substr($hash, 0, 2);
                $dir = 'items' . DIRECTORY_SEPARATOR . $folder;
                $path = $photo->storeAs($dir, $item->id . $hash,'public');

                ItemPhoto::updateOrCreate([
                    'stored' => $path,
                    'item_id' => $item->id
                ], [
                    'original' => $photo->getClientOriginalName(),
                    'mime' => $photo->getMimeType(),
                    'position' => $key + $lastPosition
                ]);
            }
        }

        if(is_array($request->gallery))
        {
            foreach ($request->gallery as $key => $gallery_id)
            {
                $immagine = $item->photo->find($gallery_id);
                $immagine->title = $request->gallery_title[$key];
                $immagine->position = $key;
                $immagine->save();
            }
        }

        if(is_array($request->gallery_delete))
        {
            foreach($request->gallery_delete as $photo_id)
            {
                $immagine = $item->photo->find($photo_id);
                Storage::disk('public')->delete($immagine->stored);
                $immagine->delete();
            }
        }

        return redirect()->route('brand.items.index');
    }

    public function destroy(Request $request)
    {
        $user = $request->user();

        $item = Item::whereBrandIn($user->brands->pluck('id')->toArray())->where('id',$request->item_id)->first();

        if(! is_null($item) && $item->traceability->isEmpty())
        {
            $item->delete();
        }

        return back();
    }
}
