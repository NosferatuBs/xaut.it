<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Survey;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class ExportSurveyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function excel(Request $request, Survey $survey)
    {
        $this->authorize('view', $survey);

        $survey->load('customerAnswers.traceability.item','customerAnswers.traceability.user');
        $answer = $survey->customerAnswers;

        $array[] = ['Username','Email','Sesso','Compleanno','Articolo','Risposta','Data'];
        foreach ($survey->customerAnswers as $answer)
        {

            if($answer->traceability->user->gender == 'male')
                $gender_value = 'Uomo';
            elseif($answer->traceability->user->gender == 'female')
                $gender_value = 'Donna';
            else
                $gender_value = 'Altro';

            $array[] = [
                $answer->traceability->user->name,
                $answer->traceability->user->email,
                $gender_value,
                $answer->traceability->user->birthday,
                ($answer->traceability->item_id) ? $answer->traceability->item->name : '-',
                $answer->answer,
                $answer->created_at->format('d/m/Y H:i:s')
            ];
        }

        Excel::create($survey->question, function($excel) use($array){

            // Set the title
            $excel->setTitle('Elenco risposte');

            // Chain the setters
            $excel->setCreator('xAuthenticity')
                  ->setCompany('Consul+ srl');

            $excel->sheet('Foglio 1', function($sheet) use($array){

                $sheet->fromArray($array, null, 'A1', false, false);

            });

        })->download('xlsx');
    }
}
?>
