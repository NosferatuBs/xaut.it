<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Branch;
use App\Entities\Brand;
use App\Entities\Item;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BranchController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-code-fork";
        $title->title = "Segmenti";
        $title->description = "Elenco";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => null),
        ];

        $menu = new \stdClass();
        $menu->area = "segmenti";
        $menu->option = "elenco";

        $branches = Branch::whereBrandIn($user->brands->pluck('id')->toArray())
            ->withCount('items')
            ->withCount('surveys')
            ->withCount('datasheets')
            ->get();

        $branches->load('brand','items');

        return view('brand.branches.index')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'branches' => $branches
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-code-fork";
        $title->title = "Segmenti";
        $title->description = "Nuovo";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Segmenti', 'url' => route('brand.branches.index')),
            (object)array('title' => 'Nuovo', 'url' => null),
        ];

        $menu = new \stdClass();
        $menu->area = "segmenti";
        $menu->option = "create";

        $brands = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->orderBy('name')->get();

        return view('brand.branches.create')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'brands' => $brands
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = [
            'brand_id' => 'required',
            'name' => [
                'required',
                Rule::unique('branches')->where(function($query) use ($request){
                    $query->where('brand_id', $request->brand_id);
                })
            ]
        ];

        $this->validate($request, $fields);

        $branch = new Branch;
        $branch->brand_id = $request->brand_id;
        $branch->name = $request->name;
        $branch->text = empty($request->text) ? null : $request->text;
        $branch->save();

        return redirect()->route('brand.branches.edit', $branch->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Branch $branch)
    {
        $this->authorize('update', $branch);

        $branch->load('brand');

        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-code-fork";
        $title->title = "Segmenti";
        $title->description = "Modifica";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Segmenti', 'url' => route('brand.branches.index')),
            (object)array('title' => 'Modifica', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "segmenti";
        $menu->option = "elenco";

        $items = Item::where('brand_id', $branch->brand_id)->get();

        return view('brand.branches.edit')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'branch' => $branch,
                'items' => $items
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
        $this->authorize('update', $branch);

        $fields = [
            'name' => [
                'required',
                Rule::unique('branches')->where(function($query) use ($request){
                    $query->where('brand_id', $request->brand_id);
                })->ignore($branch->id)
            ]
        ];

        $this->validate($request, $fields);

        $branch->name = $request->name;
        $branch->text = empty($request->text) ? null : $request->text;
        $branch->items()->sync(json_decode($request->datatable));
        $branch->save();

        return redirect()->route('brand.branches.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        $this->authorize('delete', $branch);

        if($branch->surveys->count() == 0 && $branch->datasheets->count() == 0)
        {
            $branch->items()->detach();
            $branch->delete();
        }

        return back();
    }
}
