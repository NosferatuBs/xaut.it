<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Branch;
use App\Entities\Brand;
use App\Entities\Item;
use App\Entities\Survey;
use App\Entities\SurveyAnswer;
use App\Entities\SurveyType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-puzzle-piece";
        $title->title = "Sondaggi";
        $title->description = "Elenco";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => null),
        ];

        $menu = new \stdClass();
        $menu->area = "sondaggi";
        $menu->option = "elenco";

        $brands = 1;
        $brands_option = 1;

        $brands_option = Brand::with('photo')
            ->where('survey_option','>', 0)
            ->whereBrandIn($user->brands->pluck('id')->toArray())
            ->count();

        $brands = Brand::with('photo')
            ->whereBrandIn($user->brands->pluck('id')->toArray())
            ->count();

            ### SE IN UNO O PIU' BRAND E' ATTIVA LA GARANZIA
        if($brands > 0)
            {
            $surveys = Survey::withCount('customerAnswers')->withCount('branches')->whereBrandIn($user->brands->pluck('id')->toArray())->get();
            }
        else
            $surveys = array();

        if($brands > 0)
            $surveys->load('brand','surveyType','branches');

        return view('brand.surveys.index')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'surveys' => $surveys,
                'brands' => $brands,
                'brands_option' => $brands_option
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-puzzle-piece";
        $title->title = "Sondaggi";
        $title->description = "Nuovo";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Sondaggi', 'url' => route('brand.surveys.index')),
            (object)array('title' => 'Nuovo', 'url' => null),
        ];

        $menu = new \stdClass();
        $menu->area = "sondaggi";
        $menu->option = "create";

        $brands = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->orderBy('name')->get();

        $survey_types = SurveyType::all();

        return view('brand.surveys.create')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'brands' => $brands,
                'survey_types' => $survey_types
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = [
            'brand_id' => 'required',
            'question' => 'required',
            'survey_type' => 'required'
        ];

        $this->validate($request, $fields);

        $survey = new Survey;
        $survey->brand_id = $request->brand_id;
        $survey->question = $request->question;
        $survey->help = null;
        $survey->survey_type_id = $request->survey_type;
        $survey->locale = $request->locale;
        $survey->required = $request->has('required') ? true : false;
        $survey->active = false;

        $survey->save();

        // se servono le risposte vado subito a farle inserire
        $survey_type = SurveyType::find($request->survey_type);

        return redirect()->route('brand.surveys.edit', $survey->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Survey $survey)
    {
        $this->authorize('update', $survey);

        $survey->load('brand','surveyType','answers');

        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-puzzle-piece";
        $title->title = "Sondaggi";
        $title->description = "Modifica";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Sondaggi', 'url' => route('brand.surveys.index')),
            (object)array('title' => 'Modifica', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "sondaggi";
        $menu->option = "elenco";

        $branches = Branch::where('brand_id', $survey->brand_id)->get();
        $items = Item::where('brand_id', $survey->brand_id)->get();

        return view('brand.surveys.edit')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'survey' => $survey,
                'branches' => $branches,
                'items' => $items
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Survey $survey)
    {
        $this->authorize('update', $survey);

        $fields = [
            'question' => 'required'
        ];

        $this->validate($request, $fields);

        $survey->question = $request->question;
        $survey->help = $request->help;
        $survey->required = $request->has('required') ? true : false;
        $survey->active = $request->has('active') ? true : false;

        if($survey->surveyType->has_answer)
        {
            $ids = [];
            if($request->has('answer_id') && $request->has('answer_name'))
            {
                foreach ($request->answer_name as $order => $name)
                {
                    $id = $request->answer_id[$order];

                    // Ignoro le caselle vuote create dinamicamente e lasciate vuote
                    if( empty($name) )
                        continue;

                    $answer = SurveyAnswer::firstOrNew(['id' => $id, 'survey_id' => $survey->id]);
                    $answer->name = $name;
                    $answer->order = $order;

                    $survey->answers()->save($answer);

                    $ids[ $answer->id ] = true;
                }
            }
            SurveyAnswer::where('survey_id',$survey->id)
                ->whereNotIn('id',array_keys($ids))
                ->delete();

            // Se non ci sono risposte ma questo sondaggio deve averle, lo disattivo
            if(count($ids) == 0)    $survey->active = false;
        }

        $survey->branches()->sync($request->branches ?? []);
        $survey->items()->sync($request->items ?? []);
        $survey->save();

        return redirect()->route('brand.surveys.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survey $survey)
    {
        $this->authorize('delete', $survey);

        $survey->branches()->detach();
        $survey->answers()->delete();
        $survey->customerAnswers()->delete();
        $survey->delete();

        return back();
    }
}
