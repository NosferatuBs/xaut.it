<?php
	
	namespace App\Http\Controllers\Brand;
	
	use App\Entities\BrandNumber;
	use App\Entities\Item;
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	
	class ItemNumberingController extends Controller
	{
		
		public function edit(Request $request, Item $item)
		{
			$title = new \stdClass();
			$title->icon = "fa fa-cubes";
			$title->title = "Articoli";
			$title->description = "Numerazione";
			
			$breadcumbs = [
				(object)array('title' => 'Home', 'url' => route('brand.dashboard')),
				(object)array('title' => 'Elenco', 'url' => route('brand.items.index')),
				(object)array('title' => 'Numerazione', 'url' => null)
			];
			
			$menu = new \stdClass();
			$menu->area = "articoli";
			$menu->option = "elenco";
			
			$item->load(['photo' => function ($query)
			{
				$query->orderBy('position');
			}]);
			
			$numbering = BrandNumber::where('item_id', $item->id)->paginate();
			
			return view('brand.items.numbering')
				->with([
					'title' => $title,
					'breadcumbs' => $breadcumbs,
					'menu' => $menu,
					'item' => $item,
					'numbering' => $numbering
				]);
		}
	}
