<?php

namespace App\Http\Controllers\Brand;

use App\Entities\Brand;
use App\Entities\BrandNumber;
use App\Entities\Item;
use App\Http\Controllers\Controller;
use App\Libraries\Hologram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BrandNumberingController extends Controller
{
    public function edit(Request $request, Brand $brand)
    {
        $title = new \stdClass();
        $title->icon = "fa fa-shield";
        $title->title = "Marchi";
        $title->description = "Numerazione sequenziale";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => route('brand.brands.index')),
            (object)array('title' => 'Numerazione', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "marchi";
        $menu->option = "elenco";

        $brand->load(['photo' => function($query) {
            $query->orderBy('position');
        }]);

        $numbering = BrandNumber::join("items", "brand_numbers.item_id", "=", "items.id")
                                ->where('brand_numbers.brand_id', $brand->id)->paginate();

        return view('brand.brands.numbering')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'brand' => $brand,
                'numbering' => $numbering
            ]);
    }
}
