<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Brand;
use App\Entities\BrandNumber;
use App\Entities\Item;
use App\Entities\ItemPhoto;
use App\Entities\QrcodeBrand;
use App\Http\Controllers\Controller;
use App\Libraries\ExcelBinderString;
use App\Libraries\Hologram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;

class ItemImportController extends Controller
{
	public function __construct()
    {

	}

    public function qrcodeSelect(Request $request)
    {
        //$this->authorize('import_qrcodes');

        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-upload";
        $title->title = "Importazione Qr Code da Excel";
        $title->description = "Esclusivamente in formato XSLX";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => '/home'),
            (object)array('title' => 'Importazione', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "import";
        $menu->option = "qrcodes";

        if($user->hasRole('admin'))
        {
            $brands = Brand::orderBy('name')->get();
        }
        elseif($user->hasRole('brandowner'))
        {
            $brands = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->orderBy('name')->get();
        }


        return view('import.qrcodes.all', compact('title','breadcumbs','menu','brands'));
    }

    public function qrcodeLoad(Request $request)
    {
        //$this->authorize('import_qrcodes');

        $user = $request->user();
        $fields = [
            'brand_id' => 'required',
            'file' => array('required','file'),
        ];
        $this->validate($request, $fields);

        $file = $request->file;
        $dir = 'users/' . $user->id . '/import';

        $path = $file->storeAs($dir, 'qrcode.xlsx');
        $path = storage_path('app/' . $path);

        $brand = Brand::findOrFail($request->brand_id);

        if($brand->level == 1)
        {
            $count = 0;
            $success = 0;
            $unsuccess = 0;

            Excel::load($path, function($reader) use ($brand, &$count, &$success, &$unsuccess){
                $array = $reader->toArray();
                $count = count($array);

                foreach ($array as $row) {
                    $sku = $row[0];
                    $qr_code = $row[1];
                    $price = $row[2];
                    $discount = $row[3];


                    if(! empty($sku))
                    {
                        $item = Item::where(['name' => $sku, 'brand_id' => $brand->id])->first();

                        if(!is_null($item))
                            $item_id = $item->id;
                        else
                            $item_id = null;
                    }
                    else
                        $item_id = null;

                    $qrcode = new QrcodeBrand;

                    $qrcode->brand_id = $brand->id;
                    $qrcode->item_id = $item_id;
                    $qrcode->qrcode = $qr_code;
                    $qrcode->price = $price;
                    $qrcode->discount_percentage = $discount;

                    try
                        {
                        $qrcode->save();
                        $success++;
                        }
                    catch(\Illuminate\Database\QueryException $e)
                        {
                        $unsuccess++;
                        }



                }
            });

            return back()->with(['success' => $success, 'unsuccess' => $unsuccess, 'count' => $count]);
        }

        return back();
    }

	public function numberSelect(Request $request)
	{
        $this->authorize('import_numbers');

        $user = $request->user();

		$title = new \stdClass();
        $title->icon = "fa fa-upload";
        $title->title = "Importazione articoli da Excel";
        $title->description = "Esclusivamente in formato XSLX";

    	$breadcumbs = [
    		(object)array('title' => 'Home', 'url' => '/home'),
    		(object)array('title' => 'Importazione', 'url' => null)
    	];

    	$menu = new \stdClass();
        $menu->area = "import";
        $menu->option = "numbers";

        if($user->hasRole('admin'))
        {
            $brands = Brand::whereHasHologram()->orderBy('name')->get();
        }
        elseif($user->hasRole('brandowner'))
        {
            $brands = Brand::whereHasHologram()->whereBrandIn($user->brands->pluck('id')->toArray())->orderBy('name')->get();
        }


		return view('import.holograms.all', compact('title','breadcumbs','menu','brands'));
	}

    public function numberLoad(Request $request)
    {
        $this->authorize('import_numbers');

        $user = $request->user();
        $fields = [
            'brand_id' => 'required',
            'file' => array('required','file'),
        ];
        $this->validate($request, $fields);

        $file = $request->file;
        $dir = 'users/' . $user->id . '/import';

        $path = $file->storeAs($dir, 'numerazione.xlsx');
        $path = storage_path('app/' . $path);

        $brand = Brand::findOrFail($request->brand_id);

        if($brand->level == 1)
        {
            $count = 0;
            $success = 0;

            Excel::load($path, function($reader) use ($brand, &$count, &$success){
                $array = $reader->toArray();
                $count = count($array);

                foreach ($array as $row) {
                    $sku = $row[1];
                    $first_char = $row[2];
                    $second_char = $row[3];
                    $start = $row[4];
                    $end = $row[5];

                    if(empty($start))
                        continue;

                    if($start == $end or empty($end))
                        $end = null;

                    $inizio = $start;
                    $fine = $end ?? $start;

                    $first_char = empty($first_char) ? null : strtoupper($first_char);
                    $second_char = empty($second_char) ? null : strtoupper($second_char);

                    if(! Hologram::hasConflictNumber($first_char, $inizio, $fine, $second_char))
                    {
                        $item_id = null;

                        if(! empty($sku))
                        {
                            $item = Item::where(['name' => $sku, 'brand_id' => $brand->id])->first();
                            if(is_null($item))  continue;

                            $item_id = $item->id;
                        }

                        $number = new BrandNumber;

                        $number->brand_id = $brand->id;
                        $number->item_id = $item_id;
                        $number->first_char = $first_char;
                        $number->start = $start;
                        $number->end = $end;
                        $number->second_char = $second_char;

                        $number->save();

                        $success++;
                    }
                }
            });

            return back()->with(['success' => $success, 'count' => $count]);
        }

        return back();
    }

    public function photoSelect(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-upload";
        $title->title = "Importazione foto da computer";
        $title->description = "In formato jpeg o png";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => '/home'),
            (object)array('title' => 'Importazione', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "import";
        $menu->option = "photos";

        if($user->hasRole('admin'))
        {
            $brands = Brand::orderBy('name')->get();
        }
        elseif($user->hasRole('brandowner'))
        {
            $brands = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->orderBy('name')->get();
        }


        return view('import.photo.dropzone', compact('title','breadcumbs','menu','brands'));
    }

    public function photoLoad(Request $request)
    {
        $user = $request->user();

        $fields = [
            'brand_id' => 'required'
        ];
        $nbr = count($request->file) - 1;
        foreach(range(0, $nbr) as $index) {
            $fields['file.' . $index] = 'file|mimes:jpeg,bmp,png|max:256';
        }

        $validation = Validator::make($request->all(), $fields);

        if($validation->fails())
        {
            return Response::make($validation->errors()->first(), 400);
        }

        if($user->hasRole('admin'))
        {
            $brand = Brand::find($request->brand_id);
        }
        elseif($user->hasRole('brandowner'))
        {
            $brand = Brand::whereBrandIn($user->brands->pluck('id')->toArray())
                ->where('id',$request->brand_id)
                ->first();
        }

        if(is_null($brand))
        {
            return Response::make('Marchio non trovato', 400);
        }

        if(is_array($request->file))
        {
            foreach($request->file as $photo)
            {
                $original = $photo->getClientOriginalName();
                $codice = substr($original, 0, strrpos($original, "-"));

                $item = Item::where(['name' => $codice, 'brand_id' => $brand->id])->first();

                if(! is_null($item) )
                {
                    $hash = $photo->hashName();
                    $folder = substr($hash, 0, 2);
                    $dir = 'items' . '/' . $folder;
                    $path = $photo->storeAs($dir, $item->id . $hash,'public');

                    ItemPhoto::updateOrCreate(['stored' => $path, 'item_id' => $item->id], ['original' => $photo->getClientOriginalName(), 'mime' => $photo->getMimeType()]);
                }
                else
                {
                    return Response::make('Articolo ' . $codice . ' non trovato', 400);
                }
            }
        }

        return Response::json('success',200);
    }

    public function itemSelect(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-upload";
        $title->title = "Importazione articoli da Excel";
        $title->description = "Esclusivamente in formato XSLX";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => '/home'),
            (object)array('title' => 'Importazione', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "import";
        $menu->option = "items";

        if($user->hasRole('admin'))
        {
            $brands = Brand::orderBy('name')->get();
        }
        elseif($user->hasRole('brandowner'))
        {
            $brands = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->orderBy('name')->get();
        }

        return view('import.items.items', compact('title','breadcumbs','menu','brands'));
    }

    public function itemLoad(Request $request)
    {
        $user = $request->user();
        $fields = [
            'brand_id' => 'required',
            'file' => array('required','file'),
        ];
        $this->validate($request, $fields);

        $file = $request->file;
        $dir = 'users/' . $user->id . '/import';

        $path = $file->storeAs($dir, 'articoli.xlsx');
        $path = storage_path('app/' . $path);

        if($user->hasRole('admin'))
        {
            $brand = Brand::findOrFail($request->brand_id);
        }
        elseif($user->hasRole('brandowner'))
        {
            $brand = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->findOrFail($request->brand_id);
        }


        $myValueBinder = new ExcelBinderString;
        $array = Excel::load($path)->get();

        $count = $array->count();
        $success = 0;
        foreach ($array->toArray() as $row) {
            $barcode = $row[1];
            $sku = $row[2];
            $text = $row[3];

            $validator = Validator::make(['barcode' => $barcode, 'name' => $sku, 'text' => $text], [
                'barcode' => [
                    'required',
                    'numeric',
                    Rule::unique('items'),
                    Rule::unique('barcode_item','barcode')
                ],
                'name' => 'required',
                'text' => 'required',
            ]);

            if (! $validator->fails())
            {
                $item = Item::updateOrCreate( ['barcode' => $row[1], 'name' => $row[2], 'brand_id' => $brand->id],['text' => $row[3]] );
                $success++;
            }
        }

        return back()->with(['success' => $success, 'count' => $count]);
    }
}
