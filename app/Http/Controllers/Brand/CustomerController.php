<?php
namespace App\Http\Controllers\Brand;

use App\Entities\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class CustomerController extends Controller
{

    public function index(Request $request)
    {
    	$user = $request->user();

    	$title = new \stdClass();
        $title->icon = "fa fa-users";
        $title->title = "Clienti";
        $title->description = "Elenco";

    	$breadcumbs = [
    		(object)array('title' => 'Home', 'url' => route('brand.dashboard')),
    		(object)array('title' => 'Elenco', 'url' => null)
    	];

    	$menu = new \stdClass();
        $menu->area = "clienti";
        $menu->option = "elenco";

        $form = new \stdClass();
        $form->gdpr_newsletter_consent = 0;
        $form->gdpr_profilation_consent = 0;

    	$query = User::select('users.id', 'users.name', 'users.email')
    		->leftJoin('user_workgroup','users.id','user_workgroup.user_id')
    		->join('traceability','users.id','traceability.user_id')
    		->join('items','traceability.item_id','items.id')
    		->whereNull('user_workgroup.user_id')
    		->whereIn('items.brand_id', $user->brands->pluck('id')->toArray());

        if ($request->has('gdpr_newsletter_consent'))
        {
            $form->gdpr_newsletter_consent = $request->get('gdpr_newsletter_consent');
            if ($form->gdpr_newsletter_consent != null)
            {
                $query->where('users.gdpr_newsletter_consent', $form->gdpr_newsletter_consent - 1);
            }
        }

        if ($request->has('gdpr_profilation_consent'))
        {
            $form->gdpr_profilation_consent = $request->get('gdpr_profilation_consent');
            if ($form->gdpr_profilation_consent != null)
            {
                $query->where('users.gdpr_profilation_consent', $form->gdpr_profilation_consent - 1);
            }
        }

    	$customers = $query->groupBy('users.id', 'users.name', 'users.email')
    		->get();

    	return view('brand.customers.index')
            ->with([
                'title' => $title,
                'form' => $form,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'customers' => $customers
            ]);
    }

    public function show(Request $request, $id)
    {
    	$user = $request->user();

    	$customer = User::select('users.id', 'users.name', 'users.email','users.gender','users.birthday','users.city')
    		->leftJoin('user_workgroup','users.id','user_workgroup.user_id')
    		->join('traceability','users.id','traceability.user_id')
    		->join('items','traceability.item_id','items.id')
    		->where('users.id', $id)
    		->whereNull('user_workgroup.user_id')
    		->whereIn('items.brand_id', $user->brands->pluck('id')->toArray())
    		->groupBy('users.id', 'users.name', 'users.email','users.gender','users.birthday','users.city')
    		->firstOrFail();

    	$title = new \stdClass();
        $title->icon = "fa fa-users";
        $title->title = "Clienti";
        $title->description = "Dettagli";

    	$breadcumbs = [
    		(object)array('title' => 'Home', 'url' => route('brand.dashboard')),
    		(object)array('title' => 'Elenco', 'url' => route('brand.customers.index')),
    		(object)array('title' => 'Dettagli', 'url' => null)
    	];

    	$menu = new \stdClass();
        $menu->area = "clienti";
        $menu->option = "elenco";

    	return view('brandowner.customers.show')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'customer' => $customer
            ]);
    }
    public function excel(Request $request)
    {
        $user = $request->user();

       $customers = User::select('users.id', 'users.name', 'users.email','users.gender','users.birthday','users.city')
            ->leftJoin('user_workgroup','users.id','user_workgroup.user_id')
            ->join('traceability','users.id','traceability.user_id')
            ->join('items','traceability.item_id','items.id')
            ->whereNull('user_workgroup.user_id')
            ->whereIn('items.brand_id', $user->brands->pluck('id')->toArray())
            ->groupBy('users.id', 'users.name', 'users.email','users.gender','users.birthday','users.city')
            ->get();

        $array[] = ['Username','Email','Sesso','Compleanno','Città'];
        foreach ($customers as $customer)
        {
            if($customer->gender == 'male')
                $gender_value = 'Uomo';
            elseif($customer->gender == 'female')
                $gender_value = 'Donna';
            else
                $gender_value = 'Altro';

            $array[] = [
                $customer->name,
                $customer->email,
                $gender_value,
                $customer->birthday,
                $customer->city
            ];
        }

        Excel::create('elenco-clienti', function($excel) use($array){

            // Set the title
            $excel->setTitle('Elenco clienti');

            // Chain the setters
            $excel->setCreator('xAuthenticity')
                  ->setCompany('Consul+ srl');

            $excel->sheet('Foglio 1', function($sheet) use($array){

                $sheet->fromArray($array, null, 'A1', false, false);

            });

        })->download('xlsx');
    }
}
