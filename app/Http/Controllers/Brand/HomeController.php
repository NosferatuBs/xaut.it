<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Brand;
use App\Http\Controllers\Controller;
use App\Entities\Item;
use App\Entities\ItemPhoto;
use App\Entities\Traceability;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    protected $title;
    protected $menu;
    protected $breadcumbs;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = new \stdClass();
        $this->menu = new \stdClass();
        $this->breadcumbs = [];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        $user = $request->user();

        $this->title->icon = "fa fa-dashboard";
        $this->title->title = "Riepilogo";
        $this->title->description = null;

        $this->breadcumbs = [
            (object)array('title' => 'Home', 'url' => null),
        ];

        $this->menu->area = "dashboard";
        $this->menu->option = null;

        $brands = $user->brands->pluck('id')->toArray();
        /*
        Ultime 10 ricerche del prodotto
        */
        $ricerche = Traceability::with(['user','item','brand'])
            ->select('traceability.*')
            ->whereBrandIn($brands)
            ->latest()
            ->limit(10)
            ->get();
        /*
        Totale dei marchi e ultima data inserimento
        */
        $marchi = Brand::select(DB::raw('COUNT(id) as totale, MAX(created_at) ultimo'))
            ->whereBrandIn($brands)
            ->first();
        $marchi->ultimo = $this->checkDate($marchi->ultimo);
        /*
        Totale articoli e ultima data inserimento
        */
        $articoli = Item::select(DB::raw('COUNT(id) as totale, MAX(created_at) ultimo'))
            ->whereBrandIn($brands)
            ->first();
        $articoli->ultimo = $this->checkDate($articoli->ultimo);
        /*
        Totale ricerche e ultima data di ricerca
        */
        $tracciabilita = Traceability::select(DB::raw('COUNT(traceability.id) as totale, MAX(traceability.created_at) ultimo'))
            ->whereBrandIn($brands)
            ->first();
        $tracciabilita->ultimo = $this->checkDate($tracciabilita->ultimo);
        /*
        Totale foto e ultima data di inserimento
        */
        $foto = ItemPhoto::select(DB::raw('COUNT(item_photo.id) as totale, MAX(item_photo.created_at) ultimo'))
            ->whereBrandIn($brands)
            ->first();
        $foto->ultimo = $this->checkDate($foto->ultimo);

        $title = $this->title;
        $menu = $this->menu;
        $breadcumbs = $this->breadcumbs;

        return view('brand.dashboard')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'ricerche' => $ricerche,
                'marchi' => $marchi,
                'articoli' => $articoli,
                'tracciabilita' => $tracciabilita,
                'foto' => $foto
            ]);
    }

    protected function checkDate($date)
    {
        if(! is_null($date))
            return Carbon::parse($date)->format('d/m/Y H:i');

        return "--/--/---- --:--";
    }
}
?>
