<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Brand;
use App\Entities\DiscountActivation;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class CouponController extends Controller
{
    public function index(Request $request, $flag)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-cubes";
        $title->title = "Coupon";
        $title->description = "Elenco";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "coupon";
        $menu->option = "elenco";

        $activations = DiscountActivation::select('discounts.token', 'discount_activations.created_at', 'discount_activations.vat', 'brands.name', 'items.barcode', 'discount_activations.checked_at', 'discount_activations.id', 'discount_activations.checked_at', 'discounts.amount')
            ->join('discounts','discount_activations.discount_id','discounts.id')
            ->join('traceability','discounts.traceability_id','traceability.id')
            ->join('brands','traceability.brand_id','brands.id')
            ->leftJoin('items','traceability.item_id','items.id')
            ->whereIn('brands.id', $user->brands->pluck('id')->toArray())
            ->orderBy('brands.name');

        if($flag == 1)
            $activations->whereNotNull('checked_at');
        else
            $activations->whereNull('checked_at');

        $activations = $activations->paginate();

        return view('brand.coupon.index')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'activations' => $activations,
                'flag' => $flag
            ]);

    }

    public function process(Request $request)
    {
    $user = $request->user();

    $check_activation = DiscountActivation::where('discount_activations.id', $request->activation_id)
        ->join('discounts','discount_activations.discount_id','discounts.id')
        ->join('traceability','discounts.traceability_id','traceability.id')
        ->whereIn('traceability.brand_id', $user->brands->pluck('id')->toArray())
        ->FirstOrFail();

    $activation = DiscountActivation::where('discount_activations.id', $request->activation_id)
        ->FirstOrFail();

    if($request->flag == 1)
        $activation->checked_at = null;
    else
        $activation->checked_at = Carbon::now()->toDateTimeString();

    $activation->save();

    return redirect()->route('brand.coupon.list', $request->flag);

    }

}
