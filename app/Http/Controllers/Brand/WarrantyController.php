<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Brand;
use App\Entities\Warranty;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class WarrantyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-ticket";
        $title->title = "Garanzia";
        $title->description = "Elenco dei prodotti con la garanzia attiva";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => null),
        ];

        $menu = new \stdClass();
        $menu->area = "garanzia";
        $menu->option = "elenco";

        $brands = 1;
        $brands_option = 1;

        $brands_option = Brand::with('photo')
            ->where('warranty_extension','>', 0)
            ->whereBrandIn($user->brands->pluck('id')->toArray())
            ->count();

        $brands = Brand::with('photo')
            ->whereBrandIn($user->brands->pluck('id')->toArray())
            ->count();

            ### SE IN UNO O PIU' BRAND E' ATTIVA LA GARANZIA
        if($brands_option > 0)
            {
            $warranties = Warranty::whereHas('traceability', function($query) use($user){
                $query->whereBrandIn($user->brands->pluck('id')->toArray());
            })
            ->join('traceability', 'warranties.traceability_id', '=', 'traceability.id')
            ->join('brands', 'traceability.brand_id', '=', 'brands.id')
            ->where('warranty_extension', '>', '0')
            ->get();

            }
        else
            $warranties = collect([]);

        if($brands > 0 && $warranties->count() > 0)
            $warranties->load('traceability.brand','traceability.item','traceability.user');

        return view('brand.warranties.index')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'warranties' => $warranties,
                'brands' => $brands,
                'brands_option' => $brands_option
            ]);
    }

    public function show(Request $request, Warranty $warranty)
    {
        $this->authorize('view',$warranty);

        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-ticket";
        $title->title = "Garanzia";
        $title->description = "Dettagli del prodotto in garanzia";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => route('brand.warranties.index')),
            (object)array('title' => 'Dettagli', 'url' => null),
        ];

        $menu = new \stdClass();
        $menu->area = "garanzia";
        $menu->option = "elenco";

        $warranty->load('traceability.brand','traceability.item','traceability.user');

        $item = null;
        if($warranty->traceability->item_id)
        {
            $item = $warranty->traceability->item;
        }

        return view('brand.warranties.show')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'warranty' => $warranty,
                'item' => $item
            ]);
    }
}
