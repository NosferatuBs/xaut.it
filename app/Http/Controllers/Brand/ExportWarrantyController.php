<?php
namespace App\Http\Controllers\Brand;

use App\Entities\Warranty;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExportWarrantyController extends Controller
{
    public function __construct()
    {

    }

    public function pdf(Request $request, $token)
    {
    	//$user = $request->user();

        $warranty = Warranty::select('warranties.*')
            ->leftJoin('traceability','traceability_id','traceability.id')
            ->where('token', $token)
            ->firstOrFail();

		$warranty->load(['traceability.brand.photo' => function($query) {
                $query->orderBy('position');
            },'traceability.item']);

		$pdf = \PDF::loadView('template.pdf.warrantyCertification', compact('warranty'))->setPaper('a4', 'landscape');
		return $pdf->stream('certificato.pdf');
    }
}
