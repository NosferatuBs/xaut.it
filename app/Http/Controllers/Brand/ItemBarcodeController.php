<?php
namespace App\Http\Controllers\Brand;

use App\Entities\BarcodeItem;
use App\Entities\Item;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class ItemBarcodeController extends Controller
{

    public function edit(Request $request, Item $item)
	{

		$item->load(['photo' => function($query) {
			$query->orderBy('position');
		}]);

		$title = new \stdClass();
		$title->icon = "fa fa-barcode";
		$title->title = "Barcode";
		$title->description = "Codici a barre alternativi per l'articolo";

		$breadcumbs = [
			(object)array('title' => 'Home', 'url' => route('brand.dashboard')),
			(object)array('title' => 'Articoli', 'url' => route('brand.items.index')),
			(object)array('title' => 'Barcode', 'url' => null)
		];

		$menu = new \stdClass();
		$menu->area = "articoli";
		$menu->option = "elenco";

		$barcodes = BarcodeItem::where('item_id', $item->id)->paginate();

		return view('brand.items.barcode')
			->with([
				'title' => $title,
				'breadcumbs' => $breadcumbs,
				'menu' => $menu,
				'item' => $item,
				'barcodes' => $barcodes
			]);
	}

	public function update(Request $request, Item $item)
	{
		$array['barcode'] = explode("\r\n", $request->barcode);

		Validator::make($array, [
		    'barcode.*' => [
		    	'required',
		    	'numeric',
		    	Rule::unique('items','barcode'),
		    	Rule::unique('barcode_item','barcode')
		    ]
		])->validate();

		foreach ($array['barcode'] as $value)
		{
    		$row = new BarcodeItem();

    		$row->item_id = $item->id;
    		$row->barcode = $value;
    		$row->save();
		}

		return back();
	}

	public function destroy(Request $request, Item $item)
	{
		$barcode = BarcodeItem::findOrFail($request->barcode_id);

		$barcode->delete();

		return back();
	}
}
?>
