<?php

namespace App\Http\Controllers\Brand\Account;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{

    public function edit(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-id-card";
        $title->title = "Profilo";
        $title->description = "Aggiornamento delle informazioni personali";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Profilo', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "dashboard";
        $menu->option = "";

        return view('brand.account.profile')
            ->with([
                'user' => $user,
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu
            ]);
    }

    public function update(Request $request)
    {
        $user = $request->user();

        $fields = [
            'name' => 'required',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($user->id)
            ],
            'city' => 'required',
            'gender' => 'required|in:male,female,other',
            'birthday' => 'required|date_format:d/m/Y'
        ];

        $this->validate($request, $fields);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->gender = $request->gender;
        $user->city = $request->city;
        $user->birthday = Carbon::createFromFormat('d/m/Y', $request->birthday);
        $user->save();

        return redirect()->route('brand.dashboard');
    }
}
