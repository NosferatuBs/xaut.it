<?php

namespace App\Http\Controllers\Brand\Account;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PasswordController extends Controller
{

    public function edit(Request $request)
    {
        $menu = new \stdClass();

        $title = new \stdClass();
        $title->icon = "fa fa-key";
        $title->title = "Password";
        $title->description = "Modifica della password di accesso";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Password', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "dashboard";
        $menu->option = "";

        return view('brand.account.password')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu
            ]);
    }

    public function update(Request $request)
    {
        $user = $request->user();

        $fields = [
            'current_password' => 'current_password',
            'password' => 'required|confirmed'
        ];

        $this->validate($request, $fields);

        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('brand.dashboard');
    }
}
