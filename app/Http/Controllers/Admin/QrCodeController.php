<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Brand;
use App\Entities\QrcodeBrand;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class QrcodeController extends Controller
{
 	public function __construct()
    {
    }

    public function all(Request $request, Brand $brand)
    {

        $title = new \stdClass();
        $title->icon = "fa fa-qrcode";
        $title->title = "Qrcode";
        $title->description = "Qr Code per il marchio";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => '/home'),
            (object)array('title' => 'Marchi', 'url' => '/brands'),
            (object)array('title' => 'Qrcode', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "marchi";
        $menu->option = "elenco";

        $qrcodes = QrcodeBrand::where('brand_id', $brand->id)->get();

        return view('qrcode.all', compact('title','breadcumbs','menu','brand','qrcodes'));
    }

    public function insert(Request $request, Brand $brand)
    {
        $array['qrcode'] = explode("\r\n", $request->qrcode);

        Validator::make($array, [
            'qrcode.*' => [
                'required',
                Rule::unique('qrcode_brand','qrcode')
            ]
        ])->validate();

        foreach ($array['qrcode'] as $value)
        {
            $row = new QrcodeBrand();

            $row->brand_id = $brand->id;
            $row->qrcode = $value;
            $row->save();
        }

        return back();
    }

    public function destroy(Request $request, Brand $brand)
    {
        $qrcode = QrcodeBrand::findOrFail($request->qrcode_id);

        $qrcode->delete();

        return back();
    }

}
