<?php
namespace App\Http\Controllers\Admin;

use App\Entities\Brand;
use App\Entities\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserBrandController extends Controller
{
    public function edit(User $user)
    {
        $title = new \stdClass();
        $title->icon = "fa fa-users";
        $title->title = "Utenti";
        $title->description = "Permessi utente";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('admin.dashboard')),
            (object)array('title' => 'Elenco', 'url' => route('admin.users.index')),
            (object)array('title' => 'Permessi utente', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "utenti";
        $menu->option = "elenco";

        $brands = Brand::all()->sortBy('name');
        $selected = $user->brands()->get();

        return view('admin.usersBrand.edit', compact('user','title','breadcumbs','menu','brands','selected'));
    }

    public function update(Request $request, User $user)
    {
        if(is_null($request->brands))
        {
            $request->brands = [];
        }

        $user->brands()->sync($request->brands);

        return redirect()->route('admin.users.index');
    }
}
