<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Brand;
use App\Entities\Item;
use App\Entities\ItemPhoto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class ItemImportController extends Controller
{
    protected $title;
    protected $menu;
    protected $breadcumbs;

    public function __construct()
    {
        $this->middleware('auth');

        $this->title = new \stdClass();
        $this->menu = new \stdClass();
        $this->breadcumbs = [];
    }

    public function itemSelect(Request $request)
    {
        $user = $request->user();

        $this->title->icon = "fa fa-cubes";
        $this->title->title = "Importazione articoli";
        $this->title->description = "Inserimento";

        $this->breadcumbs = [
            (object)array('title' => 'Home', 'url' => '/home'),
            (object)array('title' => 'Inserimento', 'url' => null)
        ];

        $this->menu->area = "importazione";
        $this->menu->option = "elenco";

        $brands = Brand::orderBy('name')->get();

        return view('admin.import.items.select', ['title' => $this->title, 'breadcumbs' => $this->breadcumbs, 'menu' => $this->menu, 'brands' => $brands]);
    }



    public function itemLoad(Request $request)
    {
    	$fields = [
    		'brand_id' => 'required',
            'textarea' => 'required'
            ];

        $this->validate($request, $fields);

    }

}
