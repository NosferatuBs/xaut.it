<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Repositories\SyncRepository;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    public function importBrands(Request $request)
    {
        SyncRepository::syncBrands();
    }

    public function importNumbers(Request $request, $brandId)
    {
        SyncRepository::syncNumbers($brandId);
    }
}
