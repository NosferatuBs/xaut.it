<?php
	
	namespace App\Http\Controllers\Admin;
	
	use App\Entities\Brand;
	use App\Entities\BrandNumber;
	use App\Entities\Item;
	use App\Http\Controllers\Controller;
	use App\Libraries\Hologram;
	use Illuminate\Http\Request;
	
	class NumbersController extends Controller
	{
		public function __construct()
		{
			$this->middleware('auth');
		}
		
		public function itemEdit(Item $item)
		{
			$title = new \stdClass();
			$title->icon = "fa fa-cubes";
			$title->title = "Articoli";
			$title->description = "Numerazione";
			
			$breadcumbs = [
				(object)array('title' => 'Home', 'url' => '/home'),
				(object)array('title' => 'Elenco', 'url' => '/items'),
				(object)array('title' => 'Articolo', 'url' => '/items' . DIRECTORY_SEPARATOR . $item->id),
				(object)array('title' => 'Numerazione', 'url' => null)
			];
			
			$menu = new \stdClass();
			$menu->area = "articoli";
			$menu->option = "elenco";
			
			$item->load(['photo' => function ($query)
			{
				$query->orderBy('position');
			}, 'numbers']);
			
			foreach ($item->numbers as $number)
			{
				$number->start = str_pad($number->start, 6, "0", STR_PAD_LEFT);
				if (is_null($number->end))
					$number->end = $number->start;
				else
					$number->end = str_pad($number->end, 6, "0", STR_PAD_LEFT);
			}
			
			return view('items.numbers', compact('item', 'title', 'breadcumbs', 'menu'));
		}
		
		public function itemUpdate(Request $request, Item $item)
		{
			$fields = [
				'first_char' => 'nullable|alpha',
				'start' => 'required|numeric',
				'end' => 'nullable|numeric',
				'second_char' => 'nullable|alpha',
			];
			
			$this->validate($request, $fields);
			
			if ($request->start == $request->end)
				$request->end = null;
			
			$inizio = $request->start;
			$fine = $request->end ?? $request->start;
			$request->first_char = strtoupper($request->first_char);
			$request->second_char = is_null($request->second_char) ? null : strtoupper($request->second_char);
			
			if (!Hologram::hasConflictNumber($request->first_char, $inizio, $fine, $request->second_char))
			{
				$number = new BrandNumber;
				
				$number->brand_id = $item->brand_id;
				$number->item_id = $item->id;
				$number->first_char = $request->first_char;
				$number->start = $request->start;
				$number->end = $request->end;
				$number->second_char = $request->second_char;
				
				$number->save();
				
				return back();
			}
			
			return back()->withInput()->withErrors(['code' => 'The code already exist']);
		}
		
		public function itemDelete(Request $request, Item $item)
		{
			BrandNumber::where(['item_id' => $item->id, 'id' => $request->number_id])->delete();
			
			return back();
		}
		
		public function brandEdit(Brand $brand)
		{
			$title = new \stdClass();
			$title->icon = "fa fa-shield";
			$title->title = "Marchi";
			$title->description = "Numerazione sequenziale";
			
			$breadcumbs = [
				(object)array('title' => 'Home', 'url' => '/home'),
				(object)array('title' => 'Elenco', 'url' => '/brands'),
				(object)array('title' => 'Marchio', 'url' => '/brands' . DIRECTORY_SEPARATOR . $brand->id),
				(object)array('title' => 'Numerazione', 'url' => null)
			];
			
			$menu = new \stdClass();
			$menu->area = "marchi";
			$menu->option = "elenco";
			
			$numbering = BrandNumber::join("items", "brand_numbers.item_id", "=", "items.id")
				->where('brand_numbers.brand_id', $brand->id)->paginate();
			
			return view('admin.brands.numbers', compact('brand', 'title', 'breadcumbs', 'menu', 'numbering'));
		}
		
		public function brandUpdate(Request $request, Brand $brand)
		{
			$fields = [
				'first_char' => 'nullable|alpha',
				'start' => 'required|numeric',
				'end' => 'nullable|numeric',
				'second_char' => 'nullable|alpha',
			];
			
			$this->validate($request, $fields);
			
			if ($request->start == $request->end)
				$request->end = null;
			
			$inizio = $request->start;
			$fine = $request->end ?? $request->start;
			
			$request->first_char = strtoupper($request->first_char);
			$request->second_char = is_null($request->second_char) ? null : strtoupper($request->second_char);
			
			if (!Hologram::hasConflictNumber($request->first_char, $inizio, $fine, $request->second_char))
			{
				$number = new BrandNumber;
				
				$number->brand_id = $brand->id;
				$number->item_id = null;
				$number->first_char = $request->first_char;
				$number->start = $request->start;
				$number->end = $request->end;
				$number->second_char = $request->second_char;
				
				$number->save();
				
				return back();
			}
			
			return back()->withInput()->withErrors(['code' => 'The code already exist']);
		}
		
		public function brandDelete(Request $request, Brand $brand)
		{
			BrandNumber::where(['brand_id' => $brand->id, 'id' => $request->number_id])->delete();
			
			return back();
		}
	}
