<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Brand;
use App\Http\Controllers\Controller;
use App\Entities\Item;
use App\Entities\ItemPhoto;
use App\Entities\Shop;
use App\Entities\Traceability;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    protected $title;
    protected $menu;
    protected $breadcumbs;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->title = new \stdClass();
        $this->menu = new \stdClass();
        $this->breadcumbs = [];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        $user = $request->user();

        $this->title->icon = "fa fa-dashboard";
        $this->title->title = "Riepilogo";
        $this->title->description = null;

        $this->breadcumbs = [
            (object)array('title' => 'Home', 'url' => null),
        ];

        $this->menu->area = "dashboard";
        $this->menu->option = null;

        /*
        Ultime 10 ricerche del prodotto
        */
        $ricerche = Traceability::with(['user','item','brand'])
            ->latest()
            ->limit(10)
            ->get();

        /*
        Totale dei marchi e ultima data inserimento
        */
        $marchi = Brand::select(DB::raw('COUNT(id) as totale, MAX(created_at) ultimo'))
            ->first();
        $marchi->ultimo = $this->checkDate($marchi->ultimo);
        /*
        Totale articoli e ultima data inserimento
        */
        $articoli = Item::select(DB::raw('COUNT(id) as totale, MAX(created_at) ultimo'))
            ->first();
        $articoli->ultimo = $this->checkDate($articoli->ultimo);
        /*
        Totale ricerche e ultima data di ricerca
        */
        $tracciabilita = Traceability::select(DB::raw('COUNT(id) as totale, MAX(created_at) ultimo'))
            ->first();
        $tracciabilita->ultimo = $this->checkDate($tracciabilita->ultimo);
        /*
        Totale foto e ultima data di inserimento
        */
        $foto = ItemPhoto::select(DB::raw('COUNT(id) as totale, MAX(created_at) ultimo'))
            ->first();
        $foto->ultimo = $this->checkDate($foto->ultimo);

        return view('admin.dashboard')
            ->with([
                'title' => $this->title,
                'breadcumbs' => $this->breadcumbs,
                'menu' => $this->menu,
                'ricerche' => $ricerche,
                'marchi' => $marchi,
                'articoli' => $articoli,
                'tracciabilita' => $tracciabilita,
                'foto' => $foto
            ]);
    }

    protected function checkDate($date)
    {
        if(! is_null($date))
            return Carbon::parse($date)->format('d/m/Y H:i');

        return "--/--/---- --:--";
    }
}
?>
