<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Brand;
use App\Http\Controllers\Controller;
use App\Entities\Shop;
use App\Entities\User;
use App\Entities\Workgroup;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function index()
    {
        $title = new \stdClass();
        $title->icon = "fa fa-users";
        $title->title = "Utenti";
        $title->description = "Elenco";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('admin.dashboard')),
            (object)array('title' => 'Elenco', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "utenti";
        $menu->option = "elenco";

        $users = User::with(["workgroups", "brands"])->paginate();

        return view('admin.users.index')
            ->with([
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu,
                'users' => $users
            ]);
    }

    public function edit(User $user)
    {
        $workgroups = Workgroup::all();

        $title = new \stdClass();
        $title->icon = "fa fa-users";
        $title->title = "Utenti";
        $title->description = "Modifica utente";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('admin.dashboard')),
            (object)array('title' => 'Elenco', 'url' => route('admin.users.index')),
            (object)array('title' => 'Modifica utente', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "utenti";
        $menu->option = "elenco";


        $users_workgroups = [];
        foreach($user->workgroups as $o)
            $users_workgroups[] = $o->id;

        return view('admin.users.edit')
            ->with([
                'user' => $user,
                'workgroups' => $workgroups,
                'users_workgroups' => $users_workgroups,
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu
            ]);
    }

    public function update(Request $request, User $user)
    {
        $fields = [
            'name' => 'required'
        ];

        if($request->password)
            $fields = $fields + ['password' => 'required|confirmed'];

        $this->validate($request, $fields);

        $user->name = $request->name;
        $user->is_superadmin = $request->is_superadmin;
        if($request->password)
            $user->password = bcrypt($request->password);
        $user->save();

        $roles = [];
        if($request->workgroup_id)
        {
            $workgroup = Workgroup::findOrFail($request->workgroup_id);
            $roles[] = $workgroup->id;
        }
        $user->workgroups()->sync($roles);

        return redirect()->route('admin.users.index');
    }

    public function create()
    {
        $workgroups = Workgroup::all();

        $title = new \stdClass();
        $title->icon = "fa fa-users";
        $title->title = "Utenti";
        $title->description = "Nuovo utente";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('admin.dashboard')),
            (object)array('title' => 'Nuovo utente', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "utenti";
        $menu->option = "create";

        return view('admin.users.create')
            ->with([
                'workgroups' => $workgroups,
                'title' => $title,
                'breadcumbs' => $breadcumbs,
                'menu' => $menu
            ]);
    }

    public function store(Request $request)
    {
        $fields = [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed'
        ];

        $this->validate($request, $fields);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->is_superadmin = $request->is_superadmin;
        $user->password = bcrypt($request->password);
        $user->save();

        $roles = [];
        if($request->workgroup_id)
        {
            $workgroup = Workgroup::findOrFail($request->workgroup_id);
            $roles[] = $workgroup->id;
        }
        $user->workgroups()->sync($roles);

        return redirect()->route('admin.users.index');
    }

    public function destroy(Request $request, User $user)
    {
        $user->delete();

        return redirect()->route('admin.users.index');
    }

}
