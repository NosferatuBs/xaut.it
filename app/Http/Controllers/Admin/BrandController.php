<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Brand;
use App\Entities\BrandIlProjectBrand;
use App\Entities\BrandPhoto;
use App\Entities\BrandUser;
use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Repositories\SyncRepository;
use Illuminate\Cache\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class BrandController extends Controller
{
    public function __construct()
    {

    }

    public function create()
    {

        $title = new \stdClass();
        $title->icon = "fa fa-shield";
        $title->title = "Marchi";
        $title->description = "Nuova";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Nuovo', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "marchi";
        $menu->option = "create";

        $ilprojectBrands = DB::connection("mysql_ilproject")
            ->table("marchi")
            ->orderBy("nome")
            ->get();

        $users = User::where("is_superadmin", 0)->get();

        return view('admin.brands.create', [
            'title' => $title,
            'breadcumbs' => $breadcumbs,
            'menu' => $menu,
            'ilprojectBrands' => $ilprojectBrands,
            'users' => $users
        ]);
    }

    public function store(Request $request)
    {
        $fields = [
            'name' => [
                'required',
                Rule::unique('brands')
            ],
            'name_public' => 'required',
            'url_video' => 'nullable|url'
        ];

        $this->validate($request, $fields);

        $marchio = new Brand();
        $marchio->name = $request->name;
        $marchio->name_public = $request->name_public;
        $marchio->url_video = $request->url_video;
        $marchio->text = $request->text;
        $marchio->warranty_extension = empty($request->warranty_extension) ? 0 : $request->warranty_extension;
        $marchio->discount_percentage = $request->discount_percentage;
        $marchio->discount_amount = $request->discount_amount;
        $marchio->survey_option = empty($request->survey_option) ? 0 : $request->survey_option;
        $marchio->survey_branch_option = empty($request->survey_branch_option) ? 0 : $request->survey_branch_option;
        $marchio->survey_item_option = empty($request->survey_item_option) ? 0 : $request->survey_item_option;
        $marchio->stats_option = empty($request->stats_option) ? 0 : $request->stats_option;
        $marchio->discount_option = empty($request->discount_option) ? 0 : $request->discount_option;
        $marchio->warranty_option = empty($request->warranty_option) ? 0 : $request->warranty_option;
	    $marchio->shop_url = $request->shop_url;
	    $marchio->shop_api_key = $request->shop_api_key;
	    $marchio->shop_type = $request->shop_type;
		$marchio->header_css = $request->header_css;
		
        $marchio->save();

        foreach ($request->ilprojectBrands as $ilprojectBrand) {
            $obj = new BrandIlProjectBrand();
            $obj->brand_id = $marchio->id;
            $obj->ilproject_brand_id = $ilprojectBrand;
            $obj->save();
        }

        BrandUser::where("brand_id", $marchio->id)->delete();
        foreach ($request->users as $user) {
            $obj = new BrandUser();
            $obj->brand_id = $marchio->id;
            $obj->user_id = $user;
            $obj->save();
        }

        if ($request->logo != null)
        {
            $logo = $request->logo;
            $hash = $logo->hashName();
            $folder = substr($hash, 0, 2);
            $dir = 'brands' . DIRECTORY_SEPARATOR . $folder;
            $path = $logo->storeAs($dir, $hash, 'public');
            $marchio->logo = $path;
            $marchio->save();
        }

        return redirect()->route('admin.brands.edit', $marchio->id);
    }

    public function index(Request $request)
    {
        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-shield";
        $title->title = "Marchi";
        $title->description = "Elenco";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "marchi";
        $menu->option = "elenco";

        $brands = Brand::with("photo", "users")
            ->get();

        $brands->load(['photo' => function ($query) {
            $query->orderBy('position', 'asc');
        }]);

        return view('admin.brands.index', ['brands' => $brands, 'title' => $title, 'breadcumbs' => $breadcumbs, 'menu' => $menu]);
    }

    public function edit(Request $request, Brand $brand)
    {
        $brand->load(['photo' => function ($query) {
                $query->orderBy('position', 'asc');
            }])
            ->with(["ilprojectBrands", "brands"]);

        $user = $request->user();

        $title = new \stdClass();
        $title->icon = "fa fa-shield";
        $title->title = "Marchi";
        $title->description = "Modifica";

        $breadcumbs = [
            (object)array('title' => 'Home', 'url' => route('brand.dashboard')),
            (object)array('title' => 'Elenco', 'url' => route('brand.brands.index')),
            (object)array('title' => 'Modifica', 'url' => null)
        ];

        $menu = new \stdClass();
        $menu->area = "marchi";
        $menu->option = "elenco";

        $ilprojectBrands = DB::connection("mysql_ilproject")
            ->table("marchi")
            ->orderBy("nome")
            ->get();

        $users = User::where("is_superadmin", 0)->get();

        return view('admin.brands.edit', [
            'brand' => $brand,
            'title' => $title,
            'breadcumbs' => $breadcumbs,
            'menu' => $menu,
            'ilprojectBrands' => $ilprojectBrands,
            'users' => $users
        ]);
    }

    public function update(Request $request, Brand $brand)
    {

        $brand->load('photo');
        $user = $request->user();

        $lastPosition = count($brand->photo);

        $fields = [
            'name' => [
                'required',
                Rule::unique('brands')->ignore($brand->id)
            ],
            'name_public' => 'required',
            'url_video' => 'nullable|url',
            'warranty_extension' => 'numeric',
            'discount_validity' => 'numeric'
        ];

        if (is_array($request->file)) {
            $nbr = count($request->file) - 1;
            foreach (range(0, $nbr) as $index) {
                $fields['file.' . $index] = 'file|mimes:jpeg,bmp,png|max:256';
            }
        }

        $this->validate($request, $fields);

        $brand->name = $request->name;
        $brand->name_public = $request->name_public;
        $brand->url_video = $request->url_video;
        $brand->text = $request->text;
        $brand->warranty_extension = $request->warranty_extension;
        $brand->discount_percentage = $request->discount_percentage;
        $brand->discount_amount = $request->discount_amount;
        $brand->discount_validity = $request->discount_validity;
        $brand->search_option = $request->search_option;
        $brand->survey_option = empty($request->survey_option) ? 0 : $request->survey_option;
        $brand->survey_branch_option = empty($request->survey_branch_option) ? 0 : $request->survey_branch_option;
        $brand->survey_item_option = empty($request->survey_item_option) ? 0 : $request->survey_item_option;
        $brand->stats_option = empty($request->stats_option) ? 0 : $request->stats_option;
        $brand->discount_option = empty($request->discount_option) ? 0 : $request->discount_option;
        $brand->warranty_option = empty($request->warranty_option) ? 0 : $request->warranty_option;
	    $brand->shop_url = $request->shop_url;
	    $brand->shop_api_key = $request->shop_api_key;
	    $brand->shop_type = $request->shop_type;
	    $brand->header_css = $request->header_css;

        $brand->save();

        BrandIlProjectBrand::where("brand_id", $brand->id)->delete();
        foreach ($request->ilprojectBrands as $ilprojectBrand) {
            $obj = new BrandIlProjectBrand();
            $obj->brand_id = $brand->id;
            $obj->ilproject_brand_id = $ilprojectBrand;
            $obj->save();
        }

        BrandUser::where("brand_id", $brand->id)->delete();
        foreach ($request->users as $user) {
            $obj = new BrandUser();
            $obj->brand_id = $brand->id;
            $obj->user_id = $user;
            $obj->save();
        }

        if ($request->logo != null)
        {
            $logo = $request->logo;
            $hash = $logo->hashName();
            $folder = substr($hash, 0, 2);
            $dir = 'brands' . DIRECTORY_SEPARATOR . $folder;
            $path = $logo->storeAs($dir, $hash, 'public');
            $brand->logo = $path;
            $brand->save();
        }

        if (is_array($request->file)) {
            foreach ($request->file as $key => $photo) {
                $hash = $photo->hashName();
                $folder = substr($hash, 0, 2);
                $dir = 'brands' . DIRECTORY_SEPARATOR . $folder;
                $path = $photo->storeAs($dir, $hash, 'public');

                BrandPhoto::updateOrCreate([
                    'stored' => $path,
                    'brand_id' => $brand->id
                ], [
                    'original' => $photo->getClientOriginalName(),
                    'mime' => $photo->getMimeType(),
                    'position' => $key + $lastPosition
                ]);
            }
        }

        if (is_array($request->gallery)) {
            foreach ($request->gallery as $key => $gallery_id) {
                $immagine = $brand->photo->find($gallery_id);
                $immagine->title = $request->gallery_title[$key];
                $immagine->position = $key;
                $immagine->save();
            }
        }

        if (is_array($request->gallery_delete)) {
            foreach ($request->gallery_delete as $photo_id) {
                $immagine = $brand->photo->find($photo_id);
                Storage::disk('public')->delete($immagine->stored);
                $immagine->delete();
            }
        }

        return redirect()->route('admin.brands.index');
    }

    public function destroy(Request $request)
    {
        $this->authorize('delete_brand');
        $user = $request->user();

        if ($user->hasRole('brandowner')) {
            $brand = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->where('id', $request->brand_id)->first();
        } else {
            $brand = Brand::find($request->brand_id);
        }

        if (!is_null($brand) && $brand->items->isEmpty()) {
            foreach ($brand->photo()->get() as $image) {
                Storage::disk('public')->delete($image->stored);
            }
            $brand->photo()->delete();
            $brand->numbers()->delete();
            $brand->users()->detach();
            $brand->delete();
        }

        return redirect()->route('admin.brands.index');
    }

    public function importDataFromIlProject(Request $request, Brand $brand)
    {
        SyncRepository::importDataFromIlProject($brand);
    }
}
