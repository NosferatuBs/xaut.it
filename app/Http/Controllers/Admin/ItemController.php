<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Brand;
use App\Entities\Item;
use App\Entities\ItemPhoto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class ItemController extends Controller
{
    protected $title;
    protected $menu;
    protected $breadcumbs;

    public function __construct()
    {
        $this->middleware('auth');

        $this->title = new \stdClass();
        $this->menu = new \stdClass();
        $this->breadcumbs = [];
    }

    public function create(Request $request)
    {
        $user = $request->user();
        $this->title->icon = "fa fa-cubes";
        $this->title->title = "Articoli";
        $this->title->description = "Nuovo";

    	$this->breadcumbs = [
    		(object)array('title' => 'Home', 'url' => '/home'),
    		(object)array('title' => 'Nuovo', 'url' => null)
    	];

        $this->menu->area = "articoli";
        $this->menu->option = "create";

        if($user->hasRole('admin'))
        {
            $brands = Brand::orderBy('name')->get();
        }
        elseif($user->hasRole('brandowner'))
        {
            $brands = Brand::whereBrandIn($user->brands->pluck('id')->toArray())->orderBy('name')->get();
        }

    	return view('items.create', ['title' => $this->title, 'breadcumbs' => $this->breadcumbs, 'menu' => $this->menu, 'brands' => $brands]);

    }

    public function insert(Request $request)
    {
    	$fields = [
    		'name' => 'required',
    		'brand_id' => 'required',
            'barcode' => [
                'nullable',
                'numeric',
                Rule::unique('items'),
                Rule::unique('barcode_item','barcode')
            ],
			'url_video' => 'nullable|url'
    	];

    	$this->validate($request, $fields);

		$item = new Item();
		$item->name = $request->name;
		$item->barcode = $request->barcode;
        $item->qrcode = $request->qrcode;
		$item->url_video = $request->url_video;
		$item->text = $request->text;
		$item->brand_id = $request->brand_id;

		$item->save();

		return redirect('items/' . $item->id);
    }

    public function allJson(Request $request)
    {
        $user = $request->user();

        if($user->hasRole('admin'))
        {
            $items = $this->adminItemAll();
        }
        elseif($user->hasRole('brandowner'))
        {
            $items = $this->brandItemAll($user->brands->pluck('id')->toArray());
        }

        foreach ($items as $item)
        {
            if( !$item->photo->isEmpty() )
                $item->picture = asset('storage/' . $item->photo->first()->stored);
            else
                $item->picture = '';

            $array = [];
            array_push($array, array('url' => asset('items/' . $item->id), 'class' => 'btn btn-xs btn-default', 'label' => 'Modifica'));

            if($item->url_video)
                array_push($array, array('url' => $item->url_video, 'label' => 'Apri sito web', 'target' => '_blank'));
            if($item->brand->level == 1)
                array_push($array, array('url' => asset('items/' . $item->id . '/numbers'), 'label' => 'Numerazione'));

            array_push($array, array('url' => asset('barcode/' . $item->id), 'label' => 'Barcode alternativi'));

            if($item->traceability_count == 0)
                array_push($array, array('url' => '#', 'class' => 'btn-delete', 'label' => 'Elimina', 'separator' => 1));

            $item->dropdown = $array;
        }

        return response()->json([
            'data' => $this->transform($items)
        ]);
    }
    protected function transform($items)
    {
        return array_map(function($item) {
            return [
                $item['id'],
                $item['picture'],
                $item['barcode'],
                $item['name'],
                $item['text'] ?? '',
                $item['brand']['name'],
                $item['dropdown']
            ];
        }, $items->toArray());
    }
    public function all(Request $request)
    {
        $user = $request->user();

    	$title = new \stdClass();
        $title->icon = "fa fa-cubes";
        $title->title = "Articoli";
        $title->description = "Elenco";

    	$breadcumbs = [
    		(object)array('title' => 'Home', 'url' => '/home'),
    		(object)array('title' => 'Elenco', 'url' => null)
    	];

    	$menu = new \stdClass();
        $menu->area = "articoli";
        $menu->option = "elenco";

    	return view('items.list', ['title' => $title, 'breadcumbs' => $breadcumbs, 'menu' => $menu]);
    }

    protected function adminItemAll()
    {
        return Item::with(['brand','photo' => function($query) {
            $query->orderBy('position');
        }])->withCount('traceability')->get();
    }

    protected function brandItemAll($brands)
    {
        return Item::with(['brand','photo' => function($query) {
            $query->orderBy('position');
        }])->whereBrandIn($brands)
            ->withCount('traceability')
            ->get();
    }

    public function edit(Request $request, Item $item)
    {
        $user = $request->user();

        $this->title->icon = "fa fa-cubes";
        $this->title->title = "Articoli";
        $this->title->description = "Modifica";

        $this->breadcumbs = [
            (object)array('title' => 'Home', 'url' => '/home'),
            (object)array('title' => 'Elenco', 'url' => '/items'),
            (object)array('title' => 'Modifica', 'url' => null)
        ];

        $this->menu->area = "articoli";
        $this->menu->option = "elenco";

        $item->load(['photo' => function($query) {
            $query->orderBy('position');
        }]);

        if($user->hasRole('admin'))
        {
            return $this->adminItemEdit($item);
        }

        return $this->brandItemEdit($user, $item);
    }

    protected function adminItemEdit($item)
    {
        $brands = Brand::orderBy('name')->get();

        $title = $this->title;
        $breadcumbs = $this->breadcumbs;
        $menu = $this->menu;
        return view('items.edit', ['brands' => $brands ,'title' => $title, 'breadcumbs' => $breadcumbs, 'menu' => $menu, 'item' => $item]);
    }
    protected function brandItemEdit($user, $item)
    {
        $brands = $user->brands->pluck('id')->toArray();
        $brands = Brand::whereBrandIn($brands)->orderBy('name')->get();

        $title = $this->title;
        $breadcumbs = $this->breadcumbs;
        $menu = $this->menu;
        return view('items.edit', ['brands' => $brands ,'title' => $title, 'breadcumbs' => $breadcumbs, 'menu' => $menu, 'item' => $item]);
    }

    public function update(Request $request, Item $item)
    {
    	$fields = [
    		'name' => 'required',
    		'brand_id' => 'required',
            'barcode' => [
                'nullable',
                'numeric',
                Rule::unique('items')->ignore($item->id),
                Rule::unique('barcode_item','barcode')
            ],
            'qrcode' => [
                'nullable',
                Rule::unique('items')->ignore($item->id)
            ],
            'url_video' => 'nullable|url',
    	];
        if(is_array($request->file))
            {
            $nbr = count($request->file) - 1;
            foreach(range(0, $nbr) as $index) {
                $fields['file.' . $index] = 'file|mimes:jpeg,bmp,png|max:256';
            }
            }

        $this->validate($request, $fields);

        $item->load('photo');
        $lastPosition = count($item->photo);

		$item->name = $request->name;
        $item->barcode = $request->barcode;
        $item->qrcode = $request->qrcode;
		$item->url_video = empty($request->url_video) ? null : $request->url_video;
        $item->text = $request->text;
        $item->brand_id = $request->brand_id;
        $item->save();

        if(is_array($request->file))
        {
            foreach($request->file as $key => $photo)
            {
                $hash = $photo->hashName();
                $folder = substr($hash, 0, 2);
                $dir = 'items' . DIRECTORY_SEPARATOR . $folder;
                $path = $photo->storeAs($dir, $item->id . $hash,'public');

                ItemPhoto::updateOrCreate([
                    'stored' => $path,
                    'item_id' => $item->id
                ], [
                    'original' => $photo->getClientOriginalName(),
                    'mime' => $photo->getMimeType(),
                    'position' => $key + $lastPosition
                ]);
            }
        }

        if(is_array($request->gallery))
        {
            foreach ($request->gallery as $key => $gallery_id)
            {
                $immagine = $item->photo->find($gallery_id);
                $immagine->title = $request->gallery_title[$key];
                $immagine->position = $key;
                $immagine->save();
            }
        }

        if(is_array($request->gallery_delete))
        {
            foreach($request->gallery_delete as $photo_id)
            {
                $immagine = $item->photo->find($photo_id);
                Storage::disk('public')->delete($immagine->stored);
                $immagine->delete();
            }
        }

        return redirect('items');
    }
    public function destroy(Request $request)
    {
        $user = $request->user();

        if($user->hasRole('admin'))
        {
            $item = Item::find($request->item_id);
        }
        elseif($user->hasRole('brandowner'))
        {
            $item = Item::whereBrandIn($user->brands->pluck('id')->toArray())->where('id',$request->item_id)->first();
        }

        if(! is_null($item) && $item->traceability->isEmpty())
        {
            $item->delete();
        }

        return redirect('items');
    }
}
