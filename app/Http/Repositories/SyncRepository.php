<?php

namespace App\Http\Repositories;

use App\Entities\Brand;
use App\Entities\BrandIlProjectBrand;
use App\Entities\BrandNumber;
use App\Entities\IlProjectBrand;
use App\Entities\Item;
use App\Entities\ItemPhoto;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

define("API_KEY", "479ce814-3d8a-42ce-bf9a-b64b9d8c308b");

class SyncRepository
{
    public static function syncBrands()
    {
        $response = Http::withHeaders([
            'Authorization' => API_KEY,
        ])->get("https://restapi.ilproject.net/api/export/brands");

        $brands = $response->json();

        foreach ($brands as $brand)
        {
            $ilProjectBrand = IlProjectBrand::where('ilproject_id', $brand['brandId'])
                                            ->first();
            if ($ilProjectBrand == null)
            {
                $ilProjectBrand = new IlProjectBrand();
                $ilProjectBrand->ilproject_id = $brand['brandId'];
                $ilProjectBrand->name = $brand['name'];
                $ilProjectBrand->save();
            }
        }
    }

    public static function syncNumbers($brandId)
    {
        $ilProjectBrandId = BrandIlProjectBrand::where("brand_id", $brandId)
            ->pluck("ilproject_brand_id")
            ->first();

        $url = "https://restapi.ilproject.net/api/export/numbers/$ilProjectBrandId";

        $response = Http::withHeaders([
            'Authorization' => API_KEY,
        ])->get($url);

        $result = $response->json();

        foreach ($result['products'] as $ilProjectItem)
        {
            $item = Item::where("ilproject_id", $ilProjectItem['id'])->first();
            if ($item == null) {
                $item = new Item();
            }

            $item->brand_id = $brandId;
            $item->name = $ilProjectItem['name'];
            $item->text = $ilProjectItem['description'];
            $item->ilproject_id = $ilProjectItem['id'];
            $item->save();

            /*foreach ($ilProjectPhotos as $ilProjectPhoto)
            {
                $itemPhoto = ItemPhoto::where("ilproject_id", $ilProjectPhoto->id_foto)->first();

                if ($itemPhoto == null)
                {
                    $itemPhoto = new ItemPhoto();
                }

                $itemPhoto->original = $ilProjectPhoto->nome_file_reale;
                $itemPhoto->stored = "items/" . $item->id . "/" . uniqid("", true) . "." . pathinfo($ilProjectPhoto->nome_file_reale, PATHINFO_EXTENSION);
                $itemPhoto->ilproject_id = $ilProjectPhoto->id_foto;
                $itemPhoto->item_id = $item->id;

                $content = file_get_contents("https://ilproject.net/storage/media/" . str_replace("\\", "/", $ilProjectPhoto->nome_file_server));
                Storage::put($itemPhoto->stored, $content);

                $itemPhoto->save();
            }*/
        }

        foreach($result['rows'] as $ilProjectNumber)
        {
            $item = Item::where("ilproject_id", $ilProjectNumber["productId"])->first();
            if ($item == null)
            {
                continue;
            }

            $number = BrandNumber::where("ilproject_id", $ilProjectNumber["id"])->first();
            if ($number == null)
            {
                $number = new BrandNumber();
            }

            $number->brand_id = $brandId;
            $number->item_id = $item->id;
            $number->first_letter = $ilProjectNumber["firstLetter"];
            $number->second_letter = $ilProjectNumber["lastLetter"];
            $number->first_number = $ilProjectNumber["firstNumber"];
            $number->last_number = $ilProjectNumber["firstNumber"] + $ilProjectNumber["quantityWithExcess"];
            $number->ilproject_id = $ilProjectNumber["id"];
dd($number);
            $number->save();
        }
    }

    public static function importDataFromIlProject(Brand $brand)
    {
        // Retrieve brand ids from IlProject
        $ilProjectBrandIds = BrandIlProjectBrand::where("brand_id", $brand->id)
            ->pluck("ilproject_brand_id");

        // Sync items
        $ilProjectItems = DB::connection("mysql_ilproject")
            ->table("articoli")
            ->whereIn("id_marchio", $ilProjectBrandIds)
            ->get();

        foreach($ilProjectItems as $ilProjectItem) {
            $item = Item::where("ilproject_id", $ilProjectItem->id_articolo)->first();
            if ($item == null) {
                $item = new Item();
            }

            $item->brand_id = $brand->id;
            $item->name = $ilProjectItem->nome;
            $item->text = $ilProjectItem->descrizione;
            $item->ilproject_id = $ilProjectItem->id_articolo;

            $ilProjectPhotos = DB::connection("mysql_ilproject")
                ->table("articoli_proposte")
                ->join("articoli_proposte_foto", "articoli_proposte.id_proposta", "=", "articoli_proposte_foto.id_proposta")
                ->selectRaw("articoli_proposte_foto.*")
                ->where("articoli_proposte.id_articolo", $ilProjectItem->id_articolo)
                ->where("articoli_proposte.tipo_proposta", 1)
                ->where("articoli_proposte.approvata", ">", 0)
                ->get();

            $item->save();

            foreach ($ilProjectPhotos as $ilProjectPhoto)
            {
                $itemPhoto = ItemPhoto::where("ilproject_id", $ilProjectPhoto->id_foto)->first();

                if ($itemPhoto == null)
                {
                    $itemPhoto = new ItemPhoto();
                }

                $itemPhoto->original = $ilProjectPhoto->nome_file_reale;
                $itemPhoto->stored = "items/" . $item->id . "/" . uniqid("", true) . "." . pathinfo($ilProjectPhoto->nome_file_reale, PATHINFO_EXTENSION);
                $itemPhoto->ilproject_id = $ilProjectPhoto->id_foto;
                $itemPhoto->item_id = $item->id;

                $content = file_get_contents("https://ilproject.net/storage/media/" . str_replace("\\", "/", $ilProjectPhoto->nome_file_server));
                Storage::put($itemPhoto->stored, $content);

                $itemPhoto->save();
            }
        }

        // Sync numbers
        $ilProjectNumbers = DB::connection("mysql_ilproject")
            ->table("righe")
            ->join("ordini", "righe.id_ordine", "=", "ordini.id_ordine")
            ->whereIn("ordini.id_marchio", $ilProjectBrandIds)
            ->whereRaw("primo_numero IS NOT NULL AND quantita_con_sfrido IS NOT NULL")
            ->get();

        foreach($ilProjectNumbers as $ilProjectNumber)
        {
            $item = Item::where("ilproject_id", $ilProjectNumber->id_articolo)->first();
            if ($item == null)
            {
                continue;
            }

            $number = BrandNumber::where("ilproject_id", $ilProjectNumber->id_riga)->first();
            if ($number == null)
            {
                $number = new BrandNumber();
            }

            $number->brand_id = $brand->id;
            $number->item_id = $item->id;
            $number->first_letter = $ilProjectNumber->prima_lettera;
            $number->second_letter = $ilProjectNumber->seconda_lettera;
            $number->first_number = $ilProjectNumber->primo_numero;
            $number->last_number = $ilProjectNumber->primo_numero + $ilProjectNumber->quantita_con_sfrido;
            $number->ilproject_id = $ilProjectNumber->id_riga;

            $number->save();
        }
    }
}
