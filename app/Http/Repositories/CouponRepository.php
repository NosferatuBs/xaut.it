<?php
	
	namespace App\Http\Repositories;
	
	use App\Entities\Brand;
	use App\Entities\Discount;
	use App\Entities\Traceability;
	use Carbon\Carbon;
	use Illuminate\Support\Facades\Http;
	
	class CouponRepository
	{
		public static function registerCoupon(Brand $brand, Traceability $traceability)
		{
			if ($brand->shop_type == 1)
			{
				self::registerCouponOnShopify($traceability);
			}
		}
		
		public static function registerCouponOnShopify(Traceability $traceability)
		{
			$discount = Discount::where('traceability_id', $traceability->id)->first();
			$apiKey = $traceability->brand->shop_api_key;
			$shopUrl = $traceability->brand->shop_url;
			
			$body = (object) array(
				'price_rule' => (object) array(
					'title' => $traceability->code,
					'value_type' => 'percentage',
					'value' => -$discount->amount,
					'target_type' => 'line_item',
					'target_selection' => 'all',
					'allocation_method' => 'across',
					'starts_at' => Carbon::now(),
					'customer_selection' => 'all',
					'usage_limit' => 1
				)
			);
				
			$response = Http::withBody(json_encode($body), 'application/json')
				->withHeaders([
					'X-Shopify-Access-Token' => $apiKey
				])
				->post($shopUrl . '/admin/api/2023-07/price_rules.json');
			
			if ($response->getStatusCode() != 201)
			{
				dd('KO');
			}
			
			$responseData = json_decode($response->body());
			$priceRuleId = $responseData->price_rule->id;
			
			$body = (object) array(
				'discount_code' => (object) array(
					'code' => $discount->token
				)
			);
			
			$response = Http::withBody(json_encode($body), 'application/json')
				->withHeaders([
					'X-Shopify-Access-Token' => $apiKey
				])
				->post($shopUrl . '/admin/api/2023-07/price_rules/' . $priceRuleId . '/discount_codes.json');
			
			if ($response->getStatusCode() != 201)
			{
				dd('KO');
			}
		}
	}