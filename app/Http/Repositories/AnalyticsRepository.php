<?php
	
	namespace App\Http\Repositories;
	
	use Amenadiel\JpGraph\Graph\Graph;
	use Amenadiel\JpGraph\Graph\PieGraph;
	use Amenadiel\JpGraph\Plot\BarPlot;
	use Amenadiel\JpGraph\Plot\PiePlot;
	use Amenadiel\JpGraph\Plot\ScatterPlot;
	use Amenadiel\JpGraph\Themes\UniversalTheme;
	use App\Entities\Brand;
	use App\Entities\CustomerAnswer;
	use App\Entities\Item;
	use App\Entities\Survey;
	use App\Entities\Traceability;
	use App\Libraries\Ages;
	use Illuminate\Support\Carbon;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\File;
	use PDF;
	
	define("EMPTY_IMAGE", "iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAA1JREFUGFdjMP+5+z8ABkEC6werAQoAAAAASUVORK5CYII=");
	
	class AnalyticsRepository
	{
		public static function createPdfReport($brandId, $from, $to)
		{
			// Parse arguments
			if ($from == null || $from == '')
			{
				$from = date('Y-m-d', strtotime('-30 days', strtotime(date('Y-m-d'))));
			}
			
			if ($to == null || $to == '')
			{
				$to = date('Y-m-d', strtotime(date('Y-m-d')));
			}
			
			// Load the brand
			$brand = Brand::with(['photo', 'users'])
				->where('id', $brandId)
				->first();
			
			// Load the geolocation data from traceability
			$list = Traceability::select('city', 'lat', 'lng', DB::raw('COUNT(city) as frequenza'))
				->where('traceability.created_at', '>=', $from)
				->where('traceability.created_at', '<=', $to)
				->where('brand_id', $brandId)
				->groupBy('city', 'lat', 'lng')
				->get();
			$geolocationData = [];
			$dataX = [];
			$dataY = [];
			
			foreach ($list as $el)
			{
				$geolocationData[] = (object) [
					'latitude' => $el->lat,
					'longitude' => $el->lng,
					'city' => $el->city,
					'frequency' => (int)$el->frequenza
				];
				
				$coordinates = self::degreesToPixels($el->lat, $el->lng, 1200, 1200);
				$dataX[] = $coordinates->x / 1200 * 100;
				$dataY[] = (1200 - $coordinates->y) / 1200 * 100;
			}
			
			$graph = new Graph(1200, 1200);
			$graph->SetScale('linlin', 0, 100, 0, 100);
			$graph->xaxis->Hide();
			$graph->yaxis->Hide();
			$graph->SetBackgroundImage(storage_path() . "/app/public/world-map.png", BGIMG_FILLPLOT);
			$sp = new ScatterPlot($dataY, $dataX);
			$sp->mark->SetType(MARK_IMG_PUSHPIN, 'blue', 2);
			$graph->Add($sp);
			$geolocationChart = self::graphToBase64Image($graph);
			
			// Load the ages and gender data
			$genders = Traceability::select(DB::raw('SUM( CASE WHEN gender=\'male\' THEN 1 ELSE 0 END) AS male'), DB::raw('SUM( CASE WHEN gender=\'female\' THEN 1 ELSE 0 END) AS female'), DB::raw('SUM( CASE WHEN gender=\'other\' THEN 1 ELSE 0 END) AS other'))
				->join('users', 'users.id', '=', 'traceability.user_id')
				->join('brands', 'traceability.brand_id', '=', 'brands.id')
				->where('traceability.created_at', '>=', $from)
				->where('traceability.created_at', '<=', $to)
				->where('brand_id', $brandId)
				->where('brands.stats_option', '1')
				->first();
			
			$agesList = Traceability::select('users.id', 'users.birthday')
				->join('users', 'users.id', '=', 'traceability.user_id')
				->join('brands', 'traceability.brand_id', '=', 'brands.id')
				->where('traceability.created_at', '>=', $from)
				->where('traceability.created_at', '<=', $to)
				->where('brand_id', $brandId)
				->where('brands.stats_option', '1')
				->get();
			
			$ages = ['0-17', '18-27', '28-37', '38-47', '48-57', '58-67', '68+'];
			$ageGroup = new Ages($ages);
			
			foreach ($agesList as $utente)
			{
				$age = self::getAge($utente->birthday);
				
				if ($age >= 0 && $age <= 17)
					$ageGroup->increments('0-17');
				elseif ($age >= 18 && $age <= 27)
					$ageGroup->increments('18-27');
				elseif ($age >= 28 && $age <= 37)
					$ageGroup->increments('28-37');
				elseif ($age >= 38 && $age <= 47)
					$ageGroup->increments('38-47');
				elseif ($age >= 48 && $age <= 57)
					$ageGroup->increments('48-57');
				elseif ($age >= 58 && $age <= 67)
					$ageGroup->increments('58-67');
				elseif ($age >= 68)
					$ageGroup->increments('68+');
			}
			
			$ageValues = [];
			for ($i = 0; $i < count($ages); $i++)
			{
				$ageValues[$i] = $ageGroup->get($ages[$i]);
			}
			
			$chart = new PieGraph(800, 800);
			$chart->SetShadow();
			$chart->SetTheme(new UniversalTheme);
			$chart->SetMargin(40, 20, 36, 63);
			$p1 = new PiePlot([$genders->male, $genders->female]);
			$p1->SetLabelType(PIE_VALUE_ADJPER);
			$p1->SetLabelPos(1);
			$p1->SetLegends(["Uomo", "Donna"]);
			$p1->ShowBorder(true, true);
			$chart->Add($p1);
			$gendersChart = self::graphToBase64Image($chart);
			
			$chart = new Graph(800, 400);
			$chart->SetScale("textlin");
			$chart->SetShadow();
			$chart->SetTheme(new UniversalTheme);
			$chart->SetMargin(40, 20, 36, 63);
			$chart->xaxis->SetLabelAngle(45);
			$chart->xaxis->SetTickLabels($ages);
			$p1 = new BarPlot($ageValues);
			$p1->SetColor("navy");
			$chart->Add($p1);
			$agesChart = self::graphToBase64Image($chart);
			
			// Load items rating data
			$ratings = Traceability::select(DB::raw('SUM( CASE WHEN rate=1 THEN 1 ELSE 0 END) AS one'),
				DB::raw('SUM( CASE WHEN rate=2 THEN 1 ELSE 0 END) AS two'),
				DB::raw('SUM( CASE WHEN rate=3 THEN 1 ELSE 0 END) AS three'),
				DB::raw('SUM( CASE WHEN rate=4 THEN 1 ELSE 0 END) AS four'),
				DB::raw('SUM( CASE WHEN rate=5 THEN 1 ELSE 0 END) AS five'))
				->where('brand_id', $brandId)
				->where('traceability.created_at', '>=', $from)
				->where('traceability.created_at', '<=', $to)
				->first();
			
			$items = Item::select('items.*', DB::raw('AVG(traceability.rate) as avg'))
				->leftJoin('traceability', function ($join) use ($from, $to)
				{
					$join->on('items.id', '=', 'traceability.item_id')
						 ->where('traceability.created_at', '>=', $from)
						 ->where('traceability.created_at', '<=', $to);
				})
				->where('items.brand_id', $brandId)
				->groupBy('items.id', 'items.brand_id', 'items.name', 'items.barcode', 'items.url_video', 'items.text', 'items.created_at', 'items.updated_at')
				->havingRaw('AVG(traceability.rate)')
				->with('brand')
				->get();
			
			$stars = ["★", "★★", "★★★", "★★★★", "★★★★★"];
			$starValues = [
				$ratings->one,
				$ratings->two,
				$ratings->three,
				$ratings->four,
				$ratings->five
			];
			
			$chart = new PieGraph(600, 600);
			$chart->SetShadow();
			$chart->SetTheme(new UniversalTheme);
			$chart->SetMargin(40, 20, 36, 63);
			$chart->legend->SetPos(0.5, 0.9, 'center', 'bottom');
			$chart->legend->SetShadow();
			$p1 = new PiePlot($starValues);
			$p1->SetLabelType(PIE_VALUE_ADJPER);
			$p1->SetLabelPos(1);
			$p1->SetLegends($stars);
			$p1->ShowBorder(true, true);
			$chart->Add($p1);
			$ratingsChart = self::graphToBase64Image($chart);
			
			// Load surveys data
			$surveys = Survey::withCount(['customerAnswers' => function ($query) use ($from, $to)
			{
				$query->where('customer_answers.created_at', '>=', $from)
					->where('customer_answers.created_at', '<=', $to);
			}, 'branches'])->whereHas('customerAnswers', function ($query) use ($from, $to)
			{
				$query->where('customer_answers.created_at', '>=', $from)
					->where('customer_answers.created_at', '<=', $to);
			})
				->where('brand_id', $brandId)
				->get();
			
			$surveyCharts = [];
			foreach ($surveys as $survey)
			{
				$query = CustomerAnswer::select(DB::raw(' ifnull(survey_answers.name,"Altro") as name,customer_answers.survey_id, count(*) as tot'))
					->leftJoin('survey_answers', function ($join)
					{
						$join->on('customer_answers.answer', '=', 'survey_answers.name')
							->on('customer_answers.survey_id', '=', 'survey_answers.survey_id');
					})
					->where('customer_answers.survey_id', $survey->id)
					->where('customer_answers.created_at', '>=', $from)
					->where('customer_answers.created_at', '<=', $to)
					->groupBy('customer_answers.survey_id', 'survey_answers.name')
					->get();
				
				$frequenza = [];
				$total = 0;
				
				if ($survey->surveyType->has_answer)
				{
					foreach ($survey->answers as $answer)
					{
						if (!array_key_exists($answer->name, $frequenza))
							$frequenza[$answer->name] = 0;
					}
					foreach ($query as $answer)
					{
						$frequenza[$answer->name] = $answer->tot;
						$total += $answer->tot;
					}
					
					foreach ($frequenza as $k => $v)
					{
						$frequenza[$k] /= $total;
						$frequenza[$k] *= 100.0;
					}
					
					$chart = new PieGraph(400, 400);
					$chart->SetShadow();
					$chart->SetTheme(new UniversalTheme);
					$chart->SetMargin(40, 20, 36, 63);
					$p1 = new PiePlot(array_values($frequenza));
					$p1->SetLabelType(PIE_VALUE_ADJPER);
					$p1->SetLabelPos(1);
					$p1->SetLegends(array_keys($frequenza));
					$p1->ShowBorder(true, true);
					$chart->Add($p1);
					
					$surveyCharts[] = (object)array(
						'chart' => self::graphToBase64Image($chart),
						'question' => $survey->question
					);
				}
			}

			$logoPath = storage_path() . '/app/public/' . $brand->logo;
			$logo = base64_encode(File::get($logoPath));
			
			$data = [
				'brandName' => $brand->name,
				'brandLogo' => $logo,
				'geolocationData' => $geolocationData,
				'geolocationChart' => $geolocationChart,
				'agesChart' => $agesChart,
				'gendersChart' => $gendersChart,
				'items' => $items,
				'ratingsChart' => $ratingsChart,
				'surveyCharts' => $surveyCharts,
			];
			
			$pdf = PDF::loadView('brand.analytics.export.pdf', $data);
			return $pdf->download('pdf_file.pdf');
		}
		
		public static function degreesToPixels($lat, $lng, $width, $height)
		{
			
			$lat = doubleval($lat);
			$lng = doubleval($lng);
			$width = intval($width);
			$height = intval($height);
			
			if ($lat < -90 or $lat > 90)
			{
				//throw new \RangeException ( 'Latitude out of range (valid range: ±90°)' );
			}
			
			if ($lng < -180 or $lng > 180)
			{
				//throw new \RangeException ( 'Longitude out of range (valid range: ±180°)' );
			}
			
			if ($width <= 0)
			{
				//throw new \RangeException ( 'Width must be greater than 0' );
			}
			
			if ($height <= 0)
			{
				//throw new \RangeException ( 'Height must be greater than 0' );
			}
			
			return (object)array(
				'x' => ($lng + 180) * ($width / 360),
				'y' => ($height / 2) - ($width * log(tan((M_PI / 4) + (($lat * M_PI / 180) / 2))) / (2 * M_PI))
			);
		}
		
		public static function graphToBase64Image(Graph $graph)
		{
			$tempFile = tempnam(sys_get_temp_dir(), 'brand_report_');
			$graph->Stroke($tempFile);
			ob_start();
			readfile($tempFile);
			$content = base64_encode(ob_get_contents());
			ob_end_clean();
			unlink($tempFile);
			
			return $content;
		}
		
		private static function getAge($birthday)
		{
			if (is_null($birthday))
				return null;
			
			return Carbon::make($birthday)->diffInYears();
		}
		
		public static function markCallback($y, $x)
		{
			// Return array width
			// width,color,fill color, marker filename, imgscale
			// any value can be false, in that case the default value will
			// be used.
			// We only make one pushpin another color
			if ($x == 54)
				return array(false, false, false, 'red', 0.8);
			else
				return array(false, false, false, 'green', 0.8);
		}
		
		public static function latitudeLongitudeToMercator($latitude, $longitude, $mapWidth, $mapHeight)
		{
			$x = ($longitude + 180) * ($mapWidth / 360);
			
			// convert from degrees to radians
			$latRad = $latitude * 3.1415 / 180;
			
			// get y value
			$mercN = log(tan((3.1415 / 4) + ($latRad / 2)));
			$y = ($mapHeight / 2) - ($mapWidth * $mercN / (2 * 3.1415));
			
			return (object)array('x' => $x, 'y' => $y);
		}
	}