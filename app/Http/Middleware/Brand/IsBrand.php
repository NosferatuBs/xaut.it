<?php

namespace App\Http\Middleware\Brand;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsBrand
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->hasRoles()) {
            return $next($request);
        }

        abort(403);
    }
}
