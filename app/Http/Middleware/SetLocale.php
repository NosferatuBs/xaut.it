<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && ! empty(Auth::user()->locale))
        {
            $locale = Auth::user()->locale;
        }
        elseif(Session::has('locale'))
        {
            $locale = Session::get('locale');
        }
        else
        {
            $locale = $request->getPreferredLanguage($this->supportedLocales());
        }

        if(! in_array($locale, $this->supportedLocales()))
        {
            $locale = config('app.fallback_locale');
        }

        app()->setLocale($locale);
        Carbon::setLocale($locale);
        Session::put('locale', $locale);

        return $next($request);
    }

    protected function supportedLocales()
    {
        return array_keys(config('laravellocalization.supportedLocales'));
    }
}
