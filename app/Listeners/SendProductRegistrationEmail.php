<?php
namespace App\Listeners;

use App\Events\NewProductRegistration;
use App\Mail\ProductRegistration\AttendingConfirmation;
use Illuminate\Support\Facades\Mail;

class SendProductRegistrationEmail
{
    public function __construct()
    {

    }

    public function handle(NewProductRegistration $event)
    {
        Mail::send(new AttendingConfirmation($event->traceability));
    }
}
