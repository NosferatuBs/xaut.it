<?php
namespace App\Listeners;

use App\Events\NewDiscountRegistration;
use App\Mail\Discount\AttendingConfirmation;
use Illuminate\Support\Facades\Mail;

class SendDiscountEmail
{
    public function __construct()
    {

    }

    public function handle(NewDiscountRegistration $event)
    {
        Mail::send(new AttendingConfirmation($event->traceability));
    }
}
