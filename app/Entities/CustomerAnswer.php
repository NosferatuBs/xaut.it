<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class CustomerAnswer extends Model
{
    public function traceability()
    {
    	return $this->belongsTo('App\Entities\Traceability');
    }
}
