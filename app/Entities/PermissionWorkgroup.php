<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class PermissionWorkgroup extends Model
{
	protected $table = "permission_workgroup";
}
