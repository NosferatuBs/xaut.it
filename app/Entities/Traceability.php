<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Traceability extends Model
{

    protected $table = "traceability";

    protected $casts = [
        'lat' => 'float',
        'lng' => 'float'
    ];

    public function item()
    {
    	return $this->belongsTo('App\Entities\Item');
    }

    public function user()
    {
    	return $this->belongsTo('App\Entities\User');
    }

    public function brand()
    {
        return $this->belongsTo('App\Entities\Brand');
    }

    public function warranty()
    {
        return $this->hasOne('App\Entities\Warranty');
    }

    public function discount()
    {
        return $this->hasOne('App\Entities\Discount');
    }

    public function customerAnswers()
    {
        return $this->hasMany('App\Entities\CustomerAnswer');
    }

    public function scopewhereBrandIn($query, array $brand)
    {
        return $query->whereIn('traceability.brand_id', $brand);
    }
}
