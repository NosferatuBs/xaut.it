<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['barcode', 'name', 'text', 'brand_id'];

    public function barcode()
    {
        return $this->hasMany('App\Entities\BarcodeItem');
    }
    public function brand()
    {
    	return $this->belongsTo('App\Entities\Brand');
    }
    public function photo()
    {
    	return $this->hasMany('App\Entities\ItemPhoto');
    }
    public function traceability()
    {
        return $this->hasMany('App\Entities\Traceability');
    }
    public function scopewhereBrandIn($query, array $brand)
    {
        return $query->whereIn('items.brand_id', $brand);
    }
    public function numbers()
    {
        return $this->hasMany('App\Entities\BrandNumber');
    }
    public function branches()
    {
        return $this->belongsToMany('App\Entities\Branch');
    }
}
