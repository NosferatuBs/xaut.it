<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SurveyAnswer extends Model
{
    protected $fillable = ['id','name'];
}
