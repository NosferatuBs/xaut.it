<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Workgroup extends Model
{
    /**
     * Get the permissions for the workgroup
     */
    public function permissions()
    {
    	return $this->belongsToMany('App\Entities\Permission');
    }

    /*
     * Get the users for the workgroup
     */
    public function users()
    {
        return $this->belongsToMany('App\Entities\User');
    }
}
