<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BrandUser extends Model
{
    protected $table = 'brand_user';
}
