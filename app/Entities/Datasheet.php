<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Datasheet extends Model
{
    public function scopewhereBrandIn($query, array $brand)
    {
        return $query->whereIn('brand_id', $brand);
    }

    public function brand()
    {
    	return $this->belongsTo('App\Entities\Brand');
    }

    public function datasheetType()
    {
    	return $this->belongsTo('App\Entities\DatasheetType');
    }

    public function branches()
    {
    	return $this->belongsToMany('App\Entities\Branch')->withTimestamps();
    }
}
