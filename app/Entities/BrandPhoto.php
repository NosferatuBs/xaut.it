<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BrandPhoto extends Model
{
    protected $table = "brand_photo";
    protected $fillable = ['original','stored','mime','brand_id','position'];

    public function brand()
    {
    	return $this->belongsTo('App\Entities\Brand');
    }

    public function scopewhereBrandIn($query, array $brand)
    {
    	return $query->whereIn('brand_id', $brand);
    }
}
