<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    public function photo()
    {
        return $this->hasMany('App\Entities\BrandPhoto');
    }

    public function items()
    {
    	return $this->hasMany('App\Entities\Item');
    }

    public function ilprojectBrands()
    {
        return $this->hasMany(BrandIlProjectBrand::class);
    }

    public function users()
    {
    	return $this->belongsToMany('App\Entities\User');
    }

    public function scopewhereBrandIn($query, array $brand)
    {
        return $query->whereIn('id', $brand);
    }

    public function scopewhereHasHologram($query)
    {
        return $query->where('level','1');
    }

    public function traceability()
    {
        return $this->hasMany('App\Entities\Traceability');
    }

    public function numbers()
    {
        return $this->hasMany('App\Entities\BrandNumber');
    }

    public function shops()
    {
        return $this->belongsToMany('App\Entities\Shop');
    }

    public function coupon()
    {
        return $this->hasOne('\App\Entities\BrandCoupon');
    }
}
