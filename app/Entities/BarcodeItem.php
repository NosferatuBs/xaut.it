<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BarcodeItem extends Model
{
    protected $table = "barcode_item";

    public function item()
    {
    	return $this->belongsTo('App\Entities\Item');
    }
}
