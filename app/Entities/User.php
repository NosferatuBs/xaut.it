<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Session;
use Lab404\Impersonate\Models\Impersonate;

class User extends Authenticatable
{
    use Notifiable, Impersonate;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'locale', 'birthday', 'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
     * Get the workgroups that belong to the user
     */
    public function workgroups()
    {
        return $this->belongsToMany('App\Entities\Workgroup')->withTimestamps();
    }

    /*
     * Get the workgroups that belong to the user
     */
    public function brands()
    {
        return $this->belongsToMany('App\Entities\Brand')->withTimestamps();
    }

    public function customerAnswers()
    {
        return $this->hasMany('App\Entities\CustomerAnswer');
    }

    public function traceability()
    {
        return $this->hasMany('App\Entities\Traceability');
    }

    public function hasRole($role)
    {
        if(is_string($role))
        {
            return $this->workgroups->contains('name', $role);
        }

        return !! $role->intersect($this->workgroups)->count();
    }

    public function hasNoRoles()
    {
        return $this->workgroups->count() == 0;
    }

    public function hasRoles()
    {
        return $this->workgroups->count() > 0;
    }

    public function getBirthdayAttribute($date)
    {
        if(is_null($date))
            return null;

        return Carbon::parse($date)->format('d/m/Y');
    }

    public function age()
    {
        if(is_null($this->birthday))
            return null;

        return Carbon::createFromFormat('d/m/Y', $this->birthday)->diffInYears();
    }

    public function notifications()
    {
        return $this->hasMany('App\Entities\Notification');
    }

    public function hasReadNotification($message = null)
    {
        return !! $this->notifications->where('message',$message)->where('read',1)->count();
    }

    public function shops()
    {
        return $this->belongsToMany('App\Entities\Shop')->withTimestamps();
    }

    public function isSuperadmin()
    {
        return $this->is_superadmin;
    }

    public function getRoleBasedHomePageUrl()
    {
        if($this->isSuperadmin())
        {
            return route('admin.dashboard');
        }
        elseif($this->hasRoles())
        {
            return route('brand.dashboard');
        }
        else
        {
            return route('home');
        }
    }

    public function canBeImpersonated()
    {
        return !$this->is_superadmin;
    }

    public function canImpersonate()
    {
        return $this->is_superadmin;
    }

    public function isImpersonating()
    {
        return Session::has('impersonate');
    }
}
