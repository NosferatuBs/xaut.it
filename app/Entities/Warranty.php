<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
    public function traceability()
    {
    	return $this->belongsTo('App\Entities\Traceability');
    }

    public function getBoughtAtAttribute($date)
    {
    	if(is_null($date))
            return null;

        return Carbon::parse($date)->format('d/m/Y');
    }

    public function getExpiredAtAttribute($date)
    {
        if(is_null($date))
            return null;

        return Carbon::parse($date)->format('d/m/Y');
    }
}
