<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ItemPhoto extends Model
{
	protected $table = "item_photo";
	protected $fillable = ['original','stored','mime','item_id','position'];

    public function item()
    {
    	return $this->belongsTo('App\Entities\Item');
    }

    public function scopewhereBrandIn($query, array $brand)
    {
        return $query->join('items','item_photo.item_id','items.id')
            ->whereIn('brand_id', $brand);
    }

    public function scopewhereDefault($query)
    {
        return $query->where('default',true);
    }
}
