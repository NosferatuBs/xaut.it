<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
	public function scopewhereBrandIn($query, array $brand)
    {
        return $query->whereIn('brand_id', $brand);
    }

    public function brand()
    {
    	return $this->belongsTo('App\Entities\Brand');
    }

    public function items()
    {
    	return $this->belongsToMany('App\Entities\Item')->withTimestamps();
    }

    public function surveys()
    {
        return $this->belongsToMany('App\Entities\Survey');
    }

    public function datasheets()
    {
        return $this->belongsToMany('App\Entities\Datasheet');
    }
}
