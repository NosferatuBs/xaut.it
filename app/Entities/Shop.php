<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{

    public function scopewhereBrandIn($query, array $brand)
    {
        return $query->whereIn('brand_id', $brand);
    }

    public function brands()
    {
    	return $this->belongsToMany('App\Entities\Brand');
    }


}
