<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class IlProjectBrand extends Model
{
    protected $table = "ilproject_brands";
}
