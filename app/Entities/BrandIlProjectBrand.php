<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BrandIlProjectBrand extends Model
{
    protected $table = "brand_ilproject_brands";

    public function brand()
    {
        return $this->belongsTo('App\Entities\Brand');
    }
}
