<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    public function scopewhereBrandIn($query, array $brand)
    {
        return $query->whereIn('brand_id', $brand);
    }

    public function brand()
    {
    	return $this->belongsTo('App\Entities\Brand');
    }

    public function surveyType()
    {
    	return $this->belongsTo('App\Entities\SurveyType');
    }

    public function branches()
    {
    	return $this->belongsToMany('App\Entities\Branch')->withTimestamps();
    }

    public function items()
    {
        return $this->belongsToMany('App\Entities\Item')->withTimestamps();
    }

    public function answers()
    {
    	return $this->hasMany('App\Entities\SurveyAnswer');
    }

    public function customerAnswers()
    {
        return $this->hasMany('App\Entities\CustomerAnswer');
    }
}
