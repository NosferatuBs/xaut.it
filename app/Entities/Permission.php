<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /*
     * Get the workgroups that belong to the permission
     */
    public function workgroups()
    {
    	return $this->belongsToMany('App\Entities\Workgroup');
    }
}
