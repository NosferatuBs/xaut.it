<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserWorkgroup extends Model
{
	protected $table = "user_workgroup";
	protected $fillable = ['user_id', 'workgroup_id'];
}
