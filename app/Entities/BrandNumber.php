<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class BrandNumber extends Model
{
    protected $fillable = ['code', 'inventory'];

    public function brand()
    {
        return $this->belongsTo('App\Entities\Brand');
    }
}
