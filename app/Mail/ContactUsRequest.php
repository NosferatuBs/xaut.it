<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsRequest extends Mailable
{
    use Queueable, SerializesModels;

    protected $richiesta;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->richiesta = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('vendor.notifications.email')
            ->with([
                'introLines' => [
                    'Hai ricevuto una richiesta di contatto dal sito web ' . config('app.name') . '.',
                    'L\'utente ' . $this->richiesta->name . ' con indirizzo email ' . $this->richiesta->email . ' ha scritto:',
                    'Oggetto della richiesta: ' . $this->richiesta->subject,
                    'Contenuto della richiesta: ' . $this->richiesta->message,
                    'Per rispondere al messaggio puoi rispondere direttamente a questa email.'
                ],
                'level' => 'success',
                'outroLines' => []
            ])
            ->subject('Richiesta informazioni su ' . config('app.name'))
            ->replyTo($this->richiesta->email, $this->richiesta->name)
            ->to('glr93@hotmail.it', 'Riccardo Galeazzi');
    }
}
