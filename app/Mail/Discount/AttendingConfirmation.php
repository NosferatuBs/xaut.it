<?php

namespace App\Mail\Discount;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class AttendingConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    protected $traceability;
    protected $discount;
    protected $brand;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($traceability)
    {
        $this->user = $traceability->user;
        $this->traceability = $traceability;
        $this->discount = $traceability->discount;
        $this->brand = $traceability->brand;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->user->email;
        $discount = $this->discount->amount;
	    $isPercentage = $this->discount->is_percentage;
        $url = route('validate.coupon.qrcode', $this->discount->token);
        $token = $this->discount->token;
        $expired_at = $this->discount->created_at->addMonth($this->brand->discount_validity);
        $template = Storage::path($this->brand->coupon->template);

        $pdf = \App\Libraries\CouponGenerator::make($email, $discount, $isPercentage, $this->traceability->brand_id, $url, $token, $expired_at, $template);

        return $this->markdown('emails.couponRegistration')
            ->with('user', $this->user)
            ->with('url', route('tracking.show', $this->traceability->token))
            ->subject(__('mail.couponRegistration.subject'))
            ->to($this->user->email, $this->user->name)
            ->attachData($pdf->Output('S'), 'coupon.pdf', [
                'mime' => 'application/pdf'
            ]);
    }
}
