<?php
namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait RedirectToByUserRole
{
    protected function redirectTo()
    {
        if(Auth::user()->isSuperadmin())
        {
            return route('admin.dashboard');
        }
        elseif(Auth::user()->hasRoles())
        {
            return route('brand.dashboard');
        }

        throw new \Exception("User role not identified exception");

    }
}
