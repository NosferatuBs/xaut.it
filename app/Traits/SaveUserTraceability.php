<?php
namespace App\Traits;

use App\Entities\BrandNumber;
use App\Entities\Discount;
use App\Entities\Traceability;
use App\Entities\User;
use Illuminate\Support\Str;

trait SaveUserTraceability
{
    protected function saveTraceability($surveys, $array)
    {
        $traceabilityToken = $this->generateUniqueTraceabilityToken();

        $user = User::firstOrCreate(['email' => $array['email']], [
            'name' => $array['email'],
            'password' => bcrypt(Str::random()),
            'gender' => $array['gender'],
            'birthday' => $array['birthday'],
            'locale' => \App::getLocale(),
            'gdpr_newsletter_consent' => !is_null($array['gdpr_newsletter_consent']),
            'gdpr_profilation_consent' => !is_null($array['gdpr_profilation_consent']),
            'gdpr_16years_consent' => !is_null($array['gdpr_16years_consent']),
        ]);

        $user->gdpr_newsletter_consent = !is_null($array['gdpr_newsletter_consent']);
        $user->gdpr_profilation_consent = !is_null($array['gdpr_profilation_consent']);
        $user->gdpr_16years_consent = !is_null($array['gdpr_16years_consent']);
        $user->save();

        $traccia = new Traceability;
        $traccia->user_id = $user->id;
        $traccia->brand_id = $array['brand_id'];
        $traccia->item_id = $array['item_id'];
        $traccia->brand_number_id = $array['brand_number_id'];
        $traccia->rate = $array['rate'];
        $traccia->token = $traceabilityToken;
        $traccia->city = $array['city'];
        $traccia->lat = $array['lat'];
        $traccia->lng = $array['lng'];
        $traccia->code = $array['token'];
        $traccia->save();

        $surveys->storeAnswers($user, $traccia, $array['answers']);

        return $traccia;
    }

    protected function saveDiscount(Traceability $traccia, $amount, $isPercentage)
    {
        $discountToken = $this->generateUniqueDiscountToken();

        $discount = new Discount;
        $discount->traceability_id = $traccia->id;
        $discount->token = $discountToken;
        $discount->amount = $amount;
		$discount->is_percentage = $isPercentage;
        $discount->save();

        return $discount;
    }

    protected function generateUniqueTraceabilityToken()
    {
        do{
            $token = Str::random(32);
            $validator = \Validator::make(['token'=>$token],[
                'token' => [
                    'required',
                    'unique:traceability,token'
                ]
            ]);
        }while($validator->fails());

        return $token;
    }

    protected function generateUniqueDiscountToken()
    {
        do{
            $token = Str::random(32);
            $validator = \Validator::make(['token'=>$token],[
                'token' => [
                    'required',
                    'unique:discounts,token'
                ]
            ]);
        }while($validator->fails());

        return $token;
    }

    protected function calculateDiscount($brand_number_id)
    {
        $qrcode = BrandNumber::where('id', $brand_number_id)
            ->firstOrFail();

        $amount = round($qrcode->price / 100 * $qrcode->discount_percentage);

        return $amount;
    }
}
