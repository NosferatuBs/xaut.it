<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \App\Entities\Traceability::observe(\App\Observers\TraceabilityObserver::class);
        \App\Entities\User::observe(\App\Observers\UserObserver::class);
        \App\Entities\Warranty::observe(\App\Observers\WarrantyObserver::class);
        \App\Entities\Datasheet::observe(\App\Observers\DatasheetObserver::class);
        \App\Entities\Item::observe(\App\Observers\ItemObserver::class);
        \App\Entities\ItemPhoto::observe(\App\Observers\ItemPhotoObserver::class);

        \Validator::extendImplicit('current_password', function($attribute, $value, $parameters, $validator) {
            return \Hash::check($value, auth()->user()->password);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
