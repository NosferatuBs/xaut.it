<?php

namespace App\Providers;

use App\Http\Controllers\ImpersonateController;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapAdminRoutes();

        $this->mapBrandRoutes();

        $this->mapImpersonateWebRoutes();

        if(! $this->app->environment('production'))
        {
            $this->mapDevelopmentRoutes();
        }
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace
        ], function ($router) {
            require base_path('routes/web/web.php');
        });
    }

    /**
     * Define the "admin" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapAdminRoutes()
    {
        Route::group([
            'middleware' => ['web', 'auth', 'admin'],
            'namespace' => $this->namespace . '\Admin',
            'prefix' => 'admin',
            'as' => 'admin.'
        ], function ($router) {
            require base_path('routes/web/admin.php');
        });
    }

    /**
     * Define the "brand" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapBrandRoutes()
    {
        Route::group([
            'middleware' => ['web', 'auth', 'brand'],
            'namespace' => $this->namespace . '\Brand',
            'prefix' => 'brand',
            'as' => 'brand.'
        ], function ($router) {
            require base_path('routes/web/brand.php');
        });
    }

    protected function mapDevelopmentRoutes()
    {
        Route::group([
            'middleware' => ['web'],
            'namespace' => $this->namespace . '\Development',
            'as' => 'development.',
            'prefix' => 'development',
        ], function ($router) {
            require base_path('routes/web/development.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }

    protected function mapImpersonateWebRoutes()
    {
        Route::group([
            'middleware' => ['web','auth'],
        ], function ($router) {
            Route::get('impersonate/{user}', [ImpersonateController::class, 'impersonate'])->name('impersonate.impersonate');
            Route::get('leaveImpersonation', [ImpersonateController::class, 'leaveImpersonation'])->name('impersonate.leaveImpersonation');
        });
    }
}
