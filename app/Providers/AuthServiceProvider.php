<?php

namespace App\Providers;

use App\Entities\Permission;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\Entities\Branch::class => \App\Policies\BranchPolicy::class,
        \App\Entities\Brand::class => \App\Policies\BrandPolicy::class,
        \App\Entities\Datasheet::class => \App\Policies\DatasheetPolicy::class,
        \App\Entities\Item::class => \App\Policies\ItemPolicy::class,
        \App\Entities\Survey::class => \App\Policies\SurveyPolicy::class,
        \App\Entities\Warranty::class => \App\Policies\WarrantyPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        foreach($this->getPermissions() as $permission)
        {
            Gate::define($permission->name, function($user) use ($permission){
                return $user->hasRole($permission->workgroups);
            });
        }
    }

    protected function getPermissions()
    {
        return Permission::with('workgroups')->get();
    }
}
