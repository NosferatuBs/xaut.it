<?php

namespace App\Events;

use App\Entities\Traceability;
use App\Entities\Discount;
use Illuminate\Queue\SerializesModels;

class NewDiscountRegistration
{
    use SerializesModels;

    public $traceability;
    public $discount;

    /**
     * Create a new event instance.
     *
     * @param  Traceability  $traceability
     * @param  Discount  $discount
     * @return void
     */
    public function __construct(Traceability $traceability, Discount $discount)
    {
        $this->traceability = $traceability;
        $this->discount = $discount;
    }
}