<?php

namespace App\Events;

use App\Entities\Traceability;
use Illuminate\Queue\SerializesModels;

class NewProductRegistration
{
    use SerializesModels;

    public $traceability;

    /**
     * Create a new event instance.
     *
     * @param  User  $user
     * @param  Traceability  $traceability
     * @return void
     */
    public function __construct(Traceability $traceability)
    {
        $this->traceability = $traceability;
    }
}
