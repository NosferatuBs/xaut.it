<?php

namespace App\Console\Commands;

use App\Entities\BrandNumber;
use Illuminate\Console\Command;

class CreateBrandNumberCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'brandnumber:generate
        {qty : The quantity of random tokens}
        {--inv= : The inventory code to identify this lot}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate new random tokens';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $qty = $this->argument('qty');
        $inventory = $this->option('inv');

        $bar = $this->output->createProgressBar($qty);

        for($k = 0; $k < $qty; $k++)
        {
            $token = $this->generateUniqueToken();

            BrandNumber::create(['code' => $token, 'inventory' => $inventory]);

            $bar->advance();
        }

        $bar->finish();

        $this->info("Successfully created {$qty} random tokens.");
    }

    protected function generateUniqueToken()
    {
        do{
            $token = str_random(32);
            $validator = \Validator::make(['token'=>$token],[
                'token' => [
                    'required',
                    'unique:brand_numbers,code'
                ]
            ]);
        }while($validator->fails());

        return $token;
    }
}
