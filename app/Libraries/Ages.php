<?php
namespace App\Libraries;

class Ages
{
	protected $array = [];
	protected $totale = 0;

	public function __construct(array $elements)
	{
		foreach ($elements as $element) {
			if(isset($this->array[$element]))
				throw new \Exception('Group already exists');

			$this->array[$element] = 0;
		}
	}

	public function increments($key)
	{
		if(isset($this->array[$key]))
			$this->array[$key]++;
		$this->totale ++;
	}

	public function get($key)
	{
		if(isset($this->array[$key]))
			return $this->array[$key];
		return 0;
	}

	public function count()
	{
		if($this->totale > 0)
			return $this->totale;

		return 1;
	}
}