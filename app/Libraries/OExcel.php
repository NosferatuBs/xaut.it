<?php
namespace App\Libraries;

use Carbon\Carbon;

class OExcel
{
	/**
	 * Read an XLSX document and return the array with the content
	 * @param string $path
	 * @param int|array $column
	 * @param bool $useHeader
	 * @return array
	*/
	public static function read($path, $column, $useHeader = true)
	{
		$workbook = \PHPExcel_IOFactory::load($path);
		$sheet = $workbook->getSheet(0);

		$count = ( (is_array($column)) ? count($column) : $column ) - 1;
		$colString = \PHPExcel_Cell::stringFromColumnIndex($count);
		
		$start = ($useHeader) ? "A2" : "A1";
		$stop = $colString . $sheet->getHighestRow();

		$array = $sheet->rangeToArray($start . ":" . $stop);

		if(is_array($column))
		{
			$array = Excel::cast($array, $column);
		}

		return $array;
	}

	public static function cast(array $rows, array $cols)
	{
		foreach ($rows as $row => $record)
		{
			foreach ($cols as $col => $type)
			{
				$cell = &$rows[$row][$col];
				switch($type)
				{
					case 'int':
						$cell = Excel::toInt($cell);
						break;
					case 'float':
						$cell = Excel::toFloat($cell);
						break;
					case 'date':
						$cell = Excel::toDate($cell);
						break;
					case 'string':
						$cell = (string)$cell;
						break;
					default:
						throw new \Exception('Can not parse into ' . $type . ' format.');
						break;
				}
			}
		}
		return $rows;
	}

	protected static function toInt($value)
	{
		return (int)$value;
	}
	protected static function toFloat($value)
	{
		$value = str_replace(",", ".", $value);
		preg_match('/([\d.]+)/', $value, $matches);
		return (float)$matches[0];
	}
	protected static function toDate($value)
	{
		$format = Excel::guessDateFormat($value);
		if(! is_null($format))
			return Carbon::createFromFormat($format, $value);
		return null;
	}
	protected static function guessDateFormat($date)
	{
		$patterns = [
			["/^\d{1,2}-\d{1,2}-\d{4}$/", "d-m-Y"],
			["/^\d{1,2}-\d{1,2}-\d{2}$/", "d-m-y"],
			["/^\d{1,2}\/\d{1,2}\/\d{4}$/", "d/m/Y"],
			["/^\d{1,2}\/\d{1,2}\/\d{2}$/", "d/m/y"],
			["/^\d{4}-\d{1,2}-\d{1,2}$/", "Y-m-d"],
			["/^\d{4}\/\d{1,2}\/\d{1,2}$/", "Y/m/d"],

			["/^\d{1,2}-\d{1,2}-\d{4}\s\d{1,2}:\d{2}$/", "d-m-Y H:i"],
			["/^\d{1,2}-\d{1,2}-\d{2}\s\d{1,2}:\d{2}$/", "d-m-y H:i"],
			["/^\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}:\d{2}$/", "d/m/Y H:i"],
			["/^\d{1,2}\/\d{1,2}\/\d{2}\s\d{1,2}:\d{2}$/", "d/m/y H:i"],
			["/^\d{4}-\d{1,2}-\d{1,2}\s\d{1,2}:\d{2}$/", "Y-m-d H:i"],
			["/^\d{4}\/\d{1,2}\/\d{1,2}\s\d{1,2}:\d{2}$/", "Y/m/d H:i"],

			["/^\d{1,2}-\d{1,2}-\d{4}\s\d{1,2}:\d{2}:\d{2}$/", "d-m-Y H:i:s"],
			["/^\d{1,2}-\d{1,2}-\d{2}\s\d{1,2}:\d{2}:\d{2}$/", "d-m-y H:i:s"],
			["/^\d{1,2}\/\d{1,2}\/\d{4}\s\d{1,2}:\d{2}:\d{2}$/", "d/m/Y H:i:s"],
			["/^\d{1,2}\/\d{1,2}\/\d{2}\s\d{1,2}:\d{2}:\d{2}$/", "d/m/y H:i:s"],
			["/^\d{4}-\d{1,2}-\d{1,2}\s\d{1,2}:\d{2}:\d{2}$/", "Y-m-d H:i:s"],
			["/^\d{4}\/\d{1,2}\/\d{1,2}\s\d{1,2}:\d{2}:\d{2}$/", "Y/m/d H:i:s"]
		];

		foreach ($patterns as $row)
		{
			if(preg_match($row[0], $date, $matches))
			{
				return $row[1];
			}
		}

		return null;
	}
}