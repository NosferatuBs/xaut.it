<?php
namespace App\Libraries;

use App\Entities\CustomerAnswer;
use App\Entities\Survey;
use App\Entities\Traceability;
use App\Entities\User;

class SurveyGenerator
{
	protected $brand_id;
	protected $item_id;
	protected $locale;
	protected $surveys = [];

	public function __construct($brand_id, $item_id, $locale)
	{
		$this->brand_id = $brand_id;
		$this->item_id = $item_id;
		$this->locale = $locale ?: 'it';

		$this->load();
	}

	public function all()
	{
		return $this->surveys;
	}

	public function storeAnswers(User $user, Traceability $traceability, array $array)
	{
		if(empty($this->surveys))
			return true;

		foreach($array as $id => $answer)
		{
			$this->insert($user, $traceability, $id, $answer);
		}
	}

	protected function load()
	{
		$list = Survey::where(['brand_id' => $this->brand_id, 'active' => 1, 'locale' => $this->locale])
			->whereDoesntHave('branches')
			->whereDoesntHave('items')
			->with(['surveyType', 'answers' => function($query) {
				$query->orderBy('order','asc');
			}])
			->get();

		if($this->item_id)
		{
			$item = Survey::select('surveys.*')
				->join('branch_survey','surveys.id','branch_survey.survey_id')
                ->join('branches','branches.id','branch_survey.branch_id')
                ->join('branch_item','branches.id','branch_item.branch_id')
                ->join('items','items.id','branch_item.item_id')
                ->join('brands','items.brand_id','brands.id')
                ->where(['active' => 1, 'items.id' => $this->item_id, 'locale' => $this->locale, 'survey_branch_option' => 1])
                ->with(['surveyType', 'answers' => function($query) {
					$query->orderBy('order','asc');
				}])
				->get();

			$list = $list->merge($item);

			$item = Survey::select('surveys.*')
				->join('item_survey','surveys.id','item_survey.survey_id')
                ->join('items','items.id','item_survey.item_id')
                ->join('brands','items.brand_id','brands.id')
                ->where(['active' => 1, 'items.id' => $this->item_id, 'locale' => $this->locale, 'survey_item_option' => 1])
                ->with(['surveyType', 'answers' => function($query) {
					$query->orderBy('order','asc');
				}])
				->get();

			$list = $list->merge($item);
		}

		$this->surveys = $list;
	}

	protected function insert(User $user, Traceability $traceability, $id, $answer)
	{
		if(is_array($answer))
		{
			foreach($answer as $key => $value)
			{
				$this->insert($user, $traceability, $id, $value);
			}

			return true;
		}

		if($this->isSurveyAvailable($id))
		{
			$record = new CustomerAnswer;
			$record->user_id = $user->id;
			$record->survey_id = $id;
			$record->traceability_id = $traceability->id;
			$record->answer = $answer;
			$record->save();
		}

		return true;
	}

	protected function isSurveyAvailable($id)
	{
		return ! is_null($this->surveys->firstWhere('id', $id));
	}
}
?>
