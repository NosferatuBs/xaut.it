<?php
namespace App\Libraries;

use PHPExcel_Cell;
use PHPExcel_Cell_DataType;
use PHPExcel_Cell_IValueBinder;
use PHPExcel_Cell_DefaultValueBinder;

class ExcelBinderString extends PHPExcel_Cell_DefaultValueBinder implements PHPExcel_Cell_IValueBinder
{
    public function bindValue(PHPExcel_Cell $cell, $value = null)
    {
        $cell->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);

        return true;
    }
}
?>