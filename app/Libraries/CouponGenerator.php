<?php
	namespace App\Libraries;
	
	use App\Entities\Brand;
	use Carbon\Carbon;
	use Illuminate\Support\Facades\File;
	use setasign\Fpdi\Fpdi;
	
	
	define('FPDF_FONTPATH', storage_path('app/pdf_fonts'));
	define('EURO', chr(128));
	
	class CouponGenerator
	{
		
		/**
		 * Create a PDF with the coupon code.
		 *
		 * @param string $email
		 * @param float $discount
		 * @param string $url
		 * @param string $token
		 * @param Carbon $expired_at
		 * @param string $template
		 * @return \setasign\Fpdi\Fpdi
		 */
		static public function make(string $email, float $discount, $isPercentage, int $brandId, string $url, string $token, Carbon $expired_at, string $template)
		{
			$brand = Brand::where('id', $brandId)->first();
			
			$logoPath = storage_path() . '/app/public/' . $brand->logo;
			$logo = base64_encode(File::get($logoPath));

			$fpdi = new Fpdi();
			
			$fpdi->AddPage();
			$fpdi->SetMargins(0, 0);
			$fpdi->SetAutoPageBreak(false);
			
			$fpdi->AddFont("ProximaNova", "Semibold");
			
			$fpdi->setSourceFile($template);
			// import page 1
			$tplIdx = $fpdi->importPage(1);
			//use the imported page and place it at point 0,0; calculate width and height
			//automaticallay and ajust the page size to the size of the imported page
			$fpdi->useTemplate($tplIdx, 0, 0, null, null, true);
			
			$qrcode = \QrCode::format('png')
				->size(200)->errorCorrection('H')
				->generate($url);
			
			// Logo
			//$pic = 'data://text/plain;base64,' . base64_encode($qrcode);
			$pic = 'data://text/plain;base64,' . $logo;
			$info = getimagesize($pic);
			
			$maxSize = 46;
			$width = $maxSize;
			$height = $maxSize;
			$x = 100; // 83;
			$y = 130; // 25;
			if ($info[0] > $info[1])
			{
				$width = $maxSize;
				$height = $maxSize / ($info[0] / $info[1]);
			}
			else
			{
				$height = $maxSize;
				$width = $maxSize / ($info[1] / $info[0]);
			}
			
			$fpdi->Image($pic, 105 - $width / 2, 148 - $height / 2, $width, $height, 'png');
			
			if ($isPercentage)
			{
				$symbol = "%";
			}
			else
			{
				$symbol = iconv("UTF-8", "ISO-8859-1//TRANSLIT", '€'); //utf8_encode(chr(128)); //chr(128); // iconv('UTF-8', 'windows-1252', '€');
			}
			
			// Discount
			$fpdi->SetFont('ProximaNova', 'Semibold', '67');
			$fpdi->SetTextColor(30, 30, 30);
			//set position in pdf document
			$fpdi->SetXY(56, 50);
			//first parameter defines the line height
			$fpdi->Cell(100, 10, $discount . ' ' . $symbol, 0, 0, "C");
			
			$fpdi->SetFont('ProximaNova', 'Semibold', '78');
			//set position in pdf document
			$fpdi->SetXY(95, 234);
			//first parameter defines the line height
			$fpdi->Cell(100, 10, $discount . ' ' . $symbol, 0, 0, "R");
			
			### EMAIL
			$fpdi->SetFont('ProximaNova', 'Semibold', '12');
			//set position in pdf document
			$fpdi->SetXY(14, 225);
			//first parameter defines the line height
			
			$fpdi->Cell(100, 10, "Email", 0, 0, "L");
			$fpdi->SetXY(14, 230);
			$fpdi->Cell(100, 10, $email, 0, 0, "L");
			
			### TOKEN
			//set position in pdf document
			$fpdi->SetXY(55, 250);
			//first parameter defines the line height
			$fpdi->Cell(100, 12, $token, 0, 0, "C");
			
			### DATA VALIDITA'
			//set position in pdf document
			$fpdi->SetXY(127, 258);
			//first parameter defines the line height
			$fpdi->Cell(100, 10, $expired_at->format('d/m/Y'), 0, 0, "C");
			
			
			return $fpdi;
		}
	}

?>
