<?php
namespace App\Libraries;

use App\Entities\Brand;
use App\Entities\BrandNumber;
use App\Entities\Item;

class Hologram
{
	protected $first_char = null;
	protected $second_char = null;
	protected $digits = null;
	protected $brand = null;
	protected $item = null;

	public $valid = false;
	public $token = null;

	public function __construct($str)
	{
		$output = self::validate($str);

		if(is_array($output) && !empty($output))
		{
			$this->first_char = empty($output['first_char']) ? null : $output['first_char'];
			$this->digits = $output['digits'];
			$this->second_char = empty($output['second_char']) ? null : $output['second_char'];

			$this->token = implode([$this->first_char,$this->digits,$this->second_char]);
		}
	}
	public function getHologram()
	{
		return $this->first_char . $this->digits . $this->second_char;
	}
	public function findByBrandItem(Brand $brand,Item $item)
	{
		$this->brand = $brand;
		$this->item = $item;

		if($this->digits && $this->brand->level == 1)
		{
			$collection = $this->getNumber();

			if(! empty($collection))
			{
				if($collection->item_id == $this->item->id || is_null($collection->item_id))
					$this->valid = true;
			}
		}
	}
	public function findByNumber()
	{
		return BrandNumber::where([
				'first_char' => $this->first_char,
				'second_char' => $this->second_char
			])
			->where(function($query){
				$query->where('start', '<=', $this->digits)
					->where('end', '>=', $this->digits)
					->orWhere('start', $this->digits)
					->whereNull('end');
			})
            ->first();
	}

	public static function hasConflictNumber($first_char, $inizio, $fine, $second_char)
	{
		$q = BrandNumber::where(function($query) use($inizio, $fine){
				$query->where(function($query) use($inizio, $fine){
					$query->where('start','>=',$inizio)
						->where('start','<=',$fine);
				})
				->orWhere(function($query) use($inizio, $fine){
					$query->where('start','<=',$inizio)
					->where('end','>=',$inizio);
				});
			});

		if(is_null($first_char))
			$q->whereNull('first_char');
		else
			$q->where('first_char',$first_char);

		if(is_null($second_char))
			$q->whereNull('second_char');
		else
			$q->where('second_char',$second_char);

		return $q->count();
	}

	protected function getNumber()
	{
		return BrandNumber::where([
				'brand_id' => $this->brand->id,
				'first_char' => $this->first_char,
				'second_char' => $this->second_char
			])
			->where(function($query){
				$query->where('start', '<=', $this->digits)
					->where('end', '>=', $this->digits)
					->orWhere('start', $this->digits)
					->whereNull('end');
			})
            ->first();
	}
	protected static function validate($str)
	{
		if(preg_match('/([a-z]{0,2})[^\da-z]*([\d]+)([a-z]{0,2})/i', $str, $match))
			return [
				'first_char' => strtoupper($match[1]),
				'digits' => $match[2],
				'second_char' => strtoupper($match[3])
			];
		throw new \Exception('Il token deve contenere almeno un numero');
	}
}
?>
