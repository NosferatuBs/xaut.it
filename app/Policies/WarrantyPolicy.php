<?php

namespace App\Policies;

use App\Entities\User;
use App\Entities\Warranty;
use Illuminate\Auth\Access\HandlesAuthorization;

class WarrantyPolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        if ($user->isSuperadmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the warranty.
     *
     * @param  \App\User  $user
     * @param  \App\Warranty  $warranty
     * @return mixed
     */
    public function view(User $user, Warranty $warranty)
    {
        return $user->brands->contains($warranty->traceability->brand_id);
    }
}
