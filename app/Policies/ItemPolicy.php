<?php

namespace App\Policies;

use App\Entities\Item;
use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ItemPolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        if ($user->isSuperadmin()) {
            return true;
        }
    }

    public function update(User $user, Item $item)
    {
        return $user->brands->contains($item->brand_id);
    }
}
