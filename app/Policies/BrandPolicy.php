<?php

namespace App\Policies;

use App\Entities\Brand;
use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BrandPolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        if ($user->isSuperadmin()) {
            return true;
        }
    }

    public function update(User $user, Brand $brand)
    {
        return $user->brands->contains($brand->id);
    }
}
