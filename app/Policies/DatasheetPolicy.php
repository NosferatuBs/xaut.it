<?php

namespace App\Policies;

use App\Entities\Datasheet;
use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DatasheetPolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        if ($user->isSuperadmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the datasheet.
     *
     * @param  \App\User  $user
     * @param  \App\Datasheet  $datasheet
     * @return mixed
     */
    public function view(User $user, Datasheet $datasheet)
    {
        return $user->brands->contains($datasheet->brand_id);
    }

    /**
     * Determine whether the user can update the datasheet.
     *
     * @param  \App\User  $user
     * @param  \App\Datasheet  $datasheet
     * @return mixed
     */
    public function update(User $user, Datasheet $datasheet)
    {
        return $user->brands->contains($datasheet->brand_id);
    }

    /**
     * Determine whether the user can delete the datasheet.
     *
     * @param  \App\User  $user
     * @param  \App\Datasheet  $datasheet
     * @return mixed
     */
    public function delete(User $user, Datasheet $datasheet)
    {
        return $user->brands->contains($datasheet->brand_id);
    }
}
