<?php

namespace App\Observers;

use App\Entities\Warranty;
use Illuminate\Support\Facades\Storage;

class WarrantyObserver
{
    /**
     * Listen to the Warranty deleting event.
     *
     * @param  Warranty  $user
     * @return void
     */
    public function deleted(Warranty $warranty)
    {
        if($warranty->stored)
        {
            Storage::disk('public')->delete($warranty->stored);
        }
    }
}
