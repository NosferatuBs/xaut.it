<?php

namespace App\Observers;

use App\Entities\Traceability;

class TraceabilityObserver
{
    /**
     * Listen to the Traceability deleting event.
     *
     * @param  Traceability  $traceability
     * @return void
     */
    public function deleting(Traceability $traceability)
    {
    	$traceability->warranty()->each(function($garanzia){
    		$garanzia->delete();
    	});
        if($traceability->forceDeleting === true) {
            $traceability->customerAnswers->each(function($answer){
                $answer->delete();
            });
        }
    }
}
