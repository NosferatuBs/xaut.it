<?php

namespace App\Observers;

use App\Entities\Datasheet;
use Illuminate\Support\Facades\Storage;

class DatasheetObserver
{
    /**
     * Listen to the Datasheet deleted event.
     *
     * @param  Datasheet  $datasheet
     * @return void
     */
    public function deleted(Datasheet $datasheet)
    {
        if($datasheet->stored)
        {
            Storage::disk('public')->delete($datasheet->stored);
        }
    }

    /**
     * Listen to the Datasheet deleting event.
     *
     * @param  Datasheet  $datasheet
     * @return void
     */
    public function deleting(Datasheet $datasheet)
    {
        $datasheet->branches()->detach();
    }
}
