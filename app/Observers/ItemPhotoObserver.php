<?php

namespace App\Observers;

use App\Entities\ItemPhoto;
use Illuminate\Support\Facades\Storage;

class ItemPhotoObserver
{
    /**
     * Listen to the ItemPhoto deleted event.
     *
     * @param  ItemPhoto  $photo
     * @return void
     */
    public function deleted(ItemPhoto $photo)
    {
        if($photo->stored)
        {
            Storage::disk('public')->delete($photo->stored);
        }
    }
}
