<?php

namespace App\Observers;

use App\Entities\Item;
use Illuminate\Support\Facades\Storage;

class ItemObserver
{
    /**
     * Listen to the Item deleting event.
     *
     * @param  Item  $item
     * @return void
     */
    public function deleting(Item $item)
    {
        $item->numbers->each->delete();
        $item->photo->each->delete();
        $item->barcode()->delete();
        $item->branches()->detach();
    }
}
