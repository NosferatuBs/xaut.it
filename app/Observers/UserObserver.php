<?php

namespace App\Observers;

use App\Entities\User;
use Illuminate\Support\Facades\Storage;

class UserObserver
{
    /**
     * Listen to the User deleting event.
     *
     * @param  User  $user
     * @return void
     */
    public function deleting(User $user)
    {
        $user->brands()->detach();
        $user->workgroups()->detach();
        $user->notifications->each(function($notification){
        	$notification->delete();
        });
        $user->traceability->each(function($track){
        	$track->forceDelete();
        });
    }
}
